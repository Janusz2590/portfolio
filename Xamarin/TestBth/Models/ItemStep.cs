﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arm.Models
{
    public class ItemStep
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string Servo1 { get; set; }
        public string Servo2 { get; set; }
        public string Servo3 { get; set; }
        public string Servo4 { get; set; }
        public string Servo5 { get; set; }
        public string Servo6 { get; set; }
        public string Speed { get; set; }
        public int Time { get; set; }
    }
}
