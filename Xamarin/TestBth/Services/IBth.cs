﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Arm
{
	public interface IBth
	{
        ObservableCollection<string> PairedDevices();
		void Start(string name, int sleep, bool read);
        void Send(string c);
		void Cancel();
		
	}
}

