﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arm.Services
{
    public interface IStepDataStore<T>
    {
        Task<bool> AddStepAsync(T item);
        Task<bool> UpdateStepAsync(T item);
        Task<bool> DeleteStepAsync(string id);
        Task<T> GetStepAsync(string id);
        Task<IEnumerable<T>> GetStepsAsync(bool forceRefresh = false);
    }
}
