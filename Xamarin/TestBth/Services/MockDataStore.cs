﻿using Arm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arm.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>();
            var ExampleItems = new List<Item>
            {
                new Item
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "Przykład1",
                    Description ="Przykładowy opis",
                    Servo1 = "10",
                    Servo2 = "20",
                    Servo3 = "30",
                    Servo4 = "40",
                    Servo5 = "50",
                    Servo6 = "60"
                },
                new Item
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "Przykład2",
                    Description ="Przykładowy opis",
                    Servo1 = "10",
                    Servo2 = "20",
                    Servo3 = "30",
                    Servo4 = "40",
                    Servo5 = "50",
                    Servo6 = "60"
                },
                new Item
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "Przykład3",
                    Description ="Przykładowy opis",
                    Servo1 = "10",
                    Servo2 = "20",
                    Servo3 = "30",
                    Servo4 = "40",
                    Servo5 = "50",
                    Servo6 = "60"
                },

            };
            foreach (var item in ExampleItems)
            {
                items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);
            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);
            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);
            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }

    }
}
