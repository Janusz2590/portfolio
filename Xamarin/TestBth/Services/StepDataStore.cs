﻿using Arm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arm.Services
{
    public class StepDataStore : IStepDataStore<ItemStep>
    {
        List<ItemStep> steps;
        public StepDataStore()
        {
            steps = new List<ItemStep>();
            var ExampleSteps = new List<ItemStep>
            {
                new ItemStep
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "Przykład1",
                    Description ="Przykładowy opis",
                    Servo1 = "10",
                    Servo2 = "20",
                    Servo3 = "30",
                    Servo4 = "40",
                    Servo5 = "50",
                    Servo6 = "60"
                },
                new ItemStep
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "Przykład2",
                    Description ="Przykładowy opis",
                    Servo1 = "10",
                    Servo2 = "20",
                    Servo3 = "30",
                    Servo4 = "40",
                    Servo5 = "50",
                    Servo6 = "60"
                },
                new ItemStep
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "Przykład3",
                    Description ="Przykładowy opis",
                    Servo1 = "10",
                    Servo2 = "20",
                    Servo3 = "30",
                    Servo4 = "40",
                    Servo5 = "50",
                    Servo6 = "60"
                },
            };
            foreach (var itemStep in ExampleSteps)
            {
                steps.Add(itemStep);
            }
        }
        public async Task<bool> AddStepAsync(ItemStep item)
        {
            steps.Add(item);
            return await Task.FromResult(true);
        }
        public async Task<bool> UpdateStepAsync(ItemStep item)
        {
            var oldItem = steps.Where((ItemStep arg) => arg.Id == item.Id).FirstOrDefault();
            steps.Remove(oldItem);
            steps.Add(item);
            return await Task.FromResult(true);
        }
        public async Task<bool> DeleteStepAsync(string id)
        {
            var oldItem = steps.Where((ItemStep arg) => arg.Id == id).FirstOrDefault();
            steps.Remove(oldItem);
            return await Task.FromResult(true);
        }
        public async Task<ItemStep> GetStepAsync(string id)
        {
            return await Task.FromResult(steps.FirstOrDefault(s => s.Id == id));
        }
        public async Task<IEnumerable<ItemStep>> GetStepsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(steps);
        }
    }
}
