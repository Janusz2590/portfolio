﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Arm.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "O Aplikacji";
            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://el.pcz.pl/pl/")));
        }
        public ICommand OpenWebCommand { get; }
    }
}
