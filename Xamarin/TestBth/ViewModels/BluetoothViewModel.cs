﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using PropertyChanged;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Arm
{

	public class BluetoothViewModel : INotifyPropertyChanged
	{
        public ObservableCollection<string> ListOfDevices { get; set; } = new ObservableCollection<string>();
		public ObservableCollection<string> ListOfCodes { get; set; } = new ObservableCollection<string>();

        public ICommand ConnectCommand { get; protected set; }
        public ICommand DisconnectCommand { get; protected set; }
        public ICommand SendData { get; protected set; }

        bool _isConnected { get; set; } = false;
		int sleep { get; set; } = 250;      
        public string SelectedBthDevice { get; set; } = "";		
		public string Data { get; set; }
        
        
        public String Sleep
		{
			get {
                return sleep.ToString();
                }
			set {
                sleep = int.Parse(value);
                }
		}
		private bool _isSelectedBthDevice
        {
            get {
				if (string.IsNullOrEmpty(SelectedBthDevice))
                    return false;
                return true;
			} 
		}
		public bool IsConnectEnabled
        {
            get
            {
				if (_isSelectedBthDevice == false)
					return false;
				return !_isConnected;
			} 
		}		
		public bool IsDisconnectEnabled
        { 
			get
            {
				if (_isSelectedBthDevice == false)
					return false;
				return _isConnected;
			}
		}
		public bool IsPickerEnabled
        { 
			get
            {
				return !_isConnected;
			}
		}

        public BluetoothViewModel()
		{
            Data = "HELLO WORLD!";
            this.ConnectCommand = new Command(() => 
            {		
				DependencyService.Get<IBth>().Start(SelectedBthDevice, sleep, true);
				_isConnected = true;
				MessagingCenter.Subscribe<App, string> (this, "Code", (sender, arg) => 
                {
					ListOfCodes.Insert(0, arg);
				});
			});
		    this.DisconnectCommand = new Command(() => 
            { 
				DependencyService.Get<IBth>().Cancel();
				MessagingCenter.Unsubscribe<App, string>(this, "Code");
				_isConnected = false;
			});
            this.SendData = new Command(() => 
            {
                DependencyService.Get<IBth>().Send(Data);
            });        
			MessagingCenter.Subscribe<App>(this, "Sleep",(obj) => 
			{
				if(_isConnected)
					DependencyService.Get<IBth>().Cancel();
			});

			MessagingCenter.Subscribe<App>(this, "Resume", (obj) =>
			 {
				if(_isConnected)
					 DependencyService.Get<IBth>().Start(SelectedBthDevice, sleep, true);
			});


			try
			{
				ListOfDevices = DependencyService.Get<IBth>().PairedDevices();
			}
			catch (Exception ex)
			{
				Application.Current.MainPage.DisplayAlert("Attention", ex.Message, "Ok");               
			}
		}
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
