﻿using Arm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Arm.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel 
    {
        public string Data1 { get; set; }
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.Text;
            Item = item;
        }
        
    }
}
