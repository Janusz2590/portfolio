﻿using Arm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Arm.ViewModels
{
    public class ItemStepDetailViewModel : BaseStepViewModel
    {
        public ItemStep ItemStep { get; set; }
        public ItemStepDetailViewModel(ItemStep item = null)
        {
            Title = item?.Text;
            ItemStep = item;     
        }
    }
}
