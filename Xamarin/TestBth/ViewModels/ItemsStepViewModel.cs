﻿using Arm.Models;
using Arm.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Arm.ViewModels
{
    public class ItemsStepViewModel : BaseStepViewModel
    {
        public ObservableCollection<ItemStep> Steps { get; set; }
        public Command LoadItemsStepCommand { get; set; }

        public ItemsStepViewModel()
        {
            Title = "Sterowanie PTP";
            Steps = new ObservableCollection<ItemStep>();
            LoadItemsStepCommand = new Command(async () => await ExecuteLoadItemsStepCommand());           
            MessagingCenter.Subscribe<StepPage, ItemStep>(this, "NewStep", async (obj, item) =>
            {
                var newItemStep = item as ItemStep;
                Steps.Add(newItemStep);
                await StepDataStore.AddStepAsync(newItemStep);
            });
            MessagingCenter.Subscribe<ItemStepDetailPage, ItemStep>(this, "DestroyStep", async (obj, item) =>
            {
                var newItemStep = item as ItemStep;
                Steps.Remove(newItemStep);
                string id_ = newItemStep.Id;
                await StepDataStore.DeleteStepAsync(id_);
            });
        }

        async Task ExecuteLoadItemsStepCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Steps.Clear();
                var items = await StepDataStore.GetStepsAsync(true);
                foreach (var item in items)
                {
                    Steps.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}
