﻿using Arm.Models;
using Arm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Arm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllStepsPage : ContentPage
    {
        ItemsStepViewModel viewModel;
        public AllStepsPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new ItemsStepViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as ItemStep;
            if (item == null)
                return;
            await Navigation.PushAsync(new ItemStepDetailPage(new ItemStepDetailViewModel(item)));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Steps.Count == 0)
                viewModel.LoadItemsStepCommand.Execute(null);
        }

    }
}