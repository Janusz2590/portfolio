﻿using Arm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Arm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChoosePage : ContentPage
    {
        ItemsViewModel viewModel;
        public ChoosePage()
        {
            InitializeComponent();
            BindingContext = viewModel = new ItemsViewModel();
        }

        async void Examples_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ExamplesPage());
        }
  
        async void StepCreator_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new StepPage()));
        }

        async void Controller_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));
        }

        async void ControllerOnline_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new OnlineControlPage());
        }
    }
}