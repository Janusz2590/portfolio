﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Arm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExamplesPage : ContentPage
    {
        string Data1, Data2, Data3, Data4, Data5, Data6;
        public ExamplesPage()
        {
            InitializeComponent();
            Data1 = "10501000501602500101";
            Data2 = "20592000501602500101";
            Data3 = "30501001501650501101";
            Data4 = "40501001501650501101";
            Data5 = "50501001501650501101";
            Data6 = "60201701001650500901";
        }

        async void Example1_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IBth>().Send("1055");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("2180");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("3090");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("4010");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("5100");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("6120");
            await Task.Delay(1500);
            DependencyService.Get<IBth>().Send("1055");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("2180");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("3090");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("4010");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("5100");
            await Task.Delay(300);
            DependencyService.Get<IBth>().Send("6120");
            await Task.Delay(1500);
            await DisplayAlert("Alert", "Wysłałem Przykład1", "OK");
        }

        async void Example2_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IBth>().Send(Data6);
            Task.Delay(100);
            DependencyService.Get<IBth>().Send(Data5);
            Task.Delay(100);
            DependencyService.Get<IBth>().Send(Data4);
            Task.Delay(100);
            DependencyService.Get<IBth>().Send(Data3);
            Task.Delay(100);
            DependencyService.Get<IBth>().Send(Data2);
            await DisplayAlert("Alert", "Wysłałem Przykład2", "OK");
        }

        async void Example3_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IBth>().Send(Data1);
            Task.Delay(100);
            DependencyService.Get<IBth>().Send(Data6);
            Task.Delay(100);
            DependencyService.Get<IBth>().Send(Data2);
            Task.Delay(100);
            DependencyService.Get<IBth>().Send(Data3);
            Task.Delay(100);
            DependencyService.Get<IBth>().Send(Data1);
            await DisplayAlert("Alert", "Wysłałem Przykład3", "OK");
        }
    }
}