﻿using Arm.Models;
using Arm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Arm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemDetailPage : ContentPage
    {

        public string Data1  { get; set; }
        public string Data2 { get; set; }
        public string Data3 { get; set; }
        public string Data4 { get; set; }
        public string Data5 { get; set; }
        public string Data6 { get; set; }
        public ICommand SendData { get; protected set; }
        
        ItemDetailViewModel viewModel;
        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;
            Data1 = "1" + viewModel.Item.Servo1;
            Data2 = "2" + viewModel.Item.Servo2;
            Data3 = "3" + viewModel.Item.Servo3;
            Data4 = "4" + viewModel.Item.Servo4;
            Data5 = "5" + viewModel.Item.Servo5;
            Data6 = "6" + viewModel.Item.Servo6;
        }
        public ItemDetailPage()
        {
            InitializeComponent();

            var item = new Item
            {
                Text = "Item 1",
                Description = "This is an item description.",
                Servo1 = "000",
                Servo2 = "000",
                Servo3 = "000",
                Servo4 = "000",
                Servo5 = "000",
                Servo6 = "000",
                Speed = "1"

            };

            viewModel = new ItemDetailViewModel(item);
            Data1 = "1" + viewModel.Item.Servo1;
            Data2 = "2" + viewModel.Item.Servo2;
            Data3 = "3" + viewModel.Item.Servo3;
            Data4 = "4" + viewModel.Item.Servo4;
            Data5 = "5" + viewModel.Item.Servo5;
            Data6 = "6" + viewModel.Item.Servo6;
           
            BindingContext = viewModel;
        
        }
                
        public async void Setting_Clicked(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Caaaaaaaa " + Data1);
          
                    DependencyService.Get<IBth>().Send(Data1);
                    await Task.Delay(300);
                    DependencyService.Get<IBth>().Send(Data2);
                    await Task.Delay(300);
                    DependencyService.Get<IBth>().Send(Data3);
                    await Task.Delay(300);
                    DependencyService.Get<IBth>().Send(Data4);
                    await Task.Delay(300);
                    DependencyService.Get<IBth>().Send(Data5);
                    await Task.Delay(300);
                    DependencyService.Get<IBth>().Send(Data6);
                    await Task.Delay(300);
        }
        public async void Delete_Clicked(object sender, EventArgs e)
        {
            var decision = await DisplayAlert("Uwaga", "Jesteś pewnien?", "Tak", "Nie");
            if (decision)
            {
                MessagingCenter.Send(this, "DestroyItem", viewModel.Item);
                await Navigation.PopAsync();
            }
        }


        public async void Return_Clicked(object sender, EventArgs e)
        {

            await Navigation.PopAsync();

        }

        
    }
}