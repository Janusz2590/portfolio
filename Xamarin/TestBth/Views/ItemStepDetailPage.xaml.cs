﻿using Arm.Models;
using Arm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Arm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemStepDetailPage : ContentPage
    {
        ItemStepDetailViewModel viewModel;
        ItemStep Item_;
        public ItemStepDetailPage(ItemStepDetailViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;
            Item_ = viewModel.ItemStep;
        }

        public ItemStepDetailPage()
        {
            InitializeComponent();
            var item = new ItemStep()
            {
                Text = "Item 1",
                Description = "This is an item description.",
                Servo1 = "000",
                Servo2 = "000",
                Servo3 = "000",
                Servo4 = "000",
                Servo5 = "000",
                Servo6 = "000",
                Speed = "1"
            };
            viewModel = new ItemStepDetailViewModel(item);
            BindingContext = viewModel;
        }

        public async void Save_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        public async void Destroy_Clicked(object sender, EventArgs e)
        {
            var decision = await DisplayAlert("Uwaga", "Jesteś pewnien?", "Tak", "Nie");
            if (decision)
            {
                MessagingCenter.Send(this, "DestroyStep", viewModel.ItemStep);
                await Navigation.PopAsync();
            }

        }
    }
}