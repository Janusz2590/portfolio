﻿using Arm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Arm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewItemPage : ContentPage
    {
        public Item Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();

            Item = new Item
            {
                Text = "Item 1",
                Description = "This is an item description.",
                Servo1 = "000",
                Servo2 = "000",
                Servo3 = "000",
                Servo4 = "000",
                Servo5 = "000",
                Servo6 = "000",
                Speed = "1"
            };

            BindingContext = this;
        }
        void OnSliderValueChanged1(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            int zmienna = Convert.ToInt32(value);
            Item.Servo1 = zmienna.ToString();
            if (Item.Servo1.Length == 1)
                Item.Servo1 = "00" + Item.Servo1;
            else if (Item.Servo1.Length == 2)
                Item.Servo1 = "0" + Item.Servo1;
    
        }
        void OnSliderValueChanged2(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            int zmienna = Convert.ToInt32(value);
            Item.Servo2 = zmienna.ToString();
            if (Item.Servo2.Length == 1)
                Item.Servo2 = "00" + Item.Servo2;
            else if (Item.Servo2.Length == 2)
                Item.Servo2 = "0" + Item.Servo2;
       
        }
        void OnSliderValueChanged3(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            int zmienna = Convert.ToInt32(value);
            Item.Servo3 = zmienna.ToString();
            if (Item.Servo3.Length == 1)
                Item.Servo3 = "00" + Item.Servo3;
            else if (Item.Servo3.Length == 2)
                Item.Servo3 = "0" + Item.Servo3;

        }
        void OnSliderValueChanged4(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            int zmienna = Convert.ToInt32(value);
            Item.Servo4 = zmienna.ToString();
            if (Item.Servo4.Length == 1)
                Item.Servo4 = "00" + Item.Servo4;
            else if (Item.Servo4.Length == 2)
                Item.Servo4 = "0" + Item.Servo4;
 
        }
        void OnSliderValueChanged5(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            int zmienna = Convert.ToInt32(value);
            Item.Servo5 = zmienna.ToString();
            if (Item.Servo5.Length == 1)
                Item.Servo5 = "00" + Item.Servo5;
            else if (Item.Servo5.Length == 2)
                Item.Servo5 = "0" + Item.Servo5;

        }
        void OnSliderValueChanged6(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            int zmienna = Convert.ToInt32(value);
            Item.Servo6 = zmienna.ToString();
            if (Item.Servo6.Length == 1)
                Item.Servo6 = "00" + Item.Servo6;
            else if (Item.Servo6.Length == 2)
                Item.Servo6 = "0" + Item.Servo6;

        }
        async void Save_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "AddItem", Item);
            await Navigation.PopModalAsync();
        }
        void OnStepperValueChanged(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            int zmienna = Convert.ToInt32(value);
            Item.Speed = zmienna.ToString();
        }
    }
}