﻿using Arm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Arm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OnlineControlPage : ContentPage
    {
        public Item Item { get; set; }
        int zmienna;
        string Data1 { get; set; }

        public OnlineControlPage()
        {
            InitializeComponent();
            Item = new Item
            {

                Text = "Item name",
                Description = "This is an item description.",
                Servo1 = "50",
                Servo2 = "50",
                Servo3 = "50",
                Servo4 = "50",
                Servo5 = "50",
                Servo6 = "50",
                Speed = "1"
            };
            BindingContext = this;
        }

        void OnStepperValueChanged1(object sender, ValueChangedEventArgs args)
        {
            try
            {
            int value = Convert.ToInt32(args.NewValue);
            zmienna = Convert.ToInt32(value);
            Item.Servo1 = zmienna.ToString();
            if (Item.Servo1.Length == 1)
                Item.Servo1 = "100" + Item.Servo1;
            else if (Item.Servo1.Length == 2)
                Item.Servo1 = "10" + Item.Servo1;
            else if (Item.Servo1.Length == 3)              
                Item.Servo1 = "1" + Item.Servo1;
                
                
             Data1 = Item.Servo1;
            }
            finally
            {
                DependencyService.Get<IBth>().Send(Data1);
            }
            
            
        }
        void OnStepperValueChanged2(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            zmienna = Convert.ToInt32(value);
            Item.Servo2 = zmienna.ToString();
            if (Item.Servo2.Length == 1)
                Item.Servo2 = "200" + Item.Servo2;
            else if (Item.Servo2.Length == 2)
                Item.Servo2 = "20" + Item.Servo2;
            else if (Item.Servo2.Length == 3)
                Item.Servo2 = "2" + Item.Servo2;
            Data1 = Item.Servo2;
            DependencyService.Get<IBth>().Send(Data1);
        }
        void OnStepperValueChanged3(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            zmienna = Convert.ToInt32(value);
            Item.Servo3 = zmienna.ToString();
            if (Item.Servo3.Length == 1)
                Item.Servo3 = "300" + Item.Servo3;
            else if (Item.Servo3.Length == 2)
                Item.Servo3 = "30" + Item.Servo3;
            else if (Item.Servo3.Length == 3)
                Item.Servo3 = "3" + Item.Servo3;
            Data1 = Item.Servo3;
            DependencyService.Get<IBth>().Send(Data1);

        }
        void OnStepperValueChanged4(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            zmienna = Convert.ToInt32(value);
            Item.Servo4 = zmienna.ToString();
            if (Item.Servo4.Length == 1)
                Item.Servo4 = "400" + Item.Servo4;
            else if (Item.Servo4.Length == 2)
                Item.Servo4 = "40" + Item.Servo4;
            else if (Item.Servo4.Length == 3)
                Item.Servo4 = "4" + Item.Servo4;
            Data1 = Item.Servo4;
            DependencyService.Get<IBth>().Send(Data1);

        }
        void OnStepperValueChanged5(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            zmienna = Convert.ToInt32(value);
            Item.Servo5 = zmienna.ToString();
            if (Item.Servo5.Length == 1)
                Item.Servo5 = "500" + Item.Servo5;
            else if (Item.Servo5.Length == 2)
                Item.Servo5 = "50" + Item.Servo5;
            else if (Item.Servo5.Length == 3)
                Item.Servo5 = "5" + Item.Servo5;
            Data1 = Item.Servo5;
            DependencyService.Get<IBth>().Send(Data1);
        }
        void OnStepperValueChanged6(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            zmienna = Convert.ToInt32(value);
            Item.Servo6 = zmienna.ToString();
            if (Item.Servo6.Length == 1)
                Item.Servo6 = "600" + Item.Servo6;
            else if (Item.Servo6.Length == 2)
                Item.Servo6 = "60" + Item.Servo6;
            else if (Item.Servo6.Length == 3)
                Item.Servo6 = "6" + Item.Servo6;
            Data1 = Item.Servo6;
            DependencyService.Get<IBth>().Send(Data1);
        }

        async void Save_Clicked(object sender, EventArgs e)
        {    
            MessagingCenter.Send(this, "AddItem", Item);
            await Navigation.PopAsync();
        }
        async void Return_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}