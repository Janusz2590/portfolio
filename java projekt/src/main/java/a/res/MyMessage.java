package a.res;

import a.res.b.*;
import com.google.gson.Gson;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Random;


@Path("/msg")
public class MyMessage {
    Users users = objects.users;
    Categories categories = objects.categories1;
    Tests tests = objects.tests;
    static int k = 0;



    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMessage() {
        //dodanie osoby
        UserDTO person2 = new UserDTO("Janusz","Szafranski",23);
        users.users.add(person2);
        person2 = new UserDTO("Tadeusz","Kita",29);
        users.users.add(person2);
        person2 = new UserDTO("Maria","Nowak",20);
        users.users.add(person2);

        //utworzenie kategorii
        CategoryDTO category = new CategoryDTO("Informatyka");
        //utworzenie pytania
        QuestionDTO quest = new QuestionDTO("Wybierz język programowania");
//utworzenie odpowiedzi na pytanie
        AnswerDTO odp = new AnswerDTO(true,"C");
        //doczepienie odpowiedzi do pytania
        quest.add(odp);
        odp = new AnswerDTO(false,"K++");
        quest.add(odp);
        odp = new AnswerDTO(true,"Java");
        quest.add(odp);
        odp = new AnswerDTO(true,"Python");
        quest.add(odp);

        //dodanie pytania do kategorii
        category.pytania.add(quest);
        quest = new QuestionDTO("GUI jest to");
        odp = new AnswerDTO(false,"program do tworzenia grafiki");
        quest.add(odp);
        odp = new AnswerDTO(true,"graficzny interfejs użytkownika");
        quest.add(odp);
        odp = new AnswerDTO(false,"akcelerator 3D");
        quest.add(odp);
        odp = new AnswerDTO(false,"kabel ekranowy");
        quest.add(odp);
        category.pytania.add(quest);

        quest = new QuestionDTO("Który z systemów nie należy do dystrybucji Linux?");
        odp = new AnswerDTO(false,"Debian");
        quest.add(odp);
        odp = new AnswerDTO(true,"DOS");
        quest.add(odp);
        odp = new AnswerDTO(false,"Red Hat");
        quest.add(odp);
        odp = new AnswerDTO(false,"Ubuntu");
        quest.add(odp);
        category.pytania.add(quest);

        categories.categories.add(category);
        category = new CategoryDTO("Matematyka");
        quest = new QuestionDTO("2+2:");
        odp = new AnswerDTO(true,"4");
        quest.add(odp);
        odp = new AnswerDTO(false,"5");
        quest.add(odp);
        odp = new AnswerDTO(false,"6");
        quest.add(odp);
        odp = new AnswerDTO(false,"7");
        quest.add(odp);
        category.pytania.add(quest);

        quest = new QuestionDTO("80+40:");
        odp = new AnswerDTO(true,"120");
        quest.add(odp);
        odp = new AnswerDTO(false,"100");
        quest.add(odp);
        odp = new AnswerDTO(false,"130");
        quest.add(odp);
        odp = new AnswerDTO(false,"102");
        quest.add(odp);
        category.pytania.add(quest);




        categories.categories.add(category);
        //utworzenie testu
        Random random = new Random();
        String pop = "pierwszy" + k;
        TestDTO test = new TestDTO(pop);
        k ++;
        CategoryDTO a = categories.categories.get(random.nextInt(categories.categories.size()));
        QuestionDTO b = a.pytania.get( random.nextInt(a.pytania.size()));
        int liczba = random.nextInt(b.odpowiedz.size());
        AnswerDTO o = b.odpowiedz.get(liczba);
        TestAnswerDTO po = new TestAnswerDTO( "a",o.tresc,o.poprawna);
        int liczba2 = 0;
        do {
            liczba2 = random.nextInt(b.odpowiedz.size());
        }while(liczba == liczba2);
        o = b.odpowiedz.get(liczba2);
        TestAnswerDTO po2 = new TestAnswerDTO( "b",o.tresc,o.poprawna);
        test.question.add(b);
        test.questiontest.add(po);
        test.questiontest.add(po2);
        tests.tests.add(test);
        return Response.ok().entity("OK").build() ;
        //return "Dodano przykłady\n";
    }

    @GET
    @Path("/persons")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPersons() {
        return new Gson().toJson(users.users);
    }

//    @GET
//    @Path("/persons/{id}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public String getPerson(@PathParam("id") int id) {
//        return new Gson().toJson(persons.persons.get(id));
//    }

//    @POST
//    @Path("/persons")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response addPerson(String contex) {
//        UserDTO person1 = new Gson().fromJson(contex,UserDTO.class);
//        UserDTO person2 = new UserDTO("name","surname",15);
//        users.users.add(person1);
//        users.users.add(person2);
//        return Response.ok("Dodano nowa osobe!").build();
//    }
}
