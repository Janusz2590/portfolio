package a.res;

import a.res.b.AnswerDTO;
import a.res.b.Categories;
import a.res.b.CategoryDTO;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("categories/{wartosc}/questions/{wartosc2}/answers")
public class answer {
    Categories categories = objects.categories1;
    @GET
    public Response getQuestionfromCategory(@PathParam("wartosc") String wartosc ,@PathParam("wartosc2") int wartosc2) {

        wartosc = wartosc.toUpperCase();
        String p;

        for (int k = 0;k<categories.categories.size();k++)
        {
            p = categories.categories.get(k).name.toUpperCase();
            if (wartosc.contains(p)){
                if(wartosc2<=categories.categories.get(k).pytania.size()){
                    return Response.ok().entity(new Gson().toJson(categories.categories.get(k).pytania.get(wartosc2).odpowiedz)).build();
                }else {
                    return Response.status(404).entity("Wybrano wartość spoza zakresu").build();
                }
            }
        }
        return Response.status(404).entity("Podano niewłaściwą kategorię").build();

        }



    @Path("/add")
    @POST
    public Response addAnswer(String contex,@PathParam("wartosc") String wartosc ,@PathParam("wartosc2") int wartosc2) {
        wartosc = wartosc.toUpperCase();
        String p;

        for (int k = 0;k<categories.categories.size();k++)
        {
            p = categories.categories.get(k).name.toUpperCase();
            if (wartosc.contains(p)){
                if(wartosc2<=categories.categories.get(k).pytania.size()){
                    AnswerDTO ans = new Gson().fromJson(contex, AnswerDTO.class);
                    categories.categories.get(k).pytania.get(wartosc2).odpowiedz.add(ans);

                    return Response.ok().entity(new Gson().toJson("OK")).build();
                }else {
                    return Response.status(404).entity("Wybrano wartość spoza zakresu").build();
                }
            }
        }
        return Response.status(404).entity("Podano niewłaściwą kategorię").build();



    }

    @Path("/remove/{wartosc3}")
    @DELETE
    public Response removeAnswer(@PathParam("wartosc") String wartosc ,@PathParam("wartosc2") int wartosc2,@PathParam("wartosc3") int wartosc3) {
        wartosc = wartosc.toUpperCase();
        String p;

        for (int k = 0;k<categories.categories.size();k++)
        {
            p = categories.categories.get(k).name.toUpperCase();
            if (wartosc.contains(p)){
                if(wartosc2<=categories.categories.get(k).pytania.size()){
                    if(wartosc3 <= categories.categories.get(k).pytania.get(wartosc2).odpowiedz.size()){
                        categories.categories.get(k).pytania.get(wartosc2).odpowiedz.remove(wartosc3);
                        return Response.ok().entity(new Gson().toJson("OK")).build();
                    }else {
                        return Response.status(404).entity("Wybrano wartość spoza zakresu odpowiedzi").build();
                    }
                }else {
                    return Response.status(404).entity("Wybrano wartość spoza zakresu pytań").build();
                }
            }
        }
        return Response.status(404).entity("Podano niewłaściwą kategorię").build();

    }

}
