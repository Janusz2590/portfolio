package a.res.b;

import java.util.ArrayList;

public class CategoryDTO {
    public String name;
    public ArrayList<QuestionDTO> pytania;

    public CategoryDTO(String name){
        this.name = name;
        this.pytania =  new ArrayList<>();
    }

    public void add(QuestionDTO k){
        this.pytania.add(k);
    }
}
