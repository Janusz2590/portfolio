package a.res.b;

import java.util.ArrayList;

public class QuestionDTO {
    public String tresc;
    public ArrayList<AnswerDTO> odpowiedz;

    public QuestionDTO(String tresc){
        this.tresc = tresc;
        this.odpowiedz = new ArrayList<>();
    }
    public void add (AnswerDTO k){
        odpowiedz.add(k);
    }
}
