package a.res.b;

public class TestAnswerDTO {
    public String sign;
    public String answer;
    public boolean poprawna;
    public TestAnswerDTO(String sign,String ans,boolean poprawna){
        this.sign = sign;
        this.answer = ans;
        this.poprawna = poprawna;
    }
}
