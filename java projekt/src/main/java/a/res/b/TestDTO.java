package a.res.b;

import java.util.ArrayList;

public class TestDTO {
    public String id;
    public ArrayList<QuestionDTO> question;
    public ArrayList<TestAnswerDTO> questiontest;

    public TestDTO(String id){
        this.id = id;
        this.question = new ArrayList<>();
        this.questiontest = new ArrayList<>();

    }
}
