package a.res.b;

public class UserDTO {
    public String name;
    public String surname;
    public int age;
    public UserDTO(String name,String surname,int age){
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
}
