package a.res.b;

import java.util.ArrayList;

public class UserTestDTO {

    public UserDTO user;
    public TestDTO test;
    public ArrayList<String> answer;
    public UserTestDTO(UserDTO user,TestDTO test){
        this.user = user;
        this.answer = new ArrayList<>();
        this.test = test;

    }
}
