package a.res;

import a.res.b.Categories;
import a.res.b.CategoryDTO;
import a.res.b.ObjUser;
import a.res.b.UserDTO;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("categories")
public class categories {

    Categories categories = objects.categories1;
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getCategories() {
        ArrayList<String> lista = new ArrayList<>();
        CategoryDTO category;
        String cat;
        for (int k = 0;k<categories.categories.size();k++)
        {
            category = categories.categories.get(k);
            cat = category.name;
            lista.add(cat);
        }
        return new Gson().toJson(lista);
        //return new Gson().toJson(categories.categories);
    }

    @Path("/add")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCategoriesAdd(String contex) {
        if(contex.contains("name\":")){
            try {
                CategoryDTO category = new Gson().fromJson(contex,CategoryDTO.class);
                categories.categories.add(category);
                return Response.ok("Dodano kategorię").build();
            }
            catch (Exception e){
                {return Response.status(502).entity(e.toString()).build();}
            }
        }
        else
        {return Response.status(502).entity("Podane nieprawidłowe klucze").build();}
    }


    @Path("/remove/{wartosc}")
    @DELETE
    public Response getRmoveUser(@PathParam("wartosc") String wartosc) {
        wartosc = wartosc.toUpperCase();
        String p;
        for (int k = 0;k<categories.categories.size();k++)
        {
            p = categories.categories.get(k).name.toUpperCase();
            if (wartosc.contains(p)){
                categories.categories.remove(k);
                return Response.ok().entity("ok").build();
            }
        }
        return Response.ok().entity("Podana kategoria nie istnieje").build(); }
}
