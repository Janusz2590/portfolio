package a.res;

import a.res.b.*;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("categories")
public class questions {
    Categories categories = objects.categories1;
    @Path("/{wartosc}/questions")
    @GET
    public Response getQuestionsFromCategory(@PathParam("wartosc") String wartosc) {

        wartosc = wartosc.toUpperCase();
        String p;
        for (int k = 0;k<categories.categories.size();k++)
        {
            p = categories.categories.get(k).name.toUpperCase();
            if (wartosc.contains(p)){
                ArrayList<QuestionDTO> lista = new ArrayList<>();
                lista = categories.categories.get(k).pytania;
               // return new Gson().toJson(lista);
                return Response.ok().entity( new Gson().toJson(lista)).build();
            }
        }
        return Response.status(404).entity("Podano niewłąściwą kategorię").build();
        }

    @Path("/{wartosc}/questions/add")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addQuestionToCategory(String contex, @PathParam("wartosc") String wartosc) {

        wartosc = wartosc.toUpperCase();
        String p;
        for (int k = 0;k<categories.categories.size();k++)
        {
            p = categories.categories.get(k).name.toUpperCase();
            if (wartosc.contains(p)){

                try {
                    QuestionDTO question = new Gson().fromJson(contex,QuestionDTO.class);
                    categories.categories.get(k).pytania.add(question);
                    return Response.ok().entity("Dodano pytanie").build();

                }
                catch (Exception e){
                    return Response.status(404).entity("Error").build();
                }


            }
        }
        return Response.status(404).entity("Podano niewłaściwą kategorię").build();


    }

    @Path("/{wartosc}/questions/{wartosc2}/remove")
    @DELETE
    public Response removeQuestionfromCategory(String contex,@PathParam("wartosc") String wartosc ,@PathParam("wartosc2") int wartosc2) {

        wartosc = wartosc.toUpperCase();
        String p;

        for (int k = 0;k<categories.categories.size();k++)
        {
            p = categories.categories.get(k).name.toUpperCase();
            if (wartosc.contains(p)){
                if(wartosc2<=categories.categories.get(k).pytania.size()){
                    categories.categories.get(k).pytania.remove(wartosc2);
                    return Response.ok().entity("OK").build();
                }else {
                    return Response.status(404).entity("Wybrano wartość spoza zakresu").build();
                }
            }
        }
        return Response.status(404).entity("Podano niewłaściwą kategorię").build(); }


    @Path("/{wartosc}/questions/{wartosc2}")
    @GET
    public Response getQuestionfromCategory(@PathParam("wartosc") String wartosc ,@PathParam("wartosc2") int wartosc2) {

        wartosc = wartosc.toUpperCase();
        String p;

        for (int k = 0;k<categories.categories.size();k++)
        {
            p = categories.categories.get(k).name.toUpperCase();
            if (wartosc.contains(p)){
                if(wartosc2<=categories.categories.get(k).pytania.size()){
                    return Response.ok().entity(new Gson().toJson(categories.categories.get(k).pytania.get(wartosc2))).build();
                }else {
                    return Response.status(404).entity("Wybrano wartość spoza zakresu").build();
                }
            }
        }
        return Response.status(404).entity("Podano niewłaściwą kategorię").build();
    }


}
