package a.res;


import a.res.b.*;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Random;

@Path("tests")
public class tests {
    Users users = objects.users;
    Categories categories = objects.categories1;
    Tests tests = objects.tests;
    static int k = 0;
    UserTests userTests = objects.userTests;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getTests() {
        Random random = new Random();
        return Response.ok(new Gson().toJson(tests.tests)).build();
    }


    @Path("/generate")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String generateTest( ) {
//utworzenie testu
        Random random = new Random();
        String pop = "pierwszy" + k;
        TestDTO test = new TestDTO(pop);
        k ++;
        CategoryDTO a = categories.categories.get(random.nextInt(categories.categories.size()));
        QuestionDTO b;
        for(int i = 0;i<=1;i++){
            b = a.pytania.get( random.nextInt(a.pytania.size()));
            int liczba = random.nextInt(b.odpowiedz.size());
            AnswerDTO o = b.odpowiedz.get(liczba);
            TestAnswerDTO po = new TestAnswerDTO( "a",o.tresc,o.poprawna);
            int liczba2 = 0;
            boolean odp = o.poprawna;
            do {
                do {
                    liczba2 = random.nextInt(b.odpowiedz.size());
                }while(liczba == liczba2);
                o = b.odpowiedz.get(liczba2);
            }while(odp == o.poprawna);

            TestAnswerDTO po2 = new TestAnswerDTO( "b",o.tresc,o.poprawna);

            test.question.add(b);
            test.questiontest.add(po);
            test.questiontest.add(po2);
        }

        tests.tests.add(test);
        return new Gson().toJson(test);
       // return "wygenerowalem test\n";
    }

    @Path("/checkAns/{wartosc}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public Response checkTestWithAns(String contex,@PathParam("wartosc") String wartosc ) {
        Ans odp = new Gson().fromJson(contex,Ans.class);

        wartosc = wartosc.toUpperCase();

        for(int i = 0;i< tests.tests.size();i++){
            TestDTO test = tests.tests.get(i);
            String wart2 = test.id.toUpperCase();
            if(wart2.contains(wartosc))
            {
                ArrayList<String> answer = new ArrayList<>();
                for (int k = 0; k < test.questiontest.size();k++){
                    if(k ==0){
                        if (test.questiontest.get(k).sign.contains(odp.first) && test.questiontest.get(k).poprawna == true)
                        {
                            answer.add("Odpowiedź " + k + " poprawna");
                        }
                        else {
                            answer.add("Odpowiedź " + k + " nie jest poprawna");
                        }
                    }
                    else {
                        if (test.questiontest.get(k).sign.contains(odp.second) && test.questiontest.get(k).poprawna == true)
                        {
                            answer.add("Odpowiedź " + k + " poprawna");
                        }
                        else {
                            answer.add("Odpowiedź " + k + " nie jest poprawna");
                        }
                    }
                }
                return Response.ok(new Gson().toJson(answer)).build();
            }
        }

        return Response.status(404).entity("nie znaleziono testu o identyfikatorze : " + wartosc).build();
    }

    @Path("/generateUserTest")
    @POST
    public Response generateUserTest( ) {

        Random random = new Random();

        UserDTO user = users.users.get(random.nextInt(users.users.size()));
        TestDTO test = tests.tests.get(random.nextInt(tests.tests.size()));

        UserTestDTO userTest= new UserTestDTO(user,test);
        userTest.answer.add("a");
        userTest.answer.add("b");
        userTests.userTests.add(userTest);
        return Response.ok().entity(new Gson().toJson(userTest)).build();
        }

    @Path("/check/users/{wartosc}")
    @GET
    public Response checkTestForUser(@PathParam("wartosc") int wartosc ) {
        if(wartosc >=users.users.size())
        {
            return Response.status(404).entity("nie ma USERA o ID: " + wartosc).build();
        }
        UserDTO user = users.users.get(wartosc);
        String name = user.name;
        String surname = user.surname;
        for (int i = 0; i< UserTests.userTests.size();i++){
            if ( userTests.userTests.get(i).user.name.contains(name) && userTests.userTests.get(i).user.surname.contains(surname)){
                ArrayList<String> answer = new ArrayList<>();
                TestDTO test = userTests.userTests.get(i).test;
                ArrayList<String> odp = userTests.userTests.get(i).answer;
                for (int k = 0; k < odp.size();k++){
                    if(k ==0 ){
                        if (test.questiontest.get(k).sign.contains(odp.get(k)) && test.questiontest.get(k).poprawna == true)
                        {
                            answer.add("Odpowiedź " + k + " poprawna");
                        }
                        else {
                            answer.add("Odpowiedź " + k + " nie jest poprawna");
                        }
                    }
                    else {
                        if (test.questiontest.get(k).sign.contains(odp.get(k)) && test.questiontest.get(k).poprawna == true)
                        {
                            answer.add("Odpowiedź " + k + " poprawna");
                        }
                        else {
                            answer.add("Odpowiedź " + k + " nie jest poprawna");
                        }
                    }
                }
                return Response.ok(new Gson().toJson(answer)).build();

                //return Response.ok().entity(new Gson().toJson(userTests.userTests.get(i).answer)).build();
            }
        }
        return Response.status(404).entity("nie ma testu USERA o ID: " + wartosc).build();
    }

    @Path("/check/{wartosc}")
    @GET
    public Response checkTest(@PathParam("wartosc") int wartosc ) {

        if(wartosc >=userTests.userTests.size())
        {
            return Response.status(404).entity("nie ma testu o ID: " + wartosc).build();
        }
        ArrayList<String> answer = new ArrayList<>();
        TestDTO test = userTests.userTests.get(wartosc).test;
        ArrayList<String> odp = userTests.userTests.get(wartosc).answer;

        for (int k = 0; k < odp.size();k++)
        {
            answer.add(test.questiontest.get(k).sign);
            if(k ==0){
                        if (test.questiontest.get(k).sign.contains(odp.get(k)) && test.questiontest.get(k).poprawna == true)
                        {
                            answer.add("Odpowiedź " + k + " poprawna");
                        }
                        else {
                            answer.add("Odpowiedź " + k + " nie jest poprawna");
                        }
                    }
                    else {
                        if (test.questiontest.get(k+2).sign.contains(odp.get(k)) && test.questiontest.get(k).poprawna == true)
                        {
                            answer.add("Odpowiedź " + k + " poprawna");
                        }
                        else {
                            answer.add("Odpowiedź " + k + " nie jest poprawna");
                        }

                }


                //return Response.ok().entity(answer).build();
            }
        return Response.ok(new Gson().toJson(answer)).build();
        //return Response.status(404).entity("nie ma testu o ID: " + wartosc).build();


    }

}
