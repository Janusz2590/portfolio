package a.res;

import a.res.b.AnswerDTO;
import a.res.b.ObjUser;
import a.res.b.UserDTO;
import a.res.b.Users;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;


@Path("users")
public class users {

    Users users = objects.users;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getPersons() {
        ArrayList<ObjUser> lista = new ArrayList<>();
        ObjUser user;
        for (int k = 0;k<users.users.size();k++)
        {
            user = new ObjUser(k,users.users.get(k));
            lista.add(user);

        }
        return new Gson().toJson(lista);
    }

    @GET
    @Path("/{wartosc}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPersonsNr(@PathParam("wartosc") int wartosc) {
        return new Gson().toJson(users.users.get(wartosc)); }


    @Path("/add")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPerson(String contex) {
        if(contex.contains("name\":") &&contex.contains("surname\":") &&contex.contains("age\":") ){
           try {
               UserDTO person1 = new Gson().fromJson(contex,UserDTO.class);
               users.users.add(person1);
               return Response.ok("Dodano osobę").build();
           }
           catch (Exception e){
               {return Response.status(502).entity(e.toString()).build();}
           }
        }
        else
            {return Response.status(502).entity("Podane nieprawidłowe klucze").build();}
    }



    @Path("/remove/{wartosc}")
    @DELETE
    public Response getRemoveUser(@PathParam("wartosc") int wartosc) {
        users.users.remove(wartosc);
        return Response.ok().entity(new Gson().toJson(users.users)).build(); }
}

