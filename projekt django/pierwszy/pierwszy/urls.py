"""pierwszy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include,re_path
from pierwszyApp import views

urlpatterns=  [
        
        path('admin/',admin.site.urls),
        path('kierownik/', views.current_datetime),

        path('search/',views.search),
      #  path('about/',views.about),
        path('get/',views.get),
        path('remove/',views.remove),
        path('', views.index, name='index'),

        path('accounts/', include('django.contrib.auth.urls')),
        path('register', views.register , name="register"),
        path('accounts/login/' , views.login2),
        path('admin/logut/',views.index),
        path('add/',views.addOffer),
        path('add/one/',views.addOne),
        path('checkItems/',views.checkItems),
        path('contact/',views.contact),

    
    # ex: /polls/5/
    #path('<int:question_id>/', views.detail, name='detail'),
    # ex: /polls/5/results/
    #path('<int:question_id>/results/', views.results, name='results'),
    # ex: /polls/5/vote/
    #path('<int:question_id>/vote/', views.vote, name='vote'),
    #re_path(r'^comments/(?:page-(?P<page_number>\d+)/)?$', views.checkPage)
    

]

