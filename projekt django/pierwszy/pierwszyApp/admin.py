from django.contrib import admin

# Register your models here.

from pierwszyApp.models import Muzyk, Album , Wlasciciel, Dzialka, Mieszkanie, Dom

#r e j e s t a r c j a   m o d e l i   w   p a n e l u   a d m i n i s t r a t o r a
#admin.site.register(Muzyk)
#admin.site.register(Album)
admin.site.register(Wlasciciel)
admin.site.register(Dzialka)
admin.site.register(Mieszkanie)
admin.site.register(Dom)
