from django.db import models

# Create your models here.
class Muzyk(models.Model):
	imie = models.CharField(max_length=50)
	nazwisko = models.CharField(max_length=50)
	pseudonim = models.CharField(max_length=100)

class Album(models.Model):
	artysta = models.ForeignKey(Muzyk,on_delete=models.CASCADE)
	tytul = models.CharField(max_length=100)
	data_wydania = models.DateField()
	liczba_gwiazdek = models.IntegerField()

class Wlasciciel(models.Model):
	identyfikator = models.IntegerField()
	imie = models.CharField(max_length=50)
	nazwisko = models.CharField(max_length=50)
	pesel = models.IntegerField()
	wojewodztwo = models.CharField(max_length=100)
	miasto = models.CharField(max_length=100)
	adres = models.CharField(max_length=100)
	telefon = models.IntegerField()

class Dzialka(models.Model):
	identyfikator = models.CharField(max_length=100) 
	wojewodztwo = models.CharField(max_length=100) 
	miejscowosc = models.CharField(max_length=100) 
	adres = models.CharField(max_length=100) 
	typ = models.CharField(max_length=100) 
	data_wystawienia = models.DateField()
	powierzchnia = models.IntegerField()
	wartPerM = models.IntegerField()
	wlasciciel = models.ForeignKey(Wlasciciel,on_delete=models.CASCADE)
	
class Mieszkanie(models.Model):
	identyfikator = models.CharField(max_length=100) 
	wojewodztwo = models.CharField(max_length=100) 
	miejscowosc = models.CharField(max_length=100) 
	adres = models.CharField(max_length=100) 
	typZabudowy = models.CharField(max_length=100) 
	pietro = models.IntegerField()
	pokoje = models.IntegerField()
	data_wystawienia = models.DateField()
	powierzchnia = models.IntegerField()
	wartPerM = models.IntegerField()
	wlasciciel = models.ForeignKey(Wlasciciel,on_delete=models.CASCADE)

class Dom(models.Model):
	identyfikator = models.CharField(max_length=100) 
	wojewodztwo = models.CharField(max_length=100) 
	miejscowosc = models.CharField(max_length=100) 
	adres = models.CharField(max_length=100) 
	typZabudowy = models.CharField(max_length=100) 
	iloscPieter = models.IntegerField()
	iloscBudynkow = models.IntegerField()
	data_wystawienia = models.DateField()
	powierzchnia = models.IntegerField()
	wartPerM = models.IntegerField()
	wlasciciel = models.ForeignKey(Wlasciciel,on_delete=models.CASCADE)
