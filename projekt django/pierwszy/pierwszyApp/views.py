from django.shortcuts import render
from django.template import loader
from django.contrib.auth.forms import UserCreationForm
# Create your views here.
from django.http import HttpResponse
import datetime
from .models import Wlasciciel, Dzialka, Mieszkanie, Dom
from django.contrib.auth.decorators import login_required

#def about(request):
#    return render(request, 'about.html',{})

def index(request):
    return render(request, 'index.html',{"registered":request.user.is_authenticated})

@login_required(login_url='/accounts/login/')
def search(request):
    return render(request, 'search.html',{})

def login2(request):
    return render(request,'registration/login.html',{})

@login_required(login_url='/accounts/login/')
def contact(request):
    nowy = Dzialka.objects.all()
    query = request.GET.get('idw')
    query2 = request.GET.get('id')
    if query == "mieszkanie":
        nowy = Mieszkanie.objects.get(identyfikator = query2)
    elif query == "dom":
        nowy = Dom.objects.get(identyfikator = query2)
    elif query == "działka":
        nowy = Dzialka.objects.get(identyfikator = query2)
    elif query == "dzialka":
        nowy = Dzialka.objects.get(identyfikator = query2)
    return render(request,'contact.html',{"objs": nowy, "query":query})

@login_required(login_url='/accounts/login/')    
def checkItems(request):
    user = request.user
    
    doms = Dom.objects.all()
    dzialkas = Dzialka.objects.all()
    mieszkanies = Mieszkanie.objects.all()
    
    return render(request,'checkItems.html',{"user":user,"doms":doms,"mieszkanies":mieszkanies,"dzialkas":dzialkas})

@login_required(login_url='/accounts/login/')    
def remove(request):
    user = request.user
    query2 = request.GET.get('id')
    query = request.GET.get('idw')

    if query == "mieszkanie":
        nowy1 = Mieszkanie.objects.get(identyfikator = query2)
        nowy1.delete()
    elif query == "dom":
        nowy1 = Dom.objects.get(identyfikator = query2)
        nowy1.delete()
    elif query == "działka":
        nowy1 = Dzialka.objects.get(identyfikator = query2)
        nowy1.delete()
    elif query == "dzialka":
        nowy1 = Dzialka.objects.get(identyfikator = query2)
        nowy1.delete()


    
    doms = Dom.objects.all()
    dzialkas = Dzialka.objects.all()
    mieszkanies = Mieszkanie.objects.all()
    
    return render(request,'checkItems.html',{"user":user,"doms":doms,"mieszkanies":mieszkanies,"dzialkas":dzialkas})




@login_required(login_url='/accounts/login/')
def addOffer(request):
    user = request.user
    return render(request, 'addOffer.html',{"user":user}) 


@login_required(login_url='/accounts/login/')
def addOne(request):

    user = request.user
    usr = Wlasciciel.objects.get(imie = user.first_name, nazwisko= user.last_name)
    

    query = request.GET.get('idw')
    if query == "mieszkanie":
        idx = Mieszkanie.objects.count()
        liczba = idx + 50

        woj = request.GET.get('wojewodztwo')
        miej =request.GET.get('miejscowosc')
        adr =request.GET.get('adres')
        typ = request.GET.get('typ')
        ilp =request.GET.get('pietro')
        ilb =request.GET.get('pokoje')
        pow =request.GET.get('powierzchnia')
        war = request.GET.get('wartosc')

        Mieszkanie.objects.create(
        identyfikator = liczba,
        wojewodztwo = woj, 
	    miejscowosc = miej,
	    adres = adr,
	    typZabudowy = typ ,
	    pietro = ilp,
	    pokoje = ilb,
	    data_wystawienia = datetime.date.today(),
	    powierzchnia = pow,
	    wartPerM = war,
	    wlasciciel = usr
        )


        
        
    elif query == "dom":
        woj = request.GET.get('wojewodztwo')
        miej =request.GET.get('miejscowosc')
        adr =request.GET.get('adres')
        typ = request.GET.get('typ')
        ilp =request.GET.get('iloscpieter')
        ilb =request.GET.get('iloscbudynkow')
        pow =request.GET.get('powierzchnia')
        war = request.GET.get('wartosc')

        idx = Dom.objects.count()
        liczba = idx + 50

        Dom.objects.create(identyfikator = liczba,
        wojewodztwo = woj, 
	    miejscowosc = miej,
	    adres = adr,
	    typZabudowy = typ ,
	    iloscPieter = ilp,
	    iloscBudynkow = ilb,
	    data_wystawienia = datetime.date.today(),
	    powierzchnia = pow,
	    wartPerM = war,
	    wlasciciel = usr
        )
    elif query == "dzialka":
        idx = Dzialka.objects.count()
        liczba = idx + 50

        woj = request.GET.get('wojewodztwo')
        miej =request.GET.get('miejscowosc')
        adr =request.GET.get('adres')
        typ = request.GET.get('typ')
        pow =request.GET.get('powierzchnia')
        war = request.GET.get('wartosc')

        Dzialka.objects.create(
        identyfikator = liczba,
        wojewodztwo = woj, 
	    miejscowosc = miej,
	    adres = adr,
	    typ = typ ,
	    data_wystawienia = datetime.date.today(),
	    powierzchnia = pow,
	    wartPerM = war,
	    wlasciciel = usr
        )



    #query = "as"
    #return HttpResponse("You're voting on question %s." % idx)
    return render(request, 'info.html',{"user":user}) 

def current_datetime(request) :
    now = datetime.datetime.now()
    html =  '<html><body>Jest teraz %s.</body></html>'  %  now
    return HttpResponse(html)

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request)
    else:
        form = UserCreationForm()
    context = {'form':form}
    return render(request, 'registration.html',{context})

#def detail(request, question_id):
#   element = Wlasciciel.objects.get(identyfikator = question_id)
#    return HttpResponse("You're looking at question %s." % str(element.miasto))#element.miasto)

#def results(request, question_id):
#    response = "You're looking at the results of question %s."
#    return HttpResponse(response % question_id)

#def vote(request, question_id):
#    return HttpResponse("You're voting on question %s." % question_id)

#def index(request):
    #latest_question_list = Question.objects.order_by('-pub_date')[:5]
#    context = ''
    # return HttpResponse('pierwszyApp/templates/index.html')
    #return render(request, 'index2.html')
#    return render(request, 'index.html')


def get(request):
    user = request.user
    element = "aaaaaa"
    print("user")
    print(request.user.first_name)
    #nowy = Wlasciciel.objects.all()
    #foo_instance = Foo.objects.create(name='test')
    #nowy = Wlasciciel.objects.count()
    #a = nowy.objects
    nowy = Dzialka.objects.all()
    query = request.GET.get('typ')
    if query == "mieszkanie":
        nowy = Mieszkanie.objects.all()
    elif query == "dom":
        nowy = Dom.objects.all()
    elif query == "działka":
        nowy = Dzialka.objects.all()
    elif query == "dzialka":
        nowy = Dzialka.objects.all()

    
        #nowy = Wlasciciel.objects.get(identyfikator =1)
    #nowy = Wlasciciel.objects.all()
    
    print(nowy)
    
    return render(request, 'search.html',{"objs": nowy, "query":query,"user":user})
