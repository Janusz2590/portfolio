var client = require('../server')


    exports.main = (req, res, next) => {
        // client.cli.set("liczba", 123.456, function(err, data) {
        //     console.log(data);
        //     res.send("Odpowiedź serwera: " + data); // Odpowiedź serwera: OK
        //     });
            
            client.cli.get("liczba", function(err, data) {
                console.log(data);
                res.send("Odczytana wartość: " + data); //Odczytana wartość: 123.456
                });
     }


     exports.tags = (req, res, next) => {
        client.cli.smembers("tagi", function(err, data) {
            var zbior = "<ul>";
            for(var i=0; i<data.length; i++) {
            zbior += "<li>" + data[i] + "</li>";
            }
            zbior += "</li>";
            console.log(data);
            res.send("Odczytany zbior: " + zbior);
            });
        
     }

     exports.strunowe = (req, res, next) => {
        client.cli.lrange("strunowy", 0, -1, function(err, data) {
            var studenci = "<ul>";
            for(var i=0; i<data.length; i++) {
            studenci += "<li>" + data[i] + "</li>";
            }
            studenci += "</li>";
            console.log(data);
            res.send("Odczytana lista: " + studenci);
            });
     }
     exports.drewniane = (req, res, next) => {
        client.cli.lrange("dety_drewniany", 0, -1, function(err, data) {
            var studenci = "<ul>";
            for(var i=0; i<data.length; i++) {
            studenci += "<li>" + data[i] + "</li>";
            }
            studenci += "</li>";
            console.log(data);
            res.send("Odczytana lista: " + studenci);
            });
     }
     exports.blaszane = (req, res, next) => {
        client.cli.lrange("dety_blaszany", 0, -1, function(err, data) {
            var studenci = "<ul>";
            for(var i=0; i<data.length; i++) {
            studenci += "<li>" + data[i] + "</li>";
            }
            studenci += "</li>";
            console.log(data);
            res.send("Odczytana lista: " + studenci);
            });
     }

     exports.strunoweid = (req, res, next) => {
        client.cli.lindex("dety_blaszany", req.params.id, function(err, data) {
            console.log(data);
            res.send("Odczytana element listy: " + data);
            });
            
     }

     exports.pipeline = (req, res, next) => {

        var promise = client.cli
            .pipeline()
            .set("foo", "bar")
            .get("foo")
            .lrange("strunowy", 0, -1)
            .exec();
            promise.then(function(result) {
                console.log(result);
                res.send(result);
            });
            
     }

     

  
    
        

    