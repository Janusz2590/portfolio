import Device from '../models/api';

export default {
    async findOne(req,res,next){
        const device = await Device.findOne({slug: req.params.slug});
        if(!device) return next();
        return res.status(200).send({data: device})
    },

    async findAll(req,res){},

    async create(req,res){},

    async update(req,res,next){},

    async remove(req,res,next){}
}