exports.NotFound = (req,res,next)=> {

        res.status(404).render('404');
    //const err = new Error('Nie ma takiej strony');
    //err.status= 404;
    //next(err);
};
/*exports.catchError = (err, req,res,next)=>{
    res.status(err.status||500);
    res.render('error',{
        message: err.message
    })
}*/

exports.catchErrors = (err, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
    
    
};

// try catch
exports.catchAsync = (fn) => {
    return(req,res,next) => {
        fn(req,res,next).catch(err => next(err))
    };
};