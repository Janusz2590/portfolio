const bookshelf = require('../config(mysql)/bookshelf');

const Application = bookshelf.Model.extend({
    tableName: 'applications'
});


module.exports.create = (application) => {
    return new Application({
        login: application.login,
        password: application.password
    }).save();
    
}


const devices = bookshelf.Model.extend({
    tableName: 'devices'
});

module.exports.checks = () =>{
    devices.count().then((count) => {
    console.log(`There are ${count} devices`);
    return count;
}).catch((err) => {
    console.log(err);
})};

async function fetch_all() {
    try {

        let vals = await devices.fetchAll();
        console.log(vals.toJSON());
        
    } catch (e) {

        console.log(`Failed to fetch data: ${e}`);
    }
}


module.exports.findAll = fetch_all
// http://zetcode.com/javascript/bookshelf/
//wybór w zależności od nr id
async function fetch_city() {
    try {
        let val = await devices.forge({ 'id': '4' }).fetch();
        console.log(val.toJSON());    
    } catch (e) {
        console.info(`No data found ${e}`);
    } 
}

module.exports.findOne = fetch_city