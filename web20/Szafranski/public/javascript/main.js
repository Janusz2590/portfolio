//dom elem
const time = document.getElementById('time'),
greeting = document.getElementById('greeting'),
name = document.getElementById('name'),
focus = document.getElementById('focus');
addButton = document.getElementById('buttonAdd');


function showTime() {
  let today = new Date(),
  hour = today.getHours(),
  min = today.getMinutes(),
  sec = today.getSeconds(),
  h = hour;

  //am pm
  var amPm = hour >= 12 ? 'PM' : 'AM';
  

  //12 format
  h = hour % 12 || 12;
  //output
  time. innerHTML = `${h}<span>:</span>${addZero(min)}<span>:</span>${addZero(sec)}<span>   </span>${amPm}`;
  setTimeout(showTime,1000);
}
function addZero(n){
  return (parseInt(n,10) < 10 ? '0' : '') + n;
}

//set background and greeting
function setBgGreet(){
  let today = new Date(),
  hour = today.getHours();
  if(hour<12){
    document.body.style.backgroundImage = "url('../img/morning.jpg')"
    greeting.textContent = 'Good Morning ';
    
  } else if(hour<18){
    document.body.style.backgroundImage = "url('../img/afternoon.jpg')"
    greeting.textContent = 'Good Afternoon ';
    document.body.style.color='white';
  }else{
    document.body.style.backgroundImage = "url('../img/evening.jpg')"
    greeting.textContent = 'Good Evening ';
    document.body.style.color='white';
  }
}

//get name function
function getName() {
  if(localStorage.getItem('name') === null) {
    name.textContent = 'Stranger !!';
  } else {
    name.textContent = localStorage.getItem('name');
  }
}


function setName(e) {
  if(e.type === 'keypress') {
    //make sure enter is pressed
    if(e.which === 13 || e.keyCode === 13) {
      localStorage.setItem('name', e.target.innerHTML)
      name.blur();
    }
  } else {
    localStorage.setItem('name', e.target.innerHTML)

  }

}
function getFocus() {
  if(localStorage.getItem('focus') === null) {
    focus.textContent = 'Nothing to do ;(';
  } else {
    focus.textContent = localStorage.getItem('focus');
  }
}

function setFocus(e) {
  if(e.type === 'keypress') {
    //make sure enter is pressed
    if(e.which === 13 || e.keyCode === 13) {
      localStorage.setItem('focus', e.target.innerHTML)
      focus.blur();
    }
  } else {
    localStorage.setItem('focus', e.target.innerHTML)

  }
}
name.addEventListener('keypress',setName);
name.addEventListener('blur',setName);

focus.addEventListener('keypress',setFocus);
focus.addEventListener('blur',setFocus);

function addbtn() {
  addButton.type = "button";


const elHtml = '<button type="submit" id="bloczek" title="Zaloguj się">Zaloguj się</div><button type="submit" id="bloczek1" title="zmień stronę">zmień stronę</div>';

const div = addButton

var btn = document.createElement("BUTTON");   
btn.innerHTML = "Wyloguj się";                   
btn.title = "Wyloguj się";
btn.className="bloczek1";
btn.onclick = function () {
  location.href = "http://127.0.0.1:8080";
};

var btn1 = document.createElement("BUTTON"); 
btn1.onclick = function () {
  //location.href = "http://127.0.0.1:8080/remote";
  const url = 'ws://localhost:8081'
const connection = new WebSocket(url)

connection.onopen = () => {
  connection.send('hey') 
}

connection.onerror = (error) => {
  console.log(`WebSocket error: ${error}`)
}

connection.onmessage = (e) => {
  console.log(e.data);
  window.alert(e.data);
}
};
btn1.innerHTML = "Dokumentacja";                   
btn1.title = "Dokumentacja";
btn1.className="bloczek1"



var btn2 = document.createElement("BUTTON");   
btn2.innerHTML = "Sterowanie";                   
btn2.title = "Sterowanie";
btn2.className="bloczek1"

var btn3 = document.createElement("BUTTON");   
btn3.innerHTML = "Inne";                   
btn3.title = "Inne";
btn3.className="bloczek1 disabled";

var br = document.createElement("br");

div.appendChild(btn);
div.appendChild(btn1);
div.appendChild(br);
div.appendChild(btn2);
div.appendChild(btn3);


const button = document.getElementById('bloczek').onclick = function () {
  location.href = "http://127.0.0.1:8080/";
};
}

//run
showTime();
setBgGreet();
getName();
getFocus();
addbtn();
alert("I am an alert box!");









