const express = require('express');
//const router = express.Router();
import { apiController } from '../controllers/apiController';
import {catchAsync} from '../middleware/errors'

export default() => {
    const api = express.Router();

    // GET /devices/:id
    api.get('/:slug',catchAsync(apiController.findOne));

    // GET /devices
    api.get('/',catchAsync(apiController.findAll));

    // POST /devices
    api.post('/',catchAsync(apiController.create));

    // PUT /devices/:id
    api.put('/',catchAsync(apiController.update));


    // DELETE /devices/:id
    api.delete('/slug',catchAsync(apiController.remove));


    return api;
}