const express = require('express');
const router = express.Router();

const PagesController = require('../controllers/PagesController');
router.get('/',PagesController.home) ;

const MainController = require('../controllers/MainController');
router.get('/main',MainController.main) ;

const ApplicationssController = require('../controllers/ApplicationsController');
router.post('/login', ApplicationssController.store);

const Remotecontroller = require('../controllers/RemoteController');
router.get('/remote2', Remotecontroller.remote);

const RedisMain = require('../controllers/MainRedisController');
router.get('/redis', RedisMain.main);
router.get('/redistags', RedisMain.tags);
router.get('/redisstrunowe', RedisMain.strunowe);         // http://localhost:8080/redisstrunowe
router.get('/redisdrewniane', RedisMain.drewniane);       // http://localhost:8080/redisdrewniane
router.get('/redisblaszane', RedisMain.blaszane);         // http://localhost:8080/redisblaszane
router.get('/redisstrunoweid/:id', RedisMain.strunoweid); // http://localhost:8080/redisstrunoweid/9
router.get('/redispipeline', RedisMain.pipeline);





const Application = require('../models/application')


//  router.get('/a', (req, res) => { var a = Application.checks()//.then(function(a){
//                 //  console.log(a);
//     // }); 
//    console.log(a);
//    res.json(a);

//  });


router.get('/a', (req, res) => {Application.checks();res.send('ok');});
router.get('/b', (req, res) => {Application.checks(); res.send('okb');});
router.get('/c', (req, res) => {Application.findAll(); res.send('okc');});
router.get('/d', (req, res) => {Application.findOne(); res.send('okd');});
module.exports = router;

