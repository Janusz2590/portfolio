const app = require('./servers/app');

var Logger = require("./utils/logger");
var knex = require('knex')(require('./config(mysql)/knexfile'));

var Redis = require("ioredis");
var client = new Redis("redis://:"+ process.env.redis_pass+"@"+process.env.redis_host + ":"+process.env.redis_port+"/"+process.env.redis_db);
	

Logger = new Logger();

app.set('port', process.env.server_port || 8080);

const server = app.listen(app.get('port'), () => {
	Logger.log('API',`Ustanowiono server HTTP na porcie  ${ server.address().port }`)
	
	client.get("kierunek", function(err, result) {
		console.log(result)
		if(result === 'informatyka')
		{
			Logger.log('SQL',`Połączono z bazą REDIS  `)
			Logger.log('SQL',`HOST:  ${ process.env.redis_host }  PORT:  ${ process.env.redis_port }  DB:  ${ process.env.redis_db }`)
		}
		
	  });
	client.on("error", function (err) {
	console.log("Error " + err);});

knex.raw("SELECT CURRENT_TIMESTAMP() + 1 as k").then((result) =>{
	
	var z = JSON.stringify(result[0])
	var year = z.substr(6, 4)
	var month =z.substr(10, 2)
	var day = z.substr(12, 2)
	var hour =z.substr(14, 2)
	var minute = z.substr(16, 2)
	var second = z.substr(18, 2)

	var date = new Date(year,month,day,hour,minute,second)
	
	Logger.log('SQL',`SQL INIT OK `)
	Logger.log('SQL',`SQL DATE: ${date} `)

}).catch((err)=>{
	Logger.log('SQL',`Nie zainicjowano połączenia`)
	Logger.log('SQL',err)
	})
});







const secket = require("./servers/socket")
var sacket = new secket();

const WEBSOCKET = require("./servers/webSocketServer")

exports.cli = client;
