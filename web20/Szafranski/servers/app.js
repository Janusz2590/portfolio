require('dotenv').config({path: '.env'})
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const session = require('express-session');
const routes = require('../routes/index');                       // ścieżki
var Logger = require("../utils/logger");
const app = express();   

Logger = new Logger();

Logger.forceWrite();
Logger.log('INF',`Uruchamianie serwera, środowisko ${process.env.NODE_ENV}...`);
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, '../public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

 app.use(session({
    secret: 'a',
    resave: false,
    saveUninitialized: true,
    cookie: {}
 }));


app.use(flash());

app.use('/', routes);
// app.use('/',function (req,res,next) {
//    res.render('404');
// })


const errorsHandler = require('../middleware/errors');  // obsługa błędów
app.use(errorsHandler.NotFound);
app.use(errorsHandler.catchErrors);

module.exports = app;






