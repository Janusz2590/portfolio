var net =require("net");
//import instances from "./instances";
const logger = require('../utils/logger')
Logger = new logger();
var info = require('./k');
class A{

	constructor() {

		this.clients = [];

		var k = net.createServer(socket => {

			socket.name = socket.remoteAddress + ":" + socket.remotePort;
			Logger.log("SOC", `${socket.name} joined network!`);


			

			// Gdy urządzenie pisze do serwera
			socket.on("data", function (data) {
				try {
					let receiver, sender, command, params;
					[receiver, sender, command, params] = data.toString().trim().split("|");
					var p = data.toString().trim()

					if (p){
						Logger.log("SOC", `${sender} (${socket.name}) => ${receiver} ::: ${command}`);
						// instances.websocket.broadcast("app/forceRefresh", 1);
						socket.write("PONG")

					}
					if (receiver != 0) return;
					// Gdy wiadomość kierowana NIE DO serwera

					if (!params) {
						params = "";
					}

					if (receiver && sender && command && command.length > 0) {
						Logger.log("SOC", `${sender} (${socket.name}) => ${receiver} ::: ${command}`);

						// switch (command) {
						// 	case "PONG":
						// 		mysqlClient.query("UPDATE `devices` SET `device_ping` = DEFAULT WHERE `device_huid` = ?", [sender]);
						// 		break;
						// 	case "CURRENTSTATUS":
						// 		if (sender[0] == "5") {
						// 			params = data.toString().trim().split("|").slice(3).map(e => parseInt(e)).join(",");
						// 		}
						// 		mysqlClient.query("UPDATE `devices` SET `device_status` = ? WHERE `device_huid` = ?", [params, sender]);

						// 		instances.websocket.broadcast("app/forceRefresh", null);
						// 		break;
						// 	default:
						// 		break;
						// }
					}
				} catch (error) {
					Logger.log("ERR", `${error}`);
				}
			});

			socket.on("end", () => {
			//	this.clients.splice(this.clients.indexOf(socket), 1);
				Logger.log("SOC", `${socket.name} left network!`);
			});

			socket.on("error", () => {
			//	this.clients.splice(this.clients.indexOf(socket), 1);
				Logger.log("ERR", `${socket.name} left network!`);
			});

			this.clients.push(socket);


		}).listen(process.env.socket_port,function(){
            Logger.log("SOC",`Serwer nasłuchuje na ${process.env.socket_port} czyli ${JSON.stringify(k.address())}` )
          
        });


        net.connect
		setInterval(() => {
            this.broadcast("-1|PING");
            info.None();
		}, 10000);

	}

	broadcast(message) {

		this.clients.forEach(client => {
			client.write(message + "\r\n");
		});

	}
}
module.exports = A;