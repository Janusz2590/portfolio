const fs = require("fs");
const colors = require("colors/safe");

module.exports = class logger {

	constructor() {

		if (logger.instance) {
			return logger.instance;
		}

		logger.logsHistory = [];
		logger.instance = this;
	}

	forceWrite() {
		let stream = fs.createWriteStream(`logs/${new Date().toLocaleDateString()}.log`, {
			flags: "a"
		});
		stream.write(logger.logsHistory.join("\n") + "\n");
		stream.end();
		logger.logsHistory = [];
	}

	log(type, text) {

		let typeC;
		switch (type) {
			case "API":
				typeC = colors.green(type);
				break;
			case "ERR":
				typeC = colors.red(type);
				break;
			case "INF":
				typeC = colors.cyan(type);
				break;
			case "SQL":
				typeC = colors.magenta(type);
				break;
			case "WEB":
				typeC = colors.yellow(type);
				break;
			default:
				typeC = type;
				break;
		}

		let timestamp = new Date().toLocaleString();
		console.log(`[${colors.yellow(timestamp)}] [${colors.inverse(typeC)}] ${text}`);
		logger.logsHistory.push(`[${timestamp}] [${type}] ${text}`);
		if (logger.logsHistory.length > 1) {
			let stream = fs.createWriteStream(`logs/${new Date().toLocaleDateString()}.log`, {
				flags: "a"
			});
			stream.write(logger.logsHistory.join("\n") + "\n");
			stream.end();
			logger.logsHistory = [];
		}

	}

};