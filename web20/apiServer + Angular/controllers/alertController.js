var alertModel = require('../models/alertModel.js');

/**
 * alertController.js
 *
 * @description :: Server-side logic for managing alerts.
 */
module.exports = {

    /**
     * alertController.list()
     */
    list: function (req, res) {
        alertModel.find(function (err, alerts) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting alert.',
                    error: err
                });
            }
            return res.json(alerts);
        });
    },

    /**
     * alertController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        alertModel.findOne({_id: id}, function (err, alert) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting alert.',
                    error: err
                });
            }
            if (!alert) {
                return res.status(404).json({
                    message: 'No such alert'
                });
            }
            return res.json(alert);
        });
    },

    /**
     * alertController.create()
     */
    create: function (req, res) {
        var alert = new alertModel({
			
            addr : req.body.addr,
            information : req.body.information,
            date : req.body.date,
            read : req.body.read

        });

        alert.save(function (err, alert) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating alert',
                    error: err
                });
            }
            return res.status(201).json(alert);
        });
    },

    /**
     * alertController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        alertModel.findOne({_id: id}, function (err, alert) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting alert',
                    error: err
                });
            }
            if (!alert) {
                return res.status(404).json({
                    message: 'No such alert'
                });
            }

            
            alert.addr          = req.body.addr         ? req.body.addr         : alert.addr,
            alert.information   = req.body.information  ? req.body.information  : alert.information,
            alert.date          = req.body.date         ? req.body.date         : alert.date,
            alert.read          = req.body.read         ? req.body.read         : alert.read
			
            alert.save(function (err, alert) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating alert.',
                        error: err
                    });
                }

                return res.json(alert);
            });
        });
    },

    /**
     * alertController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        alertModel.findByIdAndRemove(id, function (err, alert) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the alert.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
