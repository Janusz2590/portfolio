var deviceModel = require('../models/deviceModel.js');

/**
 * deviceController.js
 *
 * @description :: Server-side logic for managing devices.
 */
module.exports = {

    /**
     * deviceController.list()
     */
    list: function (req, res) {
        deviceModel.find(function (err, devices) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting device.',
                    error: err
                });
            }
            return res.json(devices);
        });
    },

    /**
     * deviceController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        deviceModel.findOne({_id: id}, function (err, device) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting device.',
                    error: err
                });
            }
            if (!device) {
                return res.status(404).json({
                    message: 'No such device'
                });
            }
            return res.json(device);
        });
    },

    /**
     * deviceController.create()
     */
    create: function (req, res) {
        var device = new deviceModel({
            addr : req.body.addr,
            information : req.body.information,
            type : req.body.type,
            status : req.body.status,
            error : req.body.error

        });

        device.save(function (err, device) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating device',
                    error: err
                });
            }
            return res.status(201).json(device);
        });
    },

    /**
     * deviceController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        deviceModel.findOne({_id: id}, function (err, device) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting device',
                    error: err
                });
            }
            if (!device) {
                return res.status(404).json({
                    message: 'No such device'
                });
            }


            device.addr =           req.body.addr ? req.body.addr : device.addr,
            device.information =    req.body.information ?  req.body.information:device.information,
            device.type =           req.body.type ? req.body.type:device.type ,
            device.status =         req.body.status ? req.body.status:device.status,
            device.error =          req.body.error ? req.body.error : device.error 
			
            device.save(function (err, device) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating device.',
                        error: err
                    });
                }

                return res.json(device);
            });
        });
    },

    /**
     * deviceController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        deviceModel.findByIdAndRemove(id, function (err, device) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the device.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
