var measurementModel = require('../models/measurementModel.js');

/**
 * measurementController.js
 *
 * @description :: Server-side logic for managing measurements.
 */
module.exports = {

    /**
     * measurementController.list()
     */
    list: function (req, res) {
        measurementModel.find(function (err, measurements) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting measurement.',
                    error: err
                });
            }
            return res.json(measurements);
        });
    },

    /**
     * measurementController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        measurementModel.findOne({_id: id}, function (err, measurement) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting measurement.',
                    error: err
                });
            }
            if (!measurement) {
                return res.status(404).json({
                    message: 'No such measurement'
                });
            }
            return res.json(measurement);
        });
    },

    /**
     * measurementController.create()
     */
    create: function (req, res) {
        var measurement = new measurementModel({          
            date : req.body.date,
            temperature : req.body.temperature,
            pressure : req.body.pressure,
            humidity : req.body.humidity,

        });

        measurement.save(function (err, measurement) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating measurement',
                    error: err
                });
            }
            return res.status(201).json(measurement);
        });
    },

    /**
     * measurementController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        measurementModel.findOne({_id: id}, function (err, measurement) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting measurement',
                    error: err
                });
            }
            if (!measurement) {
                return res.status(404).json({
                    message: 'No such measurement'
                });
            }

           
            measurement.date        = req.body.date         ? req.body.date         : measurement.date,
            measurement.temperature = req.body.temperature  ? req.body.temperature  : measurement.temperature,
            measurement.pressure    = req.body.pressure     ? req.body.pressure     : measurement.pressure,
            measurement.humidity    = req.body.humidity     ? req.body.humidity     : measurement.humidity,
			
            measurement.save(function (err, measurement) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating measurement.',
                        error: err
                    });
                }

                return res.json(measurement);
            });
        });
    },

    /**
     * measurementController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        measurementModel.findByIdAndRemove(id, function (err, measurement) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the measurement.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
