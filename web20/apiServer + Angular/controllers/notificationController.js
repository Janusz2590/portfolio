var notificationModel = require('../models/notificationModel.js');

/**
 * notificationController.js
 *
 * @description :: Server-side logic for managing notifications.
 */
module.exports = {

    /**
     * notificationController.list()
     */
    list: function (req, res) {
        notificationModel.find(function (err, notifications) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting notification.',
                    error: err
                });
            }
            return res.json(notifications);
        });
    },

    /**
     * notificationController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        notificationModel.findOne({_id: id}, function (err, notification) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting notification.',
                    error: err
                });
            }
            if (!notification) {
                return res.status(404).json({
                    message: 'No such notification'
                });
            }
            return res.json(notification);
        });
    },

    /**
     * notificationController.create()
     */
    create: function (req, res) {
        var notification = new notificationModel({
			
            information : req.body.information,
	        date : req.body.date,
	        rank : req.body.rank,
	        read : req.body.read

        });

        notification.save(function (err, notification) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating notification',
                    error: err
                });
            }
            return res.status(201).json(notification);
        });
    },

    /**
     * notificationController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        notificationModel.findOne({_id: id}, function (err, notification) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting notification',
                    error: err
                });
            }
            if (!notification) {
                return res.status(404).json({
                    message: 'No such notification'
                });
            }

            
            
            notification.information    = req.body.information  ?req.body.information:notification.information,
	        notification.date           = req.body.date         ?req.body.date:notification.date,
	        notification.rank           = req.body.rank         ?req.body.rank:notification.rank,
            notification.read           = req.body.read         ?req.body.read:notification.read
			
            notification.save(function (err, notification) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating notification.',
                        error: err
                    });
                }

                return res.json(notification);
            });
        });
    },

    /**
     * notificationController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        notificationModel.findByIdAndRemove(id, function (err, notification) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the notification.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
