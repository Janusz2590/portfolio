var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var alertSchema = new Schema({
	'addr' : Number,
	'information' : String,
	'date' : Date,
	'read' : { type: Boolean, default: false }
});

module.exports = mongoose.model('alert', alertSchema);
