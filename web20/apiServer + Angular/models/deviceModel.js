var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var deviceSchema = new Schema({
	'addr' : {type: Number, unique: true},
	'information' : String,
	'type' : String,
	'status' : Number,
	'error' : Boolean
});

module.exports = mongoose.model('device', deviceSchema);
