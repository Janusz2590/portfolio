var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var measurementSchema = new Schema({
	'date' : Date,
	'temperature' : Number,
	'pressure' : Number,
	'humidity' : Number,
});

module.exports = mongoose.model('measurement', measurementSchema);
