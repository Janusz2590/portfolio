var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var notificationSchema = new Schema({
	'information' : String,
	'date' : Date,
	'rank' : Number,
	'read' : { type: Boolean, default: false }
});

module.exports = mongoose.model('notification', notificationSchema);
