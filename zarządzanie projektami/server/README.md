﻿# API

## Wstęp
Przedstawione w poniższej dokumentacji API pozwala na niegraniczoną integrację zasobów zewnętrznych z zasobami serwera. Dostęp do poszczególnych metod odbywa się poprzez zapytanie HTTP GET lub poprzez WebSocket, co sprawia, że API dostępne jest w większości języków programowania i systemów operacyjnych bez instalowania dodatkowych narzędzi lub bibliotek, a także umożliwia dwustronną komunikację. API zwraca odpowiedź w formacie JSON zawierającą trzy podstawowe parametry:

> success = true / false

Parametr ten określa jednoznacznie, czy wykonanie zapytania przebiegło pomyślnie.

> respCode = [10 … 99]

Parametr ten określa kod odpowiedzi i został on omówiony w rozdziale „kody odpowiedzi”.

> message[]

Ciąg znaków lub tablica zawierająca słowny opis rezultatu zapytania i/lub błędu. Szczegółowo opisana dla każdej metody zapytania.

Dodatkowo - podczas komunikacji poprzez WebSocket, zwracany jest parametr opisujący zapytanie, którego odpowiedź dotyczy:

> queryMethod

Niektóre z metod API zostały zastrzeżone i nie mogą być wywoływane bezpośrednio, a jedynie poprzez wywołanie z innej metody. Listę metod zastrzeżonych opisuje tablica *restricetdMethods[]* z pliku config.js.

Niektóre z metod API nie wymagają autoryzacji przed wywołaniem zapytania, co pozwala na dostęp do niektórych zasobów bez wiązania zapytania z konkretnym użytkownikiem.
Listę funkcji zwolnionych z autoryzacji opisuje tablica *ignoreToken[]* z pliku config.js.

Wszystkie parametry zapytania podajemy w adresie URL lub w formacie JSON jako treść wysyłana przez WebSocket.
Parametr *method* jest zawsze wymagany.
Parametry podane nadmiarowo są ignorowane i nie powodują wystąpienia błędu.

## Przykład wywołania API - poprzez HTTP
Poniższy przykład opisuje wywołanie metody *getTimestamp*. Metoda ta nie przyjmuje żadnych parametrów, a zwraca Unixowy znacznik czasu serwera w celu synchronizacji zegarów:

    GET /?method=getTimestamp
	    message: 	1513201426
	    respCode: 	10
	    success: 	true

Poniższy przykład opisuje zakończone sukcesem wywołanie metody *getUserGroupsInfo*, która jako odpowiedź zwróciła tablicę wszystkich grup do których należy użytkownik.

	GET /?method=getUserInfo&token=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	"success":	true,
	"respCode":	10,
	"message":[
		{
			"id_group":		"1",
			"group_name":		"Admin",
			"online_color":	"#0000FF",
		}
	]

## Przykład wywołania API - poprzez Websocket
Poniższy przykład opisuje wywołanie metody *getUsersInfoById*. Metoda ta przyjmuje jako parametd *id* ciąg identyfikatorów użytkowników, a zwraca ich dane.

	websocket.connect(endpoint);
	websocket.send({
		"method": "getUsersInfoById",
		"id": "1,2,3"
	});

## Obsługiwane metody
### auth - metoda dostępna tylko przez HTTP
Metoda ta pozwala dokonać autoryzacji użytkownika. W odpowiedzi zwraca token służący do wykonywania kolejnych zapytań w danej sesji. Token jest ważny przez czas określony przez właściwość *tokenTime* od dokonania autoryzacji lub kolejnych zapytań - każde zapytanie z wykorzystaniem tokena przedłuża jego ważność.

W przypadku zbyt dużej ilości nieudanych prób zalogowania się, dostęp do API jest czasowo ograniczany dla użytkownika.
Ilość prób, po których następuje ograniczenie określa zmienna *failedLoginAttepmts*, a dostęp wstrzymywany jest na *failedLoginBanTime* sekund.
 - Parametry wejściowe:
	 - username
		 - wymagany
		 - nazwa użytkownika
	 - password
		 - wymagany
		 - rot13(sha1(username.toLowerCase() + password)), zawsze 40 znaków, np. dla admin/admin = qq94709528oo1p83q08s3088q4043s4742891s4s
 - Odpowiedź API:
	 - Jeśli logowanie powiodło się – pole message zawiera token używany do autoryzacji kolejnych zapytań. W przeciwnym razie pole message zawiera informację, że login i/lub hasło nie są poprawne

### getTimestamp
Metoda zwraca unixowy znacznik czasu (z dokładnością do milisekund) w celu synchronizacji zegarów.
 - Parametry wejściowe:
	 - brak
 - Odpowiedź API:
	 - Unix timestamp - bieżący czas serwera

### getUserInfo
Metoda zwraca dane profilu użytkownika i aktualnej sesji
 - Parametry wejściowe:
	 - brak
 - Odpowiedź API:
	 - user_id, token, last_act, username

### getDevices
Metoda zwraca aktualne dane wszystkich urządzeń zapisanych w systemie
 - Parametry wejściowe:
	 - brak
 - Odpowiedź API:
	 - group_id, device_id, device_name, device_description, device_type, device_huid, device_ping, device_status, group_name, group_description

### getGroups
Metoda zwraca aktualne dane wszystkich grup urządzeń utworzonych w systemie
 - Parametry wejściowe:
	 - brak
 - Odpowiedź API:
	 - group_id, group_name, group_description

### updateDevice
Metoda pozwala na aktualizowanie danych wskazanego urządzenia lub dodanie nowego
 - Parametry wejściowe:
	 - device_id
	  - Określa ID aktualizowanego urządzenia. Jeśli podano 0 - tworzony jest nowy wpis urządzenia
	 - device_name
	  - Nazwa urządzenia
	 - device_description
	  - Opis urządzenia
	 - device_huid
	  - Identyfikator sprzętowy urządzenia
	 - group_id
	  - ID grupy, do której przynależy urządzenie
	 - device_description
	  - Opis urządzenia
 - Odpowiedź API:
	 - Potwierdzenie utworzenia / aktualizacji urządzenia

### removeGroup / removeDevice
Metody, które w odpowiedzi na ich wywołanie powoduja usunięcie z bazy danych wpisów danej grupy lub danego urządzenia
 - Parametry wejściowe:
	 - value
	  - Określa ID urządzenia / grupy do usunięcia
 - Odpowiedź API:
	 - Potwierdzenie wykonania zapytania


### powerDevice
Metoda, której wywołanie powoduje wyzwolenie przełączenia urządzenia do innego stanu
 - Parametry wejściowe:
	 - id
	  - Określa ID urządzenia
	 - state
	  - Nowy stan urządzenia. Szczegółowy opis dostępnych wartości znajduje się w kolejnym akapicie
 - Odpowiedź API:
	 - Potwierdzenie wykonania zapytania

### updateGroup
Metoda pozwala na aktualizowanie danych wskazanej grupy lub utworzenie nowej.
 - Parametry wejściowe:
	 - group_id
	  - Określa ID aktualizowanej grupy. Jeśli podano 0 - tworzona jest nowa grupa urządzeń
	 - group_name
	  - Nazwa grupy
	 - group_description
	  - Opis grupy
 - Odpowiedź API:
	 - Potwierdzenie utworzenia / aktualizacji grupy
## Adresy
Adresy nasłuchiwania można skonfigurować w pliku *config.js*.
  * Komunikacja poprzez HTTP: http://IP_SERWERA:8080
  * Komunikacja poprzez WebSocket: ws://IP_SERWERA:8090/__tokenUżytkownika

## Ograniczenia w dostępie do metod
Medoda *auth* dostępna jest tylko przez HTTP. Pozostałe metody dostępne są również przez WebSocket.

## Obsługa błędów
* 10 - Brak błędu, zapytanie udane.
* 11 - Nieznana metoda. Wywołana metoda nie istnieje – sprawdź parametr method.
* 12 - Metoda nie może być wywołana bezpośrednio - dostęp do niej nie jest możliwy.
* 13 - Metoda wymaga autoryzacji - dokonaj autoryzacji i podaj parametr token przy wywołaniu.
* 14 - Błąd autoryzacji - login/hasło/token niepoprawne lub token wygasł.
* 15 - Błąd danych wejściowych - nieprawidłowa ilość/długość wymaganych parametrów.
* 16 - Błąd połączenia z MySQL - sprawdź konfigurację połączenia i status serwera bazy danych.
* 20 - Timeout zapytania HTTP - serwer nie uzyskał wartości, którą mógłby zwrócić klientowi, zwraca również kod HTTP 408

## Numery sprzętowe urządzeń
Ze względu na konieczność adresowania komunikatów sterujących urządzeniami, każde urządzenie w wewnętrznej siecii systemu posiada nadany identyfikator sprzętowy.
Jest to ciąg co najmniej czterech znaków alfanumerycznych, przy czym:
* -1 oznacza rozgłoszenie komunikatu do wszystkich urządzeń
* 0 zarezerwowane jest dla serwera - Raspberry Pi
* Adresy rozpoczynające się od 1 identyfikują urządzenia typu "gniazdko"
* Adresy rozpoczynające się od 2 identyfikują urządzenia typu "elektrozawór"
* Adresy rozpoczynające się od 3 identyfikują urządzenia typu "Alarm"
* Adresy rozpoczynające się od 4 identyfikują urządzenia typu "Roleta"
* Adresy rozpoczynające się od 5 identyfikują urządzenia typu "Pogodynka"

## Wewnętrzny protokół komunikacyjny
Urządzenia wykonawcze komunikują się z serwerem za pośrednictwem połączenia Wi-Fi. Serwer udostępnia na porcie 9136 socket TCP, przez który dane przesyłane są otwartym tekstem.
W ramach komunikacji stworzony został prosty protokół, którego kolejne dane rozdzielone są znakiem pionowej kreski |. Każdy komunikat składa się z

* Identyfikatora sprzętowego adresata,
* OPCJONALNIE: Identyfikatora sprzętowego nadawcy - tylko gdy odbiorcą jest serwer (0),
* Właściwej komendy
* Parametrów jej wykonania oddzielonych znakiem pionowej kreski |

* Przykładowa komenda ustawiająca stan urządzenia 12345 na 1: *12345|SETSTATUS|1*
* Przykładowa komenda zwracajaca do serwera potwierdzenie zmany stanu na 0: *0|12345|CURRENTSTATUS|1*

* Przykładowa komenda prosząca wszystkie urządzenia o odpowiedź (badanie podtrzymania komunikacji): *-1|PING*
* Oraz odpowiedź na nią: *0|12345|PONG*

W przypadku komendy SETSTATUS istnieją ograniczenia wynikające ze specyfiki danego urządzenia. Możliwe do zastosowania wartości są następujące:

* Gniazdo
	* 0 – wyłączone
	* 1 – włączone
* Zawór
	* 0 – zamknięty
	* 1 – otwarty
* Alarm
	* 0 – nieuzbrojony, wyłączony
	* 1 – uzbrojony, oczekuje na intruza
	* 2 – naruszony, ktoś włamał się do (status tylko do doczytu)
* Roleta
	* -2 – dotyka dolnej krańcówki (status tylko do doczytu)
	* -1 – rozwijaj się w dół
	* 0 – brak aktywności
	* 1 – rozwijaj się w górę
	* 2 – górna krańcówka (status tylko do doczytu)
* Stacja pogodowa – nie pozwala ustawić stanu, tylko odczytuje dane!


