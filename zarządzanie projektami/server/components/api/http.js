import express from "express";
import config from "../../config";
import bodyParser from "body-parser";
import mainApiController from "./mainApiController";
import JSONPrivateFields from "../../utils/JSONPrivateFields";
import instances from "../../utils/instances";

module.exports = class http {

	constructor() {

		this.api = new express();
		this.api.set("json replacer", JSONPrivateFields);
		this.api.use(bodyParser.json());
		this.api.use(bodyParser.urlencoded({
			extended: true
		}));
		this.api.use(function (req, res, next) {
			res.setHeader("Access-Control-Allow-Origin", "*");
			res.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate, max-age=0");
			res.setHeader("Expires", "-1");
			res.setHeader("Pragma", "no-cache");
			res.setHeader("Content-type", "application/json");
			next();
		});

		this.api.listen(config.APIHttpPort, () => {
			instances.logger.log("API", "Ustanowiono serwer HTTP");
		});

		this.api.get("/", function (req, res) {
			let requestIP = req.headers["x-forwarded-for"] || req.connection.remoteAddress;

			res.setTimeout(10000, () => {
				!res.headersSent && res.status(408).json({
					success: false,
					respCode: 20,
					message: "HTTP Request Timeout :("
				});
				instances.logger.log("ERR", `HTTP 408, IP = ${requestIP}, Q = ${JSON.stringify(req.query)}`);
			});

			if (!req.query.method || typeof (mainApiController[req.query.method]) != "function") {
				!res.headersSent && res.json({
					success: false,
					respCode: 11,
					message: "Unknown method"
				});
				instances.logger.log("ERR", `HTTP Unknown method, IP = ${requestIP}, Q = ${JSON.stringify(req.query)}`);
				return;
			}

			if (config.restricetdMethods.indexOf(req.query.method) > -1) {
				!res.headersSent && res.json({
					success: false,
					respCode: 12,
					message: "This method cannot be called directly"
				});
				instances.logger.log("ERR", `HTTP This method cannot be called directly, IP = ${requestIP}, Q = ${JSON.stringify(req.query)}`);
				return;
			}

			if (config.ignoreToken.indexOf(req.query.method) > -1) {

					mainApiController[req.query.method](req.query).then(result => {
						!res.headersSent && res.json(result);
					});
			} else {
				if (!req.query.token || req.query.token.length != 64) {
					!res.headersSent && res.json({
						success: false,
						respCode: 14,
						message: "Token is invalid or expired"
					});
					instances.logger.log("ERR", `HTTP Token is invalid or expired, IP = ${requestIP}, Q = ${JSON.stringify(req.query)}`);
					return;
				}
				mainApiController["middleware"](req.query).then(middleware => {
					if (middleware.success == true) {
						mainApiController.renewToken(req.query.token).then(() => {
							mainApiController[req.query.method](req.query, middleware.message).then(result => {
								!res.headersSent && res.json(result);
							});
						});
					} else {
						!res.headersSent && res.json(middleware);
					}
				});
			}
		});

	}

};