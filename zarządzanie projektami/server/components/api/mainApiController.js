/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import instances from "../../utils/instances";
import crypto from "crypto";
import config from "../../config";
import rot13 from "../../utils/rot13";

module.exports = {
	JSONSyntaxError: function (_params) {
		return new Promise((resolve, reject) => {
			resolve({
				success: false,
				respCode: 15,
				message: "Invalid JSON string"
			});
		});
	},

	renewToken: function (_token) {
		return new Promise((resolve, reject) => {
			mysqlClient.query("UPDATE `tokens` SET `last_act` = NOW() WHERE `api_token` = ?", [_token]).then(([rows, fields]) => {
				rows.affectedRows && resolve();
			}).catch(() => {
				resolve({
					success: false,
					respCode: 16,
					message: null
				});
			});
		});
	},

	middleware: function (_params) {
		return new Promise((resolve, reject) => {
			if (!_params.token) {
				reject({
					success: false,
					respCode: 15,
					message: "Token is required"
				});
				return;
			}

			mysqlClient.query("SELECT * FROM `tokens` t LEFT JOIN `users` u USING(`user_id`) WHERE t.`token` = ? AND DATE_ADD(t.`last_act`, INTERVAL ? SECOND) > NOW() LIMIT 0,1", [_params.token, config.tokenTime]).then(([rows, fields]) => {
				if (rows.length == 1) {
					delete rows[0].password;
					delete rows[0].token_id;
					resolve({
						success: true,
						respCode: 10,
						message: rows[0]
					});
				} else {
					resolve({
						success: false,
						respCode: 14,
						message: "Token is invalid or expired"
					});
				}
			}).catch(() => {
				reject({
					success: false,
					respCode: 16,
					message: null
				});
			});
		});
	},

	auth: function (_params) {
		return new Promise((resolve, reject) => {
			if (!_params.username || !_params.password) {
				resolve({
					success: false,
					respCode: 15,
					message: "Username and password are required"
				});
				return;
			}

			mysqlClient.query("SELECT * FROM `users` WHERE `username` = ? AND `password` = ? LIMIT 0,1", [_params.username, rot13(_params.password)]).then(([rows, fields]) => {
				if (rows.length == 1) {

					let userInfo = rows[0];
					userInfo.api_token = (crypto.randomBytes(32).toString("hex"));

					mysqlClient.query("INSERT INTO `tokens` VALUES (NULL, ?, ?, NOW())", [userInfo.api_token, userInfo.user_id]).then(([rows, fields]) => {
						resolve({
							success: true,
							respCode: 10,
							message: userInfo.api_token
						});
						return;
					});

				} else {
					resolve({
						success: false,
						respCode: 14,
						message: "Invalid username or password"
					});
				}
			});
		});
	},

	getDevices: function (_params, _userData, _webSocket = null) {
		return new Promise((resolve, reject) => {
			mysqlClient.query("SELECT * FROM `devices` d LEFT JOIN `groups` g USING(`group_id`)").then(([rows, fields]) => {
				resolve({
					success: true,
					respCode: 10,
					message: rows
				});
			}).catch(() => {
				reject({
					success: false,
					respCode: 16,
					message: null
				});
			});
		});
	},

	removeDevice: function (_params, _userData, _webSocket = null) {
		return new Promise((resolve, reject) => {

			let id = parseInt(_params.value | 0);
			if (id == 0) {
				reject({
					success: false,
					respCode: 15,
					message: "Valid value (device_id) is required!"
				});
			} else {
				mysqlClient.query("DELETE FROM `devices` WHERE `device_id` = ?", id).then(([rows, fields]) => {
					resolve({
						success: true,
						respCode: 10,
						message: "Device has been deleted!"
					});
				}).catch(() => {
					reject({
						success: false,
						respCode: 16,
						message: null
					});
				});
			}
		});
	},

	removeGroup: function (_params, _userData, _webSocket = null) {
		return new Promise((resolve, reject) => {

			let id = parseInt(_params.value | 0);
			if (id == 0) {
				reject({
					success: false,
					respCode: 15,
					message: "Valid value (group_id) is required!"
				});
			} else {
				mysqlClient.query("DELETE FROM `groups` WHERE `group_id` = ?", id).then(([rows, fields]) => {
					resolve({
						success: true,
						respCode: 10,
						message: "Group has been deleted!"
					});
				}).catch(() => {
					reject({
						success: false,
						respCode: 16,
						message: null
					});
				});
			}
		});
	},

	powerDevice: function (_params, _userData, _webSocket = null) {
		return new Promise((resolve, reject) => {

			let id = parseInt(_params.id | 0);
			let state = parseInt(_params.state | 0);
			if (id == 0 || state < -2 || state > 2) {
				resolve({
					success: false,
					respCode: 15,
					message: "Valid id (device_id) and state (-2, -1, 0, 1, 2) are required!"
				});
				return;
			} else {
				mysqlClient.query("SELECT `device_huid` FROM `devices` WHERE `device_id` = ?", [id]).then(([rows, fields]) => {
					if (rows[0].device_huid) {
						instances.deviceSocket.broadcast(`${rows[0].device_huid}|SETSTATUS|${state}`);
					}
					resolve({
						success: true,
						respCode: 10,
						message: "Device status has been updated!"
					});
				}).catch(() => {
					reject({
						success: false,
						respCode: 16,
						message: null
					});
				});
			}
		});
	},

	updateDevice: function (_params, _userData, _webSocket = null) {
		return new Promise((resolve, reject) => {

			console.log(_params);
			let id = parseInt(_params.device_id | 0);
			if (id == 0) {
				// nowy diwajs
				mysqlClient.query("INSERT INTO `devices` VALUES(null, ?, ?, 1, ?, NOW(), 0, ?)", [_params.device_name, _params.device_description, _params.device_huid, _params.group_id]).then(([rows, fields]) => {
					resolve({
						success: true,
						respCode: 10,
						message: "Device has been saved!"
					});
				}).catch((e) => {
					console.log(e);
					reject({
						success: false,
						respCode: 16,
						message: null
					});
				});
			} else {
				// aktualizacja
				mysqlClient.query("UPDATE `devices` SET `device_name` = ?, `device_description` = ?, `device_huid` = ?, `group_id` = ? WHERE `device_id` = ?", [_params.device_name, _params.device_description, _params.device_huid, _params.group_id, id]).then(([rows, fields]) => {
					resolve({
						success: true,
						respCode: 10,
						message: "Device has been saved!"
					});
				}).catch(() => {
					reject({
						success: false,
						respCode: 16,
						message: null
					});
				});
			}
		});
	},

	updateGroup: function (_params, _userData, _webSocket = null) {
		return new Promise((resolve, reject) => {

			console.log(_params);
			let id = parseInt(_params.group_id | 0);
			if (id == 0) {
				// nowy diwajs
				mysqlClient.query("INSERT INTO `groups` VALUES(null, ?, ?)", [_params.group_name, _params.group_description]).then(([rows, fields]) => {
					resolve({
						success: true,
						respCode: 10,
						message: "Group has been saved!"
					});
				}).catch((e) => {
					console.log(e);
					reject({
						success: false,
						respCode: 16,
						message: null
					});
				});
			} else {
				// aktualizacja
				mysqlClient.query("UPDATE `groups` SET `group_name` = ?, `group_description` = ? WHERE `group_id` = ?", [_params.group_name, _params.group_description, id]).then(([rows, fields]) => {
					resolve({
						success: true,
						respCode: 10,
						message: "Group has been saved!"
					});
				}).catch((e) => {
					reject({
						success: false,
						respCode: 16,
						message: null
					});
				});
			}
		});
	},

	getGroups: function (_params, _userData, _webSocket = null) {
		return new Promise((resolve, reject) => {
			mysqlClient.query("SELECT * FROM `groups`").then(([rows, fields]) => {
				resolve({
					success: true,
					respCode: 10,
					message: rows
				});
			}).catch(() => {
				reject({
					success: false,
					respCode: 16,
					message: null
				});
			});
		});
	},

	getTimestamp: function (_params) {
		return new Promise((resolve, reject) => {
			resolve({
				success: true,
				respCode: 10,
				message: Date.now()
			});
		});
	},

	getUserInfo: function (_params, _userData, _webSocket = null) {
		return new Promise((resolve, reject) => {
			resolve({
				success: true,
				respCode: 10,
				message: _userData
			});
		});
	},
};