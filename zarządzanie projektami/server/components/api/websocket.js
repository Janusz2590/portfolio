import ws from "ws";
import http from "http";
import mysql from "mysql2/promise";
import request from "request";
import buildUrl from "build-url";

import instances from "../../utils/instances";
import config from "../../config";
import mainApiController from "./mainApiController";

export default class {

	constructor() {

		if (global.websocket) {
			instances.logger.log("ERR", "You called the websocket constructor! Use instance instead!");
			return global.websocket;
		}

		global.websocket = this;

		this.httpsServer = http.createServer();
		this.websocketServer = new ws.Server({
			server: this.httpsServer
		});
		this.websocketServer.on("listening", () => {
			instances.logger.log("API", "Ustanowiono serwer WebSocket");
		});
		this.websocketServer.on("error", (error) => {
			instances.logger.log("ERR", "Błąd podczas uruchamiania serwera WS");
			instances.logger.log("ERR", error);
			process.exit();
		});

		this.mysqlClient = mysql.createPool(config.mysqlConfig);
		global.mysqlClient = this.mysqlClient;
		this.mysqlClient.query("SELECT NOW() as data", (error, result) => {
			if (!error) {
				this.httpsServer.listen(config.APIWebsocketPort);
				instances.logger.log("SQL", `SQL OK: ${result[0].data}`);
			} else {
				instances.logger.log("ERR", `Błąd MySQL: ${error}`);
			}
		});

		this.websocketServer.on("connection", (client, params) => {
			let token = params.url.substr(1);
			instances.logger.log("API", `Nowy klient, IP = ${params.connection.remoteAddress}, TOKEN = ${token}`);

			request({
				url: buildUrl(config.apiEndpoint, {
					queryParams: {
						method: "getUserInfo",
						extended: "1",
						token
					}
				}),
				rejectUnauthorized: false
			}, (error, response, body) => {
				if (!error && response.statusCode === 200) {
					let responseObject = JSON.parse(body);
					if (responseObject.success == true) {
						instances.logger.log("WEB", `${token} = ${responseObject.message.username}`);
						responseObject.message.token = token;
						//responseObject.message.isAdmin = responseObject.message.user_groups.map(group => group.id_group).some(id => config.adminGroups.includes(id));
						//if (responseObject.message.isAdmin) {
						//	instances.logger.log("WEB", `${responseObject.message.username} = admin`);
						//}

						client.userData = responseObject.message;
						client.userData.showTimetableFor = new Set();
						client.userData.showSignalBlocksFor = new Set();
						client.userData.lastPingResponse = Date.now();

						this.sendData(client, "app/timestamp", Date.now());
						this.sendData(client, "app/enableTabs", 31);
						this.requestApi(client, {
							method: "getDevices",
							token
						});
						this.requestApi(client, {
							method: "getGroups",
							token
						});
					} else {
						instances.logger.log("WEB", `Nie znaleziono użytkownika dla tokenu ${token}. Połączenie odrzucone!`);
						this.sendData(client, "app/logout");
						this.closeConnection(client);
					}
				}

			});

			client.on("close", () => {
				if (client.userData) instances.logger.log("WEB", `${client.userData.token} = ${client.userData.username} rozłączył się`);
			});

			client.on("error", () => {
				instances.logger.log("WEB", `${client.userData.token} = ${client.userData.username} utracił połączenie`);
				instances.liveData.unload();
			});

			client.on("message", (message) => {
				instances.logger.log("CMD", message);
				try {
					let json = JSON.parse(message);
					let command = json.command;
					let value = json.value;
					switch (command) {
						case "chatMessage":
							instances.chatManager.newChatMessage(client, value);
							break;
						case "getDevices":
							this.requestApi(client, {
								method: "getDevices",
								token
							});
							break;
						case "getGroups":
							this.requestApi(client, {
								method: "getGroups",
								token
							});
							break;
						case "removeDevice":
							this.requestApi(client, {
								method: "removeDevice",
								token,
								value
							});
							break;
						case "removeGroup":
							this.requestApi(client, {
								method: "removeGroup",
								token,
								value
							});
							break;
						case "powerDevice":
							this.requestApi(client, {
								method: "powerDevice",
								token,
								id: value.id,
								state: value.state
							});
							break;
						case "updateGroup":
							this.requestApi(client, {
								method: "updateGroup",
								token,
								group_id: value.group_id,
								group_name: value.group_name,
								group_description: value.group_description,
							});
							break;
						case "updateDevice":
							this.requestApi(client, {
								method: "updateDevice",
								token,
								group_id: value.group_id,
								device_id: value.device_id,
								device_name: value.device_name,
								device_description: value.device_description,
								device_huid: value.device_huid,
							});
							break;
						case "updateGroup":
							this.requestApi(client, {
								method: "updateGroup",
								token,
								group_id: value.group_id,
								group_name: value.group_name,
								group_description: value.group_description
							});
							break;
						default:
							break;
					}
				} catch (error) {
					instances.logger.log("ERR", message.toString());
					instances.logger.log(error);
				}
			});
		});

	}

	requestApi(client, queryParams) {
		request({
			url: buildUrl(config.apiEndpoint, {
				queryParams
			}),
			rejectUnauthorized: false
		}, (error, response, body) => {
			let resp = JSON.parse(body);
			if (resp.success) {
				this.sendData(client, queryParams.method, resp.message);
			} else {
				this.sendData(client, queryParams.method, null);
			}
		});
	}

	closeConnection(client) {

		try {
			client.close();
		} catch (error) {
			instances.logger.log("ERR", error);
			instances.logger.log("ERR", error.stack);
		}

	}

	sendData(client, command, value = undefined) {

		return new Promise((resolve) => {
			if (client.readyState === ws.OPEN) {
				try {
					client.send(JSON.stringify({
						command,
						value
					}));
					resolve();
				} catch (error) {
					instances.logger.log("ERR", "websocketClient.sendData() failure");
					instances.logger.log("ERR", error);
				}
			}
		});

	}

	broadcast(command, value, region = undefined) {

		this.websocketServer.clients.forEach(client => {
			if (client.readyState === ws.OPEN && client.userData != undefined) {
				if (region != undefined) {
					if (client.userData.region == region) {
						this.sendData(client, command, value);
					}
				} else {
					this.sendData(client, command, value);
				}
			}
		});

	}

	getClientByUserId(id, region) {

		let result = [];
		this.websocketServer.clients.forEach(client => {
			if (client.readyState === ws.OPEN && client.userData != undefined) {
				if (client.userData.id_member == id && client.userData.region == region) {
					result.push(client);
				}
			}
		});
		return result;

	}

}