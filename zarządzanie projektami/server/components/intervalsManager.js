import instances from "../utils/instances";
import duration from "@sindresorhus/to-milliseconds";

export default class {

	constructor() {

		if (global.intervalsManager) {
			instances.logger.log("ERR", "You called the intervalsManager constructor! Use instance instead!");
			return global.intervalsManager;
		}

		global.intervalsManager = this;

		setInterval(() => {
			instances.websocket.broadcast("app/timestamp", Date.now());
		}, duration({
			minutes: 3
		}));

	}

}