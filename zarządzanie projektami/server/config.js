import envFileLoader from "./utils/envFileLoader";
let envFileLoaderInstance = new envFileLoader();

export default {
	APIWebsocketPort: 8090,
	APIHttpPort: 8080,
	apiEndpoint: "http://localhost:8080",
	mysqlConfig: {
		connectionLimit: 2,
		host: envFileLoaderInstance.cfg.SERVER_DB_HOST,
		user: envFileLoaderInstance.cfg.SERVER_DB_USER,
		password: envFileLoaderInstance.cfg.SERVER_DB_PASS,
		database: envFileLoaderInstance.cfg.SERVER_DB_BASE,
		timeout: 4
	},
	adminGroups: [1],

	restricetdMethods: ["middleware", "renewToken"],
	ignoreToken: ["auth", "getTimestamp"],
	tokenTime: 3600,

};