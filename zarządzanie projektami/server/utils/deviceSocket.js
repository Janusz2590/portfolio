import net from "net";
import instances from "./instances";

export default class {

	constructor() {

		this.clients = [];

		net.createServer(socket => {

			socket.name = socket.remoteAddress + ":" + socket.remotePort;
			instances.logger.log("SOC", `${socket.name} joined network!`);


			// Gdy urządzenie pisze do serwera
			socket.on("data", function (data) {
				try {
					let receiver, sender, command, params;
					//console.log(data.toString().trim());
					[receiver, sender, command, params] = data.toString().trim().split("|");


					
					if (receiver != 0) return;
					// Gdy wiadomość kierowana NIE DO serwera
					
					if (!params) {
						params = "";
					}

					if (receiver && sender && command && command.length > 0) {
						instances.logger.log("SOC", `${sender} (${socket.name}) => ${receiver} ::: ${command}`);
						socket.write("sddfsdfa")
						switch (command) {
							case "PONG":
								mysqlClient.query("UPDATE `devices` SET `device_ping` = DEFAULT WHERE `device_huid` = ?", [sender]);
								break;
							case "CURRENTSTATUS":
								if (sender[0] == "5") {
									params = data.toString().trim().split("|").slice(3).map(e => parseInt(e)).join(",");
								}
								mysqlClient.query("UPDATE `devices` SET `device_status` = ? WHERE `device_huid` = ?", [params, sender]);

								instances.websocket.broadcast("app/forceRefresh", null);
								break;
							default:
								break;
						}
					 }
				} catch (error) {
					instances.logger.log("ERR", `${error}`);
				}
			});

			socket.on("end", () => {
				this.clients.splice(this.clients.indexOf(socket), 1);
				instances.logger.log("SOC", `${socket.name} left network!`);
			});

			socket.on("error", () => {
				this.clients.splice(this.clients.indexOf(socket), 1);
				instances.logger.log("ERR", `${socket.name} left network!`);
			});

			this.clients.push(socket);


		}).listen(9136);

		setInterval(() => {
			this.broadcast("-1|PING");
		}, 50000);

	}

	broadcast(message) {

		this.clients.forEach(client => {
			client.write(message + "\r\n");
		});

	}
}