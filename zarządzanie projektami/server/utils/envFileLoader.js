import path from "path";
import dotenv from "dotenv";

import instances from "./instances";

export default class {

    constructor() {

        if (global.envFileLoader) {
            return global.envFileLoader;
        }

        global.envFileLoader = this;

        let isConfigComplete = true;
        let requireVariables = ["SERVER_DB_HOST", "SERVER_DB_PASS", "SERVER_DB_USER", "SERVER_DB_BASE"];
        let envFilePath = path.resolve(process.cwd(), ".env");
        let result = dotenv.config({
            path: envFilePath
        });

        if (!result.error) {
            requireVariables.forEach(v => {
                if (Object.keys(result.parsed).includes(v) == false) {
                    instances.logger.log("ERR", `Required ENV Variable ${v} not found!`);
                    isConfigComplete = false;
                }
            });
            if (!isConfigComplete) {
                process.exit();
            } else {
                this.cfg = result.parsed;
            }
        } else {
            instances.logger.log("ERR", `Unable to read ${envFilePath} file!`);
            process.exit();
        }

    }

}