import http from "../components/api/http";
import logger from "../utils/logger";
import envFileLoader from "./envFileLoader";
import websocket from "../components/api/websocket";
import intervalsManager from "../components/intervalsManager";
import deviceSocket from "./deviceSocket";

export default {
	logger: new logger(),
	http: new http(),
	websocket: new websocket(),
	envFileLoader: new envFileLoader(),
	intervalsManager: new intervalsManager(),
	deviceSocket: new deviceSocket()
};