/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","npm"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js!./src/assets/css/CDNs.css":
/*!***********************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/assets/css/CDNs.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro);", ""]);
exports.push([module.i, "@import url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/fontawesome.min.css);", ""]);
exports.push([module.i, "@import url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/solid.min.css);", ""]);
exports.push([module.i, "@import url(https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css);", ""]);
exports.push([module.i, "@import url(https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.9/css/AdminLTE.min.css);", ""]);
exports.push([module.i, "@import url(https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/3.0.0-beta.2/jquery.contextMenu.min.css);", ""]);
exports.push([module.i, "@import url(https://cdnjs.cloudflare.com/ajax/libs/pretty-checkbox/3.0.3/pretty-checkbox.min.css);", ""]);

// Module
exports.push([module.i, "", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/assets/css/flags.css":
/*!************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/assets/css/flags.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Imports
var urlEscape = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/url-escape.js */ "./node_modules/css-loader/dist/runtime/url-escape.js");
var ___CSS_LOADER_URL___0___ = urlEscape(__webpack_require__(/*! ../img/flags.png */ "./src/assets/img/flags.png"));

// Module
exports.push([module.i, "/*!\n * Generated with CSS Flag Sprite generator (https://www.flag-sprites.com/)\n */.flag{display:inline-block;width:32px;height:32px;background:url(" + ___CSS_LOADER_URL___0___ + ") no-repeat}.flag.flag-cz{background-position:-32px 0}.flag.flag-de{background-position:-64px 0}.flag.flag-gb{background-position:0 -32px}.flag.flag-pl{background-position:-32px -32px}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/assets/css/style.css":
/*!************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/assets/css/style.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "body {\n\tbackground-color: #ecf0f5 !important;\n}\n\ntable.table-center thead tr th,\ntable.table-center tbody tr td {\n\ttext-align: center;\n}\n\ndiv.flag {\n\tmargin-right: 5px;\n\tcursor: pointer;\n}\n\ndiv.login-box {\n\tmargin: 0 auto !important;\n\twidth: 85% !important;\n}\n\ndiv.main-header {\n\tposition: fixed !important;\n\twidth: 100% !important;\n}\n\ndiv.wrapper>header>nav {\n\tbackground-color: #222d32 !important;\n\tmargin: 0 !important;\n}\n\n#navbar-collapse {\n\toverflow: visible !important;\n}\n\n#navbar-collapse>ul>li>a {\n\tbox-sizing: border-box;\n\t-moz-box-sizing: border-box;\n\t-webkit-box-sizing: border-box;\n}\n\nli.active>a[data-type='mainMenu'] {\n\tborder-top: 3px solid #ECF0F5;\n\tfont-weight: bold;\n\theight: 50px;\n}\n\nli.active>a[data-type='chatTab'] {\n\tborder-top: 3px solid #222d32;\n\tfont-weight: bold;\n\theight: 30px;\n}\n\nli.active>a[data-type='editorTab'] {\n\tborder-top: 3px solid #131313;\n\tfont-weight: bold;\n\theight: 50px;\n}\n\n.navbar-nav>li>a[data-type='chatTab'],\n.navbar-nav>li.active>a[data-type='chatTab'] {\n\tpadding-top: 5px !important;\n\tpadding-bottom: 5px !important;\n}\n\n@media (min-width: 1200px) {\n\t.container {\n\t\twidth: 100% !important;\n\t}\n}\n\n@media (min-width: 768px) {\n\t.navbar-nav>li {\n\t\tmargin-right: 3px !important;\n\t}\n}\n\ndiv>div.navbar-custom-menu>ul>li>a {\n\tcolor: white !important;\n}\n\n.navbar-nocolors>li>a:hover,\n.navbar-nocolors>li>a:active,\n.navbar-nocolors .open>a,\n.navbar-nocolors .open>a:focus,\n.navbar-nocolors .open>a:hover {\n\tbackground-color: #222d32 !important\n}\n\n.content-wrapper,\n.main-footer {\n\tmargin: 0 !important;\n}\n\n.mr {\n\tmargin-right: 10px !important;\n}\n\n.ml {\n\tmargin-left: 10px !important;\n}\n\n.askButtons {\n\ttext-align: center;\n\tmargin: 10px;\n}\n\ndiv.direct-chat-messages {\n\tpadding: 10px;\n\theight: 500px !important;\n}\n\n.cPointer {\n\tcursor: pointer;\n}\n\n.container {\n\tmargin-left: 0px;\n}\n\n@media (max-width: 768px) {\n\t#moveCustomMenuToShowBars {\n\t\tright: 30px;\n\t}\n}\n\n@media (min-width: 768px) {\n\t#moveCustomMenuToShowBars {\n\t\tright: 1px;\n\t}\n}\n\ntd.middle {\n\tvertical-align: middle !important;\n}\n\ntd.top {\n\tvertical-align: top !important;\n}\n\n.btn-file {\n\tposition: relative;\n\toverflow: hidden;\n}\n\n.btn-file input[type=file] {\n\tposition: absolute;\n\ttop: 0;\n\tright: 0;\n\tmin-width: 100%;\n\tmin-height: 100%;\n\tfont-size: 100px;\n\ttext-align: right;\n\tfilter: alpha(opacity=0);\n\topacity: 0;\n\toutline: none;\n\tbackground: white;\n\tcursor: inherit;\n\tdisplay: block;\n}\n\n.borderless td,\n.borderless th {\n\tborder: none !important;\n}\n\n@-webkit-keyframes blinker {\n\tfrom {\n\t\topacity: 1.0;\n\t}\n\n\tto {\n\t\topacity: 0.0;\n\t}\n}\n\n.blink {\n\ttext-decoration: blink;\n\t-webkit-animation-name: blinker;\n\t-webkit-animation-duration: 0.6s;\n\t-webkit-animation-iteration-count: infinite;\n\t-webkit-animation-timing-function: ease-in-out;\n\t-webkit-animation-direction: alternate;\n}", ""]);



/***/ }),

/***/ "./src/assets/css/CDNs.css":
/*!*********************************!*\
  !*** ./src/assets/css/CDNs.css ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./CDNs.css */ "./node_modules/css-loader/dist/cjs.js!./src/assets/css/CDNs.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/assets/css/flags.css":
/*!**********************************!*\
  !*** ./src/assets/css/flags.css ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./flags.css */ "./node_modules/css-loader/dist/cjs.js!./src/assets/css/flags.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/assets/css/style.css":
/*!**********************************!*\
  !*** ./src/assets/css/style.css ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./style.css */ "./node_modules/css-loader/dist/cjs.js!./src/assets/css/style.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/assets/img/flags.png":
/*!**********************************!*\
  !*** ./src/assets/img/flags.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABACAYAAADlNHIOAAAQO0lEQVR4nO2ce7QV1X3HP785Z859AqIiaFArJiJKDZYIGkgsKhYfy/hIFLNcQY3RWw1pqa3LxqpLTZplk6jVrGoM2jaaqLGV1QQfkYiPGmtNiBpERJS3F5Agj3u5556ZPfvXP/aeOXPOveCjUc5ant+6c2dmz8zeM7/v77f3b+/93Qea0pSmNKUpTWlKUz6OIrv7BRApnvWNm+IrvjKNoe0tH0mRQRBgrWXcYYeFAKjGw4YNY9u2bR9J+VlZImHxIylx19K64DfLOWvGVI4cO4IDRw1HRLItCAJEqnaSpqfHAKqa7dPj9Nxam6Wn10SElStXArSm9954443s2LGjppyWlhbCMCQMHU6FQgERoVAoDPohSZKgqiRJAkAcx8RxTKVSqXmvjo4Ourq6AFobAYAiqixZ0c2Ti15nwqc+wZnTxjO0o40gCFDVDIS8wvOgpGn547zyrbU1QIgI1lpXtpfXX3+djRs3ZnkMBnS9Ieyq/Pp9/vrIkSOzb28EACgVA8qVmNgk/HbpWpat2cS5Jx7JhEM+MeBDYeDHp2n5j8ynpcrMK8ID4MovlSiXy8RxnD33fpSfynsFoVwuUyqViOKYhgBAUXp2VDDGIiJs6Slz+0PPM/HQ0Zx30kSGdrTWKGVXUn/PYMCkXpGX7du3E8dxjcIHU/5g5/l8BzvPK19V2b59e3ZPQwCAKrFJiE2CBIJIASThf19dy9JVmzj/1KP4zLjRwM4/Pr02mBfUgzcYAGl9/UGtP593/XE9EKmnQcMAYEmsxSQWrCCCAwLY0lvm5vufYdLhB3DhqUcxrLMNGKiU/IfXK3swSRvKVIwxGGOy59+v9e+svMG8IC0HGgYAJYqN94AAETIg8Hp69uVVLH5jA5eccTSTDz/gPWed94C88vIAPAOMjSIkiiiIUBAhAAJxRpBuaX7kzrNPyD5Fs/N0s6pYIFElUUWLRZYBR9MoAGDp769QrkQEEiBBAAgiQfalitDf38/1cx/jmPEHcPHpRzO0I4siBw0/88fWWpIkyRSfByAB+isVKJcpBMEAAFAloM7r6r4gb/fqFY7IQACshUIhtasGAUCVyCQYo6hYVwWJIKKAuD+p2uEzL63i5eUbuOxLn+WY8QcOmmUawgJZf8Jam+3rAaBSITAGUa1aft4DUjC8vFs4EKhmoIgH0Pq8baXSWACot4w4SUACQDMvAAZUHwCVLVvp/taNrOl5y+eRy8//t1ZBwALWeitEkfZ2Vi9alN1vATEGoohAxFl8ECCqVeW/TwBQzV4qECGx1ilfFQlD0hCgIQBALbGxRHEC4pQvNrV+/+E5ACb2rOf8jS8xJInymeSOJVcX+/RUIQqEMUnu9gSQOEYil59ItdwP6gF5i1CcF+ABpVRqLA8AJYoTYmM9AE75WbXjlT8kiZi18SWO2b7OPzVYTvmTaoNIPhaPYmzuzgQQ3wjXW3x2nlZn/pmdAaDpNe9J6b2imnkUUdRgAKiSWNcOiAQZACCoV8bknm6+tn4RQ5NK9SPzWQySJ74ezrbUC1payAehyU3ABINo5BRXcO0OAUigCPmwlJr9gBeohkOAotbXgQokDgQVQ/ISMLthALA+DLVO+UngqgARhpgKX9v4IlO3ral9hCoIWpee7msjI5eqqkgc1QKQABoh6nvC6fAF+AaiqvxM75ovrbZ60iri1RBWPZCqiEQY/wINAYCq64SZJEFsAD76mdzzFl9f/1uGmUqmhMGsP8unNlO/05prioJJMDlwTAKoQa1BhVwADwTpXQLegeq9IM2qtg/mT1Lrz21qDWlfrCEAwCbZUABBgWGJ4ZINi/j8tjUEPpauVkq7lvTe7FgVRRH10RCKjWPiHFxRDFEUU9CYEl6xmjaeUs1MUi+obQ+qIlXr98+ob/gDn5xYiK0h8qMRDQFAMVDExhREmbSjm0s3vsRwGxGUwtpIJOfqaeMIqZJdmq95Mx0EOMXnPSICKrmB4P44ZEe/UAqKSEEoIASBByJVem6D9LjqApln5C0dF1Wr9yaL6wtUYuirhECDjIa2hcKeUuHC/pVMSbbQsc9wQnFDEgGA5sJAyXtCtRXIDE9z9X/a/qXKQDGxoT+qUGmpzr5VTDs95TbaWoZQlCJtpYCgKEgAQWb1klWNGRApGKnC/QuIr8cCBatQ8HuJIUkMOyoRfVEZ6GsMAMZJmWNWP0sQFlhcKtEiUBCturhrxdzxYLHnYFIXtLsgRClj6UkMYXt7drkQdrBoaUBLS5HOjpC21gKFQAZGOv8PUYXEKuWysL3HUAw7gM0NMSfcCYwCOj/iknuBDf54t5XfCAAUcXOzH7U3GqDfH+/u8pvSlKY0pSlNaUpTmtKUpjSlKU35mIggUnx8xox49LXXYjqG8C8PLuFH814jsbse9Xr1wbNZNnkSSU8Phy9dyrRL5rNhc98unzl7+sH8ZfASffN/zt5dXUy+/PIQ4Nn/fiYuFgqYOrbahyVpWVM/9/kQVfPuT3yI7wK0BiP2Ydhrr9Fx1FHcdOWp/H3XCTzxQjflSkJYLBAWHR++UBDPjw849NCDGXX33WgcM3zsWO64vpWeHZHnxzvwUr5nSxgw7ZAO2l7+H4L2KQy5+ipWvNUNXO6YVaqMHXsoVh1Zww09CxIEBEHgqCJU2QrBTqakbDoPnDu31qLWujmCjCYSsGTJK+DGgHo/VA2/ixSB4kXdR/DoIUfQc9ddlEaPZs+ZMznruD/hkV+v5bHn1qEpOSrHUDhn+sH0LFyILZcZPnMmTy/q5p3tFT8Rrn583PL5I0cxQ9+g944n6DjzTNomTmTjtgpf/uav0vKx1tJf6ScxScZICLzyHU/Ug1DH2ayZkMlPQapiVZ3ilQHrAwrFwoD1AbtLigDdm3r4wZObmTzly3zunRdZf801jDj/fE7/84lMGLs3d85bxsbNZdJpDQn8hxuDeqavSZQ4TidAYe9hLXx1ynCGPPYAZuhQ9r3qKrS9g4efXcXzizexbPWW7CUyS1XrmdH4MXx19MRsQgXIpWWizjhUbXViRAHxaenkiTcKax1BqhHEWYAqW3srzHt2Hc/vfRBdM8fzzv0/oe+55zjowgv5x8sm8dDClfz8mdWO2WW9KuK4FoDEVSHHH7Ufp8tydtzzE4adey5tkybx1tu9/PCnv+PNdVtpby3WEpdSC8UrXcXPguWmHjNQoH5WRvOpae2EZKyEFJB0qjKtlhpBvAsqsXHstLUbe7nm4TJnTPsqx2/+DWvnzGFEVxfnnDiZiYeN4LYHXqV7k1tLVQOAsew5tJXLjt2TYb+4BzNsGPt9+9vQ2clDC1cwb+FKKlGMehZcXolOIVVqgaSsRJFMtUIKitTrPzuXOkacA8RNJTrGdVo1DlwfsLvEe0BKC3GzxyaBB55ay7MjPsmcCybwzr/PZcfTTzOmq4ub/+YY7v/lG46T76sgEeGzE0ZySvkVen50F3tccAEdU6ey7u1ebr7tBZav2errY6d4k1jQqgIcUTad13XMgnSeNWf85DUv2Qx4SkKrUhEG4+ik88Lqr9evD9hd4j3AEkWOmylS8OQoy6r1vcz5zx3MPPEyZrz9HGu6uthn9mzOO3mK++goQj2f8vhf/xtxayv733wzMnQo9z/+Jvc99gZRnGSKV6uoJhQDAfIAWK/AlJVMbZXzHqrrem5OqvA0n4xU5auiJGkgD3jyjlMYM2YM4JZiFgqFXBTi7CkIDmborK8AbpGZiFAul7HlMuVymb2uvhqAGND+fk6bui+nTd13p/z8FeeOZMrU7wBgbUJ/fz8KFIIChULgI58gWykjQVDDP0sDgVQ0129RfATk0626Oj9JLIlNEF9mI0gR4PVTToEjjoBymUCVgqeAZAsUyNFC0vPc8fJp0wbQA/OMNOt5O4kqVgTa2lj6/PPZ/UmSkNm/5PoB2Z6qZwxS/oAEHwFlZCqtuoAg3gMaCADT14cYg40iH7o5inh+gUM9R+NdawUfc6cNoVqb5R2EIdLeDn5l+pbZ19Mz8c/Qvv7q6pQgqHKCgioQqdQDUGMAacRTsz7AZqtUpL2VLYt+93709KFJEWDMo49y8PjxpKvA66ugwdZYiQhLJ04k6Oxk7NNPD1gilN/qqyBVpW/xKzD9BMCvUIkNxDHqel6o2FzsL7kQNHuDmjOtNwmtprpD9f0EhbhIY9i/B+Ck2Q8z7rA19JYNSOAo4r4lbCkVufjMcVx0+jjs1i2sv+46kr4+Dpo7F61UIAwzYHqeeILuG26g86STmbffcdw5f4UP91IwXPTT2VZk6auLs5ewqi6cjQ0gjo6WKd33gN9nv0mzyEgzMLA+DorjhumIee6vC0EjY4njdLWKZeyBe/CzG0/g4jMPo/eXj/HmGWcQHn4E/3HsbKf0OEaNQUS45aeLaTv2OA5+6CHsW+s48d6ruOe8Uew3ooPI5xnHlshUw91UElzUpbFBjcHGLry1kfHMWVeOGgPvsmX3+eds5PPyeWtsILGuzAYQB4C6EDSOXYdMRLjsnPHc950T2L8lZnVXF3+YO5ctV36Ps1/Yh4cWrnKPVSrgw9B/vn8JfzH7UZZsVva/9VZGzplD8dq/Zu7IF7n4lDGYJAXBAZ3vB1ioeoCviogNEjuFaWzQKPbpbtO6reZaFGfPSV2e6XHSUB6Q4+ePGzOced+bzqVfPIye+b/gjZNPpvSnE7hz2pV88bblrOju8RZM1fKA2FiWrd7KaXMe559+/DJtx5/IIY8/jlm3lpN+fAX3z9qX/Ud1+nIsaLUWNqqoSbCJwRqDmsR5gjFu9US2T/w1t2F2kpZ7xmZekaBJgiYGNRbTIB7ge8KGQCyXfulQvn7Opyn29rD6ooswmzbxztW3MuvB9azZuMw/IsTG/1pJFEEUISJERhGUGOX79/6e/3pqFbf+3TF8+vbb2fbII6y9YjZ3n34G902Yzr0LVkIuDo/VEnnrDXEjn5qOdOZpyNkbDC6a26e93iwS8+1Booo1MXEjecCnRnfwwysnccEpY9j68HyWf+ELlD4zibtnXMPM299k/eZyNi/gNtdIBy0tSEsLIkKpGNTcs3pDL2f+7QK+f+9i2qbP4JAnnyR5ZzMn/ewabj97T0aPCLOXiIsBZbVEQUASFiAsEoShWx8QFgnCIuK3INuHBKWS28Jd3FcK/fUQwhANi8RqqRR2Py0WvAd845yxDEm2s+K622gvl6nc8AP+asEW1m9ez+iRjjQsfmBMEEbu1e6UPno0QWcnIsL+IztJh8DcCKSzsHlPrWLR0j/wDxdN4JO33MLbCxaw5bvf5aJRo/imf4m4pUS5pUhrRytSaiUsFClKUPtTAVI7Qpq+E7myXAfQPZBaftopVCDGYuKY/v4yUUvVAHanuI7YsmX86s47GTrpaF7eawK//9enEAko7cTZk60lFiwo0u1D0NULFhCWlw16L8CG1TD7mpeZcsQ+TBq3Bz2zZlGYPz+7Hra3sSTaQauU6EgsrRLW9ML/GJL2yMsmoifuo9je9kfM/YOLfNz5+aju1ilJ+djz83fzpHxTmtKUpjTl4yv/B7ZjzVDk6+gNAAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/components/app.js":
/*!*******************************!*\
  !*** ./src/components/app.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($, duration) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return app; });
/* harmony import */ var _main_modals__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main/modals */ "./src/components/main/modals.js");
/* harmony import */ var _main_loginPanel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./main/@loginPanel */ "./src/components/main/@loginPanel.js");
/* harmony import */ var _main_loadingPanel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main/@loadingPanel */ "./src/components/main/@loadingPanel.js");
/* harmony import */ var _main_notifier__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main/@notifier */ "./src/components/main/@notifier.js");
/* harmony import */ var _utils_timeFormatter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/timeFormatter */ "./src/utils/timeFormatter.js");
/* harmony import */ var _websocket_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./websocket.js */ "./src/components/websocket.js");
/* harmony import */ var _config_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config.js */ "./src/config.js");
/* harmony import */ var _components_devices_devicesTab__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/devices/@devicesTab */ "./src/components/devices/@devicesTab.js");









class app {

	constructor() {

		if (window.app) {
			return window.app;
		}

		window.app = this;

		this.globalTimestamp;
		this.notifier = new _main_notifier__WEBPACK_IMPORTED_MODULE_3__["default"]();
		this.websocketLib = new _websocket_js__WEBPACK_IMPORTED_MODULE_5__["default"]();
		this.websocketClient;
		this.websocketConnections = 0;
		this.devices = [];
		this.groups = [];

		Object(_main_modals__WEBPACK_IMPORTED_MODULE_0__["default"])();

		if (!localStorage.language) {
			localStorage.language = "pl-PL";
		}

		$("body").tooltip({
			selector: "[data-toggle=tooltip]"
		});

		if (sessionStorage.apiToken && sessionStorage.loginFinished) {
			this.loadingPanel = new _main_loadingPanel__WEBPACK_IMPORTED_MODULE_2__["default"]();
			document.body.className = "hold-transition login-page";
			document.body.appendChild(this.loadingPanel.render());
		} else {
			this.mainLoginPanel = new _main_loginPanel__WEBPACK_IMPORTED_MODULE_1__["default"]();
			document.body.className = "hold-transition login-page";
			document.body.appendChild(this.mainLoginPanel.render());
		}

		setInterval(() => {
			if (!this.globalTimestamp) return;
			this.globalTimestamp += duration({
				seconds: 1
			});
			$("strong#syncTimeSpan").text(Object(_utils_timeFormatter__WEBPACK_IMPORTED_MODULE_4__["default"])(this.globalTimestamp));
		}, duration({
			seconds: 1
		}));

	}

	websocketInit() {
		console.log(1);
		this.websocketClient = new WebSocket(`${_config_js__WEBPACK_IMPORTED_MODULE_6__["default"].wssEndpoint}/${sessionStorage.apiToken}`);
		this.websocketClient.onopen = (event) => this.websocketLib.onOpen(event);
		this.websocketClient.onclose = (event) => this.websocketLib.onClose(event);
		this.websocketClient.onmessage = (message) => this.websocketLib.onMessage(message);
		if (this.websocketConnections > 0) this.websocketConnections++;

	}

	sendData(command, value = undefined) {

		if (this.websocketClient.readyState === this.websocketClient.OPEN) {
			try {
				this.websocketClient.send(JSON.stringify({
					command,
					value
				}));
			} catch (error) {
				console.error("websocketClient.send() failure", error, command, value);
			}
		}

	}

}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! @sindresorhus/to-milliseconds */ "./node_modules/@sindresorhus/to-milliseconds/index.js")))

/***/ }),

/***/ "./src/components/devices/@devicesTab.js":
/*!***********************************************!*\
  !*** ./src/components/devices/@devicesTab.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");
/* harmony import */ var _main_box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../main/box */ "./src/components/main/box.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app */ "./src/components/app.js");
/* harmony import */ var _main_tab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../main/tab */ "./src/components/main/tab.js");
/* harmony import */ var _editableTextInput__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./editableTextInput */ "./src/components/devices/editableTextInput.js");








/* harmony default export */ __webpack_exports__["default"] = (class {

	constructor() {

		this.app = new _app__WEBPACK_IMPORTED_MODULE_2__["default"]();
		this.devicesEditor = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div#devicesEditor");
		this.groupsEditor = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div#groupsEditor");

		let newGroupButton = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.btn btn-primary btn-sm mr", {
				type: "button"
			},
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-file i.mr"),
			i18n `Nowa grupa`
		);

		let newDeviceButton = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.btn btn-primary btn-sm mr", {
				type: "button"
			},
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-file i.mr"),
			i18n `Nowe urządzenie`
		);

		newDeviceButton.onclick = () => {
			let continueButton = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.btn btn-success mr", {
					type: "button"
				},
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-check i.mr"),
				i18n `Utwórz urządzenie`
			);

			let inputName = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("input#name.form-control", {
				type: "text",
				placeholder: `Nazwa urządzenia`,
				maxlength: 16
			});

			let inputDescription = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("input#desc.form-control", {
				type: "text",
				placeholder: `Opis urządzenia`,
				maxlength: 256
			});

			let inputHUID = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("input#huid.form-control", {
				type: "text",
				placeholder: `Identyfikator sprzętowy`,
				maxlength: 32
			});

			let groupsOptions = [];
			this.app.groups.forEach(group => {
				groupsOptions.push(
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("option", {
						value: group.group_id
					}, group.group_name)
				);
			})



			let inputGroup = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("select#ngroupame.form-control", groupsOptions);

			continueButton.onclick = () => {
				this.app.sendData("updateDevice", {
					device_id: null,
					device_name: inputName.value,
					device_description: inputDescription.value,
					device_huid: inputHUID.value,
					group_id: inputGroup.value
				});
				$("#SmallModal").modal("hide");
			};

			let cancelButton = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.btn btn-danger", {
					type: "button"
				},
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-times i.mr"),
				i18n `Anuluj`);

			cancelButton.onclick = () => {
				$("#SmallModal").modal("hide");
			};

			$("#SmallModalTitle").text(i18n `Tworzenie nowego urządzenia`);
			$("#SmallModalFooter").hide();
			$("#SmallModalBody").empty();
			$("#SmallModalBody").append(
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.form-group",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("label", {
						for: "name"
					}, i18n `Nazwa urządzenia`),
					inputName
				)
			);
			$("#SmallModalBody").append(
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.form-group",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("label", {
						for: "desc"
					}, i18n `Opis urządzenia`),
					inputDescription
				)
			);
			$("#SmallModalBody").append(
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.form-group",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("label", {
						for: "huid"
					}, i18n `Identyfikator sprzętowy`),
					inputHUID
				)
			);
			$("#SmallModalBody").append(
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.form-group",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("label", {
						for: "group"
					}, i18n `Grupa`),
					inputGroup
				)
			);
			$("#SmallModalBody").append(Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.askButtons", continueButton, cancelButton));
			$("#SmallModal").modal("show");
		};





		newGroupButton.onclick = () => {
			let continueButton = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.btn btn-success mr", {
					type: "button"
				},
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-check i.mr"),
				i18n `Utwórz grupę`
			);

			let inputName = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("input#name.form-control", {
				type: "text",
				placeholder: `Nazwa grupy`,
				maxlength: 32
			});

			let inputDescription = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("input#desc.form-control", {
				type: "text",
				placeholder: `Opis grupy`,
				maxlength: 256
			});

			continueButton.onclick = () => {
				this.app.sendData("updateGroup", {
					group_id: null,
					group_name: inputName.value,
					group_description: inputDescription.value
				});
				$("#SmallModal").modal("hide");
			};

			let cancelButton = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.btn btn-danger", {
					type: "button"
				},
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-times i.mr"),
				i18n `Anuluj`);

			cancelButton.onclick = () => {
				$("#SmallModal").modal("hide");
			};

			$("#SmallModalTitle").text(i18n `Tworzenie nowej grupy urządzeń`);
			$("#SmallModalFooter").hide();
			$("#SmallModalBody").empty();
			$("#SmallModalBody").append(
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.form-group",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("label", {
						for: "name"
					}, i18n `Nazwa grupy`),
					inputName
				)
			);
			$("#SmallModalBody").append(
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.form-group",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("label", {
						for: "desc"
					}, i18n `Opis grupy`),
					inputDescription
				)
			);
			$("#SmallModalBody").append(Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.askButtons", continueButton, cancelButton));
			$("#SmallModal").modal("show");
		};

		return [
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.row",
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.col-xs-12",
					new _main_box__WEBPACK_IMPORTED_MODULE_1__["default"]("primary", i18n `Urządzenia`, [newDeviceButton], this.devicesEditor)
				)
			),
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.row",
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.col-xs-12",
					new _main_box__WEBPACK_IMPORTED_MODULE_1__["default"]("primary", i18n `Grupy`, [newGroupButton], this.groupsEditor)
				)
			)
		];

	}

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/components/devices/editableTextInput.js":
/*!*****************************************************!*\
  !*** ./src/components/devices/editableTextInput.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");
/* harmony import */ var dot_prop__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! dot-prop */ "./node_modules/dot-prop/index.js");
/* harmony import */ var dot_prop__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(dot_prop__WEBPACK_IMPORTED_MODULE_1__);



/* harmony default export */ __webpack_exports__["default"] = (class {

	constructor(pattern, patternHelp, placeholder, affectedObject, affectedProperty, defaultState = "") {

		this.affectedObject = affectedObject;
		this.affectedProperty = affectedProperty;
		this.patternHelp = patternHelp;
		this.pattern = pattern;

		if (dot_prop__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.affectedObject, this.affectedProperty) === undefined) {
			dot_prop__WEBPACK_IMPORTED_MODULE_1___default.a.set(this.affectedObject, this.affectedProperty, defaultState);
		}

		this.domElement = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("input.form-control", {
			placeholder,
			title: "",
			type: "text",
			value: dot_prop__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.affectedObject, this.affectedProperty)
		});

		this.domElement.onkeyup = (event) => {
			let isValid = true;

			if (this.pattern && this.pattern instanceof RegExp) {
				if (!this.pattern.test(event.target.value)) {
					isValid = false;
				}
			}

			if (isValid) {
				dot_prop__WEBPACK_IMPORTED_MODULE_1___default.a.set(this.affectedObject, this.affectedProperty, event.target.value);
				$(this.domElement).tooltip("destroy");
			} else {
				$(this.domElement).tooltip({
					trigger: "manual",
					placement: "bottom",
					title: this.patternHelp
				});
				$(this.domElement).tooltip("show");
			}
		};

		return this.domElement;

	}

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/components/main/@loadingPanel.js":
/*!**********************************************!*\
  !*** ./src/components/main/@loadingPanel.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app */ "./src/components/app.js");



/* harmony default export */ __webpack_exports__["default"] = (class {
	constructor() {

		this.app = new _app__WEBPACK_IMPORTED_MODULE_1__["default"]();

		if (sessionStorage.apiToken) {
			this.app.websocketInit();
		}

	}

	render() {

		return Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.lockscreen-wrapper",
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.lockscreen-logo",
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("strong", i18n `gnIOT 2.0`),
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("p",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-spinner fa-spin fa-fw fa-4x", {
						style: "margin-top: 15px;"
					})
				)
			)
		);

	}

});

/***/ }),

/***/ "./src/components/main/@loginPanel.js":
/*!********************************************!*\
  !*** ./src/components/main/@loginPanel.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($, jQuery) {/* harmony import */ var js_sha1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-sha1 */ "./node_modules/js-sha1/src/sha1.js");
/* harmony import */ var js_sha1__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_sha1__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../config */ "./src/config.js");
/* harmony import */ var _utils_jsonDownloader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils/jsonDownloader */ "./src/utils/jsonDownloader.js");
/* harmony import */ var _utils_rot13__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/rot13 */ "./src/utils/rot13.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app */ "./src/components/app.js");
/* harmony import */ var _loadingPanel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./@loadingPanel */ "./src/components/main/@loadingPanel.js");








/* harmony default export */ __webpack_exports__["default"] = (class {

	constructor() {}

	changeLanguage(code) {

		localStorage.language = code;
		location.reload();

	}

	logIn(process) {

		if (!process) return;

		let username = $("#loginFormUsername").val().trim().toLowerCase();
		let password = Object(_utils_rot13__WEBPACK_IMPORTED_MODULE_4__["default"])(js_sha1__WEBPACK_IMPORTED_MODULE_0___default()(username + $("#loginFormPassword").val()));
		let region = $("#loginFormRegion").find(":selected").val();

		if (username.length == 0 || $("#loginFormPassword").val().length == 0) {
			$("#errorModalText").text(i18n `Podaj nazwę użytkownika i hasło!`);
			$("#errorModal").modal();
			return;
		}

		sessionStorage.clear();

		$("#mainLoginButton").attr("disabled", "disabled");
		$("#mainLoginButton i").show(100, () => {
			Object(_utils_jsonDownloader__WEBPACK_IMPORTED_MODULE_3__["default"])(false, _config__WEBPACK_IMPORTED_MODULE_2__["default"].apiEndpoint + jQuery.param({
				method: "auth",
				username,
				password
			})).then((response) => {
				if (response.success) {
					sessionStorage.apiToken = response.message;
					sessionStorage.loginTime = new Date();
					sessionStorage.regionName = region;
					sessionStorage.localUsername = username;

					$("div.login-box").animate({
							height: "toggle"
						},
						1000
					).promise().done(() => {
						var mainloadingPanel = new _loadingPanel__WEBPACK_IMPORTED_MODULE_6__["default"]();
						$("div.login-box").remove();
						document.body.className = "hold-transition lockscreen";
						document.body.appendChild(mainloadingPanel.render());
						sessionStorage.loginFinished = true;
					});

				} else {
					switch (response.respCode) {
						case 14:
							$("#errorModalText").text(i18n `Podany login i/lub hasło nie są poprawne. Zwróć uwagę na wielkość liter w haśle!`);
							break;
						default:
							$("#errorModalText").text(i18n `Logowanie nieudane. Wystąpił nieznany błąd.`);
							break;
					}
					$("#errorModal").modal();
					$("#mainLoginButton").removeAttr("disabled");
					$("#mainLoginButton i").hide();
				}
			});
		});

	}

	render() {

		let userNameInput = Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("input#loginFormUsername.form-control", {
			placeholder: i18n `Nazwa użytkownika`,
			required: true,
			autofocus: true
		});
		userNameInput.onkeyup = (event) => this.logIn(event.keyCode == 13);

		let passwordInput = Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("input#loginFormPassword.form-control", {
			placeholder: i18n `Hasło`,
			type: "password",
			required: true
		});
		passwordInput.onkeyup = (event) => this.logIn(event.keyCode == 13);

		let loginButton = Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("button.btn btn-primary btn-block#mainLoginButton", {
			type: "button",
		}, Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("i.fas fa-spinner fa-spin fa-fw", {
			style: "display: none"
		}), i18n `Zaloguj się`);
		loginButton.onclick = () => this.logIn(true);

		let flags = [];
		_config__WEBPACK_IMPORTED_MODULE_2__["default"].availableLangs.filter(lang => lang.code != localStorage.language).forEach(element => {
			let flag = Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])(`div.flag flag-${element.flag}`, {
				"data-toggle": "tooltip",
				"data-placement": "bottom",
				"title": element.name
			});
			flag.onclick = () => this.changeLanguage(element.code);
			flags.push(flag);
		});

		return Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.login-box",
			Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.login-logo", i18n `gnIOT 2.0`),
			Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.login-box-body",
				Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.row",
					Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.col-xs-12 text-center#leftLoginPanel",
						Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("h3", i18n `Zaloguj się`),
						Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("form", {
								autocomplete: "on"
							},

							Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.form-group has-feedback",
								userNameInput,
								Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("span.glyphicon glyphicon-user form-control-feedback")
							),

							Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.form-group has-feedback",
								passwordInput,
								Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("span.glyphicon glyphicon-lock form-control-feedback")
							),

							Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.form-group",
								loginButton
							)
						)
					),
					Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.col-md-6 hidden-md hidden-lg",
						Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("hr")
					)
				),
				Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.row",
					Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("div.col-xs-12 text-center",
						[
							Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("hr"),
							Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("p", i18n `Zmień język:`),
							flags,
							Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("hr"),
							Object(redom__WEBPACK_IMPORTED_MODULE_1__["html"])("span", "Made with ❤️ by gnIOT 2.0 Developers Team")
						]
					)
				)
			)
		);

	}

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/components/main/@mainPanel.js":
/*!*******************************************!*\
  !*** ./src/components/main/@mainPanel.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($, jQuery) {/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");
/* harmony import */ var jquery_contextmenu___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery-contextmenu/ */ "./node_modules/jquery-contextmenu/dist/jquery.contextMenu.js");
/* harmony import */ var jquery_contextmenu___WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_contextmenu___WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tab__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab */ "./src/components/main/tab.js");
/* harmony import */ var _devices_devicesTab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../devices/@devicesTab */ "./src/components/devices/@devicesTab.js");
/* harmony import */ var _utils_jsonDownloader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/jsonDownloader */ "./src/utils/jsonDownloader.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../config */ "./src/config.js");








/* harmony default export */ __webpack_exports__["default"] = (class {

	constructor() {


	}

	render() {

		let logOutButton = Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a.hidden-sm", {
				style: "cursor: pointer; margin: 3px;"
			},
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.badge bg-purple", {
					"data-placement": "bottom",
					"data-toggle": "tooltip",
					"title": i18n `Wyloguj`
				},
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-sign-out-alt mr"),
				i18n `Wyloguj`
			)
		);

		logOutButton.onclick = () => {
			sessionStorage.clear();
			location.reload();
		};

		this.devicesTab = new _devices_devicesTab__WEBPACK_IMPORTED_MODULE_3__["default"]();

		return Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.wrapper",
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("header.main-header",
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("nav.navbar navbar-static-top",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.container",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.navbar-header",
							Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("strong.navbar-brand hidden-md hidden-sm hidden-xs", i18n `gnIOT 2.0`),
							Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.navbar-toggle collapsed", {
									type: "button",
									"data-toggle": "collapse",
									"data-target": "#navbar-collapse"
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-bars")
							)
						),
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.collapse navbar-collapse pull-left#navbar-collapse",
							Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("ul.nav navbar-nav", {
									role: "tablist"
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("li.active",
									Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a.bg-light-blue", {
											href: "#gniotTabDevices",
											role: "tab",
											"data-type": "mainMenu",
											"data-toggle": "tab",
											"data-role": "devices",
											style: "display: none"
										},
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-traffic-light mr"),
										i18n `Urządzenia`
									)
								),
								Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("li",
									Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a", {
											style: "background-color: #222d32 !important; cursor: default;",
											href: "javascript:;"
										},
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a.hidden-sm", {
												style: "cursor: default; margin: 3px;"
											},
											Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.badge bg-red#websocketStatusIndicatorBackground", {
													"data-placement": "bottom",
													"data-toggle": "tooltip",
													"title": i18n `Status połączenia`
												},
												Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-plug mr"),
												Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-times#websocketStatusIndicatorIcon")
											)
										),
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a.hidden-sm", {
												style: "cursor: default; margin: 3px;"
											},
											Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.badge bg-yellow", {
													"data-placement": "bottom",
													"data-toggle": "tooltip",
													"title": i18n `Nazwa użytkownika`
												},
												Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-user mr"),
												Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span#dispatchersOnlineCounter", sessionStorage.localUsername)
											)
										),
										logOutButton
									)
								)
							)
						),
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.navbar-custom-menu#moveCustomMenuToShowBars", {
								style: "position: absolute;"
							},
							Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("ul.nav navbar-nav navbar-nocolors",
								Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("li.dropdown notifications-menu hidden-sm",
									Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a.dropdown-toggle", {
											"data-toggle": "dropdown",
											style: "cursor: pointer"
										},
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-bell"),
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.label label-warning", 10)
									),
									Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("ul.dropdown-menu",
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("li.header",
											i18n `You have 10 notifications`
										),
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("li",
											Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("ul.menu",
												Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("li",
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a",
														Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-users text-aqua"),
														"5 new members joined today"
													)
												)
											)
										),
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("li.footer",
											Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a", "View all")
										)
									)
								),
								Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("li",
									Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("a", {
											style: "font-size:19px;cursor:default"
										},
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("i.fas fa-clock mr"),
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("strong#syncTimeSpan", "00:00:00")
									)
								)
							)
						)
					)
				)
			),
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.content-wrapper swdrMainWindow",
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("section.content",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.tab-content", {
							"data-type": "mainMenu"
						},
						new _tab__WEBPACK_IMPORTED_MODULE_2__["default"]("gniotTabDevices", true, this.devicesTab),
					)
				)
			)
		);

	}

	enableTabs(value) {
		[...Number(value || 0).toString(2)].map(n => Boolean(parseInt(n))).forEach((element, index) => {
			$(`a[data-type='mainMenu']:eq(${index})`).css("display", element ? "block" : "none");
		});
	}

	renderSceneriesTable(value) {
		$("tbody#sceneriesLive").empty();

		let uniqueDispatchers = [...value].map(scenery => scenery.dispatcherData.id_member).filter((v, i, a) => a.indexOf(v) === i);
		$("span#dispatchersOnlineCounter").text(uniqueDispatchers.length);
		$.contextMenu("destroy", "span[data-unique]");

		value.forEach(scenery => {
			$("tbody#sceneriesLive").append(
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("tr",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("td",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.cPointer", {
							"data-unique": scenery.uniquePairHash,
							"data-toggle": "tooltip",
							"data-placement": "bottom",
							"title": i18n `Menu podręczne`,
							style: "font-weight: bold;"
						}, scenery.sceneryName)),
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("td",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.cPointer", {
							"data-unique": scenery.uniquePairHash,
							"data-toggle": "tooltip",
							"data-placement": "bottom",
							"title": i18n `Menu podręczne`
						}, scenery.dispatcherData.member_name)),
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("td.trainsOnScenery", {
						"data-hash": scenery.sceneryHash,
					}, `${scenery.current_users} / ${scenery.max_users}`),
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("td", "status"),
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("td",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.label label-primary", {
							"data-toggle": "tooltip",
							"data-placement": "bottom",
							"title": i18n `Poziom doświadczenia`
						}, (scenery.dispatcherData.level < 2 ? "L" : scenery.dispatcherData.level))
					)
				)
			);

			new jquery_contextmenu___WEBPACK_IMPORTED_MODULE_1__["ContextMenu"]().create({
				trigger: "left",
				selector: `span[data-unique='${scenery.uniquePairHash}']`,
				items: {
					privateMessage: {
						icon: "fa-envelope",
						name: i18n `Wyślij prywatną wiadomość`,
						callback: function () {
							privateMessageDialog(scenery.dispatcherData.member_name, scenery.dispatcherData.id_member);
						}
					},
					dispatcherProfile: {
						icon: "fa-user",
						name: i18n `Wyświetl profil dyżurnego`,
						callback: function () {
							Object(_utils_jsonDownloader__WEBPACK_IMPORTED_MODULE_4__["default"])(true, _config__WEBPACK_IMPORTED_MODULE_5__["default"].apiEndpoint + jQuery.param({
								method: "getUsersInfoById",
								id: scenery.dispatcherData.id_member,
								token: sessionStorage.apiToken
							})).then(value => {
								if (value[0]) {
									let profile = value[0];
									let cust_trains = 0;
									let groups = [
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.label", {
												style: "background-color: #000; display: block; font-size: 95%; margin: 5px;"
											},
											i18n `Użytkownik`
										)
									];

									try {
										cust_trains = profile.user_stats.find(s => s.variable == "cust_trains").value;
									} catch (error) {
										cust_trains = 0;
									}

									profile.user_groups.forEach(group => {
										if (group.online_color == "") group.online_color = "#000";
										groups.push(
											Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.label", {
													style: `background-color:${group.online_color || "#000"}; display: block; font-size: 95%; margin: 5px;`
												},
												group.group_name
											)
										);
									});

									$("#SmallModalTitle").text(i18n `Profil dyżurnego ${profile.member_name}`);
									$("#SmallModalFooter").show();
									$("#SmallModalBody").empty();
									$("#SmallModalBody").append(
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.row",
											Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.col-md-6",
												Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dl.dl-horizontal",
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dt", i18n `Nazwa użytkownika:`),
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dd", profile.member_name),
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dt", i18n `Data rejestracji:`),
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dd", new Date(profile.date_registered * 1000).toLocaleDateString())
												)
											),
											Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.col-md-6",
												Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dl.dl-horizontal",
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dt", i18n `Obsłużone pociągi:`),
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dd", cust_trains),
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dt", i18n `Poziom doświadczenia:`),
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dd", Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span.label label-primary", `${profile.levels.dispatcher < 2 ? "L" : profile.levels.dispatcher + " / 20"}`))
												))
										),
										Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.row",
											Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.col-md-12",
												Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dl.dl-horizontal",
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dt", i18n `Grupy:`),
													Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("dd", groups)
												)
											)
										)
									);
									$("#SmallModal").modal("show");
								}
							});
						}
					}
				}
			});
		});
	}

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/components/main/@notifier.js":
/*!******************************************!*\
  !*** ./src/components/main/@notifier.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (class {

	constructor() {

		this.supported = false;

		if (("Notification" in window)) {
			this.supported = true;
			if (Notification.permission !== "denied") {
				Notification.requestPermission();
			}
		}

	}

	notify(body, silent, callback) {

		if (!this.supported) return;

		if (Notification.permission === "granted") {
			let notification = new Notification("gnIOT 2.0", {
				body,
				silent
			});
			notification.onclick = () => {
				window.focus();
				callback();
			};
		}
	}

});

/***/ }),

/***/ "./src/components/main/box.js":
/*!************************************!*\
  !*** ./src/components/main/box.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");


/* harmony default export */ __webpack_exports__["default"] = (class {

	constructor(type, title, tools, body, footer) {

		return Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])(`div.box box-${type}`,
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.box-header with-border",
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("h3.box-title", title),
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.box-tools pull-right", tools)
			),
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.box-body", body),
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.box-footer", footer)
		);

	}

});

/***/ }),

/***/ "./src/components/main/modals.js":
/*!***************************************!*\
  !*** ./src/components/main/modals.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");


/* harmony default export */ __webpack_exports__["default"] = (function () {

	document.body.appendChild(
		Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal fade#errorModal",
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-dialog modal-danger",
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-content",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-header",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.close", {
								"data-dismiss": "modal",
								"data-toggle": "tooltip",
								"data-placement": "bottom",
								"title": i18n `Zamknij`
							},
							Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span", "x")
						),
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("h4.modal-title", i18n `Wystąpił błąd`)
					),
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-body",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("p#errorModalText")
					),
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-footer",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.btn btn-danger", {
							"data-dismiss": "modal",
							type: "button"
						}, i18n `Zamknij`)
					)
				)
			)
		)
	);

	$("#errorModal").on("show.bs.modal	", function () {
		$("div#errorModal div.modal-content").bounce({
			interval: 100,
			distance: 20,
			times: 4
		});
	});

	document.body.appendChild(
		Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal fade#SmallModal",
			Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-dialog",
				Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-content",
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-header",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.close", {
								"data-dismiss": "modal",
								"data-toggle": "tooltip",
								"data-placement": "bottom",
								"title": i18n `Zamknij`
							},
							Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("span", "x")
						),
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("h4.modal-title#SmallModalTitle")
					),
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-body#SmallModalBody"),
					Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("div.modal-footer#SmallModalFooter",
						Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])("button.btn btn-danger", {
							"data-dismiss": "modal",
							type: "button"
						}, i18n `Zamknij`)
					)
				)
			)
		)
	);

	$("#SmallModal").on("show.bs.modal	", function () {
		$("div#SmallModal div.modal-content").bounce({
			interval: 100,
			distance: 20,
			times: 4
		});
	});

	$("#SmallModal").on("shown.bs.modal	", function () {
		$("div#SmallModal input:first").focus();
	});

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/components/main/tab.js":
/*!************************************!*\
  !*** ./src/components/main/tab.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");


/* harmony default export */ __webpack_exports__["default"] = (class {

	constructor(id, isActive, childrens, channelid) {

		return Object(redom__WEBPACK_IMPORTED_MODULE_0__["html"])(`div.tab-pane fade ${isActive ? "in active" : ""}#${id}`, channelid != undefined ? {
			"data-channelid": channelid
		} : null, childrens);

	}

});

/***/ }),

/***/ "./src/components/websocket.js":
/*!*************************************!*\
  !*** ./src/components/websocket.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _main_mainPanel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main/@mainPanel */ "./src/components/main/@mainPanel.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app */ "./src/components/app.js");
/* harmony import */ var _utils_groupBy__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/groupBy */ "./src/utils/groupBy.js");
/* harmony import */ var redom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! redom */ "./node_modules/redom/dist/redom.es.js");
/* eslint-disable no-undef */





/* harmony default export */ __webpack_exports__["default"] = (class {

	constructor() {

		this.app = new _app__WEBPACK_IMPORTED_MODULE_1__["default"]();
		this.wasKicked = false;

	}

	onOpen() {
		this.app.websocketConnections++;
		if (this.app.websocketConnections == 1) {
			this.mainPanel = new _main_mainPanel__WEBPACK_IMPORTED_MODULE_0__["default"]();
			document.body.className = "hold-transition";
			document.body.appendChild(this.mainPanel.render());
			$("div.lockscreen-wrapper").remove();
		}

		$("#websocketStatusIndicatorIcon").removeClass("fa-times");
		$("#websocketStatusIndicatorIcon").addClass("fa-check");
		$("#websocketStatusIndicatorBackground").removeClass("bg-red");
		$("#websocketStatusIndicatorBackground").addClass("bg-green");

		console.log("Websocket connected!");

	}

	onClose() {

		$("#websocketStatusIndicatorIcon").removeClass("fa-check");
		$("#websocketStatusIndicatorIcon").addClass("fa-times");
		$("#websocketStatusIndicatorBackground").removeClass("bg-green");
		$("#websocketStatusIndicatorBackground").addClass("bg-red");

		console.warn("Websocket disconnected!");
		this.tryToReconnect();

	}

	onMessage(message) {

		let command;
		try {
			command = JSON.parse(message.data).command;
		} catch (error) {
			console.error("Uncaught error during incoming data parsing", message);
		}

		if (command) {
			let value = JSON.parse(message.data).value;
			switch (command) {
				case "app/logout":
					sessionStorage.clear();
					location.reload();
					break;
				case "ping":
					this.app.sendData("pong");
					break;
				case "app/timestamp":
					this.app.globalTimestamp = value;
					break;
				case "app/enableTabs":
					this.mainPanel.enableTabs(value);
					break;
				case "app/forceRefresh":
					setTimeout(() => {
							this.app.sendData("getDevices");
							this.app.sendData("getGroups");
						},
						500);
					break;
				case "getGroups":
					this.app.groups = value;
					let rows = [];

					$("div#groupsEditor").empty();
					this.app.groups.forEach(g => {

						let editButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-warning btn-sm mr", {
								type: "button"
							},
							Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-pencil-alt i.mr"),
							i18n `Edytuj`
						);
						let removeButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-danger btn-sm mr", {
								type: "button"
							},
							Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times i.mr"),
							i18n `Usuń`
						);

						removeButton.onclick = () => {
							let continueButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-success mr", {
									type: "button"
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-check i.mr"),
								i18n `Usuń grupę ${g.group_name.toUpperCase()}`
							);

							continueButton.onclick = () => {
								this.app.sendData("removeGroup", g.group_id);
								$("#SmallModal").modal("hide");
							};

							let cancelButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-danger", {
									type: "button"
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times i.mr"),
								i18n `Anuluj`);

							cancelButton.onclick = () => {
								$("#SmallModal").modal("hide");
							};

							$("#SmallModalTitle").text(i18n `Usuwanie grupy ${g.group_name.toUpperCase()}`);
							$("#SmallModalFooter").hide();
							$("#SmallModalBody").empty();
							if (this.app.devices.filter(d => d.group_id == g.group_id).length > 0) {
								$("#SmallModalBody").append(i18n `Nie można usunąć tej grupy, ponieważ zawiera ona przypisane urządzenia!`);
								continueButton.setAttribute("disabled", "disabled");
							}
							$("#SmallModalBody").append(Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.askButtons", continueButton, cancelButton));
							$("#SmallModal").modal("show");
						};

						editButton.onclick = () => {
							let continueButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-success mr", {
									type: "button"
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-check i.mr"),
								i18n `Zapisz grupę`
							);

							let inputName = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("input#name.form-control", {
								type: "text",
								placeholder: i18n `Nazwa grupy`,
								maxlength: 32,
								value: g.group_name
							});

							let inputDescription = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("input#desc.form-control", {
								type: "text",
								placeholder: i18n `Opis grupy`,
								maxlength: 256,
								value: g.group_description
							});

							continueButton.onclick = () => {
								this.app.sendData("updateGroup", {
									group_id: g.group_id,
									group_name: inputName.value,
									group_description: inputDescription.value
								});
								$("#SmallModal").modal("hide");
							};

							let cancelButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-danger", {
									type: "button"
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times i.mr"),
								i18n `Anuluj`);

							cancelButton.onclick = () => {
								$("#SmallModal").modal("hide");
							};

							$("#SmallModalTitle").text(i18n `Tworzenie nowej grupy urządzeń`);
							$("#SmallModalFooter").hide();
							$("#SmallModalBody").empty();
							$("#SmallModalBody").append(
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.form-group",
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("label", {
										for: "name"
									}, i18n `Nazwa grupy`),
									inputName
								)
							);
							$("#SmallModalBody").append(
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.form-group",
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("label", {
										for: "desc"
									}, i18n `Opis grupy`),
									inputDescription
								)
							);
							$("#SmallModalBody").append(Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.askButtons", continueButton, cancelButton));
							$("#SmallModal").modal("show");
						};


						rows.push(
							Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("tr",
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-5", g.group_name),
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-5", g.group_description),
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-1", editButton),
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-1", removeButton)
							)
						);
					});

					groupsEditor.append(Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("table.table table-striped table-center",
						Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("tbody", rows)
					));
					break;
				case "removeDevice":
				case "removeGroup":
				case "powerDevice":
				case "updateDevice":
				case "updateGroup":
					setTimeout(() => {
							this.app.sendData("getDevices");
							this.app.sendData("getGroups");
						},
						500);
					break;
				case "getDevices":
					this.app.devices = value;
					$("div#devicesEditor").empty();

					let result = [];
					Object(_utils_groupBy__WEBPACK_IMPORTED_MODULE_2__["default"])(this.app.devices, "group_id").forEach(d => {
						if (d.length < 1) return;

						let rows = [];
						d.forEach(r => {
							let statusIndicator;
							let pingIndicator;
							let deviceIcon;
							let deviceType = 0;

							switch (r.device_huid.toString()[0]) {
								case "1":
									deviceIcon = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-plug");
									deviceType = 1;

									if (r.device_status == 0) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Wyłączone`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times"));
									} else {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Włączone`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-check"));
									}

									break;
								case "2":
									deviceIcon = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-tint");
									deviceType = 2;

									if (r.device_status == 0) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Zamknięty`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times"));
									} else {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Otwarty`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-check"));
									}

									break;
								case "3":
									deviceIcon = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-bell");
									deviceType = 3;
									statusIndicator = "";

									if (r.device_status == 0) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Nieuzbrojony`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-lock-open"));
									} else if (r.device_status == 1) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-orange", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Uzbrojony`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-lock"));
									} else if (r.device_status == 2) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-red blink", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `NARUSZONO ALARM!!!`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-skull-crossbones"));
									}

									break;
								case "4":
									deviceIcon = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-toilet-paper");
									deviceType = 4;
									statusIndicator = "";

									if (r.device_status == 0) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Zatrzymano`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-stop-circle"));
									} else if (r.device_status == 1) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Zwijanie w górę`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-arrow-circle-up"));
									} else if (r.device_status == 2) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Zwinięto w górę`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-arrow-circle-up"));
									} else if (r.device_status == -1) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Rozwijanie w dół`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-arrow-circle-down"));
									} else if (r.device_status == -2) {
										statusIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Rozwinięto w dół`
										}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-arrow-circle-down"));
									}

									break;
								case "5":
									deviceIcon = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-cloud-sun-rain");
									deviceType = 5;
									if (r.device_status.split(",")[0] == "0") {
										statusIndicator = "Brak opadów, temp. " + r.device_status.split(",")[1] + "st., wigl." + r.device_status.split(",")[2] + "%";
									} else {
										statusIndicator = "Opady, temp. " + r.device_status.split(",")[1] + "st., wigl." + r.device_status.split(",")[2] + "%";
									}
									break;
								default:
									deviceIcon = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas question-circle");
									deviceType = 0;
									statusIndicator = "";

									break;
							}



							if (new Date(r.device_ping).getTime() < Date.now() - 30000) {
								pingIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-red", {
									"data-placement": "bottom",
									"data-toggle": "tooltip",
									"title": i18n `Brak komunikacji`
								}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times"));
							} else {
								pingIndicator = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("span.badge bg-green", {
									"data-placement": "bottom",
									"data-toggle": "tooltip",
									"title": i18n `Połączenie OK`
								}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-check"));
							}

							let removeButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-danger btn-sm mr", {
									type: "button"
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times i.mr"),
								i18n `Usuń`
							);

							let editButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-warning btn-sm mr", {
									type: "button"
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-pencil-alt i.mr"),
								i18n `Edytuj`
							);





							let switchOnButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-success btn-sm mr", {
									type: "button",
									disabled: r.device_status >= 1
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-toggle-on i.mr"),
								i18n `Włącz`
							);

							let switchOffButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-danger btn-sm mr", {
									type: "button",
									disabled: r.device_status == 0
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-power-off i.mr"),
								i18n `Wyłącz`
							);

							let goUpButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-success btn-sm mr", {
									type: "button",
									disabled: r.device_status == 1 || r.device_status == 2 || r.device_status == -1
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-arrow-circle-up i.mr"),
								i18n `W górę`
							);

							let goDownButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-success btn-sm mr", {
									type: "button",
									disabled: r.device_status == -1 || r.device_status == -2 || r.device_status == 1
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-arrow-circle-down i.mr"),
								i18n `W dół`
							);

							let stopButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-danger btn-sm mr", {
									type: "button",
									disabled: r.device_status == 0 || r.device_status == 2 || r.device_status == -2
								},
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-stop-circle i.mr"),
								i18n `STOP`
							);






							editButton.onclick = () => {
								let continueButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-success mr", {
										type: "button"
									},
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-check i.mr"),
									i18n `Edytuj urządzenie`
								);

								let inputName = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("input#name.form-control", {
									type: "text",
									placeholder: i18n `Nazwa urządzenia`,
									maxlength: 16,
									value: r.device_name
								});

								let inputDescription = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("input#desc.form-control", {
									type: "text",
									placeholder: i18n `Opis urządzenia`,
									maxlength: 256,
									value: r.device_description
								});

								let inputHUID = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("input#huid.form-control", {
									type: "text",
									placeholder: i18n `Identyfikator sprzętowy`,
									maxlength: 32,
									value: r.device_huid
								});

								let groupsOptions = [];
								this.app.groups.forEach(group => {
									groupsOptions.push(
										Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("option", {
											value: group.group_id,
											selected: group.group_id == r.group_id
										}, group.group_name)
									);
								});

								let inputGroup = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("select#ngroupame.form-control", groupsOptions);

								continueButton.onclick = () => {
									this.app.sendData("updateDevice", {
										device_id: r.device_id,
										device_name: inputName.value,
										device_description: inputDescription.value,
										device_huid: inputHUID.value,
										group_id: inputGroup.value
									});
									$("#SmallModal").modal("hide");
								};

								let cancelButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-danger", {
										type: "button"
									},
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times i.mr"),
									i18n `Anuluj`);

								cancelButton.onclick = () => {
									$("#SmallModal").modal("hide");
								};

								$("#SmallModalTitle").text(i18n `Tworzenie nowego urządzenia`);
								$("#SmallModalFooter").hide();
								$("#SmallModalBody").empty();
								$("#SmallModalBody").append(
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.form-group",
										Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("label", {
											for: "name"
										}, i18n `Nazwa urządzenia`),
										inputName
									)
								);
								$("#SmallModalBody").append(
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.form-group",
										Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("label", {
											for: "desc"
										}, i18n `Opis urządzenia`),
										inputDescription
									)
								);
								$("#SmallModalBody").append(
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.form-group",
										Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("label", {
											for: "huid"
										}, i18n `Identyfikator sprzętowy`),
										inputHUID
									)
								);
								$("#SmallModalBody").append(
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.form-group",
										Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("label", {
											for: "group"
										}, i18n `Grupa`),
										inputGroup
									)
								);
								$("#SmallModalBody").append(Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.askButtons", continueButton, cancelButton));
								$("#SmallModal").modal("show");
							};

							removeButton.onclick = () => {
								let continueButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-success mr", {
										type: "button"
									},
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-check i.mr"),
									i18n `Tak, usuń urządzenie ${r.device_name.toUpperCase()}`
								);

								continueButton.onclick = () => {
									this.app.sendData("removeDevice", r.device_id);
									$("#SmallModal").modal("hide");
								};

								let cancelButton = Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("button.btn btn-danger", {
										type: "button"
									},
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("i.fas fa-times i.mr"),
									i18n `Anuluj`);

								cancelButton.onclick = () => {
									$("#SmallModal").modal("hide");
								};

								$("#SmallModalTitle").text(i18n `Usuwanie urządzenia ${r.device_name.toUpperCase()}`);
								$("#SmallModalFooter").hide();
								$("#SmallModalBody").empty();
								$("#SmallModalBody").append(i18n `Usunięcie urządzenia spowoduje utratę możliwości sterowania oraz usunięcie wszystkich danych historycznych i wyzwalaczy powiązanych z danym urządzeniem! Czy chcesz kontynuować?`);
								$("#SmallModalBody").append(Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("div.askButtons", continueButton, cancelButton));
								$("#SmallModal").modal("show");
							};

							switchOnButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: 1
								});
							};

							switchOffButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: 0
								});
							};

							goUpButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: 1
								});
							};

							goDownButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: -1
								});
							};

							stopButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: 0
								});
							};

							let buttons;
							switch (deviceType) {
								case 1:
									buttons = [
										removeButton,
										editButton,
										switchOnButton,
										switchOffButton
									];
									break;
								case 2:
									buttons = [
										removeButton,
										editButton,
										switchOnButton,
										switchOffButton
									];
									break;
								case 3:
									buttons = [
										removeButton,
										editButton,
										switchOnButton,
										switchOffButton
									];
									break;
								case 4:
									buttons = [
										removeButton,
										editButton,
										goUpButton,
										stopButton,
										goDownButton
									];
									break;
								case 5:
									buttons = [];
									break;
								default:
									buttons = [];
									break;
							}


							rows.push(
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("tr",
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-3", [deviceIcon, "  ", r.device_name]),
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-4", r.device_description),
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-1", statusIndicator),
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-1", pingIndicator),
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("td.col-md-3", buttons)
								)
							);
						});

						result.push(Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("table.table table-striped table-center",
							Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("thead",
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("tr",
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("th", {
										colspan: 5
									}, Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("h4.strong", d[0].group_name), Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("small", d[0].group_description))
								),
								Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("tr",
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("th", i18n `Urządzenie`),
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("th", i18n `Opis`),
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("th", i18n `Aktualny stan`),
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("th", i18n `Komunikacja`),
									Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("th")
								)
							),
							Object(redom__WEBPACK_IMPORTED_MODULE_3__["html"])("tbody", rows)
						));
					});
					result.forEach(r => $("div#devicesEditor").append(r));
					break;
				default:
					console.warn("There is no action associated with the message", command);
					break;
			}
		}

	}

	tryToReconnect() {

		if (this.app.websocketClient.readyState != 1 && !this.wasKicked) {
			this.app.websocketClient.close();
			setTimeout(() => {
				console.log("Attempt to reconnect...");
				this.app.websocketInit();
			}, 3000);
		}

	}

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/config.js":
/*!***********************!*\
  !*** ./src/config.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
	mode: "development",

	availableLangs: [{
			name: "Polski",
			code: "pl-PL",
			flag: "pl"
		},
		{
			name: "English",
			code: "en-EN",
			flag: "gb"
		},
		// {
		// 	name: "Deutsch",
		// 	code: "de-DE",
		// 	flag: "de"
		// }
	],

	apiEndpoint: "http://" + location.host + ":8080/?",
	wssEndpoint: "ws://" + location.host + ":8090"
});

/***/ }),

/***/ "./src/i18n/config.js":
/*!****************************!*\
  !*** ./src/i18n/config.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var es2015_i18n_tag__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! es2015-i18n-tag */ "./node_modules/es2015-i18n-tag/dist/lib/index.js");
/* harmony import */ var es2015_i18n_tag__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(es2015_i18n_tag__WEBPACK_IMPORTED_MODULE_0__);


switch (localStorage.language) {
	case "en-EN":
		Object(es2015_i18n_tag__WEBPACK_IMPORTED_MODULE_0__["i18nConfig"])({
			locales: "en-EN",
			translations: __webpack_require__(/*! ./en-EN.js */ "./src/i18n/en-EN.js").default
		});
		break;

	case "de-DE":
		Object(es2015_i18n_tag__WEBPACK_IMPORTED_MODULE_0__["i18nConfig"])({
			locales: "de-DE",
			translations: __webpack_require__(/*! ./de-DE.js */ "./src/i18n/de-DE.js").default
		});
		break;

	default:
		Object(es2015_i18n_tag__WEBPACK_IMPORTED_MODULE_0__["i18nConfig"])({
			locales: "pl-PL",
			translations: {}
		});
		break;
}

/***/ }),

/***/ "./src/i18n/de-DE.js":
/*!***************************!*\
  !*** ./src/i18n/de-DE.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({

	"gnIOT 2.0": "gnIOT 2.0 Another Language...",

});

/***/ }),

/***/ "./src/i18n/en-EN.js":
/*!***************************!*\
  !*** ./src/i18n/en-EN.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({

	"gnIOT 2.0": "gnIOT 2.0 English Version",

	// components/main/@loginPanel.js
	"Podaj nazwę użytkownika i hasło!": "Please type your username and password!",
	"Podany login i/lub hasło nie są poprawne. Zwróć uwagę na wielkość liter w haśle!": "Invalid login or password. Please verify your username and password",
	"Dostęp zablokowany za zbyt wiele nieudanych logowań! Spróbuj ponownie później.": "Too many failed login attempts! Try again later.",
	"Logowanie nieudane. Wystąpił nieznany błąd.": "Login failed. An unknown error occured",
	"Nazwa użytkownika": "Username",
	"Hasło": "Password",
	"Zaloguj się": "Log In!",
	"Zmień język:": "Choose language:",
	"Wyloguj": "Log Out!",

	// components/main/modals.js
	"Wystąpił błąd": "An error occured",
	"Zamknij": "Close",

	// components/main/@mainPanel.js
	"Urządzenia": "Devices",
	"Urządzenie": "Device",
	"Nowa grupa": "New group",
	"Nowe urządzenie": "New device",
	"Opis": "Description",
	"Aktualny stan": "Current state",
	"Komunikacja": "Connection",
	"Usuń": "Remove",
	"Edytuj": "Edit",
	"Włącz": "Turn On!",
	"Wyłącz": "Turn Off!",
	"Włączone": "Switched On",
	"Wyłączone": "Switched Off",
	"Brak komunikacji": "Connection error",
	"Komunikacja OK": "Connected",
	"Anuluj": "Cancel"
});

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery) {/* harmony import */ var _assets_css_CDNs_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets/css/CDNs.css */ "./src/assets/css/CDNs.css");
/* harmony import */ var _assets_css_CDNs_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_css_CDNs_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/npm.js");
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bootstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var admin_lte__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! admin-lte */ "./node_modules/admin-lte/dist/js/adminlte.min.js");
/* harmony import */ var admin_lte__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(admin_lte__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _assets_css_style_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./assets/css/style.css */ "./src/assets/css/style.css");
/* harmony import */ var _assets_css_style_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_css_style_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _assets_css_flags_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./assets/css/flags.css */ "./src/assets/css/flags.css");
/* harmony import */ var _assets_css_flags_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_css_flags_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _i18n_config_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./i18n/config.js */ "./src/i18n/config.js");
/* harmony import */ var _components_app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/app */ "./src/components/app.js");
var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
__webpack_provided_window_dot_jQuery = $;
window.$ = $;











__webpack_require__(/*! ./utils/sortBy */ "./src/utils/sortBy.js");
__webpack_require__(/*! ./utils/animations */ "./src/utils/animations.js");


new _components_app__WEBPACK_IMPORTED_MODULE_6__["default"]();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/utils/animations.js":
/*!*********************************!*\
  !*** ./src/utils/animations.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {(function ($) {
	$.fn.shake = function (settings) {
		if (typeof settings.interval == "undefined") {
			settings.interval = 100;
		}

		if (typeof settings.distance == "undefined") {
			settings.distance = 10;
		}

		if (typeof settings.times == "undefined") {
			settings.times = 4;
		}

		if (typeof settings.complete == "undefined") {
			settings.complete = function () {};
		}

		$(this).css("position", "relative");

		for (var iter = 0; iter < (settings.times + 1); iter++) {
			$(this).animate({
				left: ((iter % 2 == 0 ? settings.distance : settings.distance * -1))
			}, settings.interval);
		}

		$(this).animate({
			left: 0
		}, settings.interval, settings.complete);
	};
	$.fn.bounce = function (settings) {
		if (typeof settings.interval == "undefined") {
			settings.interval = 100;
		}

		if (typeof settings.distance == "undefined") {
			settings.distance = 10;
		}

		if (typeof settings.times == "undefined") {
			settings.times = 4;
		}

		if (typeof settings.complete == "undefined") {
			settings.complete = function () {};
		}

		$(this).css("position", "relative");

		for (var iter = 0; iter < (settings.times + 1); iter++) {
			$(this).animate({
				top: ((iter % 2 == 0 ? settings.distance : settings.distance * -1))
			}, settings.interval);
		}

		$(this).animate({
			top: 0
		}, settings.interval, settings.complete);
	};
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/utils/groupBy.js":
/*!******************************!*\
  !*** ./src/utils/groupBy.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (collection, property) {
	var i = 0,
		val, index,
		values = [],
		result = [];
	for (; i < collection.length; i++) {
		val = collection[i][property];
		index = values.indexOf(val);
		if (index > -1)
			result[index].push(collection[i]);
		else {
			values.push(val);
			result.push([collection[i]]);
		}
	}
	return result;
});

/***/ }),

/***/ "./src/utils/jsonDownloader.js":
/*!*************************************!*\
  !*** ./src/utils/jsonDownloader.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony default export */ __webpack_exports__["default"] = (function (onlyMessage, url) {
	return new Promise((resolve) => {
		$.ajax({
			url,
			dataType: "json",
			context: this
		}).done((result) => {
			resolve(onlyMessage ? result.message : result);
		});
	});
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/utils/rot13.js":
/*!****************************!*\
  !*** ./src/utils/rot13.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (value) {
	return value.replace(/[a-zA-Z]/g, function (c) {
		return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
	});
});

/***/ }),

/***/ "./src/utils/sortBy.js":
/*!*****************************!*\
  !*** ./src/utils/sortBy.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

function dynamicSortMultiple() {
	var props = arguments;
	return function (obj1, obj2) {
		var i = 0,
			result = 0,
			numberOfProperties = props.length;
		/* try getting a different result from 0 (equal)
		 * as long as we have extra properties to compare
		 */
		while (result === 0 && i < numberOfProperties) {
			result = dynamicSort(props[i])(obj1, obj2);
			i++;
		}
		return result;
	};
}

function dynamicSort(property) {
	var sortOrder = 1;
	if (property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function (a, b) {
		var result;
		if (typeof (a[property]) === "string") {
			result = a[property].localeCompare(b[property], localStorage.language || "pl-PL");
		} else {
			result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		}
		return result * sortOrder;
	};
}

Object.defineProperty(Array.prototype, "sortBy", {
	enumerable: false,
	writable: true,
	value: function () {
		return this.sort(dynamicSortMultiple.apply(null, arguments));
	}
});

/***/ }),

/***/ "./src/utils/timeFormatter.js":
/*!************************************!*\
  !*** ./src/utils/timeFormatter.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (timestamp, hideSeconds = false) {
	let t = new Date(timestamp || 0);
	let hours = "0" + t.getHours();
	let minutes = "0" + t.getMinutes();
	let seconds = "0" + t.getSeconds();
	if (hideSeconds) {
		return hours.substr(-2) + ":" + minutes.substr(-2);
	} else {
		return hours.substr(-2) + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);
	}
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9jc3MvQ0ROcy5jc3MiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9jc3MvZmxhZ3MuY3NzIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvY3NzL3N0eWxlLmNzcyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2Nzcy9DRE5zLmNzcz8zNmQ3Iiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvY3NzL2ZsYWdzLmNzcz9mZDgxIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvY3NzL3N0eWxlLmNzcz9jMWQyIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvaW1nL2ZsYWdzLnBuZyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvZGV2aWNlcy9AZGV2aWNlc1RhYi5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9kZXZpY2VzL2VkaXRhYmxlVGV4dElucHV0LmpzIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL21haW4vQGxvYWRpbmdQYW5lbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9tYWluL0Bsb2dpblBhbmVsLmpzIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL21haW4vQG1haW5QYW5lbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9tYWluL0Bub3RpZmllci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9tYWluL2JveC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9tYWluL21vZGFscy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9tYWluL3RhYi5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy93ZWJzb2NrZXQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbmZpZy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaTE4bi9jb25maWcuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2kxOG4vZGUtREUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2kxOG4vZW4tRU4uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy91dGlscy9hbmltYXRpb25zLmpzIiwid2VicGFjazovLy8uL3NyYy91dGlscy9ncm91cEJ5LmpzIiwid2VicGFjazovLy8uL3NyYy91dGlscy9qc29uRG93bmxvYWRlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvdXRpbHMvcm90MTMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3V0aWxzL3NvcnRCeS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvdXRpbHMvdGltZUZvcm1hdHRlci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnQkFBUSxvQkFBb0I7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBaUIsNEJBQTRCO0FBQzdDO0FBQ0E7QUFDQSwwQkFBa0IsMkJBQTJCO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBZ0IsdUJBQXVCO0FBQ3ZDOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDdEpBLDJCQUEyQixtQkFBTyxDQUFDLDJHQUFzRDtBQUN6RjtBQUNBLGNBQWMsUUFBUyx3RUFBd0U7QUFDL0YsY0FBYyxRQUFTLGtHQUFrRztBQUN6SCxjQUFjLFFBQVMsNEZBQTRGO0FBQ25ILGNBQWMsUUFBUyxxR0FBcUc7QUFDNUgsY0FBYyxRQUFTLDRGQUE0RjtBQUNuSCxjQUFjLFFBQVMsa0hBQWtIO0FBQ3pJLGNBQWMsUUFBUyxxR0FBcUc7O0FBRTVIO0FBQ0EsY0FBYyxRQUFTOzs7Ozs7Ozs7Ozs7O0FDWHZCLDJCQUEyQixtQkFBTyxDQUFDLDJHQUFzRDtBQUN6RjtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLHlIQUE2RDtBQUNyRix5Q0FBeUMsbUJBQU8sQ0FBQyxvREFBa0I7O0FBRW5FO0FBQ0EsY0FBYyxRQUFTLDhGQUE4RixxQkFBcUIsV0FBVyxZQUFZLDJEQUEyRCxjQUFjLDRCQUE0QixjQUFjLDRCQUE0QixjQUFjLDRCQUE0QixjQUFjLGdDQUFnQzs7Ozs7Ozs7Ozs7OztBQ054WSwyQkFBMkIsbUJBQU8sQ0FBQywyR0FBc0Q7QUFDekY7QUFDQSxjQUFjLFFBQVMsU0FBUyx5Q0FBeUMsR0FBRyxxRUFBcUUsdUJBQXVCLEdBQUcsY0FBYyxzQkFBc0Isb0JBQW9CLEdBQUcsbUJBQW1CLDhCQUE4QiwwQkFBMEIsR0FBRyxxQkFBcUIsK0JBQStCLDJCQUEyQixHQUFHLDRCQUE0Qix5Q0FBeUMseUJBQXlCLEdBQUcsc0JBQXNCLGlDQUFpQyxHQUFHLDhCQUE4QiwyQkFBMkIsZ0NBQWdDLG1DQUFtQyxHQUFHLHVDQUF1QyxrQ0FBa0Msc0JBQXNCLGlCQUFpQixHQUFHLHNDQUFzQyxrQ0FBa0Msc0JBQXNCLGlCQUFpQixHQUFHLHdDQUF3QyxrQ0FBa0Msc0JBQXNCLGlCQUFpQixHQUFHLDBGQUEwRixnQ0FBZ0MsbUNBQW1DLEdBQUcsZ0NBQWdDLGdCQUFnQiw2QkFBNkIsS0FBSyxHQUFHLCtCQUErQixvQkFBb0IsbUNBQW1DLEtBQUssR0FBRyx3Q0FBd0MsNEJBQTRCLEdBQUcsNkpBQTZKLDJDQUEyQyxxQ0FBcUMseUJBQXlCLEdBQUcsU0FBUyxrQ0FBa0MsR0FBRyxTQUFTLGlDQUFpQyxHQUFHLGlCQUFpQix1QkFBdUIsaUJBQWlCLEdBQUcsOEJBQThCLGtCQUFrQiw2QkFBNkIsR0FBRyxlQUFlLG9CQUFvQixHQUFHLGdCQUFnQixxQkFBcUIsR0FBRywrQkFBK0IsK0JBQStCLGtCQUFrQixLQUFLLEdBQUcsK0JBQStCLCtCQUErQixpQkFBaUIsS0FBSyxHQUFHLGVBQWUsc0NBQXNDLEdBQUcsWUFBWSxtQ0FBbUMsR0FBRyxlQUFlLHVCQUF1QixxQkFBcUIsR0FBRyxnQ0FBZ0MsdUJBQXVCLFdBQVcsYUFBYSxvQkFBb0IscUJBQXFCLHFCQUFxQixzQkFBc0IsNkJBQTZCLGVBQWUsa0JBQWtCLHNCQUFzQixvQkFBb0IsbUJBQW1CLEdBQUcscUNBQXFDLDRCQUE0QixHQUFHLGdDQUFnQyxVQUFVLG1CQUFtQixLQUFLLFVBQVUsbUJBQW1CLEtBQUssR0FBRyxZQUFZLDJCQUEyQixvQ0FBb0MscUNBQXFDLGdEQUFnRCxtREFBbUQsMkNBQTJDLEdBQUc7Ozs7Ozs7Ozs7Ozs7O0FDRHIvRixjQUFjLG1CQUFPLENBQUMsaUlBQTJEOztBQUVqRiw0Q0FBNEMsUUFBUzs7QUFFckQ7QUFDQTs7OztBQUlBLGVBQWU7O0FBRWY7QUFDQTs7QUFFQSxhQUFhLG1CQUFPLENBQUMseUdBQXNEOztBQUUzRTs7QUFFQSxHQUFHLEtBQVUsRUFBRSxFOzs7Ozs7Ozs7Ozs7QUNsQmYsY0FBYyxtQkFBTyxDQUFDLG1JQUE0RDs7QUFFbEYsNENBQTRDLFFBQVM7O0FBRXJEO0FBQ0E7Ozs7QUFJQSxlQUFlOztBQUVmO0FBQ0E7O0FBRUEsYUFBYSxtQkFBTyxDQUFDLHlHQUFzRDs7QUFFM0U7O0FBRUEsR0FBRyxLQUFVLEVBQUUsRTs7Ozs7Ozs7Ozs7O0FDbEJmLGNBQWMsbUJBQU8sQ0FBQyxtSUFBNEQ7O0FBRWxGLDRDQUE0QyxRQUFTOztBQUVyRDtBQUNBOzs7O0FBSUEsZUFBZTs7QUFFZjtBQUNBOztBQUVBLGFBQWEsbUJBQU8sQ0FBQyx5R0FBc0Q7O0FBRTNFOztBQUVBLEdBQUcsS0FBVSxFQUFFLEU7Ozs7Ozs7Ozs7O0FDbkJmLGlDQUFpQyx3L0s7Ozs7Ozs7Ozs7OztBQ0FqQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFtQztBQUNTO0FBQ0k7QUFDUjtBQUNXO0FBQ1Q7QUFDVDtBQUMwQjs7QUFFNUM7O0FBRWY7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0Esc0JBQXNCLHNEQUFRO0FBQzlCLDBCQUEwQixxREFBWTtBQUN0QztBQUNBO0FBQ0E7QUFDQTs7QUFFQSxFQUFFLDREQUFNOztBQUVSO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLDJCQUEyQiwwREFBWTtBQUN2QztBQUNBO0FBQ0EsR0FBRztBQUNILDZCQUE2Qix3REFBVTtBQUN2QztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0osaUNBQWlDLG9FQUFhO0FBQzlDLEdBQUc7QUFDSDtBQUNBLEdBQUc7O0FBRUg7O0FBRUE7QUFDQTtBQUNBLDBDQUEwQyxrREFBTSxhQUFhLEdBQUcsd0JBQXdCO0FBQ3hGO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDOzs7Ozs7Ozs7Ozs7O0FDcEZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVlO0FBQ2U7QUFDTDtBQUNLO0FBQ3NCOzs7O0FBSXJDOztBQUVmOztBQUVBLGlCQUFpQiw0Q0FBRztBQUNwQix1QkFBdUIsa0RBQUk7QUFDM0Isc0JBQXNCLGtEQUFJOztBQUUxQix1QkFBdUIsa0RBQUk7QUFDM0I7QUFDQSxJQUFJO0FBQ0osR0FBRyxrREFBSTtBQUNQO0FBQ0E7O0FBRUEsd0JBQXdCLGtEQUFJO0FBQzVCO0FBQ0EsSUFBSTtBQUNKLEdBQUcsa0RBQUk7QUFDUDtBQUNBOztBQUVBO0FBQ0Esd0JBQXdCLGtEQUFJO0FBQzVCO0FBQ0EsS0FBSztBQUNMLElBQUksa0RBQUk7QUFDUjtBQUNBOztBQUVBLG1CQUFtQixrREFBSTtBQUN2QjtBQUNBO0FBQ0E7QUFDQSxJQUFJOztBQUVKLDBCQUEwQixrREFBSTtBQUM5QjtBQUNBO0FBQ0E7QUFDQSxJQUFJOztBQUVKLG1CQUFtQixrREFBSTtBQUN2QjtBQUNBO0FBQ0E7QUFDQSxJQUFJOztBQUVKO0FBQ0E7QUFDQTtBQUNBLEtBQUssa0RBQUk7QUFDVDtBQUNBLE1BQU07QUFDTjtBQUNBLElBQUk7Ozs7QUFJSixvQkFBb0Isa0RBQUk7O0FBRXhCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUEsc0JBQXNCLGtEQUFJO0FBQzFCO0FBQ0EsS0FBSztBQUNMLElBQUksa0RBQUk7QUFDUjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGtEQUFJO0FBQ1IsS0FBSyxrREFBSTtBQUNUO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxrREFBSTtBQUNSLEtBQUssa0RBQUk7QUFDVDtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksa0RBQUk7QUFDUixLQUFLLGtEQUFJO0FBQ1Q7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGtEQUFJO0FBQ1IsS0FBSyxrREFBSTtBQUNUO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixrREFBSTtBQUNuQztBQUNBOzs7Ozs7QUFNQTtBQUNBLHdCQUF3QixrREFBSTtBQUM1QjtBQUNBLEtBQUs7QUFDTCxJQUFJLGtEQUFJO0FBQ1I7QUFDQTs7QUFFQSxtQkFBbUIsa0RBQUk7QUFDdkI7QUFDQTtBQUNBO0FBQ0EsSUFBSTs7QUFFSiwwQkFBMEIsa0RBQUk7QUFDOUI7QUFDQTtBQUNBO0FBQ0EsSUFBSTs7QUFFSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUEsc0JBQXNCLGtEQUFJO0FBQzFCO0FBQ0EsS0FBSztBQUNMLElBQUksa0RBQUk7QUFDUjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGtEQUFJO0FBQ1IsS0FBSyxrREFBSTtBQUNUO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxrREFBSTtBQUNSLEtBQUssa0RBQUk7QUFDVDtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQSwrQkFBK0Isa0RBQUk7QUFDbkM7QUFDQTs7QUFFQTtBQUNBLEdBQUcsa0RBQUk7QUFDUCxJQUFJLGtEQUFJO0FBQ1IsU0FBUyxpREFBRztBQUNaO0FBQ0E7QUFDQSxHQUFHLGtEQUFJO0FBQ1AsSUFBSSxrREFBSTtBQUNSLFNBQVMsaURBQUc7QUFDWjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQzs7Ozs7Ozs7Ozs7OztBQ3BOQTtBQUFBO0FBQUE7QUFBQTtBQUVlO0FBQ2dCOztBQUVoQjs7QUFFZjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxNQUFNLCtDQUFPO0FBQ2IsR0FBRywrQ0FBTztBQUNWOztBQUVBLG9CQUFvQixrREFBSTtBQUN4QjtBQUNBO0FBQ0E7QUFDQSxVQUFVLCtDQUFPO0FBQ2pCLEdBQUc7O0FBRUg7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsSUFBSSwrQ0FBTztBQUNYO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxDOzs7Ozs7Ozs7Ozs7O0FDbkRBO0FBQUE7QUFBQTtBQUVlO0FBQ1U7O0FBRVY7QUFDZjs7QUFFQSxpQkFBaUIsNENBQUc7O0FBRXBCO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxTQUFTLGtEQUFJO0FBQ2IsR0FBRyxrREFBSTtBQUNQLElBQUksa0RBQUk7QUFDUixJQUFJLGtEQUFJO0FBQ1IsS0FBSyxrREFBSTtBQUNULCtCQUErQjtBQUMvQixNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBOztBQUVBLEM7Ozs7Ozs7Ozs7OztBQy9CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkI7QUFHWjtBQUNtQjtBQUNzQjtBQUNsQjtBQUNiO0FBQ2tCOztBQUU1Qjs7QUFFZjs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0EsaUJBQWlCLDREQUFLLENBQUMsOENBQUk7QUFDM0I7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRyxxRUFBYyxRQUFRLCtDQUFNO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLGlDQUFpQyxxREFBWTtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07O0FBRU4sS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSixHQUFHOztBQUVIOztBQUVBOztBQUVBLHNCQUFzQixrREFBSTtBQUMxQjtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUEsc0JBQXNCLGtEQUFJO0FBQzFCO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQSxvQkFBb0Isa0RBQUk7QUFDeEI7QUFDQSxHQUFHLEVBQUUsa0RBQUk7QUFDVDtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBLEVBQUUsK0NBQU07QUFDUixjQUFjLGtEQUFJLGtCQUFrQixhQUFhO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsR0FBRzs7QUFFSCxTQUFTLGtEQUFJO0FBQ2IsR0FBRyxrREFBSTtBQUNQLEdBQUcsa0RBQUk7QUFDUCxJQUFJLGtEQUFJO0FBQ1IsS0FBSyxrREFBSTtBQUNULE1BQU0sa0RBQUk7QUFDVixNQUFNLGtEQUFJO0FBQ1Y7QUFDQSxRQUFROztBQUVSLE9BQU8sa0RBQUk7QUFDWDtBQUNBLFFBQVEsa0RBQUk7QUFDWjs7QUFFQSxPQUFPLGtEQUFJO0FBQ1g7QUFDQSxRQUFRLGtEQUFJO0FBQ1o7O0FBRUEsT0FBTyxrREFBSTtBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyxrREFBSTtBQUNULE1BQU0sa0RBQUk7QUFDVjtBQUNBO0FBQ0EsSUFBSSxrREFBSTtBQUNSLEtBQUssa0RBQUk7QUFDVDtBQUNBLE9BQU8sa0RBQUk7QUFDWCxPQUFPLGtEQUFJO0FBQ1g7QUFDQSxPQUFPLGtEQUFJO0FBQ1gsT0FBTyxrREFBSTtBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQzs7Ozs7Ozs7Ozs7OztBQy9KQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRWU7QUFHYzs7QUFFTDtBQUN3QjtBQUNRO0FBQ3RCOztBQUVuQjs7QUFFZjs7O0FBR0E7O0FBRUE7O0FBRUEscUJBQXFCLGtEQUFJO0FBQ3pCLDRCQUE0QixhQUFhO0FBQ3pDLElBQUk7QUFDSixHQUFHLGtEQUFJO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLElBQUksa0RBQUk7QUFDUjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsd0JBQXdCLDJEQUFVOztBQUVsQyxTQUFTLGtEQUFJO0FBQ2IsR0FBRyxrREFBSTtBQUNQLElBQUksa0RBQUk7QUFDUixLQUFLLGtEQUFJO0FBQ1QsTUFBTSxrREFBSTtBQUNWLE9BQU8sa0RBQUk7QUFDWCxPQUFPLGtEQUFJO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULFFBQVEsa0RBQUk7QUFDWjtBQUNBO0FBQ0EsTUFBTSxrREFBSTtBQUNWLE9BQU8sa0RBQUk7QUFDWDtBQUNBLFNBQVM7QUFDVCxRQUFRLGtEQUFJO0FBQ1osU0FBUyxrREFBSTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWCxVQUFVLGtEQUFJO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsUUFBUSxrREFBSTtBQUNaLFNBQVMsa0RBQUk7QUFDYix3REFBd0QsaUJBQWlCO0FBQ3pFLDhCQUE4QjtBQUM5QixXQUFXO0FBQ1gsVUFBVSxrREFBSTtBQUNkLG9DQUFvQyxhQUFhO0FBQ2pELFlBQVk7QUFDWixXQUFXLGtEQUFJO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFlBQVksa0RBQUk7QUFDaEIsWUFBWSxrREFBSTtBQUNoQjtBQUNBO0FBQ0EsVUFBVSxrREFBSTtBQUNkLG9DQUFvQyxhQUFhO0FBQ2pELFlBQVk7QUFDWixXQUFXLGtEQUFJO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFlBQVksa0RBQUk7QUFDaEIsWUFBWSxrREFBSTtBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU0sa0RBQUk7QUFDVixtQ0FBbUM7QUFDbkMsUUFBUTtBQUNSLE9BQU8sa0RBQUk7QUFDWCxRQUFRLGtEQUFJO0FBQ1osU0FBUyxrREFBSTtBQUNiO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsVUFBVSxrREFBSTtBQUNkLFVBQVUsa0RBQUk7QUFDZDtBQUNBLFNBQVMsa0RBQUk7QUFDYixVQUFVLGtEQUFJO0FBQ2Q7QUFDQTtBQUNBLFVBQVUsa0RBQUk7QUFDZCxXQUFXLGtEQUFJO0FBQ2YsWUFBWSxrREFBSTtBQUNoQixhQUFhLGtEQUFJO0FBQ2pCLGNBQWMsa0RBQUk7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsa0RBQUk7QUFDZCxXQUFXLGtEQUFJO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsUUFBUSxrREFBSTtBQUNaLFNBQVMsa0RBQUk7QUFDYixrQ0FBa0M7QUFDbEMsV0FBVztBQUNYLFVBQVUsa0RBQUk7QUFDZCxVQUFVLGtEQUFJO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLGtEQUFJO0FBQ1AsSUFBSSxrREFBSTtBQUNSLEtBQUssa0RBQUk7QUFDVDtBQUNBLE9BQU87QUFDUCxVQUFVLDRDQUFHO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLG1DQUFtQyxNQUFNO0FBQ3pDLEdBQUc7QUFDSDs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsSUFBSSxrREFBSTtBQUNSLEtBQUssa0RBQUk7QUFDVCxNQUFNLGtEQUFJO0FBQ1Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakMsT0FBTztBQUNQLEtBQUssa0RBQUk7QUFDVCxNQUFNLGtEQUFJO0FBQ1Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSyxrREFBSTtBQUNUO0FBQ0EsTUFBTSxLQUFLLHNCQUFzQixLQUFLLGtCQUFrQjtBQUN4RCxLQUFLLGtEQUFJO0FBQ1QsS0FBSyxrREFBSTtBQUNULE1BQU0sa0RBQUk7QUFDVjtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBLE9BQU8sK0RBQVc7QUFDbEI7QUFDQSxtQ0FBbUMsdUJBQXVCO0FBQzFEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTyxxRUFBYyxPQUFPLCtDQUFNO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsa0RBQUk7QUFDZCwyQ0FBMkMsZ0JBQWdCLGdCQUFnQixhQUFhO0FBQ3hGLFlBQVk7QUFDWjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsa0RBQUk7QUFDZix3Q0FBd0MsOEJBQThCLGdCQUFnQixnQkFBZ0IsYUFBYTtBQUNuSCxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsVUFBVTs7QUFFViw2REFBNkQsb0JBQW9CO0FBQ2pGO0FBQ0E7QUFDQTtBQUNBLFVBQVUsa0RBQUk7QUFDZCxXQUFXLGtEQUFJO0FBQ2YsWUFBWSxrREFBSTtBQUNoQixhQUFhLGtEQUFJO0FBQ2pCLGFBQWEsa0RBQUk7QUFDakIsYUFBYSxrREFBSTtBQUNqQixhQUFhLGtEQUFJO0FBQ2pCO0FBQ0E7QUFDQSxXQUFXLGtEQUFJO0FBQ2YsWUFBWSxrREFBSTtBQUNoQixhQUFhLGtEQUFJO0FBQ2pCLGFBQWEsa0RBQUk7QUFDakIsYUFBYSxrREFBSTtBQUNqQixhQUFhLGtEQUFJLE9BQU8sa0RBQUksZ0NBQWdDLDBFQUEwRTtBQUN0STtBQUNBO0FBQ0EsVUFBVSxrREFBSTtBQUNkLFdBQVcsa0RBQUk7QUFDZixZQUFZLGtEQUFJO0FBQ2hCLGFBQWEsa0RBQUk7QUFDakIsYUFBYSxrREFBSTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKLEdBQUc7QUFDSDs7QUFFQSxDOzs7Ozs7Ozs7Ozs7O0FDeFNBO0FBQWU7O0FBRWY7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxDOzs7Ozs7Ozs7Ozs7QUMvQkE7QUFBQTtBQUVlOztBQUVBOztBQUVmOztBQUVBLFNBQVMsa0RBQUksZ0JBQWdCLEtBQUs7QUFDbEMsR0FBRyxrREFBSTtBQUNQLElBQUksa0RBQUk7QUFDUixJQUFJLGtEQUFJO0FBQ1I7QUFDQSxHQUFHLGtEQUFJO0FBQ1AsR0FBRyxrREFBSTtBQUNQOztBQUVBOztBQUVBLEM7Ozs7Ozs7Ozs7OztBQ25CQTtBQUFBO0FBRWU7O0FBRUE7O0FBRWY7QUFDQSxFQUFFLGtEQUFJO0FBQ04sR0FBRyxrREFBSTtBQUNQLElBQUksa0RBQUk7QUFDUixLQUFLLGtEQUFJO0FBQ1QsTUFBTSxrREFBSTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSLE9BQU8sa0RBQUk7QUFDWDtBQUNBLE1BQU0sa0RBQUk7QUFDVjtBQUNBLEtBQUssa0RBQUk7QUFDVCxNQUFNLGtEQUFJO0FBQ1Y7QUFDQSxLQUFLLGtEQUFJO0FBQ1QsTUFBTSxrREFBSTtBQUNWO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILEVBQUU7O0FBRUY7QUFDQSxFQUFFLGtEQUFJO0FBQ04sR0FBRyxrREFBSTtBQUNQLElBQUksa0RBQUk7QUFDUixLQUFLLGtEQUFJO0FBQ1QsTUFBTSxrREFBSTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSLE9BQU8sa0RBQUk7QUFDWDtBQUNBLE1BQU0sa0RBQUk7QUFDVjtBQUNBLEtBQUssa0RBQUk7QUFDVCxLQUFLLGtEQUFJO0FBQ1QsTUFBTSxrREFBSTtBQUNWO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILEVBQUU7O0FBRUY7QUFDQTtBQUNBLEVBQUU7O0FBRUYsQzs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBRWU7O0FBRUE7O0FBRWY7O0FBRUEsU0FBUyxrREFBSSxzQkFBc0IsNEJBQTRCLEdBQUcsR0FBRztBQUNyRTtBQUNBLEdBQUc7O0FBRUg7O0FBRUEsQzs7Ozs7Ozs7Ozs7O0FDZEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQzBDO0FBQ2xCO0FBQ2U7QUFHeEI7O0FBRUE7O0FBRWY7O0FBRUEsaUJBQWlCLDRDQUFHO0FBQ3BCOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3Qix1REFBUztBQUNqQztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCQUF1QixrREFBSTtBQUMzQjtBQUNBLFFBQVE7QUFDUixPQUFPLGtEQUFJO0FBQ1g7QUFDQTtBQUNBLHlCQUF5QixrREFBSTtBQUM3QjtBQUNBLFFBQVE7QUFDUixPQUFPLGtEQUFJO0FBQ1g7QUFDQTs7QUFFQTtBQUNBLDRCQUE0QixrREFBSTtBQUNoQztBQUNBLFNBQVM7QUFDVCxRQUFRLGtEQUFJO0FBQ1osMkJBQTJCLDJCQUEyQjtBQUN0RDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwwQkFBMEIsa0RBQUk7QUFDOUI7QUFDQSxTQUFTO0FBQ1QsUUFBUSxrREFBSTtBQUNaOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSx5REFBeUQsMkJBQTJCO0FBQ3BGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQyxrREFBSTtBQUN2QztBQUNBOztBQUVBO0FBQ0EsNEJBQTRCLGtEQUFJO0FBQ2hDO0FBQ0EsU0FBUztBQUNULFFBQVEsa0RBQUk7QUFDWjtBQUNBOztBQUVBLHVCQUF1QixrREFBSTtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7O0FBRVIsOEJBQThCLGtEQUFJO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTs7QUFFUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUEsMEJBQTBCLGtEQUFJO0FBQzlCO0FBQ0EsU0FBUztBQUNULFFBQVEsa0RBQUk7QUFDWjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLGtEQUFJO0FBQ1osU0FBUyxrREFBSTtBQUNiO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxrREFBSTtBQUNaLFNBQVMsa0RBQUk7QUFDYjtBQUNBLFVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsa0RBQUk7QUFDdkM7QUFDQTs7O0FBR0E7QUFDQSxPQUFPLGtEQUFJO0FBQ1gsUUFBUSxrREFBSTtBQUNaLFFBQVEsa0RBQUk7QUFDWixRQUFRLGtEQUFJO0FBQ1osUUFBUSxrREFBSTtBQUNaO0FBQ0E7QUFDQSxNQUFNOztBQUVOLHlCQUF5QixrREFBSTtBQUM3QixNQUFNLGtEQUFJO0FBQ1Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUssOERBQU87QUFDWjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHNCQUFzQixrREFBSTtBQUMxQjs7QUFFQTtBQUNBLDRCQUE0QixrREFBSTtBQUNoQztBQUNBO0FBQ0E7QUFDQSxXQUFXLEVBQUUsa0RBQUk7QUFDakIsVUFBVTtBQUNWLDRCQUE0QixrREFBSTtBQUNoQztBQUNBO0FBQ0E7QUFDQSxXQUFXLEVBQUUsa0RBQUk7QUFDakI7O0FBRUE7QUFDQTtBQUNBLHNCQUFzQixrREFBSTtBQUMxQjs7QUFFQTtBQUNBLDRCQUE0QixrREFBSTtBQUNoQztBQUNBO0FBQ0E7QUFDQSxXQUFXLEVBQUUsa0RBQUk7QUFDakIsVUFBVTtBQUNWLDRCQUE0QixrREFBSTtBQUNoQztBQUNBO0FBQ0E7QUFDQSxXQUFXLEVBQUUsa0RBQUk7QUFDakI7O0FBRUE7QUFDQTtBQUNBLHNCQUFzQixrREFBSTtBQUMxQjtBQUNBOztBQUVBO0FBQ0EsNEJBQTRCLGtEQUFJO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRSxrREFBSTtBQUNqQixVQUFVO0FBQ1YsNEJBQTRCLGtEQUFJO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRSxrREFBSTtBQUNqQixVQUFVO0FBQ1YsNEJBQTRCLGtEQUFJO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRSxrREFBSTtBQUNqQjs7QUFFQTtBQUNBO0FBQ0Esc0JBQXNCLGtEQUFJO0FBQzFCO0FBQ0E7O0FBRUE7QUFDQSw0QkFBNEIsa0RBQUk7QUFDaEM7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFLGtEQUFJO0FBQ2pCLFVBQVU7QUFDViw0QkFBNEIsa0RBQUk7QUFDaEM7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFLGtEQUFJO0FBQ2pCLFVBQVU7QUFDViw0QkFBNEIsa0RBQUk7QUFDaEM7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFLGtEQUFJO0FBQ2pCLFVBQVU7QUFDViw0QkFBNEIsa0RBQUk7QUFDaEM7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFLGtEQUFJO0FBQ2pCLFVBQVU7QUFDViw0QkFBNEIsa0RBQUk7QUFDaEM7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFLGtEQUFJO0FBQ2pCOztBQUVBO0FBQ0E7QUFDQSxzQkFBc0Isa0RBQUk7QUFDMUI7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLGtEQUFJO0FBQzFCO0FBQ0E7O0FBRUE7QUFDQTs7OztBQUlBO0FBQ0Esd0JBQXdCLGtEQUFJO0FBQzVCO0FBQ0E7QUFDQTtBQUNBLFNBQVMsRUFBRSxrREFBSTtBQUNmLFFBQVE7QUFDUix3QkFBd0Isa0RBQUk7QUFDNUI7QUFDQTtBQUNBO0FBQ0EsU0FBUyxFQUFFLGtEQUFJO0FBQ2Y7O0FBRUEsMEJBQTBCLGtEQUFJO0FBQzlCO0FBQ0EsU0FBUztBQUNULFFBQVEsa0RBQUk7QUFDWjtBQUNBOztBQUVBLHdCQUF3QixrREFBSTtBQUM1QjtBQUNBLFNBQVM7QUFDVCxRQUFRLGtEQUFJO0FBQ1o7QUFDQTs7Ozs7O0FBTUEsNEJBQTRCLGtEQUFJO0FBQ2hDO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsUUFBUSxrREFBSTtBQUNaO0FBQ0E7O0FBRUEsNkJBQTZCLGtEQUFJO0FBQ2pDO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsUUFBUSxrREFBSTtBQUNaO0FBQ0E7O0FBRUEsd0JBQXdCLGtEQUFJO0FBQzVCO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsUUFBUSxrREFBSTtBQUNaO0FBQ0E7O0FBRUEsMEJBQTBCLGtEQUFJO0FBQzlCO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsUUFBUSxrREFBSTtBQUNaO0FBQ0E7O0FBRUEsd0JBQXdCLGtEQUFJO0FBQzVCO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsUUFBUSxrREFBSTtBQUNaO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBLDZCQUE2QixrREFBSTtBQUNqQztBQUNBLFVBQVU7QUFDVixTQUFTLGtEQUFJO0FBQ2I7QUFDQTs7QUFFQSx3QkFBd0Isa0RBQUk7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVULCtCQUErQixrREFBSTtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQsd0JBQXdCLGtEQUFJO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSxVQUFVLGtEQUFJO0FBQ2Q7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBLFNBQVM7O0FBRVQseUJBQXlCLGtEQUFJOztBQUU3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBOztBQUVBLDJCQUEyQixrREFBSTtBQUMvQjtBQUNBLFVBQVU7QUFDVixTQUFTLGtEQUFJO0FBQ2I7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUyxrREFBSTtBQUNiLFVBQVUsa0RBQUk7QUFDZDtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVMsa0RBQUk7QUFDYixVQUFVLGtEQUFJO0FBQ2Q7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTLGtEQUFJO0FBQ2IsVUFBVSxrREFBSTtBQUNkO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUyxrREFBSTtBQUNiLFVBQVUsa0RBQUk7QUFDZDtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0Msa0RBQUk7QUFDeEM7QUFDQTs7QUFFQTtBQUNBLDZCQUE2QixrREFBSTtBQUNqQztBQUNBLFVBQVU7QUFDVixTQUFTLGtEQUFJO0FBQ2Isc0NBQXNDLDRCQUE0QjtBQUNsRTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwyQkFBMkIsa0RBQUk7QUFDL0I7QUFDQSxVQUFVO0FBQ1YsU0FBUyxrREFBSTtBQUNiOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSwrREFBK0QsNEJBQTRCO0FBQzNGO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQyxrREFBSTtBQUN4QztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQSxRQUFRLGtEQUFJO0FBQ1osU0FBUyxrREFBSTtBQUNiLFNBQVMsa0RBQUk7QUFDYixTQUFTLGtEQUFJO0FBQ2IsU0FBUyxrREFBSTtBQUNiLFNBQVMsa0RBQUk7QUFDYjtBQUNBO0FBQ0EsT0FBTzs7QUFFUCxrQkFBa0Isa0RBQUk7QUFDdEIsT0FBTyxrREFBSTtBQUNYLFFBQVEsa0RBQUk7QUFDWixTQUFTLGtEQUFJO0FBQ2I7QUFDQSxVQUFVLEVBQUUsa0RBQUksZ0NBQWdDLGtEQUFJO0FBQ3BEO0FBQ0EsUUFBUSxrREFBSTtBQUNaLFNBQVMsa0RBQUk7QUFDYixTQUFTLGtEQUFJO0FBQ2IsU0FBUyxrREFBSTtBQUNiLFNBQVMsa0RBQUk7QUFDYixTQUFTLGtEQUFJO0FBQ2I7QUFDQTtBQUNBLE9BQU8sa0RBQUk7QUFDWDtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKOztBQUVBOztBQUVBLEM7Ozs7Ozs7Ozs7Ozs7QUMzckJBO0FBQWU7QUFDZjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUMsRTs7Ozs7Ozs7Ozs7O0FDdEJEO0FBQUE7QUFBQTtBQUV5Qjs7QUFFekI7QUFDQTtBQUNBLEVBQUUsa0VBQVU7QUFDWjtBQUNBLGlCQUFpQixtQkFBTyxDQUFDLHVDQUFZO0FBQ3JDLEdBQUc7QUFDSDs7QUFFQTtBQUNBLEVBQUUsa0VBQVU7QUFDWjtBQUNBLGlCQUFpQixtQkFBTyxDQUFDLHVDQUFZO0FBQ3JDLEdBQUc7QUFDSDs7QUFFQTtBQUNBLEVBQUUsa0VBQVU7QUFDWjtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQzs7Ozs7Ozs7Ozs7O0FDekJBO0FBQWU7O0FBRWY7O0FBRUEsQ0FBQyxFOzs7Ozs7Ozs7Ozs7QUNKRDtBQUFlOztBQUVmOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyxFOzs7Ozs7Ozs7Ozs7QUNwQ0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxRQUFRLG1CQUFPLENBQUMsb0RBQVE7QUFDeEIsb0NBQWE7QUFDYjs7QUFFK0I7O0FBRVo7QUFDQTs7QUFFYTtBQUNBOztBQUVOOztBQUUxQixtQkFBTyxDQUFDLDZDQUFnQjtBQUN4QixtQkFBTyxDQUFDLHFEQUFvQjs7QUFFTztBQUNuQyxJQUFJLHVEQUFHLEc7Ozs7Ozs7Ozs7OztBQ2xCUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBLG9CQUFvQiw2QkFBNkI7QUFDakQ7QUFDQTtBQUNBLElBQUk7QUFDSjs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsb0JBQW9CLDZCQUE2QjtBQUNqRDtBQUNBO0FBQ0EsSUFBSTtBQUNKOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDLFU7Ozs7Ozs7Ozs7Ozs7QUMzREQ7QUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTyx1QkFBdUI7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUNoQkE7QUFBQSx5Q0FBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0gsRUFBRTtBQUNGLEM7Ozs7Ozs7Ozs7Ozs7QUNWQTtBQUFlO0FBQ2Y7QUFDQTtBQUNBLEVBQUU7QUFDRixDOzs7Ozs7Ozs7OztBQ0pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyxFOzs7Ozs7Ozs7Ozs7QUN4Q0Q7QUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsQyIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuIFx0XHRyZXR1cm4gcmVzdWx0O1xuIFx0fVxuXG4gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBvYmplY3QgdG8gc3RvcmUgbG9hZGVkIGFuZCBsb2FkaW5nIGNodW5rc1xuIFx0Ly8gdW5kZWZpbmVkID0gY2h1bmsgbm90IGxvYWRlZCwgbnVsbCA9IGNodW5rIHByZWxvYWRlZC9wcmVmZXRjaGVkXG4gXHQvLyBQcm9taXNlID0gY2h1bmsgbG9hZGluZywgMCA9IGNodW5rIGxvYWRlZFxuIFx0dmFyIGluc3RhbGxlZENodW5rcyA9IHtcbiBcdFx0XCJtYWluXCI6IDBcbiBcdH07XG5cbiBcdHZhciBkZWZlcnJlZE1vZHVsZXMgPSBbXTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0dmFyIGpzb25wQXJyYXkgPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gfHwgW107XG4gXHR2YXIgb2xkSnNvbnBGdW5jdGlvbiA9IGpzb25wQXJyYXkucHVzaC5iaW5kKGpzb25wQXJyYXkpO1xuIFx0anNvbnBBcnJheS5wdXNoID0gd2VicGFja0pzb25wQ2FsbGJhY2s7XG4gXHRqc29ucEFycmF5ID0ganNvbnBBcnJheS5zbGljZSgpO1xuIFx0Zm9yKHZhciBpID0gMDsgaSA8IGpzb25wQXJyYXkubGVuZ3RoOyBpKyspIHdlYnBhY2tKc29ucENhbGxiYWNrKGpzb25wQXJyYXlbaV0pO1xuIFx0dmFyIHBhcmVudEpzb25wRnVuY3Rpb24gPSBvbGRKc29ucEZ1bmN0aW9uO1xuXG5cbiBcdC8vIGFkZCBlbnRyeSBtb2R1bGUgdG8gZGVmZXJyZWQgbGlzdFxuIFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2goW1wiLi9zcmMvaW5kZXguanNcIixcIm5wbVwiXSk7XG4gXHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIHJlYWR5XG4gXHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzXCIpKGZhbHNlKTtcbi8vIEltcG9ydHNcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIkBpbXBvcnQgdXJsKGh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Tb3VyY2UrU2FucytQcm8pO1wiLCBcIlwiXSk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJAaW1wb3J0IHVybChodHRwczovL2NkbmpzLmNsb3VkZmxhcmUuY29tL2FqYXgvbGlicy9mb250LWF3ZXNvbWUvNS43LjIvY3NzL2ZvbnRhd2Vzb21lLm1pbi5jc3MpO1wiLCBcIlwiXSk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJAaW1wb3J0IHVybChodHRwczovL2NkbmpzLmNsb3VkZmxhcmUuY29tL2FqYXgvbGlicy9mb250LWF3ZXNvbWUvNS43LjIvY3NzL3NvbGlkLm1pbi5jc3MpO1wiLCBcIlwiXSk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJAaW1wb3J0IHVybChodHRwczovL2NkbmpzLmNsb3VkZmxhcmUuY29tL2FqYXgvbGlicy90d2l0dGVyLWJvb3RzdHJhcC8zLjQuMS9jc3MvYm9vdHN0cmFwLm1pbi5jc3MpO1wiLCBcIlwiXSk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJAaW1wb3J0IHVybChodHRwczovL2NkbmpzLmNsb3VkZmxhcmUuY29tL2FqYXgvbGlicy9hZG1pbi1sdGUvMi40LjkvY3NzL0FkbWluTFRFLm1pbi5jc3MpO1wiLCBcIlwiXSk7XG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJAaW1wb3J0IHVybChodHRwczovL2NkbmpzLmNsb3VkZmxhcmUuY29tL2FqYXgvbGlicy9qcXVlcnktY29udGV4dG1lbnUvMy4wLjAtYmV0YS4yL2pxdWVyeS5jb250ZXh0TWVudS5taW4uY3NzKTtcIiwgXCJcIl0pO1xuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiQGltcG9ydCB1cmwoaHR0cHM6Ly9jZG5qcy5jbG91ZGZsYXJlLmNvbS9hamF4L2xpYnMvcHJldHR5LWNoZWNrYm94LzMuMC4zL3ByZXR0eS1jaGVja2JveC5taW4uY3NzKTtcIiwgXCJcIl0pO1xuXG4vLyBNb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIlwiLCBcIlwiXSk7XG5cbiIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzXCIpKGZhbHNlKTtcbi8vIEltcG9ydHNcbnZhciB1cmxFc2NhcGUgPSByZXF1aXJlKFwiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL3VybC1lc2NhcGUuanNcIik7XG52YXIgX19fQ1NTX0xPQURFUl9VUkxfX18wX19fID0gdXJsRXNjYXBlKHJlcXVpcmUoXCIuLi9pbWcvZmxhZ3MucG5nXCIpKTtcblxuLy8gTW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIvKiFcXG4gKiBHZW5lcmF0ZWQgd2l0aCBDU1MgRmxhZyBTcHJpdGUgZ2VuZXJhdG9yIChodHRwczovL3d3dy5mbGFnLXNwcml0ZXMuY29tLylcXG4gKi8uZmxhZ3tkaXNwbGF5OmlubGluZS1ibG9jazt3aWR0aDozMnB4O2hlaWdodDozMnB4O2JhY2tncm91bmQ6dXJsKFwiICsgX19fQ1NTX0xPQURFUl9VUkxfX18wX19fICsgXCIpIG5vLXJlcGVhdH0uZmxhZy5mbGFnLWN6e2JhY2tncm91bmQtcG9zaXRpb246LTMycHggMH0uZmxhZy5mbGFnLWRle2JhY2tncm91bmQtcG9zaXRpb246LTY0cHggMH0uZmxhZy5mbGFnLWdie2JhY2tncm91bmQtcG9zaXRpb246MCAtMzJweH0uZmxhZy5mbGFnLXBse2JhY2tncm91bmQtcG9zaXRpb246LTMycHggLTMycHh9XCIsIFwiXCJdKTtcblxuIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIikoZmFsc2UpO1xuLy8gTW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCJib2R5IHtcXG5cXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZWNmMGY1ICFpbXBvcnRhbnQ7XFxufVxcblxcbnRhYmxlLnRhYmxlLWNlbnRlciB0aGVhZCB0ciB0aCxcXG50YWJsZS50YWJsZS1jZW50ZXIgdGJvZHkgdHIgdGQge1xcblxcdHRleHQtYWxpZ246IGNlbnRlcjtcXG59XFxuXFxuZGl2LmZsYWcge1xcblxcdG1hcmdpbi1yaWdodDogNXB4O1xcblxcdGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuZGl2LmxvZ2luLWJveCB7XFxuXFx0bWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcXG5cXHR3aWR0aDogODUlICFpbXBvcnRhbnQ7XFxufVxcblxcbmRpdi5tYWluLWhlYWRlciB7XFxuXFx0cG9zaXRpb246IGZpeGVkICFpbXBvcnRhbnQ7XFxuXFx0d2lkdGg6IDEwMCUgIWltcG9ydGFudDtcXG59XFxuXFxuZGl2LndyYXBwZXI+aGVhZGVyPm5hdiB7XFxuXFx0YmFja2dyb3VuZC1jb2xvcjogIzIyMmQzMiAhaW1wb3J0YW50O1xcblxcdG1hcmdpbjogMCAhaW1wb3J0YW50O1xcbn1cXG5cXG4jbmF2YmFyLWNvbGxhcHNlIHtcXG5cXHRvdmVyZmxvdzogdmlzaWJsZSAhaW1wb3J0YW50O1xcbn1cXG5cXG4jbmF2YmFyLWNvbGxhcHNlPnVsPmxpPmEge1xcblxcdGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuXFx0LW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xcblxcdC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcXG59XFxuXFxubGkuYWN0aXZlPmFbZGF0YS10eXBlPSdtYWluTWVudSddIHtcXG5cXHRib3JkZXItdG9wOiAzcHggc29saWQgI0VDRjBGNTtcXG5cXHRmb250LXdlaWdodDogYm9sZDtcXG5cXHRoZWlnaHQ6IDUwcHg7XFxufVxcblxcbmxpLmFjdGl2ZT5hW2RhdGEtdHlwZT0nY2hhdFRhYiddIHtcXG5cXHRib3JkZXItdG9wOiAzcHggc29saWQgIzIyMmQzMjtcXG5cXHRmb250LXdlaWdodDogYm9sZDtcXG5cXHRoZWlnaHQ6IDMwcHg7XFxufVxcblxcbmxpLmFjdGl2ZT5hW2RhdGEtdHlwZT0nZWRpdG9yVGFiJ10ge1xcblxcdGJvcmRlci10b3A6IDNweCBzb2xpZCAjMTMxMzEzO1xcblxcdGZvbnQtd2VpZ2h0OiBib2xkO1xcblxcdGhlaWdodDogNTBweDtcXG59XFxuXFxuLm5hdmJhci1uYXY+bGk+YVtkYXRhLXR5cGU9J2NoYXRUYWInXSxcXG4ubmF2YmFyLW5hdj5saS5hY3RpdmU+YVtkYXRhLXR5cGU9J2NoYXRUYWInXSB7XFxuXFx0cGFkZGluZy10b3A6IDVweCAhaW1wb3J0YW50O1xcblxcdHBhZGRpbmctYm90dG9tOiA1cHggIWltcG9ydGFudDtcXG59XFxuXFxuQG1lZGlhIChtaW4td2lkdGg6IDEyMDBweCkge1xcblxcdC5jb250YWluZXIge1xcblxcdFxcdHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XFxuXFx0fVxcbn1cXG5cXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcXG5cXHQubmF2YmFyLW5hdj5saSB7XFxuXFx0XFx0bWFyZ2luLXJpZ2h0OiAzcHggIWltcG9ydGFudDtcXG5cXHR9XFxufVxcblxcbmRpdj5kaXYubmF2YmFyLWN1c3RvbS1tZW51PnVsPmxpPmEge1xcblxcdGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xcbn1cXG5cXG4ubmF2YmFyLW5vY29sb3JzPmxpPmE6aG92ZXIsXFxuLm5hdmJhci1ub2NvbG9ycz5saT5hOmFjdGl2ZSxcXG4ubmF2YmFyLW5vY29sb3JzIC5vcGVuPmEsXFxuLm5hdmJhci1ub2NvbG9ycyAub3Blbj5hOmZvY3VzLFxcbi5uYXZiYXItbm9jb2xvcnMgLm9wZW4+YTpob3ZlciB7XFxuXFx0YmFja2dyb3VuZC1jb2xvcjogIzIyMmQzMiAhaW1wb3J0YW50XFxufVxcblxcbi5jb250ZW50LXdyYXBwZXIsXFxuLm1haW4tZm9vdGVyIHtcXG5cXHRtYXJnaW46IDAgIWltcG9ydGFudDtcXG59XFxuXFxuLm1yIHtcXG5cXHRtYXJnaW4tcmlnaHQ6IDEwcHggIWltcG9ydGFudDtcXG59XFxuXFxuLm1sIHtcXG5cXHRtYXJnaW4tbGVmdDogMTBweCAhaW1wb3J0YW50O1xcbn1cXG5cXG4uYXNrQnV0dG9ucyB7XFxuXFx0dGV4dC1hbGlnbjogY2VudGVyO1xcblxcdG1hcmdpbjogMTBweDtcXG59XFxuXFxuZGl2LmRpcmVjdC1jaGF0LW1lc3NhZ2VzIHtcXG5cXHRwYWRkaW5nOiAxMHB4O1xcblxcdGhlaWdodDogNTAwcHggIWltcG9ydGFudDtcXG59XFxuXFxuLmNQb2ludGVyIHtcXG5cXHRjdXJzb3I6IHBvaW50ZXI7XFxufVxcblxcbi5jb250YWluZXIge1xcblxcdG1hcmdpbi1sZWZ0OiAwcHg7XFxufVxcblxcbkBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xcblxcdCNtb3ZlQ3VzdG9tTWVudVRvU2hvd0JhcnMge1xcblxcdFxcdHJpZ2h0OiAzMHB4O1xcblxcdH1cXG59XFxuXFxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XFxuXFx0I21vdmVDdXN0b21NZW51VG9TaG93QmFycyB7XFxuXFx0XFx0cmlnaHQ6IDFweDtcXG5cXHR9XFxufVxcblxcbnRkLm1pZGRsZSB7XFxuXFx0dmVydGljYWwtYWxpZ246IG1pZGRsZSAhaW1wb3J0YW50O1xcbn1cXG5cXG50ZC50b3Age1xcblxcdHZlcnRpY2FsLWFsaWduOiB0b3AgIWltcG9ydGFudDtcXG59XFxuXFxuLmJ0bi1maWxlIHtcXG5cXHRwb3NpdGlvbjogcmVsYXRpdmU7XFxuXFx0b3ZlcmZsb3c6IGhpZGRlbjtcXG59XFxuXFxuLmJ0bi1maWxlIGlucHV0W3R5cGU9ZmlsZV0ge1xcblxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG5cXHR0b3A6IDA7XFxuXFx0cmlnaHQ6IDA7XFxuXFx0bWluLXdpZHRoOiAxMDAlO1xcblxcdG1pbi1oZWlnaHQ6IDEwMCU7XFxuXFx0Zm9udC1zaXplOiAxMDBweDtcXG5cXHR0ZXh0LWFsaWduOiByaWdodDtcXG5cXHRmaWx0ZXI6IGFscGhhKG9wYWNpdHk9MCk7XFxuXFx0b3BhY2l0eTogMDtcXG5cXHRvdXRsaW5lOiBub25lO1xcblxcdGJhY2tncm91bmQ6IHdoaXRlO1xcblxcdGN1cnNvcjogaW5oZXJpdDtcXG5cXHRkaXNwbGF5OiBibG9jaztcXG59XFxuXFxuLmJvcmRlcmxlc3MgdGQsXFxuLmJvcmRlcmxlc3MgdGgge1xcblxcdGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xcbn1cXG5cXG5ALXdlYmtpdC1rZXlmcmFtZXMgYmxpbmtlciB7XFxuXFx0ZnJvbSB7XFxuXFx0XFx0b3BhY2l0eTogMS4wO1xcblxcdH1cXG5cXG5cXHR0byB7XFxuXFx0XFx0b3BhY2l0eTogMC4wO1xcblxcdH1cXG59XFxuXFxuLmJsaW5rIHtcXG5cXHR0ZXh0LWRlY29yYXRpb246IGJsaW5rO1xcblxcdC13ZWJraXQtYW5pbWF0aW9uLW5hbWU6IGJsaW5rZXI7XFxuXFx0LXdlYmtpdC1hbmltYXRpb24tZHVyYXRpb246IDAuNnM7XFxuXFx0LXdlYmtpdC1hbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcXG5cXHQtd2Via2l0LWFuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGVhc2UtaW4tb3V0O1xcblxcdC13ZWJraXQtYW5pbWF0aW9uLWRpcmVjdGlvbjogYWx0ZXJuYXRlO1xcbn1cIiwgXCJcIl0pO1xuXG4iLCJcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9janMuanMhLi9DRE5zLmNzc1wiKTtcblxuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG5cbnZhciB0cmFuc2Zvcm07XG52YXIgaW5zZXJ0SW50bztcblxuXG5cbnZhciBvcHRpb25zID0ge1wiaG1yXCI6dHJ1ZX1cblxub3B0aW9ucy50cmFuc2Zvcm0gPSB0cmFuc2Zvcm1cbm9wdGlvbnMuaW5zZXJ0SW50byA9IHVuZGVmaW5lZDtcblxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzLmpzXCIpKGNvbnRlbnQsIG9wdGlvbnMpO1xuXG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscztcblxuaWYobW9kdWxlLmhvdCkge1xuXHRtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9janMuanMhLi9DRE5zLmNzc1wiLCBmdW5jdGlvbigpIHtcblx0XHR2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzIS4vQ0ROcy5jc3NcIik7XG5cblx0XHRpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcblxuXHRcdHZhciBsb2NhbHMgPSAoZnVuY3Rpb24oYSwgYikge1xuXHRcdFx0dmFyIGtleSwgaWR4ID0gMDtcblxuXHRcdFx0Zm9yKGtleSBpbiBhKSB7XG5cdFx0XHRcdGlmKCFiIHx8IGFba2V5XSAhPT0gYltrZXldKSByZXR1cm4gZmFsc2U7XG5cdFx0XHRcdGlkeCsrO1xuXHRcdFx0fVxuXG5cdFx0XHRmb3Ioa2V5IGluIGIpIGlkeC0tO1xuXG5cdFx0XHRyZXR1cm4gaWR4ID09PSAwO1xuXHRcdH0oY29udGVudC5sb2NhbHMsIG5ld0NvbnRlbnQubG9jYWxzKSk7XG5cblx0XHRpZighbG9jYWxzKSB0aHJvdyBuZXcgRXJyb3IoJ0Fib3J0aW5nIENTUyBITVIgZHVlIHRvIGNoYW5nZWQgY3NzLW1vZHVsZXMgbG9jYWxzLicpO1xuXG5cdFx0dXBkYXRlKG5ld0NvbnRlbnQpO1xuXHR9KTtcblxuXHRtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn0iLCJcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9janMuanMhLi9mbGFncy5jc3NcIik7XG5cbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuXG52YXIgdHJhbnNmb3JtO1xudmFyIGluc2VydEludG87XG5cblxuXG52YXIgb3B0aW9ucyA9IHtcImhtclwiOnRydWV9XG5cbm9wdGlvbnMudHJhbnNmb3JtID0gdHJhbnNmb3JtXG5vcHRpb25zLmluc2VydEludG8gPSB1bmRlZmluZWQ7XG5cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlcy5qc1wiKShjb250ZW50LCBvcHRpb25zKTtcblxuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG5cbmlmKG1vZHVsZS5ob3QpIHtcblx0bW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzIS4vZmxhZ3MuY3NzXCIsIGZ1bmN0aW9uKCkge1xuXHRcdHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9janMuanMhLi9mbGFncy5jc3NcIik7XG5cblx0XHRpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcblxuXHRcdHZhciBsb2NhbHMgPSAoZnVuY3Rpb24oYSwgYikge1xuXHRcdFx0dmFyIGtleSwgaWR4ID0gMDtcblxuXHRcdFx0Zm9yKGtleSBpbiBhKSB7XG5cdFx0XHRcdGlmKCFiIHx8IGFba2V5XSAhPT0gYltrZXldKSByZXR1cm4gZmFsc2U7XG5cdFx0XHRcdGlkeCsrO1xuXHRcdFx0fVxuXG5cdFx0XHRmb3Ioa2V5IGluIGIpIGlkeC0tO1xuXG5cdFx0XHRyZXR1cm4gaWR4ID09PSAwO1xuXHRcdH0oY29udGVudC5sb2NhbHMsIG5ld0NvbnRlbnQubG9jYWxzKSk7XG5cblx0XHRpZighbG9jYWxzKSB0aHJvdyBuZXcgRXJyb3IoJ0Fib3J0aW5nIENTUyBITVIgZHVlIHRvIGNoYW5nZWQgY3NzLW1vZHVsZXMgbG9jYWxzLicpO1xuXG5cdFx0dXBkYXRlKG5ld0NvbnRlbnQpO1xuXHR9KTtcblxuXHRtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn0iLCJcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9janMuanMhLi9zdHlsZS5jc3NcIik7XG5cbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuXG52YXIgdHJhbnNmb3JtO1xudmFyIGluc2VydEludG87XG5cblxuXG52YXIgb3B0aW9ucyA9IHtcImhtclwiOnRydWV9XG5cbm9wdGlvbnMudHJhbnNmb3JtID0gdHJhbnNmb3JtXG5vcHRpb25zLmluc2VydEludG8gPSB1bmRlZmluZWQ7XG5cbnZhciB1cGRhdGUgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlcy5qc1wiKShjb250ZW50LCBvcHRpb25zKTtcblxuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG5cbmlmKG1vZHVsZS5ob3QpIHtcblx0bW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzIS4vc3R5bGUuY3NzXCIsIGZ1bmN0aW9uKCkge1xuXHRcdHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9janMuanMhLi9zdHlsZS5jc3NcIik7XG5cblx0XHRpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcblxuXHRcdHZhciBsb2NhbHMgPSAoZnVuY3Rpb24oYSwgYikge1xuXHRcdFx0dmFyIGtleSwgaWR4ID0gMDtcblxuXHRcdFx0Zm9yKGtleSBpbiBhKSB7XG5cdFx0XHRcdGlmKCFiIHx8IGFba2V5XSAhPT0gYltrZXldKSByZXR1cm4gZmFsc2U7XG5cdFx0XHRcdGlkeCsrO1xuXHRcdFx0fVxuXG5cdFx0XHRmb3Ioa2V5IGluIGIpIGlkeC0tO1xuXG5cdFx0XHRyZXR1cm4gaWR4ID09PSAwO1xuXHRcdH0oY29udGVudC5sb2NhbHMsIG5ld0NvbnRlbnQubG9jYWxzKSk7XG5cblx0XHRpZighbG9jYWxzKSB0aHJvdyBuZXcgRXJyb3IoJ0Fib3J0aW5nIENTUyBITVIgZHVlIHRvIGNoYW5nZWQgY3NzLW1vZHVsZXMgbG9jYWxzLicpO1xuXG5cdFx0dXBkYXRlKG5ld0NvbnRlbnQpO1xuXHR9KTtcblxuXHRtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IFwiZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFHQUFBQUJBQ0FZQUFBRGxOSElPQUFBUU8wbEVRVlI0bk8yY2U3UVYxWDNIUDc4NVo4NTlBcUlpYUZBckppSktEWllJR2tnc0toWWZ5L2hJRkxOY1FZM1JXdzFwcWEzTHhxcExUWnBsazZqVnJHb00yamFhcUxHVjFRUWZrWWlQR210TmlCcEVSSlMzRjVBZ2ozdTU1NTZaUGZ2WFAvYWVPWFBPdmVDalVjNWFudCs2YzJkbXo4emVNNy92NzdmM2IrLzkzUWVhMHBTbU5LVXBUV2xLVXo2T0lydjdCUkFwbnZXTm0rSXJ2aktOb2UwdEgwbVJRUkJncldYY1lZZUZBS2pHdzRZTlk5dTJiUjlKK1ZsWkltSHhJeWx4MTlLNjREZkxPV3ZHVkk0Y080SURSdzFIUkxJdENBSkVxbmFTcHFmSEFLcWE3ZFBqOU54YW02V24xMFNFbFN0WEFyU205OTU0NDQzczJMR2pwcHlXbGhiQ01DUU1IVTZGUWdFUm9WQW9EUG9oU1pLZ3FpUkpBa0FjeDhSeFRLVlNxWG12am80T3VycTZBRm9iQVlBaXFpeFowYzJUaTE1bndxYyt3Wm5UeGpPMG80MGdDRkRWRElTOHd2T2dwR241NDd6eXJiVTFRSWdJMWxwWHRwZlhYMytkalJzM1pua01CblM5SWV5cS9QcDkvdnJJa1NPemIyOEVBQ2dWQThxVm1OZ2svSGJwV3BhdDJjUzVKeDdKaEVNK01lQkRZZURIcDJuNWo4eW5wY3JNSzhJRDRNb3ZsU2lYeThSeG5EMzNmcFNmeW5zRm9Wd3VVeXFWaU9LWWhnQkFVWHAyVkRER0lpSnM2U2x6KzBQUE0vSFEwWngzMGtTR2RyVFdLR1ZYVW4vUFlNQ2tYcEdYN2R1M0U4ZHhqY0lIVS81ZzUvbDhCenZQSzE5VjJiNTllM1pQUXdDQUtyRkppRTJDQklKSUFTVGhmMTlkeTlKVm16ai8xS1A0ekxqUndNNC9QcjAybUJmVWd6Y1lBR2w5L1VHdFA1OTMvWEU5RUttblFjTUFZRW1zeFNRV3JDQ0NBd0xZMGx2bTV2dWZZZExoQjNEaHFVY3hyTE1OR0tpVS9JZlhLM3N3U1J2S1ZJd3hHR095NTkrdjllK3N2TUc4SUMwSEdnWUFKWXFOOTRBQUVUSWc4SHA2OXVWVkxINWpBNWVjY1RTVER6L2dQV2VkOTRDODh2SUFQQU9NalNJa2lpaUlVQkFoQUFKeFJwQnVhWDdrenJOUHlENUZzL04wczZwWUlGRWxVVVdMUlpZQlI5TW9BR0RwNzY5UXJrUUVFaUJCQUFnaVFmYWxpdERmMzgvMWN4L2ptUEVIY1BIcFJ6TzBJNHNpQncwLzg4ZldXcElreVJTZkJ5QUIraXNWS0pjcEJNRUFBRkFsb003cjZyNGdiL2ZxRlk3SVFBQ3NoVUlodGFzR0FVQ1Z5Q1FZbzZoWVZ3V0pJS0tBdUQrcDJ1RXpMNjNpNWVVYnVPeExuK1dZOFFjT21tVWF3Z0paZjhKYW0rM3JBYUJTSVRBR1VhMWFmdDREVWpDOHZGczRFS2htb0lnSDBQcThiYVhTV0FDb3Q0dzRTVUFDUURNdkFBWlVId0NWTFZ2cC90YU5yT2w1eStlUnk4Ly90MVpCd0FMV2VpdEVrZloyVmk5YWxOMXZBVEVHb29oQXhGbDhFQ0NxVmVXL1R3QlF6VjRxRUNHeDFpbGZGUWxEMGhDZ0lRQkFMYkd4UkhFQzRwUXZOclYrLytFNUFDYjJyT2Y4alM4eEpJbnltZVNPSlZjWCsvUlVJUXFFTVVudTlnU1FPRVlpbDU5SXRkd1A2Z0Y1aTFDY0YrQUJwVlJxTEE4QUpZb1RZbU05QUU3NVdiWGpsVDhraVppMThTV08yYjdPUHpWWVR2bVRhb05JUGhhUFltenV6Z1FRM3dqWFczeDJubFpuL3BtZEFhRHBOZTlKNmIyaW1ua1VVZFJnQUtpU1dOY09pQVFaQUNDb1Y4YmtubTYrdG40UlE1Tks5U1B6V1F5U0o3NGV6cmJVQzFwYXlBZWh5VTNBQklObzVCUlhjTzBPQVVpZ0NQbXdsSnI5Z0Jlb2hrT0FvdGJYZ1Fva0RnUVZRL0lTTUx0aEFMQStETFZPK1VuZ3FnQVJocGdLWDl2NElsTzNyYWw5aENvSVdwZWU3bXNqSTVlcXFrZ2MxUUtRQUJvaDZudkM2ZkFGK0FhaXF2eE03NW92cmJaNjBpcmkxUkJXUFpDcWlFUVkvd0lOQVlDcTY0U1pKRUZzQUQ3Nm1kenpGbDlmLzF1R21VcW1oTUdzUDh1bk5sTy8wNXByaW9KSk1EbHdUQUtvUWExQmhWd0FEd1RwWFFMZWdlcTlJTTJxdGcvbVQxTHJ6MjFxRFdsZnJDRUF3Q2JaVUFCQmdXR0o0WklOaS9qOHRqVUVQcGF1VmtxN2x2VGU3RmdWUlJIMTBSQ0tqV1BpSEZ4UkRGRVVVOUNZRWw2eG1qYWVVczFNVWkrb2JRK3FJbFhyOTgrb2IvZ0RuNXhZaUswaDhxTVJEUUZBTVZERXhoUkVtYlNqbTBzM3ZzUndHeEdVd3RwSUpPZnFhZU1JcVpKZG1xOTVNeDBFT01YblBTSUNLcm1CNFA0NFpFZS9VQXFLU0VFb0lBU0JCeUpWZW02RDlManFBcGxuNUMwZEYxV3I5eWFMNnd0VVl1aXJoRUNEaklhMmhjS2VVdUhDL3BWTVNiYlFzYzl3UW5GREVnR0E1c0pBeVh0Q3RSWElERTl6OVgvYS9xWEtRREd4b1QrcVVHbXB6cjVWVERzOTVUYmFXb1pRbENKdHBZQ2dLRWdBUVdiMWtsV05HUkFwR0tuQy9RdUlyOGNDQmF0UThIdUpJVWtNT3lvUmZWRVo2R3NNQU1aSm1XTldQMHNRRmxoY0t0RWlVQkN0dXJocnhkenhZTEhuWUZJWHRMc2dSQ2xqNlVrTVlYdDdkcmtRZHJCb2FVQkxTNUhPanBDMjFnS0ZRQVpHT3Y4UFVZWEVLdVd5c0wzSFVBdzdnTTBOTVNmY0NZd0NPai9pa251QkRmNTR0NVhmQ0FBVWNYT3pIN1UzR3FEZkgrL3U4cHZTbEtZMHBTbE5hVXBUbXRLVXBqU2xLVTM1bUlnZ1VueDh4b3g0OUxYWFlqcUc4QzhQTHVGSDgxNGpzYnNlOVhyMXdiTlpObmtTU1U4UGh5OWR5clJMNXJOaGM5OHVuemw3K3NIOFpmQVNmZk4venQ1ZFhVeSsvUElRNE5uL2ZpWXVGZ3FZT3JiYWh5VnBXVk0vOS9rUVZmUHVUM3lJN3dLMEJpUDJZZGhycjlGeDFGSGNkT1dwL0gzWENUenhRamZsU2tKWUxCQVdIUisrVUJEUGp3ODQ5TkNER1hYMzNXZ2NNM3pzV082NHZwV2VIWkhueHp2d1VyNW5TeGd3N1pBTzJsNytINEwyS1F5NStpcFd2TlVOWE82WVZhcU1IWHNvVmgxWnd3MDlDeElFQkVIZ3FDSlUyUXJCVHFha2JEb1BuRHUzMXFMV3VqbUNqQ1lTc0dUSksrREdnSG8vVkEyL2l4U0I0a1hkUi9Eb0lVZlFjOWRkbEVhUFpzK1pNem5ydUQvaGtWK3Y1YkhuMXFFcE9TckhVRGhuK3NIMExGeUlMWmNaUG5NbVR5L3E1cDN0RlQ4UnJuNTgzUEw1STBjeFE5K2c5NDRuNkRqelROb21UbVRqdGdwZi91YXYwdkt4MXRKZjZTY3hTY1pJQ0x6eUhVL1VnMURIMmF5WmtNbFBRYXBpVlozaWxRSHJBd3JGd29EMUFidExpZ0RkbTNyNHdaT2JtVHpseTN6dW5SZFpmODAxakRqL2ZFNy84NGxNR0xzM2Q4NWJ4c2JOWmRKcERRbjhoeHVEZXFhdlNaUTRUaWRBWWU5aExYeDF5bkNHUFBZQVp1aFE5cjNxS3JTOWc0ZWZYY1h6aXpleGJQV1c3Q1V5UzFYcm1kSDRNWHgxOU1Sc1FnWElwV1dpempoVWJYVmlSQUh4YWVua2lUY0theDFCcWhIRVdZQXFXM3Nyekh0MkhjL3ZmUkJkTThmenp2MC9vZSs1NXpqb3dndjV4OHNtOGREQ2xmejhtZFdPMldXOUt1SzRGb0RFVlNISEg3VWZwOHR5ZHR6ekU0YWRleTV0a3lieDF0dTkvUENuditQTmRWdHBieTNXRXBkU0M4VXJYY1hQZ3VXbUhqTlFvSDVXUnZPcGFlMkVaS3lFRkpCMHFqS3RsaHBCdkFzcXNYSHN0TFViZTdubTRUSm5UUHNxeDIvK0RXdm56R0ZFVnhmbm5EaVppWWVONExZSFhxVjdrMXRMVlFPQXNldzV0SlhManQyVFliKzRCek5zR1B0OSs5dlEyY2xEQzFjd2IrRktLbEdNZWhaY1hvbE9JVlZxZ2FTc1JKRk10VUlLaXRUclB6dVhPa2FjQThSTkpUckdkVm8xRGx3ZnNMdkVlMEJLQzNHenh5YUJCNTVheTdNalBzbWNDeWJ3enIvUFpjZlRUek9tcTR1Yi8rWVk3di9sRzQ2VDc2c2dFZUd6RTBaeVN2a1ZlbjUwRjN0Y2NBRWRVNmV5N3UxZWJyN3RCWmF2MmVyclk2ZDRrMWpRcWdJY1VUYWQxM1hNZ25TZU5XZjg1RFV2MlF4NFNrS3JVaEVHNCtpazg4THFyOWV2RDloZDRqM0FFa1dPbXlsUzhPUW95NnIxdmN6NXp4M01QUEV5WnJ6OUhHdTZ1dGhuOW16T08zbUsrK2dvUWoyZjh2aGYveHR4YXl2NzMzd3pNblFvOXovK0p2Yzk5Z1pSbkdTS1Y2dW9KaFFEQWZJQVdLL0FsSlZNYlpYekhxcnJlbTVPcXZBMG40eFU1YXVpSkdrZ0QzanlqbE1ZTTJZTTRKWmlGZ3FGWEJUaTdDa0lEbWJvcks4QWJwR1ppRkF1bDdIbE11VnltYjJ1dmhxQUdORCtmazZidWkrblRkMTNwL3o4RmVlT1pNclU3d0JnYlVKL2Z6OEtGSUlDaFVMZ0k1OGdXeWtqUVZERFAwc0RnVlEwMTI5UmZBVGswNjI2T2o5SkxJbE5FRjltSTBnUjRQVlRUb0Vqam9CeW1VQ1ZncWVBWkFzVXlORkMwdlBjOGZKcDB3YlFBL09NTk90NU80a3FWZ1RhMmxqNi9QUFovVW1Ta05tLzVQb0IyWjZxWnd4Uy9vQUVId0ZsWkNxdHVvQWczZ01hQ0FEVDE0Y1lnNDBpSDdvNWluaCtnVU05UitOZGF3VWZjNmNOb1ZxYjVSMkVJZExlRG41bCtwYloxOU16OGMvUXZ2N3E2cFFncUhLQ2dpb1FxZFFEVUdNQWFjUlRzejdBWnF0VXBMMlZMWXQrOTM3MDlLRkpFV0RNbzQ5eThQanhwS3ZBNjZ1Z3dkWllpUWhMSjA0azZPeGs3Tk5QRDFnaWxOL3FxeUJWcFcveEt6RDlCTUN2VUlrTnhESHFlbDZvMkZ6c0w3a1FOSHVEbWpPdE53bXRwcnBEOWYwRWhiaElZOWkvQitDazJROHo3ckExOUpZTlNPQW80cjRsYkNrVnVmak1jVngwK2pqczFpMnN2KzQ2a3I0K0RwbzdGNjFVSUF3ellIcWVlSUx1RzI2Zzg2U1RtYmZmY2R3NWY0VVA5MUl3WFBUVDJWWms2YXVMczVld3FpNmNqUTBnam82V0tkMzNnTjludjBtenlFZ3pNTEErRG9yamh1bUllZTZ2QzBFalk0bmpkTFdLWmV5QmUvQ3pHMC9nNGpNUG8vZVhqL0htR1djUUhuNEUvM0hzYktmME9FYU5RVVM0NWFlTGFUdjJPQTUrNkNIc1crczQ4ZDZydU9lOFVldzNvb1BJNXhuSGxzaFV3OTFVRWx6VXBiRkJqY0hHTHJ5MWtmSE1XVmVPR2dQdnNtWDMrZWRzNVBQeWVXdHNJTEd1ekFZUUI0QzZFRFNPWFlkTVJManNuUEhjOTUwVDJMOGxablZYRjMrWU81Y3RWMzZQczEvWWg0Y1dybktQVlNyZ3c5Qi92bjhKZnpIN1VaWnNWdmEvOVZaR3pwbEQ4ZHEvWnU3SUY3bjRsREdZSkFYQkFaM3ZCMWlvZW9DdmlvZ05FanVGYVd6UUtQYnBidE82cmVaYUZHZlBTVjJlNlhIU1VCNlE0K2VQR3pPY2VkK2J6cVZmUEl5ZStiL2dqWk5QcHZTbkU3aHoycFY4OGJibHJPanU4UlpNMWZLQTJGaVdyZDdLYVhNZTU1OSsvREp0eDUvSUlZOC9qbG0zbHBOK2ZBWDN6OXFYL1VkMStuSXNhTFVXTnFxb1NiQ0p3UnFEbXNSNWdqRnU5VVMyVC93MXQyRjJrcFo3eG1aZWthQkpnaVlHTlJiVElCN2dlOEtHUUN5WGZ1bFF2bjdPcHluMjlyRDZvb3N3bXpieHp0VzNNdXZCOWF6WnVNdy9Jc1RHLzFwSkZFRVVJU0pFUmhHVUdPWDc5LzZlLzNwcUZiZiszVEY4K3ZiYjJmYklJNnk5WWpaM24zNEc5MDJZenIwTFZrSXVEby9WRW5uckRYRWpuNXFPZE9acHlOa2JEQzZhMjZlOTNpd1M4KzFCb29vMU1YRWplY0NuUm5md3d5c25jY0VwWTlqNjhIeVdmK0VMbEQ0emlidG5YTVBNMjk5ay9lWnlOaS9nTnRkSUJ5MHRTRXNMSWtLcEdOVGNzM3BETDJmKzdRSytmKzlpMnFiUDRKQW5ueVI1WnpNbi9ld2Fiajk3VDBhUENMT1hpSXNCWmJWRVFVQVNGaUFzRW9TaFd4OFFGZ25DSXVLM0lOdUhCS1dTMjhKZDNGY0svZlVRd2hBTmk4UnFxUlIyUHkwV3ZBZDg0NXl4REVtMnMrSzYyMmd2bDZuYzhBUCthc0VXMW05ZXoraVJqalFzZm1CTUVFYnUxZTZVUG5vMFFXY25Jc0wrSXp0Smg4RGNDS1N6c0hsUHJXTFIwai93RHhkTjRKTzMzTUxiQ3hhdzVidmY1YUpSby9pbWY0bTRwVVM1cFVoclJ5dFNhaVVzRkNsS1VQdFRBVkk3UXBxK0U3bXlYQWZRUFpCYWZ0b3BWQ0RHWXVLWS92NHlVVXZWQUhhbnVJN1lzbVg4NnM0N0dUcnBhRjdlYXdLLy85ZW5FQWtvN2NUWms2MGxGaXdvMHUxRDBOVUxGaENXbHcxNkw4Q0cxVEQ3bXBlWmNzUStUQnEzQnoyelpsR1lQeis3SHJhM3NTVGFRYXVVNkVnc3JSTFc5TUwvR0pMMnlNc21vaWZ1bzlqZTlrZk0vWU9MZk56NSthanUxaWxKK2RqejgzZnpwSHhUbXRLVXBqVGw0eXYvQjdaanpWRGs2K2dOQUFBQUFFbEZUa1N1UW1DQ1wiIiwiaW1wb3J0IG1vZGFscyBmcm9tIFwiLi9tYWluL21vZGFsc1wiO1xuaW1wb3J0IGxvZ2luUGFuZWwgZnJvbSBcIi4vbWFpbi9AbG9naW5QYW5lbFwiO1xuaW1wb3J0IGxvYWRpbmdQYW5lbCBmcm9tIFwiLi9tYWluL0Bsb2FkaW5nUGFuZWxcIjtcbmltcG9ydCBub3RpZmllciBmcm9tIFwiLi9tYWluL0Bub3RpZmllclwiO1xuaW1wb3J0IHRpbWVGb3JtYXR0ZXIgZnJvbSBcIi4uL3V0aWxzL3RpbWVGb3JtYXR0ZXJcIjtcbmltcG9ydCB3ZWJzb2NrZXRMaWIgZnJvbSBcIi4vd2Vic29ja2V0LmpzXCI7XG5pbXBvcnQgY29uZmlnIGZyb20gXCIuLi9jb25maWcuanNcIlxuaW1wb3J0IGRldmljZXNUYWIgZnJvbSBcIi4uL2NvbXBvbmVudHMvZGV2aWNlcy9AZGV2aWNlc1RhYlwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBhcHAge1xuXG5cdGNvbnN0cnVjdG9yKCkge1xuXG5cdFx0aWYgKHdpbmRvdy5hcHApIHtcblx0XHRcdHJldHVybiB3aW5kb3cuYXBwO1xuXHRcdH1cblxuXHRcdHdpbmRvdy5hcHAgPSB0aGlzO1xuXG5cdFx0dGhpcy5nbG9iYWxUaW1lc3RhbXA7XG5cdFx0dGhpcy5ub3RpZmllciA9IG5ldyBub3RpZmllcigpO1xuXHRcdHRoaXMud2Vic29ja2V0TGliID0gbmV3IHdlYnNvY2tldExpYigpO1xuXHRcdHRoaXMud2Vic29ja2V0Q2xpZW50O1xuXHRcdHRoaXMud2Vic29ja2V0Q29ubmVjdGlvbnMgPSAwO1xuXHRcdHRoaXMuZGV2aWNlcyA9IFtdO1xuXHRcdHRoaXMuZ3JvdXBzID0gW107XG5cblx0XHRtb2RhbHMoKTtcblxuXHRcdGlmICghbG9jYWxTdG9yYWdlLmxhbmd1YWdlKSB7XG5cdFx0XHRsb2NhbFN0b3JhZ2UubGFuZ3VhZ2UgPSBcInBsLVBMXCI7XG5cdFx0fVxuXG5cdFx0JChcImJvZHlcIikudG9vbHRpcCh7XG5cdFx0XHRzZWxlY3RvcjogXCJbZGF0YS10b2dnbGU9dG9vbHRpcF1cIlxuXHRcdH0pO1xuXG5cdFx0aWYgKHNlc3Npb25TdG9yYWdlLmFwaVRva2VuICYmIHNlc3Npb25TdG9yYWdlLmxvZ2luRmluaXNoZWQpIHtcblx0XHRcdHRoaXMubG9hZGluZ1BhbmVsID0gbmV3IGxvYWRpbmdQYW5lbCgpO1xuXHRcdFx0ZG9jdW1lbnQuYm9keS5jbGFzc05hbWUgPSBcImhvbGQtdHJhbnNpdGlvbiBsb2dpbi1wYWdlXCI7XG5cdFx0XHRkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHRoaXMubG9hZGluZ1BhbmVsLnJlbmRlcigpKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5tYWluTG9naW5QYW5lbCA9IG5ldyBsb2dpblBhbmVsKCk7XG5cdFx0XHRkb2N1bWVudC5ib2R5LmNsYXNzTmFtZSA9IFwiaG9sZC10cmFuc2l0aW9uIGxvZ2luLXBhZ2VcIjtcblx0XHRcdGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5tYWluTG9naW5QYW5lbC5yZW5kZXIoKSk7XG5cdFx0fVxuXG5cdFx0c2V0SW50ZXJ2YWwoKCkgPT4ge1xuXHRcdFx0aWYgKCF0aGlzLmdsb2JhbFRpbWVzdGFtcCkgcmV0dXJuO1xuXHRcdFx0dGhpcy5nbG9iYWxUaW1lc3RhbXAgKz0gZHVyYXRpb24oe1xuXHRcdFx0XHRzZWNvbmRzOiAxXG5cdFx0XHR9KTtcblx0XHRcdCQoXCJzdHJvbmcjc3luY1RpbWVTcGFuXCIpLnRleHQodGltZUZvcm1hdHRlcih0aGlzLmdsb2JhbFRpbWVzdGFtcCkpO1xuXHRcdH0sIGR1cmF0aW9uKHtcblx0XHRcdHNlY29uZHM6IDFcblx0XHR9KSk7XG5cblx0fVxuXG5cdHdlYnNvY2tldEluaXQoKSB7XG5cdFx0Y29uc29sZS5sb2coMSk7XG5cdFx0dGhpcy53ZWJzb2NrZXRDbGllbnQgPSBuZXcgV2ViU29ja2V0KGAke2NvbmZpZy53c3NFbmRwb2ludH0vJHtzZXNzaW9uU3RvcmFnZS5hcGlUb2tlbn1gKTtcblx0XHR0aGlzLndlYnNvY2tldENsaWVudC5vbm9wZW4gPSAoZXZlbnQpID0+IHRoaXMud2Vic29ja2V0TGliLm9uT3BlbihldmVudCk7XG5cdFx0dGhpcy53ZWJzb2NrZXRDbGllbnQub25jbG9zZSA9IChldmVudCkgPT4gdGhpcy53ZWJzb2NrZXRMaWIub25DbG9zZShldmVudCk7XG5cdFx0dGhpcy53ZWJzb2NrZXRDbGllbnQub25tZXNzYWdlID0gKG1lc3NhZ2UpID0+IHRoaXMud2Vic29ja2V0TGliLm9uTWVzc2FnZShtZXNzYWdlKTtcblx0XHRpZiAodGhpcy53ZWJzb2NrZXRDb25uZWN0aW9ucyA+IDApIHRoaXMud2Vic29ja2V0Q29ubmVjdGlvbnMrKztcblxuXHR9XG5cblx0c2VuZERhdGEoY29tbWFuZCwgdmFsdWUgPSB1bmRlZmluZWQpIHtcblxuXHRcdGlmICh0aGlzLndlYnNvY2tldENsaWVudC5yZWFkeVN0YXRlID09PSB0aGlzLndlYnNvY2tldENsaWVudC5PUEVOKSB7XG5cdFx0XHR0cnkge1xuXHRcdFx0XHR0aGlzLndlYnNvY2tldENsaWVudC5zZW5kKEpTT04uc3RyaW5naWZ5KHtcblx0XHRcdFx0XHRjb21tYW5kLFxuXHRcdFx0XHRcdHZhbHVlXG5cdFx0XHRcdH0pKTtcblx0XHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRcdGNvbnNvbGUuZXJyb3IoXCJ3ZWJzb2NrZXRDbGllbnQuc2VuZCgpIGZhaWx1cmVcIiwgZXJyb3IsIGNvbW1hbmQsIHZhbHVlKTtcblx0XHRcdH1cblx0XHR9XG5cblx0fVxuXG59IiwiaW1wb3J0IHtcblx0aHRtbFxufSBmcm9tIFwicmVkb21cIjtcbmltcG9ydCBib3ggZnJvbSBcIi4uL21haW4vYm94XCI7XG5pbXBvcnQgYXBwIGZyb20gXCIuLi9hcHBcIjtcbmltcG9ydCB0YWIgZnJvbSBcIi4uL21haW4vdGFiXCI7XG5pbXBvcnQgZWRpdGFibGVUZXh0SW5wdXQgZnJvbSBcIi4vZWRpdGFibGVUZXh0SW5wdXRcIjtcblxuXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIHtcblxuXHRjb25zdHJ1Y3RvcigpIHtcblxuXHRcdHRoaXMuYXBwID0gbmV3IGFwcCgpO1xuXHRcdHRoaXMuZGV2aWNlc0VkaXRvciA9IGh0bWwoXCJkaXYjZGV2aWNlc0VkaXRvclwiKTtcblx0XHR0aGlzLmdyb3Vwc0VkaXRvciA9IGh0bWwoXCJkaXYjZ3JvdXBzRWRpdG9yXCIpO1xuXG5cdFx0bGV0IG5ld0dyb3VwQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLXByaW1hcnkgYnRuLXNtIG1yXCIsIHtcblx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0fSxcblx0XHRcdGh0bWwoXCJpLmZhcyBmYS1maWxlIGkubXJcIiksXG5cdFx0XHRpMThuIGBOb3dhIGdydXBhYFxuXHRcdCk7XG5cblx0XHRsZXQgbmV3RGV2aWNlQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLXByaW1hcnkgYnRuLXNtIG1yXCIsIHtcblx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0fSxcblx0XHRcdGh0bWwoXCJpLmZhcyBmYS1maWxlIGkubXJcIiksXG5cdFx0XHRpMThuIGBOb3dlIHVyesSFZHplbmllYFxuXHRcdCk7XG5cblx0XHRuZXdEZXZpY2VCdXR0b24ub25jbGljayA9ICgpID0+IHtcblx0XHRcdGxldCBjb250aW51ZUJ1dHRvbiA9IGh0bWwoXCJidXR0b24uYnRuIGJ0bi1zdWNjZXNzIG1yXCIsIHtcblx0XHRcdFx0XHR0eXBlOiBcImJ1dHRvblwiXG5cdFx0XHRcdH0sXG5cdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS1jaGVjayBpLm1yXCIpLFxuXHRcdFx0XHRpMThuIGBVdHfDs3J6IHVyesSFZHplbmllYFxuXHRcdFx0KTtcblxuXHRcdFx0bGV0IGlucHV0TmFtZSA9IGh0bWwoXCJpbnB1dCNuYW1lLmZvcm0tY29udHJvbFwiLCB7XG5cdFx0XHRcdHR5cGU6IFwidGV4dFwiLFxuXHRcdFx0XHRwbGFjZWhvbGRlcjogYE5hendhIHVyesSFZHplbmlhYCxcblx0XHRcdFx0bWF4bGVuZ3RoOiAxNlxuXHRcdFx0fSk7XG5cblx0XHRcdGxldCBpbnB1dERlc2NyaXB0aW9uID0gaHRtbChcImlucHV0I2Rlc2MuZm9ybS1jb250cm9sXCIsIHtcblx0XHRcdFx0dHlwZTogXCJ0ZXh0XCIsXG5cdFx0XHRcdHBsYWNlaG9sZGVyOiBgT3BpcyB1cnrEhWR6ZW5pYWAsXG5cdFx0XHRcdG1heGxlbmd0aDogMjU2XG5cdFx0XHR9KTtcblxuXHRcdFx0bGV0IGlucHV0SFVJRCA9IGh0bWwoXCJpbnB1dCNodWlkLmZvcm0tY29udHJvbFwiLCB7XG5cdFx0XHRcdHR5cGU6IFwidGV4dFwiLFxuXHRcdFx0XHRwbGFjZWhvbGRlcjogYElkZW50eWZpa2F0b3Igc3ByesSZdG93eWAsXG5cdFx0XHRcdG1heGxlbmd0aDogMzJcblx0XHRcdH0pO1xuXG5cdFx0XHRsZXQgZ3JvdXBzT3B0aW9ucyA9IFtdO1xuXHRcdFx0dGhpcy5hcHAuZ3JvdXBzLmZvckVhY2goZ3JvdXAgPT4ge1xuXHRcdFx0XHRncm91cHNPcHRpb25zLnB1c2goXG5cdFx0XHRcdFx0aHRtbChcIm9wdGlvblwiLCB7XG5cdFx0XHRcdFx0XHR2YWx1ZTogZ3JvdXAuZ3JvdXBfaWRcblx0XHRcdFx0XHR9LCBncm91cC5ncm91cF9uYW1lKVxuXHRcdFx0XHQpO1xuXHRcdFx0fSlcblxuXG5cblx0XHRcdGxldCBpbnB1dEdyb3VwID0gaHRtbChcInNlbGVjdCNuZ3JvdXBhbWUuZm9ybS1jb250cm9sXCIsIGdyb3Vwc09wdGlvbnMpO1xuXG5cdFx0XHRjb250aW51ZUJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHR0aGlzLmFwcC5zZW5kRGF0YShcInVwZGF0ZURldmljZVwiLCB7XG5cdFx0XHRcdFx0ZGV2aWNlX2lkOiBudWxsLFxuXHRcdFx0XHRcdGRldmljZV9uYW1lOiBpbnB1dE5hbWUudmFsdWUsXG5cdFx0XHRcdFx0ZGV2aWNlX2Rlc2NyaXB0aW9uOiBpbnB1dERlc2NyaXB0aW9uLnZhbHVlLFxuXHRcdFx0XHRcdGRldmljZV9odWlkOiBpbnB1dEhVSUQudmFsdWUsXG5cdFx0XHRcdFx0Z3JvdXBfaWQ6IGlucHV0R3JvdXAudmFsdWVcblx0XHRcdFx0fSk7XG5cdFx0XHRcdCQoXCIjU21hbGxNb2RhbFwiKS5tb2RhbChcImhpZGVcIik7XG5cdFx0XHR9O1xuXG5cdFx0XHRsZXQgY2FuY2VsQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLWRhbmdlclwiLCB7XG5cdFx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0XHR9LFxuXHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtdGltZXMgaS5tclwiKSxcblx0XHRcdFx0aTE4biBgQW51bHVqYCk7XG5cblx0XHRcdGNhbmNlbEJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJoaWRlXCIpO1xuXHRcdFx0fTtcblxuXHRcdFx0JChcIiNTbWFsbE1vZGFsVGl0bGVcIikudGV4dChpMThuIGBUd29yemVuaWUgbm93ZWdvIHVyesSFZHplbmlhYCk7XG5cdFx0XHQkKFwiI1NtYWxsTW9kYWxGb290ZXJcIikuaGlkZSgpO1xuXHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5lbXB0eSgpO1xuXHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5hcHBlbmQoXG5cdFx0XHRcdGh0bWwoXCJkaXYuZm9ybS1ncm91cFwiLFxuXHRcdFx0XHRcdGh0bWwoXCJsYWJlbFwiLCB7XG5cdFx0XHRcdFx0XHRmb3I6IFwibmFtZVwiXG5cdFx0XHRcdFx0fSwgaTE4biBgTmF6d2EgdXJ6xIVkemVuaWFgKSxcblx0XHRcdFx0XHRpbnB1dE5hbWVcblx0XHRcdFx0KVxuXHRcdFx0KTtcblx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKFxuXHRcdFx0XHRodG1sKFwiZGl2LmZvcm0tZ3JvdXBcIixcblx0XHRcdFx0XHRodG1sKFwibGFiZWxcIiwge1xuXHRcdFx0XHRcdFx0Zm9yOiBcImRlc2NcIlxuXHRcdFx0XHRcdH0sIGkxOG4gYE9waXMgdXJ6xIVkemVuaWFgKSxcblx0XHRcdFx0XHRpbnB1dERlc2NyaXB0aW9uXG5cdFx0XHRcdClcblx0XHRcdCk7XG5cdFx0XHQkKFwiI1NtYWxsTW9kYWxCb2R5XCIpLmFwcGVuZChcblx0XHRcdFx0aHRtbChcImRpdi5mb3JtLWdyb3VwXCIsXG5cdFx0XHRcdFx0aHRtbChcImxhYmVsXCIsIHtcblx0XHRcdFx0XHRcdGZvcjogXCJodWlkXCJcblx0XHRcdFx0XHR9LCBpMThuIGBJZGVudHlmaWthdG9yIHNwcnrEmXRvd3lgKSxcblx0XHRcdFx0XHRpbnB1dEhVSURcblx0XHRcdFx0KVxuXHRcdFx0KTtcblx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKFxuXHRcdFx0XHRodG1sKFwiZGl2LmZvcm0tZ3JvdXBcIixcblx0XHRcdFx0XHRodG1sKFwibGFiZWxcIiwge1xuXHRcdFx0XHRcdFx0Zm9yOiBcImdyb3VwXCJcblx0XHRcdFx0XHR9LCBpMThuIGBHcnVwYWApLFxuXHRcdFx0XHRcdGlucHV0R3JvdXBcblx0XHRcdFx0KVxuXHRcdFx0KTtcblx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKGh0bWwoXCJkaXYuYXNrQnV0dG9uc1wiLCBjb250aW51ZUJ1dHRvbiwgY2FuY2VsQnV0dG9uKSk7XG5cdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJzaG93XCIpO1xuXHRcdH07XG5cblxuXG5cblxuXHRcdG5ld0dyb3VwQnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRsZXQgY29udGludWVCdXR0b24gPSBodG1sKFwiYnV0dG9uLmJ0biBidG4tc3VjY2VzcyBtclwiLCB7XG5cdFx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0XHR9LFxuXHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtY2hlY2sgaS5tclwiKSxcblx0XHRcdFx0aTE4biBgVXR3w7NyeiBncnVwxJlgXG5cdFx0XHQpO1xuXG5cdFx0XHRsZXQgaW5wdXROYW1lID0gaHRtbChcImlucHV0I25hbWUuZm9ybS1jb250cm9sXCIsIHtcblx0XHRcdFx0dHlwZTogXCJ0ZXh0XCIsXG5cdFx0XHRcdHBsYWNlaG9sZGVyOiBgTmF6d2EgZ3J1cHlgLFxuXHRcdFx0XHRtYXhsZW5ndGg6IDMyXG5cdFx0XHR9KTtcblxuXHRcdFx0bGV0IGlucHV0RGVzY3JpcHRpb24gPSBodG1sKFwiaW5wdXQjZGVzYy5mb3JtLWNvbnRyb2xcIiwge1xuXHRcdFx0XHR0eXBlOiBcInRleHRcIixcblx0XHRcdFx0cGxhY2Vob2xkZXI6IGBPcGlzIGdydXB5YCxcblx0XHRcdFx0bWF4bGVuZ3RoOiAyNTZcblx0XHRcdH0pO1xuXG5cdFx0XHRjb250aW51ZUJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHR0aGlzLmFwcC5zZW5kRGF0YShcInVwZGF0ZUdyb3VwXCIsIHtcblx0XHRcdFx0XHRncm91cF9pZDogbnVsbCxcblx0XHRcdFx0XHRncm91cF9uYW1lOiBpbnB1dE5hbWUudmFsdWUsXG5cdFx0XHRcdFx0Z3JvdXBfZGVzY3JpcHRpb246IGlucHV0RGVzY3JpcHRpb24udmFsdWVcblx0XHRcdFx0fSk7XG5cdFx0XHRcdCQoXCIjU21hbGxNb2RhbFwiKS5tb2RhbChcImhpZGVcIik7XG5cdFx0XHR9O1xuXG5cdFx0XHRsZXQgY2FuY2VsQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLWRhbmdlclwiLCB7XG5cdFx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0XHR9LFxuXHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtdGltZXMgaS5tclwiKSxcblx0XHRcdFx0aTE4biBgQW51bHVqYCk7XG5cblx0XHRcdGNhbmNlbEJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJoaWRlXCIpO1xuXHRcdFx0fTtcblxuXHRcdFx0JChcIiNTbWFsbE1vZGFsVGl0bGVcIikudGV4dChpMThuIGBUd29yemVuaWUgbm93ZWogZ3J1cHkgdXJ6xIVkemXFhGApO1xuXHRcdFx0JChcIiNTbWFsbE1vZGFsRm9vdGVyXCIpLmhpZGUoKTtcblx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuZW1wdHkoKTtcblx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKFxuXHRcdFx0XHRodG1sKFwiZGl2LmZvcm0tZ3JvdXBcIixcblx0XHRcdFx0XHRodG1sKFwibGFiZWxcIiwge1xuXHRcdFx0XHRcdFx0Zm9yOiBcIm5hbWVcIlxuXHRcdFx0XHRcdH0sIGkxOG4gYE5hendhIGdydXB5YCksXG5cdFx0XHRcdFx0aW5wdXROYW1lXG5cdFx0XHRcdClcblx0XHRcdCk7XG5cdFx0XHQkKFwiI1NtYWxsTW9kYWxCb2R5XCIpLmFwcGVuZChcblx0XHRcdFx0aHRtbChcImRpdi5mb3JtLWdyb3VwXCIsXG5cdFx0XHRcdFx0aHRtbChcImxhYmVsXCIsIHtcblx0XHRcdFx0XHRcdGZvcjogXCJkZXNjXCJcblx0XHRcdFx0XHR9LCBpMThuIGBPcGlzIGdydXB5YCksXG5cdFx0XHRcdFx0aW5wdXREZXNjcmlwdGlvblxuXHRcdFx0XHQpXG5cdFx0XHQpO1xuXHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5hcHBlbmQoaHRtbChcImRpdi5hc2tCdXR0b25zXCIsIGNvbnRpbnVlQnV0dG9uLCBjYW5jZWxCdXR0b24pKTtcblx0XHRcdCQoXCIjU21hbGxNb2RhbFwiKS5tb2RhbChcInNob3dcIik7XG5cdFx0fTtcblxuXHRcdHJldHVybiBbXG5cdFx0XHRodG1sKFwiZGl2LnJvd1wiLFxuXHRcdFx0XHRodG1sKFwiZGl2LmNvbC14cy0xMlwiLFxuXHRcdFx0XHRcdG5ldyBib3goXCJwcmltYXJ5XCIsIGkxOG4gYFVyesSFZHplbmlhYCwgW25ld0RldmljZUJ1dHRvbl0sIHRoaXMuZGV2aWNlc0VkaXRvcilcblx0XHRcdFx0KVxuXHRcdFx0KSxcblx0XHRcdGh0bWwoXCJkaXYucm93XCIsXG5cdFx0XHRcdGh0bWwoXCJkaXYuY29sLXhzLTEyXCIsXG5cdFx0XHRcdFx0bmV3IGJveChcInByaW1hcnlcIiwgaTE4biBgR3J1cHlgLCBbbmV3R3JvdXBCdXR0b25dLCB0aGlzLmdyb3Vwc0VkaXRvcilcblx0XHRcdFx0KVxuXHRcdFx0KVxuXHRcdF07XG5cblx0fVxuXG59IiwiaW1wb3J0IHtcblx0aHRtbFxufSBmcm9tIFwicmVkb21cIjtcbmltcG9ydCBkb3RQcm9wIGZyb20gXCJkb3QtcHJvcFwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyB7XG5cblx0Y29uc3RydWN0b3IocGF0dGVybiwgcGF0dGVybkhlbHAsIHBsYWNlaG9sZGVyLCBhZmZlY3RlZE9iamVjdCwgYWZmZWN0ZWRQcm9wZXJ0eSwgZGVmYXVsdFN0YXRlID0gXCJcIikge1xuXG5cdFx0dGhpcy5hZmZlY3RlZE9iamVjdCA9IGFmZmVjdGVkT2JqZWN0O1xuXHRcdHRoaXMuYWZmZWN0ZWRQcm9wZXJ0eSA9IGFmZmVjdGVkUHJvcGVydHk7XG5cdFx0dGhpcy5wYXR0ZXJuSGVscCA9IHBhdHRlcm5IZWxwO1xuXHRcdHRoaXMucGF0dGVybiA9IHBhdHRlcm47XG5cblx0XHRpZiAoZG90UHJvcC5nZXQodGhpcy5hZmZlY3RlZE9iamVjdCwgdGhpcy5hZmZlY3RlZFByb3BlcnR5KSA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRkb3RQcm9wLnNldCh0aGlzLmFmZmVjdGVkT2JqZWN0LCB0aGlzLmFmZmVjdGVkUHJvcGVydHksIGRlZmF1bHRTdGF0ZSk7XG5cdFx0fVxuXG5cdFx0dGhpcy5kb21FbGVtZW50ID0gaHRtbChcImlucHV0LmZvcm0tY29udHJvbFwiLCB7XG5cdFx0XHRwbGFjZWhvbGRlcixcblx0XHRcdHRpdGxlOiBcIlwiLFxuXHRcdFx0dHlwZTogXCJ0ZXh0XCIsXG5cdFx0XHR2YWx1ZTogZG90UHJvcC5nZXQodGhpcy5hZmZlY3RlZE9iamVjdCwgdGhpcy5hZmZlY3RlZFByb3BlcnR5KVxuXHRcdH0pO1xuXG5cdFx0dGhpcy5kb21FbGVtZW50Lm9ua2V5dXAgPSAoZXZlbnQpID0+IHtcblx0XHRcdGxldCBpc1ZhbGlkID0gdHJ1ZTtcblxuXHRcdFx0aWYgKHRoaXMucGF0dGVybiAmJiB0aGlzLnBhdHRlcm4gaW5zdGFuY2VvZiBSZWdFeHApIHtcblx0XHRcdFx0aWYgKCF0aGlzLnBhdHRlcm4udGVzdChldmVudC50YXJnZXQudmFsdWUpKSB7XG5cdFx0XHRcdFx0aXNWYWxpZCA9IGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdGlmIChpc1ZhbGlkKSB7XG5cdFx0XHRcdGRvdFByb3Auc2V0KHRoaXMuYWZmZWN0ZWRPYmplY3QsIHRoaXMuYWZmZWN0ZWRQcm9wZXJ0eSwgZXZlbnQudGFyZ2V0LnZhbHVlKTtcblx0XHRcdFx0JCh0aGlzLmRvbUVsZW1lbnQpLnRvb2x0aXAoXCJkZXN0cm95XCIpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0JCh0aGlzLmRvbUVsZW1lbnQpLnRvb2x0aXAoe1xuXHRcdFx0XHRcdHRyaWdnZXI6IFwibWFudWFsXCIsXG5cdFx0XHRcdFx0cGxhY2VtZW50OiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdHRpdGxlOiB0aGlzLnBhdHRlcm5IZWxwXG5cdFx0XHRcdH0pO1xuXHRcdFx0XHQkKHRoaXMuZG9tRWxlbWVudCkudG9vbHRpcChcInNob3dcIik7XG5cdFx0XHR9XG5cdFx0fTtcblxuXHRcdHJldHVybiB0aGlzLmRvbUVsZW1lbnQ7XG5cblx0fVxuXG59IiwiaW1wb3J0IHtcblx0aHRtbFxufSBmcm9tIFwicmVkb21cIjtcbmltcG9ydCBhcHAgZnJvbSBcIi4uL2FwcFwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyB7XG5cdGNvbnN0cnVjdG9yKCkge1xuXG5cdFx0dGhpcy5hcHAgPSBuZXcgYXBwKCk7XG5cblx0XHRpZiAoc2Vzc2lvblN0b3JhZ2UuYXBpVG9rZW4pIHtcblx0XHRcdHRoaXMuYXBwLndlYnNvY2tldEluaXQoKTtcblx0XHR9XG5cblx0fVxuXG5cdHJlbmRlcigpIHtcblxuXHRcdHJldHVybiBodG1sKFwiZGl2LmxvY2tzY3JlZW4td3JhcHBlclwiLFxuXHRcdFx0aHRtbChcImRpdi5sb2Nrc2NyZWVuLWxvZ29cIixcblx0XHRcdFx0aHRtbChcInN0cm9uZ1wiLCBpMThuIGBnbklPVCAyLjBgKSxcblx0XHRcdFx0aHRtbChcInBcIixcblx0XHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtc3Bpbm5lciBmYS1zcGluIGZhLWZ3IGZhLTR4XCIsIHtcblx0XHRcdFx0XHRcdHN0eWxlOiBcIm1hcmdpbi10b3A6IDE1cHg7XCJcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHQpXG5cdFx0XHQpXG5cdFx0KTtcblxuXHR9XG5cbn0iLCJpbXBvcnQgc2hhMSBmcm9tIFwianMtc2hhMVwiO1xuaW1wb3J0IHtcblx0aHRtbFxufSBmcm9tIFwicmVkb21cIjtcbmltcG9ydCBjb25maWcgZnJvbSBcIi4uLy4uL2NvbmZpZ1wiO1xuaW1wb3J0IGpzb25Eb3dubG9hZGVyIGZyb20gXCIuLi8uLi91dGlscy9qc29uRG93bmxvYWRlclwiO1xuaW1wb3J0IHJvdDEzIGZyb20gXCIuLi8uLi91dGlscy9yb3QxM1wiO1xuaW1wb3J0IGFwcCBmcm9tIFwiLi4vYXBwXCI7XG5pbXBvcnQgbG9hZGluZ1BhbmVsIGZyb20gXCIuL0Bsb2FkaW5nUGFuZWxcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3Mge1xuXG5cdGNvbnN0cnVjdG9yKCkge31cblxuXHRjaGFuZ2VMYW5ndWFnZShjb2RlKSB7XG5cblx0XHRsb2NhbFN0b3JhZ2UubGFuZ3VhZ2UgPSBjb2RlO1xuXHRcdGxvY2F0aW9uLnJlbG9hZCgpO1xuXG5cdH1cblxuXHRsb2dJbihwcm9jZXNzKSB7XG5cblx0XHRpZiAoIXByb2Nlc3MpIHJldHVybjtcblxuXHRcdGxldCB1c2VybmFtZSA9ICQoXCIjbG9naW5Gb3JtVXNlcm5hbWVcIikudmFsKCkudHJpbSgpLnRvTG93ZXJDYXNlKCk7XG5cdFx0bGV0IHBhc3N3b3JkID0gcm90MTMoc2hhMSh1c2VybmFtZSArICQoXCIjbG9naW5Gb3JtUGFzc3dvcmRcIikudmFsKCkpKTtcblx0XHRsZXQgcmVnaW9uID0gJChcIiNsb2dpbkZvcm1SZWdpb25cIikuZmluZChcIjpzZWxlY3RlZFwiKS52YWwoKTtcblxuXHRcdGlmICh1c2VybmFtZS5sZW5ndGggPT0gMCB8fCAkKFwiI2xvZ2luRm9ybVBhc3N3b3JkXCIpLnZhbCgpLmxlbmd0aCA9PSAwKSB7XG5cdFx0XHQkKFwiI2Vycm9yTW9kYWxUZXh0XCIpLnRleHQoaTE4biBgUG9kYWogbmF6d8SZIHXFvHl0a293bmlrYSBpIGhhc8WCbyFgKTtcblx0XHRcdCQoXCIjZXJyb3JNb2RhbFwiKS5tb2RhbCgpO1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdHNlc3Npb25TdG9yYWdlLmNsZWFyKCk7XG5cblx0XHQkKFwiI21haW5Mb2dpbkJ1dHRvblwiKS5hdHRyKFwiZGlzYWJsZWRcIiwgXCJkaXNhYmxlZFwiKTtcblx0XHQkKFwiI21haW5Mb2dpbkJ1dHRvbiBpXCIpLnNob3coMTAwLCAoKSA9PiB7XG5cdFx0XHRqc29uRG93bmxvYWRlcihmYWxzZSwgY29uZmlnLmFwaUVuZHBvaW50ICsgalF1ZXJ5LnBhcmFtKHtcblx0XHRcdFx0bWV0aG9kOiBcImF1dGhcIixcblx0XHRcdFx0dXNlcm5hbWUsXG5cdFx0XHRcdHBhc3N3b3JkXG5cdFx0XHR9KSkudGhlbigocmVzcG9uc2UpID0+IHtcblx0XHRcdFx0aWYgKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcblx0XHRcdFx0XHRzZXNzaW9uU3RvcmFnZS5hcGlUb2tlbiA9IHJlc3BvbnNlLm1lc3NhZ2U7XG5cdFx0XHRcdFx0c2Vzc2lvblN0b3JhZ2UubG9naW5UaW1lID0gbmV3IERhdGUoKTtcblx0XHRcdFx0XHRzZXNzaW9uU3RvcmFnZS5yZWdpb25OYW1lID0gcmVnaW9uO1xuXHRcdFx0XHRcdHNlc3Npb25TdG9yYWdlLmxvY2FsVXNlcm5hbWUgPSB1c2VybmFtZTtcblxuXHRcdFx0XHRcdCQoXCJkaXYubG9naW4tYm94XCIpLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0XHRoZWlnaHQ6IFwidG9nZ2xlXCJcblx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHQxMDAwXG5cdFx0XHRcdFx0KS5wcm9taXNlKCkuZG9uZSgoKSA9PiB7XG5cdFx0XHRcdFx0XHR2YXIgbWFpbmxvYWRpbmdQYW5lbCA9IG5ldyBsb2FkaW5nUGFuZWwoKTtcblx0XHRcdFx0XHRcdCQoXCJkaXYubG9naW4tYm94XCIpLnJlbW92ZSgpO1xuXHRcdFx0XHRcdFx0ZG9jdW1lbnQuYm9keS5jbGFzc05hbWUgPSBcImhvbGQtdHJhbnNpdGlvbiBsb2Nrc2NyZWVuXCI7XG5cdFx0XHRcdFx0XHRkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKG1haW5sb2FkaW5nUGFuZWwucmVuZGVyKCkpO1xuXHRcdFx0XHRcdFx0c2Vzc2lvblN0b3JhZ2UubG9naW5GaW5pc2hlZCA9IHRydWU7XG5cdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRzd2l0Y2ggKHJlc3BvbnNlLnJlc3BDb2RlKSB7XG5cdFx0XHRcdFx0XHRjYXNlIDE0OlxuXHRcdFx0XHRcdFx0XHQkKFwiI2Vycm9yTW9kYWxUZXh0XCIpLnRleHQoaTE4biBgUG9kYW55IGxvZ2luIGkvbHViIGhhc8WCbyBuaWUgc8SFIHBvcHJhd25lLiBad3LDs8SHIHV3YWfEmSBuYSB3aWVsa2/Fm8SHIGxpdGVyIHcgaGHFm2xlIWApO1xuXHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0XHRcdCQoXCIjZXJyb3JNb2RhbFRleHRcIikudGV4dChpMThuIGBMb2dvd2FuaWUgbmlldWRhbmUuIFd5c3TEhXBpxYIgbmllem5hbnkgYsWCxIVkLmApO1xuXHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0JChcIiNlcnJvck1vZGFsXCIpLm1vZGFsKCk7XG5cdFx0XHRcdFx0JChcIiNtYWluTG9naW5CdXR0b25cIikucmVtb3ZlQXR0cihcImRpc2FibGVkXCIpO1xuXHRcdFx0XHRcdCQoXCIjbWFpbkxvZ2luQnV0dG9uIGlcIikuaGlkZSgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9KTtcblxuXHR9XG5cblx0cmVuZGVyKCkge1xuXG5cdFx0bGV0IHVzZXJOYW1lSW5wdXQgPSBodG1sKFwiaW5wdXQjbG9naW5Gb3JtVXNlcm5hbWUuZm9ybS1jb250cm9sXCIsIHtcblx0XHRcdHBsYWNlaG9sZGVyOiBpMThuIGBOYXp3YSB1xbx5dGtvd25pa2FgLFxuXHRcdFx0cmVxdWlyZWQ6IHRydWUsXG5cdFx0XHRhdXRvZm9jdXM6IHRydWVcblx0XHR9KTtcblx0XHR1c2VyTmFtZUlucHV0Lm9ua2V5dXAgPSAoZXZlbnQpID0+IHRoaXMubG9nSW4oZXZlbnQua2V5Q29kZSA9PSAxMyk7XG5cblx0XHRsZXQgcGFzc3dvcmRJbnB1dCA9IGh0bWwoXCJpbnB1dCNsb2dpbkZvcm1QYXNzd29yZC5mb3JtLWNvbnRyb2xcIiwge1xuXHRcdFx0cGxhY2Vob2xkZXI6IGkxOG4gYEhhc8WCb2AsXG5cdFx0XHR0eXBlOiBcInBhc3N3b3JkXCIsXG5cdFx0XHRyZXF1aXJlZDogdHJ1ZVxuXHRcdH0pO1xuXHRcdHBhc3N3b3JkSW5wdXQub25rZXl1cCA9IChldmVudCkgPT4gdGhpcy5sb2dJbihldmVudC5rZXlDb2RlID09IDEzKTtcblxuXHRcdGxldCBsb2dpbkJ1dHRvbiA9IGh0bWwoXCJidXR0b24uYnRuIGJ0bi1wcmltYXJ5IGJ0bi1ibG9jayNtYWluTG9naW5CdXR0b25cIiwge1xuXHRcdFx0dHlwZTogXCJidXR0b25cIixcblx0XHR9LCBodG1sKFwiaS5mYXMgZmEtc3Bpbm5lciBmYS1zcGluIGZhLWZ3XCIsIHtcblx0XHRcdHN0eWxlOiBcImRpc3BsYXk6IG5vbmVcIlxuXHRcdH0pLCBpMThuIGBaYWxvZ3VqIHNpxJlgKTtcblx0XHRsb2dpbkJ1dHRvbi5vbmNsaWNrID0gKCkgPT4gdGhpcy5sb2dJbih0cnVlKTtcblxuXHRcdGxldCBmbGFncyA9IFtdO1xuXHRcdGNvbmZpZy5hdmFpbGFibGVMYW5ncy5maWx0ZXIobGFuZyA9PiBsYW5nLmNvZGUgIT0gbG9jYWxTdG9yYWdlLmxhbmd1YWdlKS5mb3JFYWNoKGVsZW1lbnQgPT4ge1xuXHRcdFx0bGV0IGZsYWcgPSBodG1sKGBkaXYuZmxhZyBmbGFnLSR7ZWxlbWVudC5mbGFnfWAsIHtcblx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcblx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcInRpdGxlXCI6IGVsZW1lbnQubmFtZVxuXHRcdFx0fSk7XG5cdFx0XHRmbGFnLm9uY2xpY2sgPSAoKSA9PiB0aGlzLmNoYW5nZUxhbmd1YWdlKGVsZW1lbnQuY29kZSk7XG5cdFx0XHRmbGFncy5wdXNoKGZsYWcpO1xuXHRcdH0pO1xuXG5cdFx0cmV0dXJuIGh0bWwoXCJkaXYubG9naW4tYm94XCIsXG5cdFx0XHRodG1sKFwiZGl2LmxvZ2luLWxvZ29cIiwgaTE4biBgZ25JT1QgMi4wYCksXG5cdFx0XHRodG1sKFwiZGl2LmxvZ2luLWJveC1ib2R5XCIsXG5cdFx0XHRcdGh0bWwoXCJkaXYucm93XCIsXG5cdFx0XHRcdFx0aHRtbChcImRpdi5jb2wteHMtMTIgdGV4dC1jZW50ZXIjbGVmdExvZ2luUGFuZWxcIixcblx0XHRcdFx0XHRcdGh0bWwoXCJoM1wiLCBpMThuIGBaYWxvZ3VqIHNpxJlgKSxcblx0XHRcdFx0XHRcdGh0bWwoXCJmb3JtXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRhdXRvY29tcGxldGU6IFwib25cIlxuXHRcdFx0XHRcdFx0XHR9LFxuXG5cdFx0XHRcdFx0XHRcdGh0bWwoXCJkaXYuZm9ybS1ncm91cCBoYXMtZmVlZGJhY2tcIixcblx0XHRcdFx0XHRcdFx0XHR1c2VyTmFtZUlucHV0LFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJzcGFuLmdseXBoaWNvbiBnbHlwaGljb24tdXNlciBmb3JtLWNvbnRyb2wtZmVlZGJhY2tcIilcblx0XHRcdFx0XHRcdFx0KSxcblxuXHRcdFx0XHRcdFx0XHRodG1sKFwiZGl2LmZvcm0tZ3JvdXAgaGFzLWZlZWRiYWNrXCIsXG5cdFx0XHRcdFx0XHRcdFx0cGFzc3dvcmRJbnB1dCxcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwic3Bhbi5nbHlwaGljb24gZ2x5cGhpY29uLWxvY2sgZm9ybS1jb250cm9sLWZlZWRiYWNrXCIpXG5cdFx0XHRcdFx0XHRcdCksXG5cblx0XHRcdFx0XHRcdFx0aHRtbChcImRpdi5mb3JtLWdyb3VwXCIsXG5cdFx0XHRcdFx0XHRcdFx0bG9naW5CdXR0b25cblx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0aHRtbChcImRpdi5jb2wtbWQtNiBoaWRkZW4tbWQgaGlkZGVuLWxnXCIsXG5cdFx0XHRcdFx0XHRodG1sKFwiaHJcIilcblx0XHRcdFx0XHQpXG5cdFx0XHRcdCksXG5cdFx0XHRcdGh0bWwoXCJkaXYucm93XCIsXG5cdFx0XHRcdFx0aHRtbChcImRpdi5jb2wteHMtMTIgdGV4dC1jZW50ZXJcIixcblx0XHRcdFx0XHRcdFtcblx0XHRcdFx0XHRcdFx0aHRtbChcImhyXCIpLFxuXHRcdFx0XHRcdFx0XHRodG1sKFwicFwiLCBpMThuIGBabWllxYQgasSZenlrOmApLFxuXHRcdFx0XHRcdFx0XHRmbGFncyxcblx0XHRcdFx0XHRcdFx0aHRtbChcImhyXCIpLFxuXHRcdFx0XHRcdFx0XHRodG1sKFwic3BhblwiLCBcIk1hZGUgd2l0aCDinaTvuI8gYnkgZ25JT1QgMi4wIERldmVsb3BlcnMgVGVhbVwiKVxuXHRcdFx0XHRcdFx0XVxuXHRcdFx0XHRcdClcblx0XHRcdFx0KVxuXHRcdFx0KVxuXHRcdCk7XG5cblx0fVxuXG59IiwiaW1wb3J0IHtcblx0aHRtbFxufSBmcm9tIFwicmVkb21cIjtcbmltcG9ydCB7XG5cdENvbnRleHRNZW51XG59IGZyb20gXCJqcXVlcnktY29udGV4dG1lbnUvXCI7XG5cbmltcG9ydCB0YWIgZnJvbSBcIi4vdGFiXCI7XG5pbXBvcnQgZGV2aWNlc1RhYiBmcm9tIFwiLi4vZGV2aWNlcy9AZGV2aWNlc1RhYlwiO1xuaW1wb3J0IGpzb25Eb3dubG9hZGVyIGZyb20gXCIuLi8uLi91dGlscy9qc29uRG93bmxvYWRlclwiO1xuaW1wb3J0IGNvbmZpZyBmcm9tIFwiLi4vLi4vY29uZmlnXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIHtcblxuXHRjb25zdHJ1Y3RvcigpIHtcblxuXG5cdH1cblxuXHRyZW5kZXIoKSB7XG5cblx0XHRsZXQgbG9nT3V0QnV0dG9uID0gaHRtbChcImEuaGlkZGVuLXNtXCIsIHtcblx0XHRcdFx0c3R5bGU6IFwiY3Vyc29yOiBwb2ludGVyOyBtYXJnaW46IDNweDtcIlxuXHRcdFx0fSxcblx0XHRcdGh0bWwoXCJzcGFuLmJhZGdlIGJnLXB1cnBsZVwiLCB7XG5cdFx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XCJ0aXRsZVwiOiBpMThuIGBXeWxvZ3VqYFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtc2lnbi1vdXQtYWx0IG1yXCIpLFxuXHRcdFx0XHRpMThuIGBXeWxvZ3VqYFxuXHRcdFx0KVxuXHRcdCk7XG5cblx0XHRsb2dPdXRCdXR0b24ub25jbGljayA9ICgpID0+IHtcblx0XHRcdHNlc3Npb25TdG9yYWdlLmNsZWFyKCk7XG5cdFx0XHRsb2NhdGlvbi5yZWxvYWQoKTtcblx0XHR9O1xuXG5cdFx0dGhpcy5kZXZpY2VzVGFiID0gbmV3IGRldmljZXNUYWIoKTtcblxuXHRcdHJldHVybiBodG1sKFwiZGl2LndyYXBwZXJcIixcblx0XHRcdGh0bWwoXCJoZWFkZXIubWFpbi1oZWFkZXJcIixcblx0XHRcdFx0aHRtbChcIm5hdi5uYXZiYXIgbmF2YmFyLXN0YXRpYy10b3BcIixcblx0XHRcdFx0XHRodG1sKFwiZGl2LmNvbnRhaW5lclwiLFxuXHRcdFx0XHRcdFx0aHRtbChcImRpdi5uYXZiYXItaGVhZGVyXCIsXG5cdFx0XHRcdFx0XHRcdGh0bWwoXCJzdHJvbmcubmF2YmFyLWJyYW5kIGhpZGRlbi1tZCBoaWRkZW4tc20gaGlkZGVuLXhzXCIsIGkxOG4gYGduSU9UIDIuMGApLFxuXHRcdFx0XHRcdFx0XHRodG1sKFwiYnV0dG9uLm5hdmJhci10b2dnbGUgY29sbGFwc2VkXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtdG9nZ2xlXCI6IFwiY29sbGFwc2VcIixcblx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10YXJnZXRcIjogXCIjbmF2YmFyLWNvbGxhcHNlXCJcblx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS1iYXJzXCIpXG5cdFx0XHRcdFx0XHRcdClcblx0XHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0XHRodG1sKFwiZGl2LmNvbGxhcHNlIG5hdmJhci1jb2xsYXBzZSBwdWxsLWxlZnQjbmF2YmFyLWNvbGxhcHNlXCIsXG5cdFx0XHRcdFx0XHRcdGh0bWwoXCJ1bC5uYXYgbmF2YmFyLW5hdlwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRyb2xlOiBcInRhYmxpc3RcIlxuXHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0aHRtbChcImxpLmFjdGl2ZVwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImEuYmctbGlnaHQtYmx1ZVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHJlZjogXCIjZ25pb3RUYWJEZXZpY2VzXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0cm9sZTogXCJ0YWJcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtdHlwZVwiOiBcIm1haW5NZW51XCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcInRhYlwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS1yb2xlXCI6IFwiZGV2aWNlc1wiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHN0eWxlOiBcImRpc3BsYXk6IG5vbmVcIlxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtdHJhZmZpYy1saWdodCBtclwiKSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0aTE4biBgVXJ6xIVkemVuaWFgXG5cdFx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdFx0KSxcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwibGlcIixcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJhXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzdHlsZTogXCJiYWNrZ3JvdW5kLWNvbG9yOiAjMjIyZDMyICFpbXBvcnRhbnQ7IGN1cnNvcjogZGVmYXVsdDtcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRocmVmOiBcImphdmFzY3JpcHQ6O1wiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJhLmhpZGRlbi1zbVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzdHlsZTogXCJjdXJzb3I6IGRlZmF1bHQ7IG1hcmdpbjogM3B4O1wiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwic3Bhbi5iYWRnZSBiZy1yZWQjd2Vic29ja2V0U3RhdHVzSW5kaWNhdG9yQmFja2dyb3VuZFwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJ0aXRsZVwiOiBpMThuIGBTdGF0dXMgcG/FgsSFY3plbmlhYFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS1wbHVnIG1yXCIpLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImkuZmFzIGZhLXRpbWVzI3dlYnNvY2tldFN0YXR1c0luZGljYXRvckljb25cIilcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJhLmhpZGRlbi1zbVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzdHlsZTogXCJjdXJzb3I6IGRlZmF1bHQ7IG1hcmdpbjogM3B4O1wiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwic3Bhbi5iYWRnZSBiZy15ZWxsb3dcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgTmF6d2EgdcW8eXRrb3duaWthYFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS11c2VyIG1yXCIpLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcInNwYW4jZGlzcGF0Y2hlcnNPbmxpbmVDb3VudGVyXCIsIHNlc3Npb25TdG9yYWdlLmxvY2FsVXNlcm5hbWUpXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdFx0XHQpLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRsb2dPdXRCdXR0b25cblx0XHRcdFx0XHRcdFx0XHRcdClcblx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdClcblx0XHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0XHRodG1sKFwiZGl2Lm5hdmJhci1jdXN0b20tbWVudSNtb3ZlQ3VzdG9tTWVudVRvU2hvd0JhcnNcIiwge1xuXHRcdFx0XHRcdFx0XHRcdHN0eWxlOiBcInBvc2l0aW9uOiBhYnNvbHV0ZTtcIlxuXHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRodG1sKFwidWwubmF2IG5hdmJhci1uYXYgbmF2YmFyLW5vY29sb3JzXCIsXG5cdFx0XHRcdFx0XHRcdFx0aHRtbChcImxpLmRyb3Bkb3duIG5vdGlmaWNhdGlvbnMtbWVudSBoaWRkZW4tc21cIixcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJhLmRyb3Bkb3duLXRvZ2dsZVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcImRyb3Bkb3duXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0c3R5bGU6IFwiY3Vyc29yOiBwb2ludGVyXCJcblx0XHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImkuZmFzIGZhLWJlbGxcIiksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJzcGFuLmxhYmVsIGxhYmVsLXdhcm5pbmdcIiwgMTApXG5cdFx0XHRcdFx0XHRcdFx0XHQpLFxuXHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcInVsLmRyb3Bkb3duLW1lbnVcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImxpLmhlYWRlclwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGkxOG4gYFlvdSBoYXZlIDEwIG5vdGlmaWNhdGlvbnNgXG5cdFx0XHRcdFx0XHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJsaVwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJ1bC5tZW51XCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwibGlcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImFcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtdXNlcnMgdGV4dC1hcXVhXCIpLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiNSBuZXcgbWVtYmVycyBqb2luZWQgdG9kYXlcIlxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdFx0XHQpLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwibGkuZm9vdGVyXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImFcIiwgXCJWaWV3IGFsbFwiKVxuXHRcdFx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdFx0KSxcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwibGlcIixcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJhXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzdHlsZTogXCJmb250LXNpemU6MTlweDtjdXJzb3I6ZGVmYXVsdFwiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS1jbG9jayBtclwiKSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcInN0cm9uZyNzeW5jVGltZVNwYW5cIiwgXCIwMDowMDowMFwiKVxuXHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdClcblx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdClcblx0XHRcdFx0KVxuXHRcdFx0KSxcblx0XHRcdGh0bWwoXCJkaXYuY29udGVudC13cmFwcGVyIHN3ZHJNYWluV2luZG93XCIsXG5cdFx0XHRcdGh0bWwoXCJzZWN0aW9uLmNvbnRlbnRcIixcblx0XHRcdFx0XHRodG1sKFwiZGl2LnRhYi1jb250ZW50XCIsIHtcblx0XHRcdFx0XHRcdFx0XCJkYXRhLXR5cGVcIjogXCJtYWluTWVudVwiXG5cdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0bmV3IHRhYihcImduaW90VGFiRGV2aWNlc1wiLCB0cnVlLCB0aGlzLmRldmljZXNUYWIpLFxuXHRcdFx0XHRcdClcblx0XHRcdFx0KVxuXHRcdFx0KVxuXHRcdCk7XG5cblx0fVxuXG5cdGVuYWJsZVRhYnModmFsdWUpIHtcblx0XHRbLi4uTnVtYmVyKHZhbHVlIHx8IDApLnRvU3RyaW5nKDIpXS5tYXAobiA9PiBCb29sZWFuKHBhcnNlSW50KG4pKSkuZm9yRWFjaCgoZWxlbWVudCwgaW5kZXgpID0+IHtcblx0XHRcdCQoYGFbZGF0YS10eXBlPSdtYWluTWVudSddOmVxKCR7aW5kZXh9KWApLmNzcyhcImRpc3BsYXlcIiwgZWxlbWVudCA/IFwiYmxvY2tcIiA6IFwibm9uZVwiKTtcblx0XHR9KTtcblx0fVxuXG5cdHJlbmRlclNjZW5lcmllc1RhYmxlKHZhbHVlKSB7XG5cdFx0JChcInRib2R5I3NjZW5lcmllc0xpdmVcIikuZW1wdHkoKTtcblxuXHRcdGxldCB1bmlxdWVEaXNwYXRjaGVycyA9IFsuLi52YWx1ZV0ubWFwKHNjZW5lcnkgPT4gc2NlbmVyeS5kaXNwYXRjaGVyRGF0YS5pZF9tZW1iZXIpLmZpbHRlcigodiwgaSwgYSkgPT4gYS5pbmRleE9mKHYpID09PSBpKTtcblx0XHQkKFwic3BhbiNkaXNwYXRjaGVyc09ubGluZUNvdW50ZXJcIikudGV4dCh1bmlxdWVEaXNwYXRjaGVycy5sZW5ndGgpO1xuXHRcdCQuY29udGV4dE1lbnUoXCJkZXN0cm95XCIsIFwic3BhbltkYXRhLXVuaXF1ZV1cIik7XG5cblx0XHR2YWx1ZS5mb3JFYWNoKHNjZW5lcnkgPT4ge1xuXHRcdFx0JChcInRib2R5I3NjZW5lcmllc0xpdmVcIikuYXBwZW5kKFxuXHRcdFx0XHRodG1sKFwidHJcIixcblx0XHRcdFx0XHRodG1sKFwidGRcIixcblx0XHRcdFx0XHRcdGh0bWwoXCJzcGFuLmNQb2ludGVyXCIsIHtcblx0XHRcdFx0XHRcdFx0XCJkYXRhLXVuaXF1ZVwiOiBzY2VuZXJ5LnVuaXF1ZVBhaXJIYXNoLFxuXHRcdFx0XHRcdFx0XHRcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuXHRcdFx0XHRcdFx0XHRcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG5cdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgTWVudSBwb2RyxJljem5lYCxcblx0XHRcdFx0XHRcdFx0c3R5bGU6IFwiZm9udC13ZWlnaHQ6IGJvbGQ7XCJcblx0XHRcdFx0XHRcdH0sIHNjZW5lcnkuc2NlbmVyeU5hbWUpKSxcblx0XHRcdFx0XHRodG1sKFwidGRcIixcblx0XHRcdFx0XHRcdGh0bWwoXCJzcGFuLmNQb2ludGVyXCIsIHtcblx0XHRcdFx0XHRcdFx0XCJkYXRhLXVuaXF1ZVwiOiBzY2VuZXJ5LnVuaXF1ZVBhaXJIYXNoLFxuXHRcdFx0XHRcdFx0XHRcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuXHRcdFx0XHRcdFx0XHRcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG5cdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgTWVudSBwb2RyxJljem5lYFxuXHRcdFx0XHRcdFx0fSwgc2NlbmVyeS5kaXNwYXRjaGVyRGF0YS5tZW1iZXJfbmFtZSkpLFxuXHRcdFx0XHRcdGh0bWwoXCJ0ZC50cmFpbnNPblNjZW5lcnlcIiwge1xuXHRcdFx0XHRcdFx0XCJkYXRhLWhhc2hcIjogc2NlbmVyeS5zY2VuZXJ5SGFzaCxcblx0XHRcdFx0XHR9LCBgJHtzY2VuZXJ5LmN1cnJlbnRfdXNlcnN9IC8gJHtzY2VuZXJ5Lm1heF91c2Vyc31gKSxcblx0XHRcdFx0XHRodG1sKFwidGRcIiwgXCJzdGF0dXNcIiksXG5cdFx0XHRcdFx0aHRtbChcInRkXCIsXG5cdFx0XHRcdFx0XHRodG1sKFwic3Bhbi5sYWJlbCBsYWJlbC1wcmltYXJ5XCIsIHtcblx0XHRcdFx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcblx0XHRcdFx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdFx0XHRcInRpdGxlXCI6IGkxOG4gYFBvemlvbSBkb8Wbd2lhZGN6ZW5pYWBcblx0XHRcdFx0XHRcdH0sIChzY2VuZXJ5LmRpc3BhdGNoZXJEYXRhLmxldmVsIDwgMiA/IFwiTFwiIDogc2NlbmVyeS5kaXNwYXRjaGVyRGF0YS5sZXZlbCkpXG5cdFx0XHRcdFx0KVxuXHRcdFx0XHQpXG5cdFx0XHQpO1xuXG5cdFx0XHRuZXcgQ29udGV4dE1lbnUoKS5jcmVhdGUoe1xuXHRcdFx0XHR0cmlnZ2VyOiBcImxlZnRcIixcblx0XHRcdFx0c2VsZWN0b3I6IGBzcGFuW2RhdGEtdW5pcXVlPScke3NjZW5lcnkudW5pcXVlUGFpckhhc2h9J11gLFxuXHRcdFx0XHRpdGVtczoge1xuXHRcdFx0XHRcdHByaXZhdGVNZXNzYWdlOiB7XG5cdFx0XHRcdFx0XHRpY29uOiBcImZhLWVudmVsb3BlXCIsXG5cdFx0XHRcdFx0XHRuYW1lOiBpMThuIGBXecWbbGlqIHByeXdhdG7EhSB3aWFkb21vxZvEh2AsXG5cdFx0XHRcdFx0XHRjYWxsYmFjazogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdFx0XHRwcml2YXRlTWVzc2FnZURpYWxvZyhzY2VuZXJ5LmRpc3BhdGNoZXJEYXRhLm1lbWJlcl9uYW1lLCBzY2VuZXJ5LmRpc3BhdGNoZXJEYXRhLmlkX21lbWJlcik7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRkaXNwYXRjaGVyUHJvZmlsZToge1xuXHRcdFx0XHRcdFx0aWNvbjogXCJmYS11c2VyXCIsXG5cdFx0XHRcdFx0XHRuYW1lOiBpMThuIGBXecWbd2lldGwgcHJvZmlsIGR5xbx1cm5lZ29gLFxuXHRcdFx0XHRcdFx0Y2FsbGJhY2s6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRcdFx0anNvbkRvd25sb2FkZXIodHJ1ZSwgY29uZmlnLmFwaUVuZHBvaW50ICsgalF1ZXJ5LnBhcmFtKHtcblx0XHRcdFx0XHRcdFx0XHRtZXRob2Q6IFwiZ2V0VXNlcnNJbmZvQnlJZFwiLFxuXHRcdFx0XHRcdFx0XHRcdGlkOiBzY2VuZXJ5LmRpc3BhdGNoZXJEYXRhLmlkX21lbWJlcixcblx0XHRcdFx0XHRcdFx0XHR0b2tlbjogc2Vzc2lvblN0b3JhZ2UuYXBpVG9rZW5cblx0XHRcdFx0XHRcdFx0fSkpLnRoZW4odmFsdWUgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdGlmICh2YWx1ZVswXSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0bGV0IHByb2ZpbGUgPSB2YWx1ZVswXTtcblx0XHRcdFx0XHRcdFx0XHRcdGxldCBjdXN0X3RyYWlucyA9IDA7XG5cdFx0XHRcdFx0XHRcdFx0XHRsZXQgZ3JvdXBzID0gW1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwic3Bhbi5sYWJlbFwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzdHlsZTogXCJiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwOyBkaXNwbGF5OiBibG9jazsgZm9udC1zaXplOiA5NSU7IG1hcmdpbjogNXB4O1wiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpMThuIGBVxbx5dGtvd25pa2Bcblx0XHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdFx0XTtcblxuXHRcdFx0XHRcdFx0XHRcdFx0dHJ5IHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0Y3VzdF90cmFpbnMgPSBwcm9maWxlLnVzZXJfc3RhdHMuZmluZChzID0+IHMudmFyaWFibGUgPT0gXCJjdXN0X3RyYWluc1wiKS52YWx1ZTtcblx0XHRcdFx0XHRcdFx0XHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdGN1c3RfdHJhaW5zID0gMDtcblx0XHRcdFx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0XHRcdFx0cHJvZmlsZS51c2VyX2dyb3Vwcy5mb3JFYWNoKGdyb3VwID0+IHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0aWYgKGdyb3VwLm9ubGluZV9jb2xvciA9PSBcIlwiKSBncm91cC5vbmxpbmVfY29sb3IgPSBcIiMwMDBcIjtcblx0XHRcdFx0XHRcdFx0XHRcdFx0Z3JvdXBzLnB1c2goXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcInNwYW4ubGFiZWxcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzdHlsZTogYGJhY2tncm91bmQtY29sb3I6JHtncm91cC5vbmxpbmVfY29sb3IgfHwgXCIjMDAwXCJ9OyBkaXNwbGF5OiBibG9jazsgZm9udC1zaXplOiA5NSU7IG1hcmdpbjogNXB4O2Bcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRncm91cC5ncm91cF9uYW1lXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbFRpdGxlXCIpLnRleHQoaTE4biBgUHJvZmlsIGR5xbx1cm5lZ28gJHtwcm9maWxlLm1lbWJlcl9uYW1lfWApO1xuXHRcdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsRm9vdGVyXCIpLnNob3coKTtcblx0XHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuZW1wdHkoKTtcblx0XHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiZGl2LnJvd1wiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJkaXYuY29sLW1kLTZcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJkbC5kbC1ob3Jpem9udGFsXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJkdFwiLCBpMThuIGBOYXp3YSB1xbx5dGtvd25pa2E6YCksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJkZFwiLCBwcm9maWxlLm1lbWJlcl9uYW1lKSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImR0XCIsIGkxOG4gYERhdGEgcmVqZXN0cmFjamk6YCksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJkZFwiLCBuZXcgRGF0ZShwcm9maWxlLmRhdGVfcmVnaXN0ZXJlZCAqIDEwMDApLnRvTG9jYWxlRGF0ZVN0cmluZygpKVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImRpdi5jb2wtbWQtNlwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImRsLmRsLWhvcml6b250YWxcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImR0XCIsIGkxOG4gYE9ic8WCdcW8b25lIHBvY2nEhWdpOmApLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiZGRcIiwgY3VzdF90cmFpbnMpLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiZHRcIiwgaTE4biBgUG96aW9tIGRvxZt3aWFkY3plbmlhOmApLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiZGRcIiwgaHRtbChcInNwYW4ubGFiZWwgbGFiZWwtcHJpbWFyeVwiLCBgJHtwcm9maWxlLmxldmVscy5kaXNwYXRjaGVyIDwgMiA/IFwiTFwiIDogcHJvZmlsZS5sZXZlbHMuZGlzcGF0Y2hlciArIFwiIC8gMjBcIn1gKSlcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdCkpXG5cdFx0XHRcdFx0XHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJkaXYucm93XCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImRpdi5jb2wtbWQtMTJcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJkbC5kbC1ob3Jpem9udGFsXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJkdFwiLCBpMThuIGBHcnVweTpgKSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImRkXCIsIGdyb3Vwcylcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdClcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdFx0XHRcdClcblx0XHRcdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJzaG93XCIpO1xuXHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0fVxuXG59IiwiZXhwb3J0IGRlZmF1bHQgY2xhc3Mge1xuXG5cdGNvbnN0cnVjdG9yKCkge1xuXG5cdFx0dGhpcy5zdXBwb3J0ZWQgPSBmYWxzZTtcblxuXHRcdGlmICgoXCJOb3RpZmljYXRpb25cIiBpbiB3aW5kb3cpKSB7XG5cdFx0XHR0aGlzLnN1cHBvcnRlZCA9IHRydWU7XG5cdFx0XHRpZiAoTm90aWZpY2F0aW9uLnBlcm1pc3Npb24gIT09IFwiZGVuaWVkXCIpIHtcblx0XHRcdFx0Tm90aWZpY2F0aW9uLnJlcXVlc3RQZXJtaXNzaW9uKCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdH1cblxuXHRub3RpZnkoYm9keSwgc2lsZW50LCBjYWxsYmFjaykge1xuXG5cdFx0aWYgKCF0aGlzLnN1cHBvcnRlZCkgcmV0dXJuO1xuXG5cdFx0aWYgKE5vdGlmaWNhdGlvbi5wZXJtaXNzaW9uID09PSBcImdyYW50ZWRcIikge1xuXHRcdFx0bGV0IG5vdGlmaWNhdGlvbiA9IG5ldyBOb3RpZmljYXRpb24oXCJnbklPVCAyLjBcIiwge1xuXHRcdFx0XHRib2R5LFxuXHRcdFx0XHRzaWxlbnRcblx0XHRcdH0pO1xuXHRcdFx0bm90aWZpY2F0aW9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdHdpbmRvdy5mb2N1cygpO1xuXHRcdFx0XHRjYWxsYmFjaygpO1xuXHRcdFx0fTtcblx0XHR9XG5cdH1cblxufSIsImltcG9ydCB7XG5cdGh0bWxcbn0gZnJvbSBcInJlZG9tXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIHtcblxuXHRjb25zdHJ1Y3Rvcih0eXBlLCB0aXRsZSwgdG9vbHMsIGJvZHksIGZvb3Rlcikge1xuXG5cdFx0cmV0dXJuIGh0bWwoYGRpdi5ib3ggYm94LSR7dHlwZX1gLFxuXHRcdFx0aHRtbChcImRpdi5ib3gtaGVhZGVyIHdpdGgtYm9yZGVyXCIsXG5cdFx0XHRcdGh0bWwoXCJoMy5ib3gtdGl0bGVcIiwgdGl0bGUpLFxuXHRcdFx0XHRodG1sKFwiZGl2LmJveC10b29scyBwdWxsLXJpZ2h0XCIsIHRvb2xzKVxuXHRcdFx0KSxcblx0XHRcdGh0bWwoXCJkaXYuYm94LWJvZHlcIiwgYm9keSksXG5cdFx0XHRodG1sKFwiZGl2LmJveC1mb290ZXJcIiwgZm9vdGVyKVxuXHRcdCk7XG5cblx0fVxuXG59IiwiaW1wb3J0IHtcblx0aHRtbFxufSBmcm9tIFwicmVkb21cIjtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKCkge1xuXG5cdGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoXG5cdFx0aHRtbChcImRpdi5tb2RhbCBmYWRlI2Vycm9yTW9kYWxcIixcblx0XHRcdGh0bWwoXCJkaXYubW9kYWwtZGlhbG9nIG1vZGFsLWRhbmdlclwiLFxuXHRcdFx0XHRodG1sKFwiZGl2Lm1vZGFsLWNvbnRlbnRcIixcblx0XHRcdFx0XHRodG1sKFwiZGl2Lm1vZGFsLWhlYWRlclwiLFxuXHRcdFx0XHRcdFx0aHRtbChcImJ1dHRvbi5jbG9zZVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiLFxuXHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgWmFta25pamBcblx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0aHRtbChcInNwYW5cIiwgXCJ4XCIpXG5cdFx0XHRcdFx0XHQpLFxuXHRcdFx0XHRcdFx0aHRtbChcImg0Lm1vZGFsLXRpdGxlXCIsIGkxOG4gYFd5c3TEhXBpxYIgYsWCxIVkYClcblx0XHRcdFx0XHQpLFxuXHRcdFx0XHRcdGh0bWwoXCJkaXYubW9kYWwtYm9keVwiLFxuXHRcdFx0XHRcdFx0aHRtbChcInAjZXJyb3JNb2RhbFRleHRcIilcblx0XHRcdFx0XHQpLFxuXHRcdFx0XHRcdGh0bWwoXCJkaXYubW9kYWwtZm9vdGVyXCIsXG5cdFx0XHRcdFx0XHRodG1sKFwiYnV0dG9uLmJ0biBidG4tZGFuZ2VyXCIsIHtcblx0XHRcdFx0XHRcdFx0XCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiLFxuXHRcdFx0XHRcdFx0XHR0eXBlOiBcImJ1dHRvblwiXG5cdFx0XHRcdFx0XHR9LCBpMThuIGBaYW1rbmlqYClcblx0XHRcdFx0XHQpXG5cdFx0XHRcdClcblx0XHRcdClcblx0XHQpXG5cdCk7XG5cblx0JChcIiNlcnJvck1vZGFsXCIpLm9uKFwic2hvdy5icy5tb2RhbFx0XCIsIGZ1bmN0aW9uICgpIHtcblx0XHQkKFwiZGl2I2Vycm9yTW9kYWwgZGl2Lm1vZGFsLWNvbnRlbnRcIikuYm91bmNlKHtcblx0XHRcdGludGVydmFsOiAxMDAsXG5cdFx0XHRkaXN0YW5jZTogMjAsXG5cdFx0XHR0aW1lczogNFxuXHRcdH0pO1xuXHR9KTtcblxuXHRkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKFxuXHRcdGh0bWwoXCJkaXYubW9kYWwgZmFkZSNTbWFsbE1vZGFsXCIsXG5cdFx0XHRodG1sKFwiZGl2Lm1vZGFsLWRpYWxvZ1wiLFxuXHRcdFx0XHRodG1sKFwiZGl2Lm1vZGFsLWNvbnRlbnRcIixcblx0XHRcdFx0XHRodG1sKFwiZGl2Lm1vZGFsLWhlYWRlclwiLFxuXHRcdFx0XHRcdFx0aHRtbChcImJ1dHRvbi5jbG9zZVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XCJkYXRhLWRpc21pc3NcIjogXCJtb2RhbFwiLFxuXHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgWmFta25pamBcblx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0aHRtbChcInNwYW5cIiwgXCJ4XCIpXG5cdFx0XHRcdFx0XHQpLFxuXHRcdFx0XHRcdFx0aHRtbChcImg0Lm1vZGFsLXRpdGxlI1NtYWxsTW9kYWxUaXRsZVwiKVxuXHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0aHRtbChcImRpdi5tb2RhbC1ib2R5I1NtYWxsTW9kYWxCb2R5XCIpLFxuXHRcdFx0XHRcdGh0bWwoXCJkaXYubW9kYWwtZm9vdGVyI1NtYWxsTW9kYWxGb290ZXJcIixcblx0XHRcdFx0XHRcdGh0bWwoXCJidXR0b24uYnRuIGJ0bi1kYW5nZXJcIiwge1xuXHRcdFx0XHRcdFx0XHRcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCIsXG5cdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdH0sIGkxOG4gYFphbWtuaWpgKVxuXHRcdFx0XHRcdClcblx0XHRcdFx0KVxuXHRcdFx0KVxuXHRcdClcblx0KTtcblxuXHQkKFwiI1NtYWxsTW9kYWxcIikub24oXCJzaG93LmJzLm1vZGFsXHRcIiwgZnVuY3Rpb24gKCkge1xuXHRcdCQoXCJkaXYjU21hbGxNb2RhbCBkaXYubW9kYWwtY29udGVudFwiKS5ib3VuY2Uoe1xuXHRcdFx0aW50ZXJ2YWw6IDEwMCxcblx0XHRcdGRpc3RhbmNlOiAyMCxcblx0XHRcdHRpbWVzOiA0XG5cdFx0fSk7XG5cdH0pO1xuXG5cdCQoXCIjU21hbGxNb2RhbFwiKS5vbihcInNob3duLmJzLm1vZGFsXHRcIiwgZnVuY3Rpb24gKCkge1xuXHRcdCQoXCJkaXYjU21hbGxNb2RhbCBpbnB1dDpmaXJzdFwiKS5mb2N1cygpO1xuXHR9KTtcblxufSIsImltcG9ydCB7XG5cdGh0bWxcbn0gZnJvbSBcInJlZG9tXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIHtcblxuXHRjb25zdHJ1Y3RvcihpZCwgaXNBY3RpdmUsIGNoaWxkcmVucywgY2hhbm5lbGlkKSB7XG5cblx0XHRyZXR1cm4gaHRtbChgZGl2LnRhYi1wYW5lIGZhZGUgJHtpc0FjdGl2ZSA/IFwiaW4gYWN0aXZlXCIgOiBcIlwifSMke2lkfWAsIGNoYW5uZWxpZCAhPSB1bmRlZmluZWQgPyB7XG5cdFx0XHRcImRhdGEtY2hhbm5lbGlkXCI6IGNoYW5uZWxpZFxuXHRcdH0gOiBudWxsLCBjaGlsZHJlbnMpO1xuXG5cdH1cblxufSIsIi8qIGVzbGludC1kaXNhYmxlIG5vLXVuZGVmICovXG5pbXBvcnQgbWFpblBhbmVsIGZyb20gXCIuL21haW4vQG1haW5QYW5lbFwiO1xuaW1wb3J0IGFwcCBmcm9tIFwiLi9hcHBcIjtcbmltcG9ydCBncm91cEJ5IGZyb20gXCIuLi91dGlscy9ncm91cEJ5XCI7XG5pbXBvcnQge1xuXHRodG1sXG59IGZyb20gXCJyZWRvbVwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyB7XG5cblx0Y29uc3RydWN0b3IoKSB7XG5cblx0XHR0aGlzLmFwcCA9IG5ldyBhcHAoKTtcblx0XHR0aGlzLndhc0tpY2tlZCA9IGZhbHNlO1xuXG5cdH1cblxuXHRvbk9wZW4oKSB7XG5cdFx0dGhpcy5hcHAud2Vic29ja2V0Q29ubmVjdGlvbnMrKztcblx0XHRpZiAodGhpcy5hcHAud2Vic29ja2V0Q29ubmVjdGlvbnMgPT0gMSkge1xuXHRcdFx0dGhpcy5tYWluUGFuZWwgPSBuZXcgbWFpblBhbmVsKCk7XG5cdFx0XHRkb2N1bWVudC5ib2R5LmNsYXNzTmFtZSA9IFwiaG9sZC10cmFuc2l0aW9uXCI7XG5cdFx0XHRkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHRoaXMubWFpblBhbmVsLnJlbmRlcigpKTtcblx0XHRcdCQoXCJkaXYubG9ja3NjcmVlbi13cmFwcGVyXCIpLnJlbW92ZSgpO1xuXHRcdH1cblxuXHRcdCQoXCIjd2Vic29ja2V0U3RhdHVzSW5kaWNhdG9ySWNvblwiKS5yZW1vdmVDbGFzcyhcImZhLXRpbWVzXCIpO1xuXHRcdCQoXCIjd2Vic29ja2V0U3RhdHVzSW5kaWNhdG9ySWNvblwiKS5hZGRDbGFzcyhcImZhLWNoZWNrXCIpO1xuXHRcdCQoXCIjd2Vic29ja2V0U3RhdHVzSW5kaWNhdG9yQmFja2dyb3VuZFwiKS5yZW1vdmVDbGFzcyhcImJnLXJlZFwiKTtcblx0XHQkKFwiI3dlYnNvY2tldFN0YXR1c0luZGljYXRvckJhY2tncm91bmRcIikuYWRkQ2xhc3MoXCJiZy1ncmVlblwiKTtcblxuXHRcdGNvbnNvbGUubG9nKFwiV2Vic29ja2V0IGNvbm5lY3RlZCFcIik7XG5cblx0fVxuXG5cdG9uQ2xvc2UoKSB7XG5cblx0XHQkKFwiI3dlYnNvY2tldFN0YXR1c0luZGljYXRvckljb25cIikucmVtb3ZlQ2xhc3MoXCJmYS1jaGVja1wiKTtcblx0XHQkKFwiI3dlYnNvY2tldFN0YXR1c0luZGljYXRvckljb25cIikuYWRkQ2xhc3MoXCJmYS10aW1lc1wiKTtcblx0XHQkKFwiI3dlYnNvY2tldFN0YXR1c0luZGljYXRvckJhY2tncm91bmRcIikucmVtb3ZlQ2xhc3MoXCJiZy1ncmVlblwiKTtcblx0XHQkKFwiI3dlYnNvY2tldFN0YXR1c0luZGljYXRvckJhY2tncm91bmRcIikuYWRkQ2xhc3MoXCJiZy1yZWRcIik7XG5cblx0XHRjb25zb2xlLndhcm4oXCJXZWJzb2NrZXQgZGlzY29ubmVjdGVkIVwiKTtcblx0XHR0aGlzLnRyeVRvUmVjb25uZWN0KCk7XG5cblx0fVxuXG5cdG9uTWVzc2FnZShtZXNzYWdlKSB7XG5cblx0XHRsZXQgY29tbWFuZDtcblx0XHR0cnkge1xuXHRcdFx0Y29tbWFuZCA9IEpTT04ucGFyc2UobWVzc2FnZS5kYXRhKS5jb21tYW5kO1xuXHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cdFx0XHRjb25zb2xlLmVycm9yKFwiVW5jYXVnaHQgZXJyb3IgZHVyaW5nIGluY29taW5nIGRhdGEgcGFyc2luZ1wiLCBtZXNzYWdlKTtcblx0XHR9XG5cblx0XHRpZiAoY29tbWFuZCkge1xuXHRcdFx0bGV0IHZhbHVlID0gSlNPTi5wYXJzZShtZXNzYWdlLmRhdGEpLnZhbHVlO1xuXHRcdFx0c3dpdGNoIChjb21tYW5kKSB7XG5cdFx0XHRcdGNhc2UgXCJhcHAvbG9nb3V0XCI6XG5cdFx0XHRcdFx0c2Vzc2lvblN0b3JhZ2UuY2xlYXIoKTtcblx0XHRcdFx0XHRsb2NhdGlvbi5yZWxvYWQoKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSBcInBpbmdcIjpcblx0XHRcdFx0XHR0aGlzLmFwcC5zZW5kRGF0YShcInBvbmdcIik7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UgXCJhcHAvdGltZXN0YW1wXCI6XG5cdFx0XHRcdFx0dGhpcy5hcHAuZ2xvYmFsVGltZXN0YW1wID0gdmFsdWU7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UgXCJhcHAvZW5hYmxlVGFic1wiOlxuXHRcdFx0XHRcdHRoaXMubWFpblBhbmVsLmVuYWJsZVRhYnModmFsdWUpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlIFwiYXBwL2ZvcmNlUmVmcmVzaFwiOlxuXHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHR0aGlzLmFwcC5zZW5kRGF0YShcImdldERldmljZXNcIik7XG5cdFx0XHRcdFx0XHRcdHRoaXMuYXBwLnNlbmREYXRhKFwiZ2V0R3JvdXBzXCIpO1xuXHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdDUwMCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UgXCJnZXRHcm91cHNcIjpcblx0XHRcdFx0XHR0aGlzLmFwcC5ncm91cHMgPSB2YWx1ZTtcblx0XHRcdFx0XHRsZXQgcm93cyA9IFtdO1xuXG5cdFx0XHRcdFx0JChcImRpdiNncm91cHNFZGl0b3JcIikuZW1wdHkoKTtcblx0XHRcdFx0XHR0aGlzLmFwcC5ncm91cHMuZm9yRWFjaChnID0+IHtcblxuXHRcdFx0XHRcdFx0bGV0IGVkaXRCdXR0b24gPSBodG1sKFwiYnV0dG9uLmJ0biBidG4td2FybmluZyBidG4tc20gbXJcIiwge1xuXHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0aHRtbChcImkuZmFzIGZhLXBlbmNpbC1hbHQgaS5tclwiKSxcblx0XHRcdFx0XHRcdFx0aTE4biBgRWR5dHVqYFxuXHRcdFx0XHRcdFx0KTtcblx0XHRcdFx0XHRcdGxldCByZW1vdmVCdXR0b24gPSBodG1sKFwiYnV0dG9uLmJ0biBidG4tZGFuZ2VyIGJ0bi1zbSBtclwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtdGltZXMgaS5tclwiKSxcblx0XHRcdFx0XHRcdFx0aTE4biBgVXN1xYRgXG5cdFx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0XHRyZW1vdmVCdXR0b24ub25jbGljayA9ICgpID0+IHtcblx0XHRcdFx0XHRcdFx0bGV0IGNvbnRpbnVlQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLXN1Y2Nlc3MgbXJcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0aHRtbChcImkuZmFzIGZhLWNoZWNrIGkubXJcIiksXG5cdFx0XHRcdFx0XHRcdFx0aTE4biBgVXN1xYQgZ3J1cMSZICR7Zy5ncm91cF9uYW1lLnRvVXBwZXJDYXNlKCl9YFxuXHRcdFx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0XHRcdGNvbnRpbnVlQnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5hcHAuc2VuZERhdGEoXCJyZW1vdmVHcm91cFwiLCBnLmdyb3VwX2lkKTtcblx0XHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJoaWRlXCIpO1xuXHRcdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRcdGxldCBjYW5jZWxCdXR0b24gPSBodG1sKFwiYnV0dG9uLmJ0biBidG4tZGFuZ2VyXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS10aW1lcyBpLm1yXCIpLFxuXHRcdFx0XHRcdFx0XHRcdGkxOG4gYEFudWx1amApO1xuXG5cdFx0XHRcdFx0XHRcdGNhbmNlbEJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbFwiKS5tb2RhbChcImhpZGVcIik7XG5cdFx0XHRcdFx0XHRcdH07XG5cblx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsVGl0bGVcIikudGV4dChpMThuIGBVc3V3YW5pZSBncnVweSAke2cuZ3JvdXBfbmFtZS50b1VwcGVyQ2FzZSgpfWApO1xuXHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxGb290ZXJcIikuaGlkZSgpO1xuXHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxCb2R5XCIpLmVtcHR5KCk7XG5cdFx0XHRcdFx0XHRcdGlmICh0aGlzLmFwcC5kZXZpY2VzLmZpbHRlcihkID0+IGQuZ3JvdXBfaWQgPT0gZy5ncm91cF9pZCkubGVuZ3RoID4gMCkge1xuXHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKGkxOG4gYE5pZSBtb8W8bmEgdXN1bsSFxIcgdGVqIGdydXB5LCBwb25pZXdhxbwgemF3aWVyYSBvbmEgcHJ6eXBpc2FuZSB1cnrEhWR6ZW5pYSFgKTtcblx0XHRcdFx0XHRcdFx0XHRjb250aW51ZUJ1dHRvbi5zZXRBdHRyaWJ1dGUoXCJkaXNhYmxlZFwiLCBcImRpc2FibGVkXCIpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKGh0bWwoXCJkaXYuYXNrQnV0dG9uc1wiLCBjb250aW51ZUJ1dHRvbiwgY2FuY2VsQnV0dG9uKSk7XG5cdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbFwiKS5tb2RhbChcInNob3dcIik7XG5cdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRlZGl0QnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdGxldCBjb250aW51ZUJ1dHRvbiA9IGh0bWwoXCJidXR0b24uYnRuIGJ0bi1zdWNjZXNzIG1yXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS1jaGVjayBpLm1yXCIpLFxuXHRcdFx0XHRcdFx0XHRcdGkxOG4gYFphcGlzeiBncnVwxJlgXG5cdFx0XHRcdFx0XHRcdCk7XG5cblx0XHRcdFx0XHRcdFx0bGV0IGlucHV0TmFtZSA9IGh0bWwoXCJpbnB1dCNuYW1lLmZvcm0tY29udHJvbFwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0dHlwZTogXCJ0ZXh0XCIsXG5cdFx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI6IGkxOG4gYE5hendhIGdydXB5YCxcblx0XHRcdFx0XHRcdFx0XHRtYXhsZW5ndGg6IDMyLFxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBnLmdyb3VwX25hbWVcblx0XHRcdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHRcdFx0bGV0IGlucHV0RGVzY3JpcHRpb24gPSBodG1sKFwiaW5wdXQjZGVzYy5mb3JtLWNvbnRyb2xcIiwge1xuXHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwidGV4dFwiLFxuXHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyOiBpMThuIGBPcGlzIGdydXB5YCxcblx0XHRcdFx0XHRcdFx0XHRtYXhsZW5ndGg6IDI1Nixcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogZy5ncm91cF9kZXNjcmlwdGlvblxuXHRcdFx0XHRcdFx0XHR9KTtcblxuXHRcdFx0XHRcdFx0XHRjb250aW51ZUJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdHRoaXMuYXBwLnNlbmREYXRhKFwidXBkYXRlR3JvdXBcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0Z3JvdXBfaWQ6IGcuZ3JvdXBfaWQsXG5cdFx0XHRcdFx0XHRcdFx0XHRncm91cF9uYW1lOiBpbnB1dE5hbWUudmFsdWUsXG5cdFx0XHRcdFx0XHRcdFx0XHRncm91cF9kZXNjcmlwdGlvbjogaW5wdXREZXNjcmlwdGlvbi52YWx1ZVxuXHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbFwiKS5tb2RhbChcImhpZGVcIik7XG5cdFx0XHRcdFx0XHRcdH07XG5cblx0XHRcdFx0XHRcdFx0bGV0IGNhbmNlbEJ1dHRvbiA9IGh0bWwoXCJidXR0b24uYnRuIGJ0bi1kYW5nZXJcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0aHRtbChcImkuZmFzIGZhLXRpbWVzIGkubXJcIiksXG5cdFx0XHRcdFx0XHRcdFx0aTE4biBgQW51bHVqYCk7XG5cblx0XHRcdFx0XHRcdFx0Y2FuY2VsQnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsXCIpLm1vZGFsKFwiaGlkZVwiKTtcblx0XHRcdFx0XHRcdFx0fTtcblxuXHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxUaXRsZVwiKS50ZXh0KGkxOG4gYFR3b3J6ZW5pZSBub3dlaiBncnVweSB1cnrEhWR6ZcWEYCk7XG5cdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEZvb3RlclwiKS5oaWRlKCk7XG5cdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuZW1wdHkoKTtcblx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5hcHBlbmQoXG5cdFx0XHRcdFx0XHRcdFx0aHRtbChcImRpdi5mb3JtLWdyb3VwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwibGFiZWxcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRmb3I6IFwibmFtZVwiXG5cdFx0XHRcdFx0XHRcdFx0XHR9LCBpMThuIGBOYXp3YSBncnVweWApLFxuXHRcdFx0XHRcdFx0XHRcdFx0aW5wdXROYW1lXG5cdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxCb2R5XCIpLmFwcGVuZChcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwiZGl2LmZvcm0tZ3JvdXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJsYWJlbFwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdGZvcjogXCJkZXNjXCJcblx0XHRcdFx0XHRcdFx0XHRcdH0sIGkxOG4gYE9waXMgZ3J1cHlgKSxcblx0XHRcdFx0XHRcdFx0XHRcdGlucHV0RGVzY3JpcHRpb25cblx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKGh0bWwoXCJkaXYuYXNrQnV0dG9uc1wiLCBjb250aW51ZUJ1dHRvbiwgY2FuY2VsQnV0dG9uKSk7XG5cdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbFwiKS5tb2RhbChcInNob3dcIik7XG5cdFx0XHRcdFx0XHR9O1xuXG5cblx0XHRcdFx0XHRcdHJvd3MucHVzaChcblx0XHRcdFx0XHRcdFx0aHRtbChcInRyXCIsXG5cdFx0XHRcdFx0XHRcdFx0aHRtbChcInRkLmNvbC1tZC01XCIsIGcuZ3JvdXBfbmFtZSksXG5cdFx0XHRcdFx0XHRcdFx0aHRtbChcInRkLmNvbC1tZC01XCIsIGcuZ3JvdXBfZGVzY3JpcHRpb24pLFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJ0ZC5jb2wtbWQtMVwiLCBlZGl0QnV0dG9uKSxcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwidGQuY29sLW1kLTFcIiwgcmVtb3ZlQnV0dG9uKVxuXHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdFx0Z3JvdXBzRWRpdG9yLmFwcGVuZChodG1sKFwidGFibGUudGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1jZW50ZXJcIixcblx0XHRcdFx0XHRcdGh0bWwoXCJ0Ym9keVwiLCByb3dzKVxuXHRcdFx0XHRcdCkpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlIFwicmVtb3ZlRGV2aWNlXCI6XG5cdFx0XHRcdGNhc2UgXCJyZW1vdmVHcm91cFwiOlxuXHRcdFx0XHRjYXNlIFwicG93ZXJEZXZpY2VcIjpcblx0XHRcdFx0Y2FzZSBcInVwZGF0ZURldmljZVwiOlxuXHRcdFx0XHRjYXNlIFwidXBkYXRlR3JvdXBcIjpcblx0XHRcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0XHRcdFx0dGhpcy5hcHAuc2VuZERhdGEoXCJnZXREZXZpY2VzXCIpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLmFwcC5zZW5kRGF0YShcImdldEdyb3Vwc1wiKTtcblx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHQ1MDApO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlIFwiZ2V0RGV2aWNlc1wiOlxuXHRcdFx0XHRcdHRoaXMuYXBwLmRldmljZXMgPSB2YWx1ZTtcblx0XHRcdFx0XHQkKFwiZGl2I2RldmljZXNFZGl0b3JcIikuZW1wdHkoKTtcblxuXHRcdFx0XHRcdGxldCByZXN1bHQgPSBbXTtcblx0XHRcdFx0XHRncm91cEJ5KHRoaXMuYXBwLmRldmljZXMsIFwiZ3JvdXBfaWRcIikuZm9yRWFjaChkID0+IHtcblx0XHRcdFx0XHRcdGlmIChkLmxlbmd0aCA8IDEpIHJldHVybjtcblxuXHRcdFx0XHRcdFx0bGV0IHJvd3MgPSBbXTtcblx0XHRcdFx0XHRcdGQuZm9yRWFjaChyID0+IHtcblx0XHRcdFx0XHRcdFx0bGV0IHN0YXR1c0luZGljYXRvcjtcblx0XHRcdFx0XHRcdFx0bGV0IHBpbmdJbmRpY2F0b3I7XG5cdFx0XHRcdFx0XHRcdGxldCBkZXZpY2VJY29uO1xuXHRcdFx0XHRcdFx0XHRsZXQgZGV2aWNlVHlwZSA9IDA7XG5cblx0XHRcdFx0XHRcdFx0c3dpdGNoIChyLmRldmljZV9odWlkLnRvU3RyaW5nKClbMF0pIHtcblx0XHRcdFx0XHRcdFx0XHRjYXNlIFwiMVwiOlxuXHRcdFx0XHRcdFx0XHRcdFx0ZGV2aWNlSWNvbiA9IGh0bWwoXCJpLmZhcyBmYS1wbHVnXCIpO1xuXHRcdFx0XHRcdFx0XHRcdFx0ZGV2aWNlVHlwZSA9IDE7XG5cblx0XHRcdFx0XHRcdFx0XHRcdGlmIChyLmRldmljZV9zdGF0dXMgPT0gMCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBodG1sKFwic3Bhbi5iYWRnZSBiZy1yZWRcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgV3nFgsSFY3pvbmVgXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGh0bWwoXCJpLmZhcyBmYS10aW1lc1wiKSk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBodG1sKFwic3Bhbi5iYWRnZSBiZy1ncmVlblwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJ0aXRsZVwiOiBpMThuIGBXxYLEhWN6b25lYFxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9LCBodG1sKFwiaS5mYXMgZmEtY2hlY2tcIikpO1xuXHRcdFx0XHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdFx0XHRjYXNlIFwiMlwiOlxuXHRcdFx0XHRcdFx0XHRcdFx0ZGV2aWNlSWNvbiA9IGh0bWwoXCJpLmZhcyBmYS10aW50XCIpO1xuXHRcdFx0XHRcdFx0XHRcdFx0ZGV2aWNlVHlwZSA9IDI7XG5cblx0XHRcdFx0XHRcdFx0XHRcdGlmIChyLmRldmljZV9zdGF0dXMgPT0gMCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBodG1sKFwic3Bhbi5iYWRnZSBiZy1yZWRcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgWmFta25pxJl0eWBcblx0XHRcdFx0XHRcdFx0XHRcdFx0fSwgaHRtbChcImkuZmFzIGZhLXRpbWVzXCIpKTtcblx0XHRcdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHN0YXR1c0luZGljYXRvciA9IGh0bWwoXCJzcGFuLmJhZGdlIGJnLWdyZWVuXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcInRpdGxlXCI6IGkxOG4gYE90d2FydHlgXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGh0bWwoXCJpLmZhcyBmYS1jaGVja1wiKSk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0XHRcdGNhc2UgXCIzXCI6XG5cdFx0XHRcdFx0XHRcdFx0XHRkZXZpY2VJY29uID0gaHRtbChcImkuZmFzIGZhLWJlbGxcIik7XG5cdFx0XHRcdFx0XHRcdFx0XHRkZXZpY2VUeXBlID0gMztcblx0XHRcdFx0XHRcdFx0XHRcdHN0YXR1c0luZGljYXRvciA9IFwiXCI7XG5cblx0XHRcdFx0XHRcdFx0XHRcdGlmIChyLmRldmljZV9zdGF0dXMgPT0gMCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBodG1sKFwic3Bhbi5iYWRnZSBiZy1ncmVlblwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJ0aXRsZVwiOiBpMThuIGBOaWV1emJyb2pvbnlgXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGh0bWwoXCJpLmZhcyBmYS1sb2NrLW9wZW5cIikpO1xuXHRcdFx0XHRcdFx0XHRcdFx0fSBlbHNlIGlmIChyLmRldmljZV9zdGF0dXMgPT0gMSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBodG1sKFwic3Bhbi5iYWRnZSBiZy1vcmFuZ2VcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgVXpicm9qb255YFxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9LCBodG1sKFwiaS5mYXMgZmEtbG9ja1wiKSk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHIuZGV2aWNlX3N0YXR1cyA9PSAyKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHN0YXR1c0luZGljYXRvciA9IGh0bWwoXCJzcGFuLmJhZGdlIGJnLXJlZCBibGlua1wiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJ0aXRsZVwiOiBpMThuIGBOQVJVU1pPTk8gQUxBUk0hISFgXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGh0bWwoXCJpLmZhcyBmYS1za3VsbC1jcm9zc2JvbmVzXCIpKTtcblx0XHRcdFx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFx0Y2FzZSBcIjRcIjpcblx0XHRcdFx0XHRcdFx0XHRcdGRldmljZUljb24gPSBodG1sKFwiaS5mYXMgZmEtdG9pbGV0LXBhcGVyXCIpO1xuXHRcdFx0XHRcdFx0XHRcdFx0ZGV2aWNlVHlwZSA9IDQ7XG5cdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBcIlwiO1xuXG5cdFx0XHRcdFx0XHRcdFx0XHRpZiAoci5kZXZpY2Vfc3RhdHVzID09IDApIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0c3RhdHVzSW5kaWNhdG9yID0gaHRtbChcInNwYW4uYmFkZ2UgYmctcmVkXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcInRpdGxlXCI6IGkxOG4gYFphdHJ6eW1hbm9gXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGh0bWwoXCJpLmZhcyBmYS1zdG9wLWNpcmNsZVwiKSk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHIuZGV2aWNlX3N0YXR1cyA9PSAxKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHN0YXR1c0luZGljYXRvciA9IGh0bWwoXCJzcGFuLmJhZGdlIGJnLWdyZWVuXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcInRpdGxlXCI6IGkxOG4gYFp3aWphbmllIHcgZ8OzcsSZYFxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9LCBodG1sKFwiaS5mYXMgZmEtYXJyb3ctY2lyY2xlLXVwXCIpKTtcblx0XHRcdFx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoci5kZXZpY2Vfc3RhdHVzID09IDIpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0c3RhdHVzSW5kaWNhdG9yID0gaHRtbChcInNwYW4uYmFkZ2UgYmctcmVkXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXRvZ2dsZVwiOiBcInRvb2x0aXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcInRpdGxlXCI6IGkxOG4gYFp3aW5pxJl0byB3IGfDs3LEmWBcblx0XHRcdFx0XHRcdFx0XHRcdFx0fSwgaHRtbChcImkuZmFzIGZhLWFycm93LWNpcmNsZS11cFwiKSk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHIuZGV2aWNlX3N0YXR1cyA9PSAtMSkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBodG1sKFwic3Bhbi5iYWRnZSBiZy1ncmVlblwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJkYXRhLXBsYWNlbWVudFwiOiBcImJvdHRvbVwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XCJ0aXRsZVwiOiBpMThuIGBSb3p3aWphbmllIHcgZMOzxYJgXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGh0bWwoXCJpLmZhcyBmYS1hcnJvdy1jaXJjbGUtZG93blwiKSk7XG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHIuZGV2aWNlX3N0YXR1cyA9PSAtMikge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBodG1sKFwic3Bhbi5iYWRnZSBiZy1yZWRcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFwidGl0bGVcIjogaTE4biBgUm96d2luacSZdG8gdyBkw7PFgmBcblx0XHRcdFx0XHRcdFx0XHRcdFx0fSwgaHRtbChcImkuZmFzIGZhLWFycm93LWNpcmNsZS1kb3duXCIpKTtcblx0XHRcdFx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFx0Y2FzZSBcIjVcIjpcblx0XHRcdFx0XHRcdFx0XHRcdGRldmljZUljb24gPSBodG1sKFwiaS5mYXMgZmEtY2xvdWQtc3VuLXJhaW5cIik7XG5cdFx0XHRcdFx0XHRcdFx0XHRkZXZpY2VUeXBlID0gNTtcblx0XHRcdFx0XHRcdFx0XHRcdGlmIChyLmRldmljZV9zdGF0dXMuc3BsaXQoXCIsXCIpWzBdID09IFwiMFwiKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHN0YXR1c0luZGljYXRvciA9IFwiQnJhayBvcGFkw7N3LCB0ZW1wLiBcIiArIHIuZGV2aWNlX3N0YXR1cy5zcGxpdChcIixcIilbMV0gKyBcInN0Liwgd2lnbC5cIiArIHIuZGV2aWNlX3N0YXR1cy5zcGxpdChcIixcIilbMl0gKyBcIiVcIjtcblx0XHRcdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHN0YXR1c0luZGljYXRvciA9IFwiT3BhZHksIHRlbXAuIFwiICsgci5kZXZpY2Vfc3RhdHVzLnNwbGl0KFwiLFwiKVsxXSArIFwic3QuLCB3aWdsLlwiICsgci5kZXZpY2Vfc3RhdHVzLnNwbGl0KFwiLFwiKVsyXSArIFwiJVwiO1xuXHRcdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRcdFx0XHRcdGRldmljZUljb24gPSBodG1sKFwiaS5mYXMgcXVlc3Rpb24tY2lyY2xlXCIpO1xuXHRcdFx0XHRcdFx0XHRcdFx0ZGV2aWNlVHlwZSA9IDA7XG5cdFx0XHRcdFx0XHRcdFx0XHRzdGF0dXNJbmRpY2F0b3IgPSBcIlwiO1xuXG5cdFx0XHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdFx0fVxuXG5cblxuXHRcdFx0XHRcdFx0XHRpZiAobmV3IERhdGUoci5kZXZpY2VfcGluZykuZ2V0VGltZSgpIDwgRGF0ZS5ub3coKSAtIDMwMDAwKSB7XG5cdFx0XHRcdFx0XHRcdFx0cGluZ0luZGljYXRvciA9IGh0bWwoXCJzcGFuLmJhZGdlIGJnLXJlZFwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtcGxhY2VtZW50XCI6IFwiYm90dG9tXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcImRhdGEtdG9nZ2xlXCI6IFwidG9vbHRpcFwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0XCJ0aXRsZVwiOiBpMThuIGBCcmFrIGtvbXVuaWthY2ppYFxuXHRcdFx0XHRcdFx0XHRcdH0sIGh0bWwoXCJpLmZhcyBmYS10aW1lc1wiKSk7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0cGluZ0luZGljYXRvciA9IGh0bWwoXCJzcGFuLmJhZGdlIGJnLWdyZWVuXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS1wbGFjZW1lbnRcIjogXCJib3R0b21cIixcblx0XHRcdFx0XHRcdFx0XHRcdFwiZGF0YS10b2dnbGVcIjogXCJ0b29sdGlwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcInRpdGxlXCI6IGkxOG4gYFBvxYLEhWN6ZW5pZSBPS2Bcblx0XHRcdFx0XHRcdFx0XHR9LCBodG1sKFwiaS5mYXMgZmEtY2hlY2tcIikpO1xuXHRcdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdFx0bGV0IHJlbW92ZUJ1dHRvbiA9IGh0bWwoXCJidXR0b24uYnRuIGJ0bi1kYW5nZXIgYnRuLXNtIG1yXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS10aW1lcyBpLm1yXCIpLFxuXHRcdFx0XHRcdFx0XHRcdGkxOG4gYFVzdcWEYFxuXHRcdFx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0XHRcdGxldCBlZGl0QnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLXdhcm5pbmcgYnRuLXNtIG1yXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS1wZW5jaWwtYWx0IGkubXJcIiksXG5cdFx0XHRcdFx0XHRcdFx0aTE4biBgRWR5dHVqYFxuXHRcdFx0XHRcdFx0XHQpO1xuXG5cblxuXG5cblx0XHRcdFx0XHRcdFx0bGV0IHN3aXRjaE9uQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLXN1Y2Nlc3MgYnRuLXNtIG1yXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRkaXNhYmxlZDogci5kZXZpY2Vfc3RhdHVzID49IDFcblx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS10b2dnbGUtb24gaS5tclwiKSxcblx0XHRcdFx0XHRcdFx0XHRpMThuIGBXxYLEhWN6YFxuXHRcdFx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0XHRcdGxldCBzd2l0Y2hPZmZCdXR0b24gPSBodG1sKFwiYnV0dG9uLmJ0biBidG4tZGFuZ2VyIGJ0bi1zbSBtclwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHR0eXBlOiBcImJ1dHRvblwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0ZGlzYWJsZWQ6IHIuZGV2aWNlX3N0YXR1cyA9PSAwXG5cdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtcG93ZXItb2ZmIGkubXJcIiksXG5cdFx0XHRcdFx0XHRcdFx0aTE4biBgV3nFgsSFY3pgXG5cdFx0XHRcdFx0XHRcdCk7XG5cblx0XHRcdFx0XHRcdFx0bGV0IGdvVXBCdXR0b24gPSBodG1sKFwiYnV0dG9uLmJ0biBidG4tc3VjY2VzcyBidG4tc20gbXJcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0dHlwZTogXCJidXR0b25cIixcblx0XHRcdFx0XHRcdFx0XHRcdGRpc2FibGVkOiByLmRldmljZV9zdGF0dXMgPT0gMSB8fCByLmRldmljZV9zdGF0dXMgPT0gMiB8fCByLmRldmljZV9zdGF0dXMgPT0gLTFcblx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS1hcnJvdy1jaXJjbGUtdXAgaS5tclwiKSxcblx0XHRcdFx0XHRcdFx0XHRpMThuIGBXIGfDs3LEmWBcblx0XHRcdFx0XHRcdFx0KTtcblxuXHRcdFx0XHRcdFx0XHRsZXQgZ29Eb3duQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLXN1Y2Nlc3MgYnRuLXNtIG1yXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRkaXNhYmxlZDogci5kZXZpY2Vfc3RhdHVzID09IC0xIHx8IHIuZGV2aWNlX3N0YXR1cyA9PSAtMiB8fCByLmRldmljZV9zdGF0dXMgPT0gMVxuXHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0aHRtbChcImkuZmFzIGZhLWFycm93LWNpcmNsZS1kb3duIGkubXJcIiksXG5cdFx0XHRcdFx0XHRcdFx0aTE4biBgVyBkw7PFgmBcblx0XHRcdFx0XHRcdFx0KTtcblxuXHRcdFx0XHRcdFx0XHRsZXQgc3RvcEJ1dHRvbiA9IGh0bWwoXCJidXR0b24uYnRuIGJ0bi1kYW5nZXIgYnRuLXNtIG1yXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRkaXNhYmxlZDogci5kZXZpY2Vfc3RhdHVzID09IDAgfHwgci5kZXZpY2Vfc3RhdHVzID09IDIgfHwgci5kZXZpY2Vfc3RhdHVzID09IC0yXG5cdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtc3RvcC1jaXJjbGUgaS5tclwiKSxcblx0XHRcdFx0XHRcdFx0XHRpMThuIGBTVE9QYFxuXHRcdFx0XHRcdFx0XHQpO1xuXG5cblxuXG5cblxuXHRcdFx0XHRcdFx0XHRlZGl0QnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0bGV0IGNvbnRpbnVlQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLXN1Y2Nlc3MgbXJcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR0eXBlOiBcImJ1dHRvblwiXG5cdFx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImkuZmFzIGZhLWNoZWNrIGkubXJcIiksXG5cdFx0XHRcdFx0XHRcdFx0XHRpMThuIGBFZHl0dWogdXJ6xIVkemVuaWVgXG5cdFx0XHRcdFx0XHRcdFx0KTtcblxuXHRcdFx0XHRcdFx0XHRcdGxldCBpbnB1dE5hbWUgPSBodG1sKFwiaW5wdXQjbmFtZS5mb3JtLWNvbnRyb2xcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0dHlwZTogXCJ0ZXh0XCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRwbGFjZWhvbGRlcjogaTE4biBgTmF6d2EgdXJ6xIVkemVuaWFgLFxuXHRcdFx0XHRcdFx0XHRcdFx0bWF4bGVuZ3RoOiAxNixcblx0XHRcdFx0XHRcdFx0XHRcdHZhbHVlOiByLmRldmljZV9uYW1lXG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHRcdFx0XHRsZXQgaW5wdXREZXNjcmlwdGlvbiA9IGh0bWwoXCJpbnB1dCNkZXNjLmZvcm0tY29udHJvbFwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHR0eXBlOiBcInRleHRcIixcblx0XHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyOiBpMThuIGBPcGlzIHVyesSFZHplbmlhYCxcblx0XHRcdFx0XHRcdFx0XHRcdG1heGxlbmd0aDogMjU2LFxuXHRcdFx0XHRcdFx0XHRcdFx0dmFsdWU6IHIuZGV2aWNlX2Rlc2NyaXB0aW9uXG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHRcdFx0XHRsZXQgaW5wdXRIVUlEID0gaHRtbChcImlucHV0I2h1aWQuZm9ybS1jb250cm9sXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwidGV4dFwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI6IGkxOG4gYElkZW50eWZpa2F0b3Igc3ByesSZdG93eWAsXG5cdFx0XHRcdFx0XHRcdFx0XHRtYXhsZW5ndGg6IDMyLFxuXHRcdFx0XHRcdFx0XHRcdFx0dmFsdWU6IHIuZGV2aWNlX2h1aWRcblx0XHRcdFx0XHRcdFx0XHR9KTtcblxuXHRcdFx0XHRcdFx0XHRcdGxldCBncm91cHNPcHRpb25zID0gW107XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5hcHAuZ3JvdXBzLmZvckVhY2goZ3JvdXAgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdFx0Z3JvdXBzT3B0aW9ucy5wdXNoKFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwib3B0aW9uXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogZ3JvdXAuZ3JvdXBfaWQsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZWN0ZWQ6IGdyb3VwLmdyb3VwX2lkID09IHIuZ3JvdXBfaWRcblx0XHRcdFx0XHRcdFx0XHRcdFx0fSwgZ3JvdXAuZ3JvdXBfbmFtZSlcblx0XHRcdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHRcdFx0XHRsZXQgaW5wdXRHcm91cCA9IGh0bWwoXCJzZWxlY3Qjbmdyb3VwYW1lLmZvcm0tY29udHJvbFwiLCBncm91cHNPcHRpb25zKTtcblxuXHRcdFx0XHRcdFx0XHRcdGNvbnRpbnVlQnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0XHR0aGlzLmFwcC5zZW5kRGF0YShcInVwZGF0ZURldmljZVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdGRldmljZV9pZDogci5kZXZpY2VfaWQsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGRldmljZV9uYW1lOiBpbnB1dE5hbWUudmFsdWUsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGRldmljZV9kZXNjcmlwdGlvbjogaW5wdXREZXNjcmlwdGlvbi52YWx1ZSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0ZGV2aWNlX2h1aWQ6IGlucHV0SFVJRC52YWx1ZSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0Z3JvdXBfaWQ6IGlucHV0R3JvdXAudmFsdWVcblx0XHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsXCIpLm1vZGFsKFwiaGlkZVwiKTtcblx0XHRcdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRcdFx0bGV0IGNhbmNlbEJ1dHRvbiA9IGh0bWwoXCJidXR0b24uYnRuIGJ0bi1kYW5nZXJcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHR0eXBlOiBcImJ1dHRvblwiXG5cdFx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImkuZmFzIGZhLXRpbWVzIGkubXJcIiksXG5cdFx0XHRcdFx0XHRcdFx0XHRpMThuIGBBbnVsdWpgKTtcblxuXHRcdFx0XHRcdFx0XHRcdGNhbmNlbEJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsXCIpLm1vZGFsKFwiaGlkZVwiKTtcblx0XHRcdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsVGl0bGVcIikudGV4dChpMThuIGBUd29yemVuaWUgbm93ZWdvIHVyesSFZHplbmlhYCk7XG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsRm9vdGVyXCIpLmhpZGUoKTtcblx0XHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxCb2R5XCIpLmVtcHR5KCk7XG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5hcHBlbmQoXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiZGl2LmZvcm0tZ3JvdXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImxhYmVsXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRmb3I6IFwibmFtZVwiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGkxOG4gYE5hendhIHVyesSFZHplbmlhYCksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGlucHV0TmFtZVxuXHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5hcHBlbmQoXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiZGl2LmZvcm0tZ3JvdXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImxhYmVsXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRmb3I6IFwiZGVzY1wiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGkxOG4gYE9waXMgdXJ6xIVkemVuaWFgKSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0aW5wdXREZXNjcmlwdGlvblxuXHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5hcHBlbmQoXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiZGl2LmZvcm0tZ3JvdXBcIixcblx0XHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImxhYmVsXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRmb3I6IFwiaHVpZFwiXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIGkxOG4gYElkZW50eWZpa2F0b3Igc3ByesSZdG93eWApLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRpbnB1dEhVSURcblx0XHRcdFx0XHRcdFx0XHRcdClcblx0XHRcdFx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKFxuXHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcImRpdi5mb3JtLWdyb3VwXCIsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJsYWJlbFwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0Zm9yOiBcImdyb3VwXCJcblx0XHRcdFx0XHRcdFx0XHRcdFx0fSwgaTE4biBgR3J1cGFgKSxcblx0XHRcdFx0XHRcdFx0XHRcdFx0aW5wdXRHcm91cFxuXHRcdFx0XHRcdFx0XHRcdFx0KVxuXHRcdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5hcHBlbmQoaHRtbChcImRpdi5hc2tCdXR0b25zXCIsIGNvbnRpbnVlQnV0dG9uLCBjYW5jZWxCdXR0b24pKTtcblx0XHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJzaG93XCIpO1xuXHRcdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRcdHJlbW92ZUJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdGxldCBjb250aW51ZUJ1dHRvbiA9IGh0bWwoXCJidXR0b24uYnRuIGJ0bi1zdWNjZXNzIG1yXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0dHlwZTogXCJidXR0b25cIlxuXHRcdFx0XHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJpLmZhcyBmYS1jaGVjayBpLm1yXCIpLFxuXHRcdFx0XHRcdFx0XHRcdFx0aTE4biBgVGFrLCB1c3XFhCB1cnrEhWR6ZW5pZSAke3IuZGV2aWNlX25hbWUudG9VcHBlckNhc2UoKX1gXG5cdFx0XHRcdFx0XHRcdFx0KTtcblxuXHRcdFx0XHRcdFx0XHRcdGNvbnRpbnVlQnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0XHR0aGlzLmFwcC5zZW5kRGF0YShcInJlbW92ZURldmljZVwiLCByLmRldmljZV9pZCk7XG5cdFx0XHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJoaWRlXCIpO1xuXHRcdFx0XHRcdFx0XHRcdH07XG5cblx0XHRcdFx0XHRcdFx0XHRsZXQgY2FuY2VsQnV0dG9uID0gaHRtbChcImJ1dHRvbi5idG4gYnRuLWRhbmdlclwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6IFwiYnV0dG9uXCJcblx0XHRcdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwiaS5mYXMgZmEtdGltZXMgaS5tclwiKSxcblx0XHRcdFx0XHRcdFx0XHRcdGkxOG4gYEFudWx1amApO1xuXG5cdFx0XHRcdFx0XHRcdFx0Y2FuY2VsQnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJoaWRlXCIpO1xuXHRcdFx0XHRcdFx0XHRcdH07XG5cblx0XHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxUaXRsZVwiKS50ZXh0KGkxOG4gYFVzdXdhbmllIHVyesSFZHplbmlhICR7ci5kZXZpY2VfbmFtZS50b1VwcGVyQ2FzZSgpfWApO1xuXHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEZvb3RlclwiKS5oaWRlKCk7XG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5lbXB0eSgpO1xuXHRcdFx0XHRcdFx0XHRcdCQoXCIjU21hbGxNb2RhbEJvZHlcIikuYXBwZW5kKGkxOG4gYFVzdW5pxJljaWUgdXJ6xIVkemVuaWEgc3Bvd29kdWplIHV0cmF0xJkgbW/FvGxpd2/Fm2NpIHN0ZXJvd2FuaWEgb3JheiB1c3VuacSZY2llIHdzenlzdGtpY2ggZGFueWNoIGhpc3Rvcnljem55Y2ggaSB3eXp3YWxhY3p5IHBvd2nEhXphbnljaCB6IGRhbnltIHVyesSFZHplbmllbSEgQ3p5IGNoY2VzeiBrb250eW51b3dhxIc/YCk7XG5cdFx0XHRcdFx0XHRcdFx0JChcIiNTbWFsbE1vZGFsQm9keVwiKS5hcHBlbmQoaHRtbChcImRpdi5hc2tCdXR0b25zXCIsIGNvbnRpbnVlQnV0dG9uLCBjYW5jZWxCdXR0b24pKTtcblx0XHRcdFx0XHRcdFx0XHQkKFwiI1NtYWxsTW9kYWxcIikubW9kYWwoXCJzaG93XCIpO1xuXHRcdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRcdHN3aXRjaE9uQnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5hcHAuc2VuZERhdGEoXCJwb3dlckRldmljZVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRpZDogci5kZXZpY2VfaWQsXG5cdFx0XHRcdFx0XHRcdFx0XHRzdGF0ZTogMVxuXHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRcdHN3aXRjaE9mZkJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdHRoaXMuYXBwLnNlbmREYXRhKFwicG93ZXJEZXZpY2VcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0aWQ6IHIuZGV2aWNlX2lkLFxuXHRcdFx0XHRcdFx0XHRcdFx0c3RhdGU6IDBcblx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0fTtcblxuXHRcdFx0XHRcdFx0XHRnb1VwQnV0dG9uLm9uY2xpY2sgPSAoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5hcHAuc2VuZERhdGEoXCJwb3dlckRldmljZVwiLCB7XG5cdFx0XHRcdFx0XHRcdFx0XHRpZDogci5kZXZpY2VfaWQsXG5cdFx0XHRcdFx0XHRcdFx0XHRzdGF0ZTogMVxuXHRcdFx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRcdGdvRG93bkJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdHRoaXMuYXBwLnNlbmREYXRhKFwicG93ZXJEZXZpY2VcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0aWQ6IHIuZGV2aWNlX2lkLFxuXHRcdFx0XHRcdFx0XHRcdFx0c3RhdGU6IC0xXG5cdFx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdH07XG5cblx0XHRcdFx0XHRcdFx0c3RvcEJ1dHRvbi5vbmNsaWNrID0gKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdHRoaXMuYXBwLnNlbmREYXRhKFwicG93ZXJEZXZpY2VcIiwge1xuXHRcdFx0XHRcdFx0XHRcdFx0aWQ6IHIuZGV2aWNlX2lkLFxuXHRcdFx0XHRcdFx0XHRcdFx0c3RhdGU6IDBcblx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0fTtcblxuXHRcdFx0XHRcdFx0XHRsZXQgYnV0dG9ucztcblx0XHRcdFx0XHRcdFx0c3dpdGNoIChkZXZpY2VUeXBlKSB7XG5cdFx0XHRcdFx0XHRcdFx0Y2FzZSAxOlxuXHRcdFx0XHRcdFx0XHRcdFx0YnV0dG9ucyA9IFtcblx0XHRcdFx0XHRcdFx0XHRcdFx0cmVtb3ZlQnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRlZGl0QnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRzd2l0Y2hPbkJ1dHRvbixcblx0XHRcdFx0XHRcdFx0XHRcdFx0c3dpdGNoT2ZmQnV0dG9uXG5cdFx0XHRcdFx0XHRcdFx0XHRdO1xuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFx0Y2FzZSAyOlxuXHRcdFx0XHRcdFx0XHRcdFx0YnV0dG9ucyA9IFtcblx0XHRcdFx0XHRcdFx0XHRcdFx0cmVtb3ZlQnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRlZGl0QnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRzd2l0Y2hPbkJ1dHRvbixcblx0XHRcdFx0XHRcdFx0XHRcdFx0c3dpdGNoT2ZmQnV0dG9uXG5cdFx0XHRcdFx0XHRcdFx0XHRdO1xuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFx0Y2FzZSAzOlxuXHRcdFx0XHRcdFx0XHRcdFx0YnV0dG9ucyA9IFtcblx0XHRcdFx0XHRcdFx0XHRcdFx0cmVtb3ZlQnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRlZGl0QnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRzd2l0Y2hPbkJ1dHRvbixcblx0XHRcdFx0XHRcdFx0XHRcdFx0c3dpdGNoT2ZmQnV0dG9uXG5cdFx0XHRcdFx0XHRcdFx0XHRdO1xuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFx0Y2FzZSA0OlxuXHRcdFx0XHRcdFx0XHRcdFx0YnV0dG9ucyA9IFtcblx0XHRcdFx0XHRcdFx0XHRcdFx0cmVtb3ZlQnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRlZGl0QnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRnb1VwQnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRzdG9wQnV0dG9uLFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRnb0Rvd25CdXR0b25cblx0XHRcdFx0XHRcdFx0XHRcdF07XG5cdFx0XHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdFx0XHRjYXNlIDU6XG5cdFx0XHRcdFx0XHRcdFx0XHRidXR0b25zID0gW107XG5cdFx0XHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRcdFx0XHRcdFx0YnV0dG9ucyA9IFtdO1xuXHRcdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdH1cblxuXG5cdFx0XHRcdFx0XHRcdHJvd3MucHVzaChcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwidHJcIixcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJ0ZC5jb2wtbWQtM1wiLCBbZGV2aWNlSWNvbiwgXCIgIFwiLCByLmRldmljZV9uYW1lXSksXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwidGQuY29sLW1kLTRcIiwgci5kZXZpY2VfZGVzY3JpcHRpb24pLFxuXHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcInRkLmNvbC1tZC0xXCIsIHN0YXR1c0luZGljYXRvciksXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwidGQuY29sLW1kLTFcIiwgcGluZ0luZGljYXRvciksXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwidGQuY29sLW1kLTNcIiwgYnV0dG9ucylcblx0XHRcdFx0XHRcdFx0XHQpXG5cdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHR9KTtcblxuXHRcdFx0XHRcdFx0cmVzdWx0LnB1c2goaHRtbChcInRhYmxlLnRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtY2VudGVyXCIsXG5cdFx0XHRcdFx0XHRcdGh0bWwoXCJ0aGVhZFwiLFxuXHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJ0clwiLFxuXHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcInRoXCIsIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0Y29sc3BhbjogNVxuXHRcdFx0XHRcdFx0XHRcdFx0fSwgaHRtbChcImg0LnN0cm9uZ1wiLCBkWzBdLmdyb3VwX25hbWUpLCBodG1sKFwic21hbGxcIiwgZFswXS5ncm91cF9kZXNjcmlwdGlvbikpXG5cdFx0XHRcdFx0XHRcdFx0KSxcblx0XHRcdFx0XHRcdFx0XHRodG1sKFwidHJcIixcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJ0aFwiLCBpMThuIGBVcnrEhWR6ZW5pZWApLFxuXHRcdFx0XHRcdFx0XHRcdFx0aHRtbChcInRoXCIsIGkxOG4gYE9waXNgKSxcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJ0aFwiLCBpMThuIGBBa3R1YWxueSBzdGFuYCksXG5cdFx0XHRcdFx0XHRcdFx0XHRodG1sKFwidGhcIiwgaTE4biBgS29tdW5pa2FjamFgKSxcblx0XHRcdFx0XHRcdFx0XHRcdGh0bWwoXCJ0aFwiKVxuXHRcdFx0XHRcdFx0XHRcdClcblx0XHRcdFx0XHRcdFx0KSxcblx0XHRcdFx0XHRcdFx0aHRtbChcInRib2R5XCIsIHJvd3MpXG5cdFx0XHRcdFx0XHQpKTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRyZXN1bHQuZm9yRWFjaChyID0+ICQoXCJkaXYjZGV2aWNlc0VkaXRvclwiKS5hcHBlbmQocikpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRcdGNvbnNvbGUud2FybihcIlRoZXJlIGlzIG5vIGFjdGlvbiBhc3NvY2lhdGVkIHdpdGggdGhlIG1lc3NhZ2VcIiwgY29tbWFuZCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdH1cblxuXHR0cnlUb1JlY29ubmVjdCgpIHtcblxuXHRcdGlmICh0aGlzLmFwcC53ZWJzb2NrZXRDbGllbnQucmVhZHlTdGF0ZSAhPSAxICYmICF0aGlzLndhc0tpY2tlZCkge1xuXHRcdFx0dGhpcy5hcHAud2Vic29ja2V0Q2xpZW50LmNsb3NlKCk7XG5cdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0Y29uc29sZS5sb2coXCJBdHRlbXB0IHRvIHJlY29ubmVjdC4uLlwiKTtcblx0XHRcdFx0dGhpcy5hcHAud2Vic29ja2V0SW5pdCgpO1xuXHRcdFx0fSwgMzAwMCk7XG5cdFx0fVxuXG5cdH1cblxufSIsImV4cG9ydCBkZWZhdWx0IHtcblx0bW9kZTogXCJkZXZlbG9wbWVudFwiLFxuXG5cdGF2YWlsYWJsZUxhbmdzOiBbe1xuXHRcdFx0bmFtZTogXCJQb2xza2lcIixcblx0XHRcdGNvZGU6IFwicGwtUExcIixcblx0XHRcdGZsYWc6IFwicGxcIlxuXHRcdH0sXG5cdFx0e1xuXHRcdFx0bmFtZTogXCJFbmdsaXNoXCIsXG5cdFx0XHRjb2RlOiBcImVuLUVOXCIsXG5cdFx0XHRmbGFnOiBcImdiXCJcblx0XHR9LFxuXHRcdC8vIHtcblx0XHQvLyBcdG5hbWU6IFwiRGV1dHNjaFwiLFxuXHRcdC8vIFx0Y29kZTogXCJkZS1ERVwiLFxuXHRcdC8vIFx0ZmxhZzogXCJkZVwiXG5cdFx0Ly8gfVxuXHRdLFxuXG5cdGFwaUVuZHBvaW50OiBcImh0dHA6Ly9cIiArIGxvY2F0aW9uLmhvc3QgKyBcIjo4MDgwLz9cIixcblx0d3NzRW5kcG9pbnQ6IFwid3M6Ly9cIiArIGxvY2F0aW9uLmhvc3QgKyBcIjo4MDkwXCJcbn07IiwiaW1wb3J0IGkxOG4sIHtcblx0aTE4bkNvbmZpZ1xufSBmcm9tIFwiZXMyMDE1LWkxOG4tdGFnXCI7XG5cbnN3aXRjaCAobG9jYWxTdG9yYWdlLmxhbmd1YWdlKSB7XG5cdGNhc2UgXCJlbi1FTlwiOlxuXHRcdGkxOG5Db25maWcoe1xuXHRcdFx0bG9jYWxlczogXCJlbi1FTlwiLFxuXHRcdFx0dHJhbnNsYXRpb25zOiByZXF1aXJlKFwiLi9lbi1FTi5qc1wiKS5kZWZhdWx0XG5cdFx0fSk7XG5cdFx0YnJlYWs7XG5cblx0Y2FzZSBcImRlLURFXCI6XG5cdFx0aTE4bkNvbmZpZyh7XG5cdFx0XHRsb2NhbGVzOiBcImRlLURFXCIsXG5cdFx0XHR0cmFuc2xhdGlvbnM6IHJlcXVpcmUoXCIuL2RlLURFLmpzXCIpLmRlZmF1bHRcblx0XHR9KTtcblx0XHRicmVhaztcblxuXHRkZWZhdWx0OlxuXHRcdGkxOG5Db25maWcoe1xuXHRcdFx0bG9jYWxlczogXCJwbC1QTFwiLFxuXHRcdFx0dHJhbnNsYXRpb25zOiB7fVxuXHRcdH0pO1xuXHRcdGJyZWFrO1xufSIsImV4cG9ydCBkZWZhdWx0IHtcblxuXHRcImduSU9UIDIuMFwiOiBcImduSU9UIDIuMCBBbm90aGVyIExhbmd1YWdlLi4uXCIsXG5cbn07IiwiZXhwb3J0IGRlZmF1bHQge1xuXG5cdFwiZ25JT1QgMi4wXCI6IFwiZ25JT1QgMi4wIEVuZ2xpc2ggVmVyc2lvblwiLFxuXG5cdC8vIGNvbXBvbmVudHMvbWFpbi9AbG9naW5QYW5lbC5qc1xuXHRcIlBvZGFqIG5henfEmSB1xbx5dGtvd25pa2EgaSBoYXPFgm8hXCI6IFwiUGxlYXNlIHR5cGUgeW91ciB1c2VybmFtZSBhbmQgcGFzc3dvcmQhXCIsXG5cdFwiUG9kYW55IGxvZ2luIGkvbHViIGhhc8WCbyBuaWUgc8SFIHBvcHJhd25lLiBad3LDs8SHIHV3YWfEmSBuYSB3aWVsa2/Fm8SHIGxpdGVyIHcgaGHFm2xlIVwiOiBcIkludmFsaWQgbG9naW4gb3IgcGFzc3dvcmQuIFBsZWFzZSB2ZXJpZnkgeW91ciB1c2VybmFtZSBhbmQgcGFzc3dvcmRcIixcblx0XCJEb3N0xJlwIHphYmxva293YW55IHphIHpieXQgd2llbGUgbmlldWRhbnljaCBsb2dvd2HFhCEgU3Byw7NidWogcG9ub3duaWUgcMOzxbpuaWVqLlwiOiBcIlRvbyBtYW55IGZhaWxlZCBsb2dpbiBhdHRlbXB0cyEgVHJ5IGFnYWluIGxhdGVyLlwiLFxuXHRcIkxvZ293YW5pZSBuaWV1ZGFuZS4gV3lzdMSFcGnFgiBuaWV6bmFueSBixYLEhWQuXCI6IFwiTG9naW4gZmFpbGVkLiBBbiB1bmtub3duIGVycm9yIG9jY3VyZWRcIixcblx0XCJOYXp3YSB1xbx5dGtvd25pa2FcIjogXCJVc2VybmFtZVwiLFxuXHRcIkhhc8WCb1wiOiBcIlBhc3N3b3JkXCIsXG5cdFwiWmFsb2d1aiBzacSZXCI6IFwiTG9nIEluIVwiLFxuXHRcIlptaWXFhCBqxJl6eWs6XCI6IFwiQ2hvb3NlIGxhbmd1YWdlOlwiLFxuXHRcIld5bG9ndWpcIjogXCJMb2cgT3V0IVwiLFxuXG5cdC8vIGNvbXBvbmVudHMvbWFpbi9tb2RhbHMuanNcblx0XCJXeXN0xIVwacWCIGLFgsSFZFwiOiBcIkFuIGVycm9yIG9jY3VyZWRcIixcblx0XCJaYW1rbmlqXCI6IFwiQ2xvc2VcIixcblxuXHQvLyBjb21wb25lbnRzL21haW4vQG1haW5QYW5lbC5qc1xuXHRcIlVyesSFZHplbmlhXCI6IFwiRGV2aWNlc1wiLFxuXHRcIlVyesSFZHplbmllXCI6IFwiRGV2aWNlXCIsXG5cdFwiTm93YSBncnVwYVwiOiBcIk5ldyBncm91cFwiLFxuXHRcIk5vd2UgdXJ6xIVkemVuaWVcIjogXCJOZXcgZGV2aWNlXCIsXG5cdFwiT3Bpc1wiOiBcIkRlc2NyaXB0aW9uXCIsXG5cdFwiQWt0dWFsbnkgc3RhblwiOiBcIkN1cnJlbnQgc3RhdGVcIixcblx0XCJLb211bmlrYWNqYVwiOiBcIkNvbm5lY3Rpb25cIixcblx0XCJVc3XFhFwiOiBcIlJlbW92ZVwiLFxuXHRcIkVkeXR1alwiOiBcIkVkaXRcIixcblx0XCJXxYLEhWN6XCI6IFwiVHVybiBPbiFcIixcblx0XCJXecWCxIVjelwiOiBcIlR1cm4gT2ZmIVwiLFxuXHRcIlfFgsSFY3pvbmVcIjogXCJTd2l0Y2hlZCBPblwiLFxuXHRcIld5xYLEhWN6b25lXCI6IFwiU3dpdGNoZWQgT2ZmXCIsXG5cdFwiQnJhayBrb211bmlrYWNqaVwiOiBcIkNvbm5lY3Rpb24gZXJyb3JcIixcblx0XCJLb211bmlrYWNqYSBPS1wiOiBcIkNvbm5lY3RlZFwiLFxuXHRcIkFudWx1alwiOiBcIkNhbmNlbFwiXG59OyIsInZhciAkID0gcmVxdWlyZShcImpxdWVyeVwiKTtcbndpbmRvdy5qUXVlcnkgPSAkO1xud2luZG93LiQgPSAkO1xuXG5pbXBvcnQgXCIuL2Fzc2V0cy9jc3MvQ0ROcy5jc3NcIjtcblxuaW1wb3J0IFwiYm9vdHN0cmFwXCI7XG5pbXBvcnQgXCJhZG1pbi1sdGVcIjtcblxuaW1wb3J0IFwiLi9hc3NldHMvY3NzL3N0eWxlLmNzc1wiO1xuaW1wb3J0IFwiLi9hc3NldHMvY3NzL2ZsYWdzLmNzc1wiO1xuXG5pbXBvcnQgXCIuL2kxOG4vY29uZmlnLmpzXCI7XG5cbnJlcXVpcmUoXCIuL3V0aWxzL3NvcnRCeVwiKTtcbnJlcXVpcmUoXCIuL3V0aWxzL2FuaW1hdGlvbnNcIik7XG5cbmltcG9ydCBhcHAgZnJvbSBcIi4vY29tcG9uZW50cy9hcHBcIjtcbm5ldyBhcHAoKTsiLCIoZnVuY3Rpb24gKCQpIHtcblx0JC5mbi5zaGFrZSA9IGZ1bmN0aW9uIChzZXR0aW5ncykge1xuXHRcdGlmICh0eXBlb2Ygc2V0dGluZ3MuaW50ZXJ2YWwgPT0gXCJ1bmRlZmluZWRcIikge1xuXHRcdFx0c2V0dGluZ3MuaW50ZXJ2YWwgPSAxMDA7XG5cdFx0fVxuXG5cdFx0aWYgKHR5cGVvZiBzZXR0aW5ncy5kaXN0YW5jZSA9PSBcInVuZGVmaW5lZFwiKSB7XG5cdFx0XHRzZXR0aW5ncy5kaXN0YW5jZSA9IDEwO1xuXHRcdH1cblxuXHRcdGlmICh0eXBlb2Ygc2V0dGluZ3MudGltZXMgPT0gXCJ1bmRlZmluZWRcIikge1xuXHRcdFx0c2V0dGluZ3MudGltZXMgPSA0O1xuXHRcdH1cblxuXHRcdGlmICh0eXBlb2Ygc2V0dGluZ3MuY29tcGxldGUgPT0gXCJ1bmRlZmluZWRcIikge1xuXHRcdFx0c2V0dGluZ3MuY29tcGxldGUgPSBmdW5jdGlvbiAoKSB7fTtcblx0XHR9XG5cblx0XHQkKHRoaXMpLmNzcyhcInBvc2l0aW9uXCIsIFwicmVsYXRpdmVcIik7XG5cblx0XHRmb3IgKHZhciBpdGVyID0gMDsgaXRlciA8IChzZXR0aW5ncy50aW1lcyArIDEpOyBpdGVyKyspIHtcblx0XHRcdCQodGhpcykuYW5pbWF0ZSh7XG5cdFx0XHRcdGxlZnQ6ICgoaXRlciAlIDIgPT0gMCA/IHNldHRpbmdzLmRpc3RhbmNlIDogc2V0dGluZ3MuZGlzdGFuY2UgKiAtMSkpXG5cdFx0XHR9LCBzZXR0aW5ncy5pbnRlcnZhbCk7XG5cdFx0fVxuXG5cdFx0JCh0aGlzKS5hbmltYXRlKHtcblx0XHRcdGxlZnQ6IDBcblx0XHR9LCBzZXR0aW5ncy5pbnRlcnZhbCwgc2V0dGluZ3MuY29tcGxldGUpO1xuXHR9O1xuXHQkLmZuLmJvdW5jZSA9IGZ1bmN0aW9uIChzZXR0aW5ncykge1xuXHRcdGlmICh0eXBlb2Ygc2V0dGluZ3MuaW50ZXJ2YWwgPT0gXCJ1bmRlZmluZWRcIikge1xuXHRcdFx0c2V0dGluZ3MuaW50ZXJ2YWwgPSAxMDA7XG5cdFx0fVxuXG5cdFx0aWYgKHR5cGVvZiBzZXR0aW5ncy5kaXN0YW5jZSA9PSBcInVuZGVmaW5lZFwiKSB7XG5cdFx0XHRzZXR0aW5ncy5kaXN0YW5jZSA9IDEwO1xuXHRcdH1cblxuXHRcdGlmICh0eXBlb2Ygc2V0dGluZ3MudGltZXMgPT0gXCJ1bmRlZmluZWRcIikge1xuXHRcdFx0c2V0dGluZ3MudGltZXMgPSA0O1xuXHRcdH1cblxuXHRcdGlmICh0eXBlb2Ygc2V0dGluZ3MuY29tcGxldGUgPT0gXCJ1bmRlZmluZWRcIikge1xuXHRcdFx0c2V0dGluZ3MuY29tcGxldGUgPSBmdW5jdGlvbiAoKSB7fTtcblx0XHR9XG5cblx0XHQkKHRoaXMpLmNzcyhcInBvc2l0aW9uXCIsIFwicmVsYXRpdmVcIik7XG5cblx0XHRmb3IgKHZhciBpdGVyID0gMDsgaXRlciA8IChzZXR0aW5ncy50aW1lcyArIDEpOyBpdGVyKyspIHtcblx0XHRcdCQodGhpcykuYW5pbWF0ZSh7XG5cdFx0XHRcdHRvcDogKChpdGVyICUgMiA9PSAwID8gc2V0dGluZ3MuZGlzdGFuY2UgOiBzZXR0aW5ncy5kaXN0YW5jZSAqIC0xKSlcblx0XHRcdH0sIHNldHRpbmdzLmludGVydmFsKTtcblx0XHR9XG5cblx0XHQkKHRoaXMpLmFuaW1hdGUoe1xuXHRcdFx0dG9wOiAwXG5cdFx0fSwgc2V0dGluZ3MuaW50ZXJ2YWwsIHNldHRpbmdzLmNvbXBsZXRlKTtcblx0fTtcbn0pKGpRdWVyeSk7IiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKGNvbGxlY3Rpb24sIHByb3BlcnR5KSB7XG5cdHZhciBpID0gMCxcblx0XHR2YWwsIGluZGV4LFxuXHRcdHZhbHVlcyA9IFtdLFxuXHRcdHJlc3VsdCA9IFtdO1xuXHRmb3IgKDsgaSA8IGNvbGxlY3Rpb24ubGVuZ3RoOyBpKyspIHtcblx0XHR2YWwgPSBjb2xsZWN0aW9uW2ldW3Byb3BlcnR5XTtcblx0XHRpbmRleCA9IHZhbHVlcy5pbmRleE9mKHZhbCk7XG5cdFx0aWYgKGluZGV4ID4gLTEpXG5cdFx0XHRyZXN1bHRbaW5kZXhdLnB1c2goY29sbGVjdGlvbltpXSk7XG5cdFx0ZWxzZSB7XG5cdFx0XHR2YWx1ZXMucHVzaCh2YWwpO1xuXHRcdFx0cmVzdWx0LnB1c2goW2NvbGxlY3Rpb25baV1dKTtcblx0XHR9XG5cdH1cblx0cmV0dXJuIHJlc3VsdDtcbn0iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAob25seU1lc3NhZ2UsIHVybCkge1xuXHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcblx0XHQkLmFqYXgoe1xuXHRcdFx0dXJsLFxuXHRcdFx0ZGF0YVR5cGU6IFwianNvblwiLFxuXHRcdFx0Y29udGV4dDogdGhpc1xuXHRcdH0pLmRvbmUoKHJlc3VsdCkgPT4ge1xuXHRcdFx0cmVzb2x2ZShvbmx5TWVzc2FnZSA/IHJlc3VsdC5tZXNzYWdlIDogcmVzdWx0KTtcblx0XHR9KTtcblx0fSk7XG59IiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKHZhbHVlKSB7XG5cdHJldHVybiB2YWx1ZS5yZXBsYWNlKC9bYS16QS1aXS9nLCBmdW5jdGlvbiAoYykge1xuXHRcdHJldHVybiBTdHJpbmcuZnJvbUNoYXJDb2RlKChjIDw9IFwiWlwiID8gOTAgOiAxMjIpID49IChjID0gYy5jaGFyQ29kZUF0KDApICsgMTMpID8gYyA6IGMgLSAyNik7XG5cdH0pO1xufSIsImZ1bmN0aW9uIGR5bmFtaWNTb3J0TXVsdGlwbGUoKSB7XG5cdHZhciBwcm9wcyA9IGFyZ3VtZW50cztcblx0cmV0dXJuIGZ1bmN0aW9uIChvYmoxLCBvYmoyKSB7XG5cdFx0dmFyIGkgPSAwLFxuXHRcdFx0cmVzdWx0ID0gMCxcblx0XHRcdG51bWJlck9mUHJvcGVydGllcyA9IHByb3BzLmxlbmd0aDtcblx0XHQvKiB0cnkgZ2V0dGluZyBhIGRpZmZlcmVudCByZXN1bHQgZnJvbSAwIChlcXVhbClcblx0XHQgKiBhcyBsb25nIGFzIHdlIGhhdmUgZXh0cmEgcHJvcGVydGllcyB0byBjb21wYXJlXG5cdFx0ICovXG5cdFx0d2hpbGUgKHJlc3VsdCA9PT0gMCAmJiBpIDwgbnVtYmVyT2ZQcm9wZXJ0aWVzKSB7XG5cdFx0XHRyZXN1bHQgPSBkeW5hbWljU29ydChwcm9wc1tpXSkob2JqMSwgb2JqMik7XG5cdFx0XHRpKys7XG5cdFx0fVxuXHRcdHJldHVybiByZXN1bHQ7XG5cdH07XG59XG5cbmZ1bmN0aW9uIGR5bmFtaWNTb3J0KHByb3BlcnR5KSB7XG5cdHZhciBzb3J0T3JkZXIgPSAxO1xuXHRpZiAocHJvcGVydHlbMF0gPT09IFwiLVwiKSB7XG5cdFx0c29ydE9yZGVyID0gLTE7XG5cdFx0cHJvcGVydHkgPSBwcm9wZXJ0eS5zdWJzdHIoMSk7XG5cdH1cblx0cmV0dXJuIGZ1bmN0aW9uIChhLCBiKSB7XG5cdFx0dmFyIHJlc3VsdDtcblx0XHRpZiAodHlwZW9mIChhW3Byb3BlcnR5XSkgPT09IFwic3RyaW5nXCIpIHtcblx0XHRcdHJlc3VsdCA9IGFbcHJvcGVydHldLmxvY2FsZUNvbXBhcmUoYltwcm9wZXJ0eV0sIGxvY2FsU3RvcmFnZS5sYW5ndWFnZSB8fCBcInBsLVBMXCIpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRyZXN1bHQgPSAoYVtwcm9wZXJ0eV0gPCBiW3Byb3BlcnR5XSkgPyAtMSA6IChhW3Byb3BlcnR5XSA+IGJbcHJvcGVydHldKSA/IDEgOiAwO1xuXHRcdH1cblx0XHRyZXR1cm4gcmVzdWx0ICogc29ydE9yZGVyO1xuXHR9O1xufVxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoQXJyYXkucHJvdG90eXBlLCBcInNvcnRCeVwiLCB7XG5cdGVudW1lcmFibGU6IGZhbHNlLFxuXHR3cml0YWJsZTogdHJ1ZSxcblx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gdGhpcy5zb3J0KGR5bmFtaWNTb3J0TXVsdGlwbGUuYXBwbHkobnVsbCwgYXJndW1lbnRzKSk7XG5cdH1cbn0pOyIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uICh0aW1lc3RhbXAsIGhpZGVTZWNvbmRzID0gZmFsc2UpIHtcblx0bGV0IHQgPSBuZXcgRGF0ZSh0aW1lc3RhbXAgfHwgMCk7XG5cdGxldCBob3VycyA9IFwiMFwiICsgdC5nZXRIb3VycygpO1xuXHRsZXQgbWludXRlcyA9IFwiMFwiICsgdC5nZXRNaW51dGVzKCk7XG5cdGxldCBzZWNvbmRzID0gXCIwXCIgKyB0LmdldFNlY29uZHMoKTtcblx0aWYgKGhpZGVTZWNvbmRzKSB7XG5cdFx0cmV0dXJuIGhvdXJzLnN1YnN0cigtMikgKyBcIjpcIiArIG1pbnV0ZXMuc3Vic3RyKC0yKTtcblx0fSBlbHNlIHtcblx0XHRyZXR1cm4gaG91cnMuc3Vic3RyKC0yKSArIFwiOlwiICsgbWludXRlcy5zdWJzdHIoLTIpICsgXCI6XCIgKyBzZWNvbmRzLnN1YnN0cigtMik7XG5cdH1cbn0iXSwic291cmNlUm9vdCI6IiJ9