import modals from "./main/modals";
import loginPanel from "./main/@loginPanel";
import loadingPanel from "./main/@loadingPanel";
import notifier from "./main/@notifier";
import timeFormatter from "../utils/timeFormatter";
import websocketLib from "./websocket.js";
import config from "../config.js"
import devicesTab from "../components/devices/@devicesTab";

export default class app {

	constructor() {

		if (window.app) {
			return window.app;
		}

		window.app = this;

		this.globalTimestamp;
		this.notifier = new notifier();
		this.websocketLib = new websocketLib();
		this.websocketClient;
		this.websocketConnections = 0;
		this.devices = [];
		this.groups = [];

		modals();

		if (!localStorage.language) {
			localStorage.language = "pl-PL";
		}

		$("body").tooltip({
			selector: "[data-toggle=tooltip]"
		});

		if (sessionStorage.apiToken && sessionStorage.loginFinished) {
			this.loadingPanel = new loadingPanel();
			document.body.className = "hold-transition login-page";
			document.body.appendChild(this.loadingPanel.render());
		} else {
			this.mainLoginPanel = new loginPanel();
			document.body.className = "hold-transition login-page";
			document.body.appendChild(this.mainLoginPanel.render());
		}

		setInterval(() => {
			if (!this.globalTimestamp) return;
			this.globalTimestamp += duration({
				seconds: 1
			});
			$("strong#syncTimeSpan").text(timeFormatter(this.globalTimestamp));
		}, duration({
			seconds: 1
		}));

	}

	websocketInit() {
		console.log(1);
		this.websocketClient = new WebSocket(`${config.wssEndpoint}/${sessionStorage.apiToken}`);
		this.websocketClient.onopen = (event) => this.websocketLib.onOpen(event);
		this.websocketClient.onclose = (event) => this.websocketLib.onClose(event);
		this.websocketClient.onmessage = (message) => this.websocketLib.onMessage(message);
		if (this.websocketConnections > 0) this.websocketConnections++;

	}

	sendData(command, value = undefined) {

		if (this.websocketClient.readyState === this.websocketClient.OPEN) {
			try {
				this.websocketClient.send(JSON.stringify({
					command,
					value
				}));
			} catch (error) {
				console.error("websocketClient.send() failure", error, command, value);
			}
		}

	}

}