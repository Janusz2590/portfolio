import {
	html
} from "redom";
import box from "../main/box";
import app from "../app";
import tab from "../main/tab";
import editableTextInput from "./editableTextInput";



export default class {

	constructor() {

		this.app = new app();
		this.devicesEditor = html("div#devicesEditor");
		this.groupsEditor = html("div#groupsEditor");

		let newGroupButton = html("button.btn btn-primary btn-sm mr", {
				type: "button"
			},
			html("i.fas fa-file i.mr"),
			i18n `Nowa grupa`
		);

		let newDeviceButton = html("button.btn btn-primary btn-sm mr", {
				type: "button"
			},
			html("i.fas fa-file i.mr"),
			i18n `Nowe urządzenie`
		);

		newDeviceButton.onclick = () => {
			let continueButton = html("button.btn btn-success mr", {
					type: "button"
				},
				html("i.fas fa-check i.mr"),
				i18n `Utwórz urządzenie`
			);

			let inputName = html("input#name.form-control", {
				type: "text",
				placeholder: `Nazwa urządzenia`,
				maxlength: 16
			});

			let inputDescription = html("input#desc.form-control", {
				type: "text",
				placeholder: `Opis urządzenia`,
				maxlength: 256
			});

			let inputHUID = html("input#huid.form-control", {
				type: "text",
				placeholder: `Identyfikator sprzętowy`,
				maxlength: 32
			});

			let groupsOptions = [];
			this.app.groups.forEach(group => {
				groupsOptions.push(
					html("option", {
						value: group.group_id
					}, group.group_name)
				);
			})



			let inputGroup = html("select#ngroupame.form-control", groupsOptions);

			continueButton.onclick = () => {
				this.app.sendData("updateDevice", {
					device_id: null,
					device_name: inputName.value,
					device_description: inputDescription.value,
					device_huid: inputHUID.value,
					group_id: inputGroup.value
				});
				$("#SmallModal").modal("hide");
			};

			let cancelButton = html("button.btn btn-danger", {
					type: "button"
				},
				html("i.fas fa-times i.mr"),
				i18n `Anuluj`);

			cancelButton.onclick = () => {
				$("#SmallModal").modal("hide");
			};

			$("#SmallModalTitle").text(i18n `Tworzenie nowego urządzenia`);
			$("#SmallModalFooter").hide();
			$("#SmallModalBody").empty();
			$("#SmallModalBody").append(
				html("div.form-group",
					html("label", {
						for: "name"
					}, i18n `Nazwa urządzenia`),
					inputName
				)
			);
			$("#SmallModalBody").append(
				html("div.form-group",
					html("label", {
						for: "desc"
					}, i18n `Opis urządzenia`),
					inputDescription
				)
			);
			$("#SmallModalBody").append(
				html("div.form-group",
					html("label", {
						for: "huid"
					}, i18n `Identyfikator sprzętowy`),
					inputHUID
				)
			);
			$("#SmallModalBody").append(
				html("div.form-group",
					html("label", {
						for: "group"
					}, i18n `Grupa`),
					inputGroup
				)
			);
			$("#SmallModalBody").append(html("div.askButtons", continueButton, cancelButton));
			$("#SmallModal").modal("show");
		};





		newGroupButton.onclick = () => {
			let continueButton = html("button.btn btn-success mr", {
					type: "button"
				},
				html("i.fas fa-check i.mr"),
				i18n `Utwórz grupę`
			);

			let inputName = html("input#name.form-control", {
				type: "text",
				placeholder: `Nazwa grupy`,
				maxlength: 32
			});

			let inputDescription = html("input#desc.form-control", {
				type: "text",
				placeholder: `Opis grupy`,
				maxlength: 256
			});

			continueButton.onclick = () => {
				this.app.sendData("updateGroup", {
					group_id: null,
					group_name: inputName.value,
					group_description: inputDescription.value
				});
				$("#SmallModal").modal("hide");
			};

			let cancelButton = html("button.btn btn-danger", {
					type: "button"
				},
				html("i.fas fa-times i.mr"),
				i18n `Anuluj`);

			cancelButton.onclick = () => {
				$("#SmallModal").modal("hide");
			};

			$("#SmallModalTitle").text(i18n `Tworzenie nowej grupy urządzeń`);
			$("#SmallModalFooter").hide();
			$("#SmallModalBody").empty();
			$("#SmallModalBody").append(
				html("div.form-group",
					html("label", {
						for: "name"
					}, i18n `Nazwa grupy`),
					inputName
				)
			);
			$("#SmallModalBody").append(
				html("div.form-group",
					html("label", {
						for: "desc"
					}, i18n `Opis grupy`),
					inputDescription
				)
			);
			$("#SmallModalBody").append(html("div.askButtons", continueButton, cancelButton));
			$("#SmallModal").modal("show");
		};

		return [
			html("div.row",
				html("div.col-xs-12",
					new box("primary", i18n `Urządzenia`, [newDeviceButton], this.devicesEditor)
				)
			),
			html("div.row",
				html("div.col-xs-12",
					new box("primary", i18n `Grupy`, [newGroupButton], this.groupsEditor)
				)
			)
		];

	}

}