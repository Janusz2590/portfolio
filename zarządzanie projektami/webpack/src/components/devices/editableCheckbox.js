import {
	html
} from "redom";
import dotProp from "dot-prop";

export default class {

	constructor(label, css, affectedObject, affectedProperty, defaultState = false) {

		this.affectedObject = affectedObject;
		this.affectedProperty = affectedProperty;

		if (dotProp.get(this.affectedObject, this.affectedProperty) === undefined) {
			dotProp.set(this.affectedObject, this.affectedProperty, defaultState);
		}

		this.domElement = html(`div.pretty ${css}`,
			html("input", {
				type: "checkbox",
				checked: dotProp.get(this.affectedObject, this.affectedProperty)
			}),
			html("div.state p-primary",
				html("label", label)
			)
		);

		this.domElement.onchange = (event) => {
			dotProp.set(this.affectedObject, this.affectedProperty, event.target.checked);
		};

		return this.domElement;

	}

}