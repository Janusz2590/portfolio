import {
	html
} from "redom";
import dotProp from "dot-prop";

export default class {

	constructor(elements, affectedObject, affectedProperty, defaultState = 0) {

		this.affectedObject = affectedObject;
		this.affectedProperty = affectedProperty;

		let options = [];
		elements.forEach(element => {
			options.push(html("option", {
				value: element.value,
			}, element.label ? element.label : element.value.toString()));
		});

		this.domElement = html("select.form-control", options);

		if (dotProp.get(this.affectedObject, this.affectedProperty) === undefined) {
			dotProp.set(this.affectedObject, this.affectedProperty, defaultState);
			$(this.domElement).children(`option:eq(${defaultState})`).prop("selected", true);
		} else {
			$(this.domElement).children(`option[value='${dotProp.get(this.affectedObject, this.affectedProperty)}']`).prop("selected", true);
		}

		this.domElement.onchange = (event) => {
			dotProp.set(this.affectedObject, this.affectedProperty, event.target.value);
		};

		return this.domElement;

	}

}