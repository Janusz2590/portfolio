import {
	html,
	setAttr
} from "redom";
import dotProp from "dot-prop";

export default class {

	constructor(min, max, step, affectedObject, affectedProperty, defaultState = 0, callback = null, uuid = null, disabled = null) {

		this.affectedObject = affectedObject;
		this.affectedProperty = affectedProperty;

		if (dotProp.get(this.affectedObject, this.affectedProperty) === undefined) {
			dotProp.set(this.affectedObject, this.affectedProperty, defaultState);
		}

		this.domElement = html("input.form-control", {
			min,
			max,
			step,
			type: "number",
			value: dotProp.get(this.affectedObject, this.affectedProperty)
		});

		if (uuid) {
			setAttr(this.domElement, {
				"data-uuid": uuid
			});
		}

		if (disabled) {
			setAttr(this.domElement, {
				"disabled": true
			});
		}

		this.domElement.onchange = (event) => {
			if (isNaN(parseFloat(event.target.value))) event.target.value = min;
			if (parseFloat(event.target.value) < min) event.target.value = min;
			if (parseFloat(event.target.value) > max) event.target.value = max;

			dotProp.set(this.affectedObject, this.affectedProperty, parseFloat(event.target.value));
			if (callback != null && typeof (callback) === "function") {
				callback();
			}
		};

		return this.domElement;

	}

}