import {
	html
} from "redom";
import dotProp from "dot-prop";

export default class {

	constructor(pattern, patternHelp, placeholder, affectedObject, affectedProperty, defaultState = "") {

		this.affectedObject = affectedObject;
		this.affectedProperty = affectedProperty;
		this.patternHelp = patternHelp;
		this.pattern = pattern;

		if (dotProp.get(this.affectedObject, this.affectedProperty) === undefined) {
			dotProp.set(this.affectedObject, this.affectedProperty, defaultState);
		}

		this.domElement = html("input.form-control", {
			placeholder,
			title: "",
			type: "text",
			value: dotProp.get(this.affectedObject, this.affectedProperty)
		});

		this.domElement.onkeyup = (event) => {
			let isValid = true;

			if (this.pattern && this.pattern instanceof RegExp) {
				if (!this.pattern.test(event.target.value)) {
					isValid = false;
				}
			}

			if (isValid) {
				dotProp.set(this.affectedObject, this.affectedProperty, event.target.value);
				$(this.domElement).tooltip("destroy");
			} else {
				$(this.domElement).tooltip({
					trigger: "manual",
					placement: "bottom",
					title: this.patternHelp
				});
				$(this.domElement).tooltip("show");
			}
		};

		return this.domElement;

	}

}