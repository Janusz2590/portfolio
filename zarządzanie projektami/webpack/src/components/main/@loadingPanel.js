import {
	html
} from "redom";
import app from "../app";

export default class {
	constructor() {

		this.app = new app();

		if (sessionStorage.apiToken) {
			this.app.websocketInit();
		}

	}

	render() {

		return html("div.lockscreen-wrapper",
			html("div.lockscreen-logo",
				html("strong", i18n `gnIOT 2.0`),
				html("p",
					html("i.fas fa-spinner fa-spin fa-fw fa-4x", {
						style: "margin-top: 15px;"
					})
				)
			)
		);

	}

}