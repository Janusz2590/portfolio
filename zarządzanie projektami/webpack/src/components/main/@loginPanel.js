import sha1 from "js-sha1";
import {
	html
} from "redom";
import config from "../../config";
import jsonDownloader from "../../utils/jsonDownloader";
import rot13 from "../../utils/rot13";
import app from "../app";
import loadingPanel from "./@loadingPanel";

export default class {

	constructor() {}

	changeLanguage(code) {

		localStorage.language = code;
		location.reload();

	}

	logIn(process) {

		if (!process) return;

		let username = $("#loginFormUsername").val().trim().toLowerCase();
		let password = rot13(sha1(username + $("#loginFormPassword").val()));
		let region = $("#loginFormRegion").find(":selected").val();

		if (username.length == 0 || $("#loginFormPassword").val().length == 0) {
			$("#errorModalText").text(i18n `Podaj nazwę użytkownika i hasło!`);
			$("#errorModal").modal();
			return;
		}

		sessionStorage.clear();

		$("#mainLoginButton").attr("disabled", "disabled");
		$("#mainLoginButton i").show(100, () => {
			jsonDownloader(false, config.apiEndpoint + jQuery.param({
				method: "auth",
				username,
				password
			})).then((response) => {
				if (response.success) {
					sessionStorage.apiToken = response.message;
					sessionStorage.loginTime = new Date();
					sessionStorage.regionName = region;
					sessionStorage.localUsername = username;

					$("div.login-box").animate({
							height: "toggle"
						},
						1000
					).promise().done(() => {
						var mainloadingPanel = new loadingPanel();
						$("div.login-box").remove();
						document.body.className = "hold-transition lockscreen";
						document.body.appendChild(mainloadingPanel.render());
						sessionStorage.loginFinished = true;
					});

				} else {
					switch (response.respCode) {
						case 14:
							$("#errorModalText").text(i18n `Podany login i/lub hasło nie są poprawne. Zwróć uwagę na wielkość liter w haśle!`);
							break;
						default:
							$("#errorModalText").text(i18n `Logowanie nieudane. Wystąpił nieznany błąd.`);
							break;
					}
					$("#errorModal").modal();
					$("#mainLoginButton").removeAttr("disabled");
					$("#mainLoginButton i").hide();
				}
			});
		});

	}

	render() {

		let userNameInput = html("input#loginFormUsername.form-control", {
			placeholder: i18n `Nazwa użytkownika`,
			required: true,
			autofocus: true
		});
		userNameInput.onkeyup = (event) => this.logIn(event.keyCode == 13);

		let passwordInput = html("input#loginFormPassword.form-control", {
			placeholder: i18n `Hasło`,
			type: "password",
			required: true
		});
		passwordInput.onkeyup = (event) => this.logIn(event.keyCode == 13);

		let loginButton = html("button.btn btn-primary btn-block#mainLoginButton", {
			type: "button",
		}, html("i.fas fa-spinner fa-spin fa-fw", {
			style: "display: none"
		}), i18n `Zaloguj się`);
		loginButton.onclick = () => this.logIn(true);

		let flags = [];
		config.availableLangs.filter(lang => lang.code != localStorage.language).forEach(element => {
			let flag = html(`div.flag flag-${element.flag}`, {
				"data-toggle": "tooltip",
				"data-placement": "bottom",
				"title": element.name
			});
			flag.onclick = () => this.changeLanguage(element.code);
			flags.push(flag);
		});

		return html("div.login-box",
			html("div.login-logo", i18n `gnIOT 2.0`),
			html("div.login-box-body",
				html("div.row",
					html("div.col-xs-12 text-center#leftLoginPanel",
						html("h3", i18n `Zaloguj się`),
						html("form", {
								autocomplete: "on"
							},

							html("div.form-group has-feedback",
								userNameInput,
								html("span.glyphicon glyphicon-user form-control-feedback")
							),

							html("div.form-group has-feedback",
								passwordInput,
								html("span.glyphicon glyphicon-lock form-control-feedback")
							),

							html("div.form-group",
								loginButton
							)
						)
					),
					html("div.col-md-6 hidden-md hidden-lg",
						html("hr")
					)
				),
				html("div.row",
					html("div.col-xs-12 text-center",
						[
							html("hr"),
							html("p", i18n `Zmień język:`),
							flags,
							html("hr"),
							html("span", "Made with ❤️ by gnIOT 2.0 Developers Team")
						]
					)
				)
			)
		);

	}

}