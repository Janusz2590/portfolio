import {
	html
} from "redom";
import {
	ContextMenu
} from "jquery-contextmenu/";

import tab from "./tab";
import devicesTab from "../devices/@devicesTab";
import jsonDownloader from "../../utils/jsonDownloader";
import config from "../../config";

export default class {

	constructor() {


	}

	render() {

		let logOutButton = html("a.hidden-sm", {
				style: "cursor: pointer; margin: 3px;"
			},
			html("span.badge bg-purple", {
					"data-placement": "bottom",
					"data-toggle": "tooltip",
					"title": i18n `Wyloguj`
				},
				html("i.fas fa-sign-out-alt mr"),
				i18n `Wyloguj`
			)
		);

		logOutButton.onclick = () => {
			sessionStorage.clear();
			location.reload();
		};

		this.devicesTab = new devicesTab();

		return html("div.wrapper",
			html("header.main-header",
				html("nav.navbar navbar-static-top",
					html("div.container",
						html("div.navbar-header",
							html("strong.navbar-brand hidden-md hidden-sm hidden-xs", i18n `gnIOT 2.0`),
							html("button.navbar-toggle collapsed", {
									type: "button",
									"data-toggle": "collapse",
									"data-target": "#navbar-collapse"
								},
								html("i.fas fa-bars")
							)
						),
						html("div.collapse navbar-collapse pull-left#navbar-collapse",
							html("ul.nav navbar-nav", {
									role: "tablist"
								},
								html("li.active",
									html("a.bg-light-blue", {
											href: "#gniotTabDevices",
											role: "tab",
											"data-type": "mainMenu",
											"data-toggle": "tab",
											"data-role": "devices",
											style: "display: none"
										},
										html("i.fas fa-traffic-light mr"),
										i18n `Urządzenia`
									)
								),
								html("li",
									html("a", {
											style: "background-color: #222d32 !important; cursor: default;",
											href: "javascript:;"
										},
										html("a.hidden-sm", {
												style: "cursor: default; margin: 3px;"
											},
											html("span.badge bg-red#websocketStatusIndicatorBackground", {
													"data-placement": "bottom",
													"data-toggle": "tooltip",
													"title": i18n `Status połączenia`
												},
												html("i.fas fa-plug mr"),
												html("i.fas fa-times#websocketStatusIndicatorIcon")
											)
										),
										html("a.hidden-sm", {
												style: "cursor: default; margin: 3px;"
											},
											html("span.badge bg-yellow", {
													"data-placement": "bottom",
													"data-toggle": "tooltip",
													"title": i18n `Nazwa użytkownika`
												},
												html("i.fas fa-user mr"),
												html("span#dispatchersOnlineCounter", sessionStorage.localUsername)
											)
										),
										logOutButton
									)
								)
							)
						),
						html("div.navbar-custom-menu#moveCustomMenuToShowBars", {
								style: "position: absolute;"
							},
							html("ul.nav navbar-nav navbar-nocolors",
								html("li.dropdown notifications-menu hidden-sm",
									html("a.dropdown-toggle", {
											"data-toggle": "dropdown",
											style: "cursor: pointer"
										},
										html("i.fas fa-bell"),
										html("span.label label-warning", 10)
									),
									html("ul.dropdown-menu",
										html("li.header",
											i18n `You have 10 notifications`
										),
										html("li",
											html("ul.menu",
												html("li",
													html("a",
														html("i.fas fa-users text-aqua"),
														"5 new members joined today"
													)
												)
											)
										),
										html("li.footer",
											html("a", "View all")
										)
									)
								),
								html("li",
									html("a", {
											style: "font-size:19px;cursor:default"
										},
										html("i.fas fa-clock mr"),
										html("strong#syncTimeSpan", "00:00:00")
									)
								)
							)
						)
					)
				)
			),
			html("div.content-wrapper swdrMainWindow",
				html("section.content",
					html("div.tab-content", {
							"data-type": "mainMenu"
						},
						new tab("gniotTabDevices", true, this.devicesTab),
					)
				)
			)
		);

	}

	enableTabs(value) {
		[...Number(value || 0).toString(2)].map(n => Boolean(parseInt(n))).forEach((element, index) => {
			$(`a[data-type='mainMenu']:eq(${index})`).css("display", element ? "block" : "none");
		});
	}

	renderSceneriesTable(value) {
		$("tbody#sceneriesLive").empty();

		let uniqueDispatchers = [...value].map(scenery => scenery.dispatcherData.id_member).filter((v, i, a) => a.indexOf(v) === i);
		$("span#dispatchersOnlineCounter").text(uniqueDispatchers.length);
		$.contextMenu("destroy", "span[data-unique]");

		value.forEach(scenery => {
			$("tbody#sceneriesLive").append(
				html("tr",
					html("td",
						html("span.cPointer", {
							"data-unique": scenery.uniquePairHash,
							"data-toggle": "tooltip",
							"data-placement": "bottom",
							"title": i18n `Menu podręczne`,
							style: "font-weight: bold;"
						}, scenery.sceneryName)),
					html("td",
						html("span.cPointer", {
							"data-unique": scenery.uniquePairHash,
							"data-toggle": "tooltip",
							"data-placement": "bottom",
							"title": i18n `Menu podręczne`
						}, scenery.dispatcherData.member_name)),
					html("td.trainsOnScenery", {
						"data-hash": scenery.sceneryHash,
					}, `${scenery.current_users} / ${scenery.max_users}`),
					html("td", "status"),
					html("td",
						html("span.label label-primary", {
							"data-toggle": "tooltip",
							"data-placement": "bottom",
							"title": i18n `Poziom doświadczenia`
						}, (scenery.dispatcherData.level < 2 ? "L" : scenery.dispatcherData.level))
					)
				)
			);

			new ContextMenu().create({
				trigger: "left",
				selector: `span[data-unique='${scenery.uniquePairHash}']`,
				items: {
					privateMessage: {
						icon: "fa-envelope",
						name: i18n `Wyślij prywatną wiadomość`,
						callback: function () {
							privateMessageDialog(scenery.dispatcherData.member_name, scenery.dispatcherData.id_member);
						}
					},
					dispatcherProfile: {
						icon: "fa-user",
						name: i18n `Wyświetl profil dyżurnego`,
						callback: function () {
							jsonDownloader(true, config.apiEndpoint + jQuery.param({
								method: "getUsersInfoById",
								id: scenery.dispatcherData.id_member,
								token: sessionStorage.apiToken
							})).then(value => {
								if (value[0]) {
									let profile = value[0];
									let cust_trains = 0;
									let groups = [
										html("span.label", {
												style: "background-color: #000; display: block; font-size: 95%; margin: 5px;"
											},
											i18n `Użytkownik`
										)
									];

									try {
										cust_trains = profile.user_stats.find(s => s.variable == "cust_trains").value;
									} catch (error) {
										cust_trains = 0;
									}

									profile.user_groups.forEach(group => {
										if (group.online_color == "") group.online_color = "#000";
										groups.push(
											html("span.label", {
													style: `background-color:${group.online_color || "#000"}; display: block; font-size: 95%; margin: 5px;`
												},
												group.group_name
											)
										);
									});

									$("#SmallModalTitle").text(i18n `Profil dyżurnego ${profile.member_name}`);
									$("#SmallModalFooter").show();
									$("#SmallModalBody").empty();
									$("#SmallModalBody").append(
										html("div.row",
											html("div.col-md-6",
												html("dl.dl-horizontal",
													html("dt", i18n `Nazwa użytkownika:`),
													html("dd", profile.member_name),
													html("dt", i18n `Data rejestracji:`),
													html("dd", new Date(profile.date_registered * 1000).toLocaleDateString())
												)
											),
											html("div.col-md-6",
												html("dl.dl-horizontal",
													html("dt", i18n `Obsłużone pociągi:`),
													html("dd", cust_trains),
													html("dt", i18n `Poziom doświadczenia:`),
													html("dd", html("span.label label-primary", `${profile.levels.dispatcher < 2 ? "L" : profile.levels.dispatcher + " / 20"}`))
												))
										),
										html("div.row",
											html("div.col-md-12",
												html("dl.dl-horizontal",
													html("dt", i18n `Grupy:`),
													html("dd", groups)
												)
											)
										)
									);
									$("#SmallModal").modal("show");
								}
							});
						}
					}
				}
			});
		});
	}

}