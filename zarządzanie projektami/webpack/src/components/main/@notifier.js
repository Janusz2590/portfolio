export default class {

	constructor() {

		this.supported = false;

		if (("Notification" in window)) {
			this.supported = true;
			if (Notification.permission !== "denied") {
				Notification.requestPermission();
			}
		}

	}

	notify(body, silent, callback) {

		if (!this.supported) return;

		if (Notification.permission === "granted") {
			let notification = new Notification("gnIOT 2.0", {
				body,
				silent
			});
			notification.onclick = () => {
				window.focus();
				callback();
			};
		}
	}

}