import {
	html
} from "redom";

export default class {

	constructor(type, title, tools, body, footer) {

		return html(`div.box box-${type}`,
			html("div.box-header with-border",
				html("h3.box-title", title),
				html("div.box-tools pull-right", tools)
			),
			html("div.box-body", body),
			html("div.box-footer", footer)
		);

	}

}