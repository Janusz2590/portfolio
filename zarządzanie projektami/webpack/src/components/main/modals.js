import {
	html
} from "redom";

export default function () {

	document.body.appendChild(
		html("div.modal fade#errorModal",
			html("div.modal-dialog modal-danger",
				html("div.modal-content",
					html("div.modal-header",
						html("button.close", {
								"data-dismiss": "modal",
								"data-toggle": "tooltip",
								"data-placement": "bottom",
								"title": i18n `Zamknij`
							},
							html("span", "x")
						),
						html("h4.modal-title", i18n `Wystąpił błąd`)
					),
					html("div.modal-body",
						html("p#errorModalText")
					),
					html("div.modal-footer",
						html("button.btn btn-danger", {
							"data-dismiss": "modal",
							type: "button"
						}, i18n `Zamknij`)
					)
				)
			)
		)
	);

	$("#errorModal").on("show.bs.modal	", function () {
		$("div#errorModal div.modal-content").bounce({
			interval: 100,
			distance: 20,
			times: 4
		});
	});

	document.body.appendChild(
		html("div.modal fade#SmallModal",
			html("div.modal-dialog",
				html("div.modal-content",
					html("div.modal-header",
						html("button.close", {
								"data-dismiss": "modal",
								"data-toggle": "tooltip",
								"data-placement": "bottom",
								"title": i18n `Zamknij`
							},
							html("span", "x")
						),
						html("h4.modal-title#SmallModalTitle")
					),
					html("div.modal-body#SmallModalBody"),
					html("div.modal-footer#SmallModalFooter",
						html("button.btn btn-danger", {
							"data-dismiss": "modal",
							type: "button"
						}, i18n `Zamknij`)
					)
				)
			)
		)
	);

	$("#SmallModal").on("show.bs.modal	", function () {
		$("div#SmallModal div.modal-content").bounce({
			interval: 100,
			distance: 20,
			times: 4
		});
	});

	$("#SmallModal").on("shown.bs.modal	", function () {
		$("div#SmallModal input:first").focus();
	});

}