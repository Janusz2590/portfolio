import {
	html
} from "redom";

export default class {

	constructor(id, isActive, childrens, channelid) {

		return html(`div.tab-pane fade ${isActive ? "in active" : ""}#${id}`, channelid != undefined ? {
			"data-channelid": channelid
		} : null, childrens);

	}

}