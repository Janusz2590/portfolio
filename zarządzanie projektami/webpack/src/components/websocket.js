/* eslint-disable no-undef */
import mainPanel from "./main/@mainPanel";
import app from "./app";
import groupBy from "../utils/groupBy";
import {
	html
} from "redom";

export default class {

	constructor() {

		this.app = new app();
		this.wasKicked = false;

	}

	onOpen() {
		this.app.websocketConnections++;
		if (this.app.websocketConnections == 1) {
			this.mainPanel = new mainPanel();
			document.body.className = "hold-transition";
			document.body.appendChild(this.mainPanel.render());
			$("div.lockscreen-wrapper").remove();
		}

		$("#websocketStatusIndicatorIcon").removeClass("fa-times");
		$("#websocketStatusIndicatorIcon").addClass("fa-check");
		$("#websocketStatusIndicatorBackground").removeClass("bg-red");
		$("#websocketStatusIndicatorBackground").addClass("bg-green");

		console.log("Websocket connected!");

	}

	onClose() {

		$("#websocketStatusIndicatorIcon").removeClass("fa-check");
		$("#websocketStatusIndicatorIcon").addClass("fa-times");
		$("#websocketStatusIndicatorBackground").removeClass("bg-green");
		$("#websocketStatusIndicatorBackground").addClass("bg-red");

		console.warn("Websocket disconnected!");
		this.tryToReconnect();

	}

	onMessage(message) {

		let command;
		try {
			command = JSON.parse(message.data).command;
		} catch (error) {
			console.error("Uncaught error during incoming data parsing", message);
		}

		if (command) {
			let value = JSON.parse(message.data).value;
			switch (command) {
				case "app/logout":
					sessionStorage.clear();
					location.reload();
					break;
				case "ping":
					this.app.sendData("pong");
					break;
				case "app/timestamp":
					this.app.globalTimestamp = value;
					break;
				case "app/enableTabs":
					this.mainPanel.enableTabs(value);
					break;
				case "app/forceRefresh":
					setTimeout(() => {
							this.app.sendData("getDevices");
							this.app.sendData("getGroups");
						},
						500);
					break;
				case "getGroups":
					this.app.groups = value;
					let rows = [];

					$("div#groupsEditor").empty();
					this.app.groups.forEach(g => {

						let editButton = html("button.btn btn-warning btn-sm mr", {
								type: "button"
							},
							html("i.fas fa-pencil-alt i.mr"),
							i18n `Edytuj`
						);
						let removeButton = html("button.btn btn-danger btn-sm mr", {
								type: "button"
							},
							html("i.fas fa-times i.mr"),
							i18n `Usuń`
						);

						removeButton.onclick = () => {
							let continueButton = html("button.btn btn-success mr", {
									type: "button"
								},
								html("i.fas fa-check i.mr"),
								i18n `Usuń grupę ${g.group_name.toUpperCase()}`
							);

							continueButton.onclick = () => {
								this.app.sendData("removeGroup", g.group_id);
								$("#SmallModal").modal("hide");
							};

							let cancelButton = html("button.btn btn-danger", {
									type: "button"
								},
								html("i.fas fa-times i.mr"),
								i18n `Anuluj`);

							cancelButton.onclick = () => {
								$("#SmallModal").modal("hide");
							};

							$("#SmallModalTitle").text(i18n `Usuwanie grupy ${g.group_name.toUpperCase()}`);
							$("#SmallModalFooter").hide();
							$("#SmallModalBody").empty();
							if (this.app.devices.filter(d => d.group_id == g.group_id).length > 0) {
								$("#SmallModalBody").append(i18n `Nie można usunąć tej grupy, ponieważ zawiera ona przypisane urządzenia!`);
								continueButton.setAttribute("disabled", "disabled");
							}
							$("#SmallModalBody").append(html("div.askButtons", continueButton, cancelButton));
							$("#SmallModal").modal("show");
						};

						editButton.onclick = () => {
							let continueButton = html("button.btn btn-success mr", {
									type: "button"
								},
								html("i.fas fa-check i.mr"),
								i18n `Zapisz grupę`
							);

							let inputName = html("input#name.form-control", {
								type: "text",
								placeholder: i18n `Nazwa grupy`,
								maxlength: 32,
								value: g.group_name
							});

							let inputDescription = html("input#desc.form-control", {
								type: "text",
								placeholder: i18n `Opis grupy`,
								maxlength: 256,
								value: g.group_description
							});

							continueButton.onclick = () => {
								this.app.sendData("updateGroup", {
									group_id: g.group_id,
									group_name: inputName.value,
									group_description: inputDescription.value
								});
								$("#SmallModal").modal("hide");
							};

							let cancelButton = html("button.btn btn-danger", {
									type: "button"
								},
								html("i.fas fa-times i.mr"),
								i18n `Anuluj`);

							cancelButton.onclick = () => {
								$("#SmallModal").modal("hide");
							};

							$("#SmallModalTitle").text(i18n `Tworzenie nowej grupy urządzeń`);
							$("#SmallModalFooter").hide();
							$("#SmallModalBody").empty();
							$("#SmallModalBody").append(
								html("div.form-group",
									html("label", {
										for: "name"
									}, i18n `Nazwa grupy`),
									inputName
								)
							);
							$("#SmallModalBody").append(
								html("div.form-group",
									html("label", {
										for: "desc"
									}, i18n `Opis grupy`),
									inputDescription
								)
							);
							$("#SmallModalBody").append(html("div.askButtons", continueButton, cancelButton));
							$("#SmallModal").modal("show");
						};


						rows.push(
							html("tr",
								html("td.col-md-5", g.group_name),
								html("td.col-md-5", g.group_description),
								html("td.col-md-1", editButton),
								html("td.col-md-1", removeButton)
							)
						);
					});

					groupsEditor.append(html("table.table table-striped table-center",
						html("tbody", rows)
					));
					break;
				case "removeDevice":
				case "removeGroup":
				case "powerDevice":
				case "updateDevice":
				case "updateGroup":
					setTimeout(() => {
							this.app.sendData("getDevices");
							this.app.sendData("getGroups");
						},
						500);
					break;
				case "getDevices":
					this.app.devices = value;
					$("div#devicesEditor").empty();

					let result = [];
					groupBy(this.app.devices, "group_id").forEach(d => {
						if (d.length < 1) return;

						let rows = [];
						d.forEach(r => {
							let statusIndicator;
							let pingIndicator;
							let deviceIcon;
							let deviceType = 0;

							switch (r.device_huid.toString()[0]) {
								case "1":
									deviceIcon = html("i.fas fa-plug");
									deviceType = 1;

									if (r.device_status == 0) {
										statusIndicator = html("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Wyłączone`
										}, html("i.fas fa-times"));
									} else {
										statusIndicator = html("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Włączone`
										}, html("i.fas fa-check"));
									}

									break;
								case "2":
									deviceIcon = html("i.fas fa-tint");
									deviceType = 2;

									if (r.device_status == 0) {
										statusIndicator = html("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Zamknięty`
										}, html("i.fas fa-times"));
									} else {
										statusIndicator = html("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Otwarty`
										}, html("i.fas fa-check"));
									}

									break;
								case "3":
									deviceIcon = html("i.fas fa-bell");
									deviceType = 3;
									statusIndicator = "";

									if (r.device_status == 0) {
										statusIndicator = html("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Nieuzbrojony`
										}, html("i.fas fa-lock-open"));
									} else if (r.device_status == 1) {
										statusIndicator = html("span.badge bg-orange", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Uzbrojony`
										}, html("i.fas fa-lock"));
									} else if (r.device_status == 2) {
										statusIndicator = html("span.badge bg-red blink", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `NARUSZONO ALARM!!!`
										}, html("i.fas fa-skull-crossbones"));
									}

									break;
								case "4":
									deviceIcon = html("i.fas fa-toilet-paper");
									deviceType = 4;
									statusIndicator = "";

									if (r.device_status == 0) {
										statusIndicator = html("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Zatrzymano`
										}, html("i.fas fa-stop-circle"));
									} else if (r.device_status == 1) {
										statusIndicator = html("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Zwijanie w górę`
										}, html("i.fas fa-arrow-circle-up"));
									} else if (r.device_status == 2) {
										statusIndicator = html("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Zwinięto w górę`
										}, html("i.fas fa-arrow-circle-up"));
									} else if (r.device_status == -1) {
										statusIndicator = html("span.badge bg-green", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Rozwijanie w dół`
										}, html("i.fas fa-arrow-circle-down"));
									} else if (r.device_status == -2) {
										statusIndicator = html("span.badge bg-red", {
											"data-placement": "bottom",
											"data-toggle": "tooltip",
											"title": i18n `Rozwinięto w dół`
										}, html("i.fas fa-arrow-circle-down"));
									}

									break;
								case "5":
									deviceIcon = html("i.fas fa-cloud-sun-rain");
									deviceType = 5;
									if (r.device_status.split(",")[0] == "0") {
										statusIndicator = "Brak opadów, temp. " + r.device_status.split(",")[1] + "st., wigl." + r.device_status.split(",")[2] + "%";
									} else {
										statusIndicator = "Opady, temp. " + r.device_status.split(",")[1] + "st., wigl." + r.device_status.split(",")[2] + "%";
									}
									break;
								default:
									deviceIcon = html("i.fas question-circle");
									deviceType = 0;
									statusIndicator = "";

									break;
							}



							if (new Date(r.device_ping).getTime() < Date.now() - 30000) {
								pingIndicator = html("span.badge bg-red", {
									"data-placement": "bottom",
									"data-toggle": "tooltip",
									"title": i18n `Brak komunikacji`
								}, html("i.fas fa-times"));
							} else {
								pingIndicator = html("span.badge bg-green", {
									"data-placement": "bottom",
									"data-toggle": "tooltip",
									"title": i18n `Połączenie OK`
								}, html("i.fas fa-check"));
							}

							let removeButton = html("button.btn btn-danger btn-sm mr", {
									type: "button"
								},
								html("i.fas fa-times i.mr"),
								i18n `Usuń`
							);

							let editButton = html("button.btn btn-warning btn-sm mr", {
									type: "button"
								},
								html("i.fas fa-pencil-alt i.mr"),
								i18n `Edytuj`
							);





							let switchOnButton = html("button.btn btn-success btn-sm mr", {
									type: "button",
									disabled: r.device_status >= 1
								},
								html("i.fas fa-toggle-on i.mr"),
								i18n `Włącz`
							);

							let switchOffButton = html("button.btn btn-danger btn-sm mr", {
									type: "button",
									disabled: r.device_status == 0
								},
								html("i.fas fa-power-off i.mr"),
								i18n `Wyłącz`
							);

							let goUpButton = html("button.btn btn-success btn-sm mr", {
									type: "button",
									disabled: r.device_status == 1 || r.device_status == 2 || r.device_status == -1
								},
								html("i.fas fa-arrow-circle-up i.mr"),
								i18n `W górę`
							);

							let goDownButton = html("button.btn btn-success btn-sm mr", {
									type: "button",
									disabled: r.device_status == -1 || r.device_status == -2 || r.device_status == 1
								},
								html("i.fas fa-arrow-circle-down i.mr"),
								i18n `W dół`
							);

							let stopButton = html("button.btn btn-danger btn-sm mr", {
									type: "button",
									disabled: r.device_status == 0 || r.device_status == 2 || r.device_status == -2
								},
								html("i.fas fa-stop-circle i.mr"),
								i18n `STOP`
							);






							editButton.onclick = () => {
								let continueButton = html("button.btn btn-success mr", {
										type: "button"
									},
									html("i.fas fa-check i.mr"),
									i18n `Edytuj urządzenie`
								);

								let inputName = html("input#name.form-control", {
									type: "text",
									placeholder: i18n `Nazwa urządzenia`,
									maxlength: 16,
									value: r.device_name
								});

								let inputDescription = html("input#desc.form-control", {
									type: "text",
									placeholder: i18n `Opis urządzenia`,
									maxlength: 256,
									value: r.device_description
								});

								let inputHUID = html("input#huid.form-control", {
									type: "text",
									placeholder: i18n `Identyfikator sprzętowy`,
									maxlength: 32,
									value: r.device_huid
								});

								let groupsOptions = [];
								this.app.groups.forEach(group => {
									groupsOptions.push(
										html("option", {
											value: group.group_id,
											selected: group.group_id == r.group_id
										}, group.group_name)
									);
								});

								let inputGroup = html("select#ngroupame.form-control", groupsOptions);

								continueButton.onclick = () => {
									this.app.sendData("updateDevice", {
										device_id: r.device_id,
										device_name: inputName.value,
										device_description: inputDescription.value,
										device_huid: inputHUID.value,
										group_id: inputGroup.value
									});
									$("#SmallModal").modal("hide");
								};

								let cancelButton = html("button.btn btn-danger", {
										type: "button"
									},
									html("i.fas fa-times i.mr"),
									i18n `Anuluj`);

								cancelButton.onclick = () => {
									$("#SmallModal").modal("hide");
								};

								$("#SmallModalTitle").text(i18n `Tworzenie nowego urządzenia`);
								$("#SmallModalFooter").hide();
								$("#SmallModalBody").empty();
								$("#SmallModalBody").append(
									html("div.form-group",
										html("label", {
											for: "name"
										}, i18n `Nazwa urządzenia`),
										inputName
									)
								);
								$("#SmallModalBody").append(
									html("div.form-group",
										html("label", {
											for: "desc"
										}, i18n `Opis urządzenia`),
										inputDescription
									)
								);
								$("#SmallModalBody").append(
									html("div.form-group",
										html("label", {
											for: "huid"
										}, i18n `Identyfikator sprzętowy`),
										inputHUID
									)
								);
								$("#SmallModalBody").append(
									html("div.form-group",
										html("label", {
											for: "group"
										}, i18n `Grupa`),
										inputGroup
									)
								);
								$("#SmallModalBody").append(html("div.askButtons", continueButton, cancelButton));
								$("#SmallModal").modal("show");
							};

							removeButton.onclick = () => {
								let continueButton = html("button.btn btn-success mr", {
										type: "button"
									},
									html("i.fas fa-check i.mr"),
									i18n `Tak, usuń urządzenie ${r.device_name.toUpperCase()}`
								);

								continueButton.onclick = () => {
									this.app.sendData("removeDevice", r.device_id);
									$("#SmallModal").modal("hide");
								};

								let cancelButton = html("button.btn btn-danger", {
										type: "button"
									},
									html("i.fas fa-times i.mr"),
									i18n `Anuluj`);

								cancelButton.onclick = () => {
									$("#SmallModal").modal("hide");
								};

								$("#SmallModalTitle").text(i18n `Usuwanie urządzenia ${r.device_name.toUpperCase()}`);
								$("#SmallModalFooter").hide();
								$("#SmallModalBody").empty();
								$("#SmallModalBody").append(i18n `Usunięcie urządzenia spowoduje utratę możliwości sterowania oraz usunięcie wszystkich danych historycznych i wyzwalaczy powiązanych z danym urządzeniem! Czy chcesz kontynuować?`);
								$("#SmallModalBody").append(html("div.askButtons", continueButton, cancelButton));
								$("#SmallModal").modal("show");
							};

							switchOnButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: 1
								});
							};

							switchOffButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: 0
								});
							};

							goUpButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: 1
								});
							};

							goDownButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: -1
								});
							};

							stopButton.onclick = () => {
								this.app.sendData("powerDevice", {
									id: r.device_id,
									state: 0
								});
							};

							let buttons;
							switch (deviceType) {
								case 1:
									buttons = [
										removeButton,
										editButton,
										switchOnButton,
										switchOffButton
									];
									break;
								case 2:
									buttons = [
										removeButton,
										editButton,
										switchOnButton,
										switchOffButton
									];
									break;
								case 3:
									buttons = [
										removeButton,
										editButton,
										switchOnButton,
										switchOffButton
									];
									break;
								case 4:
									buttons = [
										removeButton,
										editButton,
										goUpButton,
										stopButton,
										goDownButton
									];
									break;
								case 5:
									buttons = [];
									break;
								default:
									buttons = [];
									break;
							}


							rows.push(
								html("tr",
									html("td.col-md-3", [deviceIcon, "  ", r.device_name]),
									html("td.col-md-4", r.device_description),
									html("td.col-md-1", statusIndicator),
									html("td.col-md-1", pingIndicator),
									html("td.col-md-3", buttons)
								)
							);
						});

						result.push(html("table.table table-striped table-center",
							html("thead",
								html("tr",
									html("th", {
										colspan: 5
									}, html("h4.strong", d[0].group_name), html("small", d[0].group_description))
								),
								html("tr",
									html("th", i18n `Urządzenie`),
									html("th", i18n `Opis`),
									html("th", i18n `Aktualny stan`),
									html("th", i18n `Komunikacja`),
									html("th")
								)
							),
							html("tbody", rows)
						));
					});
					result.forEach(r => $("div#devicesEditor").append(r));
					break;
				default:
					console.warn("There is no action associated with the message", command);
					break;
			}
		}

	}

	tryToReconnect() {

		if (this.app.websocketClient.readyState != 1 && !this.wasKicked) {
			this.app.websocketClient.close();
			setTimeout(() => {
				console.log("Attempt to reconnect...");
				this.app.websocketInit();
			}, 3000);
		}

	}

}