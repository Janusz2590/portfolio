export default {
	mode: "development",

	availableLangs: [{
			name: "Polski",
			code: "pl-PL",
			flag: "pl"
		},
		{
			name: "English",
			code: "en-EN",
			flag: "gb"
		},
		// {
		// 	name: "Deutsch",
		// 	code: "de-DE",
		// 	flag: "de"
		// }
	],

	apiEndpoint: "http://" + location.host + ":8080/?",
	wssEndpoint: "ws://" + location.host + ":8090"
};