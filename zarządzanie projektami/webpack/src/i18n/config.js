import i18n, {
	i18nConfig
} from "es2015-i18n-tag";

switch (localStorage.language) {
	case "en-EN":
		i18nConfig({
			locales: "en-EN",
			translations: require("./en-EN.js").default
		});
		break;

	case "de-DE":
		i18nConfig({
			locales: "de-DE",
			translations: require("./de-DE.js").default
		});
		break;

	default:
		i18nConfig({
			locales: "pl-PL",
			translations: {}
		});
		break;
}