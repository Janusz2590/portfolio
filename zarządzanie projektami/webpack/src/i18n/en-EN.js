export default {

	"gnIOT 2.0": "gnIOT 2.0 English Version",

	// components/main/@loginPanel.js
	"Podaj nazwę użytkownika i hasło!": "Please type your username and password!",
	"Podany login i/lub hasło nie są poprawne. Zwróć uwagę na wielkość liter w haśle!": "Invalid login or password. Please verify your username and password",
	"Dostęp zablokowany za zbyt wiele nieudanych logowań! Spróbuj ponownie później.": "Too many failed login attempts! Try again later.",
	"Logowanie nieudane. Wystąpił nieznany błąd.": "Login failed. An unknown error occured",
	"Nazwa użytkownika": "Username",
	"Hasło": "Password",
	"Zaloguj się": "Log In!",
	"Zmień język:": "Choose language:",
	"Wyloguj": "Log Out!",

	// components/main/modals.js
	"Wystąpił błąd": "An error occured",
	"Zamknij": "Close",

	// components/main/@mainPanel.js
	"Urządzenia": "Devices",
	"Urządzenie": "Device",
	"Nowa grupa": "New group",
	"Nowe urządzenie": "New device",
	"Opis": "Description",
	"Aktualny stan": "Current state",
	"Komunikacja": "Connection",
	"Usuń": "Remove",
	"Edytuj": "Edit",
	"Włącz": "Turn On!",
	"Wyłącz": "Turn Off!",
	"Włączone": "Switched On",
	"Wyłączone": "Switched Off",
	"Brak komunikacji": "Connection error",
	"Komunikacja OK": "Connected",
	"Anuluj": "Cancel"
};