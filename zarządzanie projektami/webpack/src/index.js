var $ = require("jquery");
window.jQuery = $;
window.$ = $;

import "./assets/css/CDNs.css";

import "bootstrap";
import "admin-lte";

import "./assets/css/style.css";
import "./assets/css/flags.css";

import "./i18n/config.js";

require("./utils/sortBy");
require("./utils/animations");

import app from "./components/app";
new app();