export default function (onlyMessage, url) {
	return new Promise((resolve) => {
		$.ajax({
			url,
			dataType: "json",
			context: this
		}).done((result) => {
			resolve(onlyMessage ? result.message : result);
		});
	});
}