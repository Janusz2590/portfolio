function dynamicSortMultiple() {
	var props = arguments;
	return function (obj1, obj2) {
		var i = 0,
			result = 0,
			numberOfProperties = props.length;
		/* try getting a different result from 0 (equal)
		 * as long as we have extra properties to compare
		 */
		while (result === 0 && i < numberOfProperties) {
			result = dynamicSort(props[i])(obj1, obj2);
			i++;
		}
		return result;
	};
}

function dynamicSort(property) {
	var sortOrder = 1;
	if (property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function (a, b) {
		var result;
		if (typeof (a[property]) === "string") {
			result = a[property].localeCompare(b[property], localStorage.language || "pl-PL");
		} else {
			result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		}
		return result * sortOrder;
	};
}

Object.defineProperty(Array.prototype, "sortBy", {
	enumerable: false,
	writable: true,
	value: function () {
		return this.sort(dynamicSortMultiple.apply(null, arguments));
	}
});