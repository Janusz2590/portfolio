export default function (timestamp, hideSeconds = false) {
	let t = new Date(timestamp || 0);
	let hours = "0" + t.getHours();
	let minutes = "0" + t.getMinutes();
	let seconds = "0" + t.getSeconds();
	if (hideSeconds) {
		return hours.substr(-2) + ":" + minutes.substr(-2);
	} else {
		return hours.substr(-2) + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);
	}
}