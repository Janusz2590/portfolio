/* eslint-disable no-undef */
const path = require("path");
const webpack = require("webpack");
const SpeedMeasurePlugin = require("speed-measure-webpack-plugin");

module.exports = new SpeedMeasurePlugin().wrap({
	mode: "development",
	devtool: "inline-source-map",

	entry: {
		main: path.resolve(__dirname, "src/index.js")
	},

	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "[name].js",
	},

	devServer: {
		contentBase: path.resolve(__dirname, "dist")
	},

	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery"
		}),
		new webpack.ProvidePlugin({
			"duration": "@sindresorhus/to-milliseconds"
		})
	],

	optimization: {
		minimize: false,
		splitChunks: {
			chunks: "all",
			maxInitialRequests: Infinity,
			minSize: 0,
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/]/,
					name() {
						return "npm";
					},
				},
			}
		}
	},

	module: {
		rules: [{
				test: /\.css$/,
				use: [{
						loader: "style-loader"
					},
					{
						loader: "css-loader"
					}
				]
			},
			{
				test: /\.(png|woff|woff2|eot|ttf|svg|jpg)$/,
				loader: "url-loader"
			},
			{
				test: /\.aac$/,
				loader: "url-loader",
				options: {
					mimetype: "audio/aac"
				}
			}
		]
	}
});